<?php

namespace app\controllers;

use app\components\PublicController;
use app\models\Stock;
//use app\models\Settings;
use Yii;
//use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class StockController extends PublicController
{
    public function actionIndex()
    {
		
		
        $this->tempInit('', 'stock');

        $criteria = Stock::find()->published();
        $criteria->orderBy('t.id DESC');

        /*$countQuery = clone $criteria;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = Settings::getParam('newsOnPage');
        $pages->route = 'stock/index';
        $pages->page = $page - 1;*/

        $cars = $criteria->limit(30)->all();

        return $this->render('index', ['cars' => $cars]);
    }
}
