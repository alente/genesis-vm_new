<?php

namespace app\controllers;

use yii\web\Controller;

class ModelController extends Controller
{
	public function actionIndex($model, $page='main')
	{
		$this->layout = 'model';
		
		$modelName = strtoupper($model);
		$this->view->params['model'] = $model;
		$this->view->params['modelName'] = $modelName;
		
		return $this->render($model . "/" . $page, [
			'model' => $model,
			'modelName' => $modelName,
		]);
	}
}
