<?php

namespace app\controllers;

use app\models\Arr;
use app\models\News;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class NewsController extends Controller
{
    public function actionIndex()
    {
	    $models = new ActiveDataProvider([
            'query' => News::find()
                ->where(['active' => 1])
                ->OrderBy(['date' => SORT_DESC]),
            'sort' => false,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        return $this->render('index', [
            'news' => $models->getModels(),
	        'pages' => $models->getPagination(),
        ]);
    }

    public function actionView($code)
    {

        $model = News::find()->where(['code' => $code])->one();

        if ( !empty($model) ) {

            return $this->render('view', [
                'model' => $model
            ]);

        }

    }

}