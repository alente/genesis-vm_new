<?
$this->title = "GENESIS G90 L";
$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
$mobilePrefix = $isMobile?'m-':null;

if($isMobile){
	include "../static/models/source/html/g90-L-mobile.html";
}else{
	include "../static/models/source/html/g90-L.html";
}
echo $this->render('/partials/footer');
echo $this->render('/model/scripts');
if($isMobile){
	include "../static/models/source/html/inc/g90-scripts-design-mobile.html";
}else{
	include "../static/models/source/html/inc/g90-scripts-design.html";
}