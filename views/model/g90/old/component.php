<?
$this->title = "Component - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	
	<div class="inner-container">
		
		<!--
		<div class="select-option">
			<label for="moduleSkin" class="blind">스킨 선택</label>
			<select>
				<option value="module-skin1">kv TYPE</option>
				<option value="module-skin2">sub TYPE</option>
				<option value="module-skin3">motion TYPE</option>
			</select>
			<label for="titlePostion" class="blind">타이틀위치</label>
			<select id="titlePostion">
				<option value="title-type1">좌상단</option>
				<option value="title-type2">우상단</option>
				<option value="title-type3">우하단</option>
				<option value="title-type4">좌하단</option>
				<option value="title-type5">상단중앙</option>
				<option value="motion-type1">풀스크린이미지</option>
			</select>

			<label for="colors" class="blind">색상</label>
			<select id="colors">
				<option value="color-type1">블랙</option>
				<option value="color-type2">화이트</option>
			</select>
		</div>
		 -->
		
		<a class="btn-back" href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>#comport" title="">BACK <i></i></a>
		<a class="btn-down" href="#" title=""><strong>CLICK</strong> FOR MORE<i></i></a>
		
		<!-- KV타입 좌상단 블랙 -->
		<section class="section module-skin1 title-type1 color-type1">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">DESIGN</h2>
					<p class="desc">ATHLETIC<br />ELEGANCE</p>
					<a href="#" class="desc2">
						<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
						<strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
					</a>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature01.jpg" alt="">
					</figure>
					<button class="btn-pop"><span>상세</span></button>
				</div>
			</article>
		</section>
		
		<section class="section module-skin1 title-type2 color-type1">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">DESIGN</h2>
					<p class="desc">ATHLETIC<br />ELEGANCE</p>
					<a href="#" class="desc2">
						<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
						<strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
					</a>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature01.jpg" alt="">
					</figure>
					<button class="btn-pop"><span>상세</span></button>
				</div>
			</article>
		</section>
		
		<section class="section module-skin2 title-type3 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">DESIGN</h2>
					<p class="desc">ATHLETIC<br />ELEGANCE</p>
					<a href="#" class="desc2">
						<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
						<strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
					</a>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature01.jpg" alt="">
					</figure>
					<button class="btn-pop"><span>상세</span></button>
				</div>
			</article>
		</section>
		
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">DESIGN</h2>
					<p class="desc">ATHLETIC<br />ELEGANCE</p>
					<a href="#" class="desc2">
						<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
						<em>GENESIS EQ900 Interior #01</em>
					</a>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature01.jpg" alt="">
					</figure>
					<button class="btn-pop"><span>상세</span></button>
				</div>
			</article>
		</section>
		
		<!-- layer cont : cell-1 -->
		<section class="section layer-type module-skin4 opacity-on">
			<article class="feature">
				<ul class="layer-box cell-1">
					<li>
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature14.jpg" alt=""></span>
							<span class="on"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature14_on.jpg" alt=""></span>
						</figure>
						<div class="brand-header">
							<h2 class="title">엠비언트 무드램프</h2>
							<p class="desc">사용자 취향에 따라 7종의 컬러 선택이 가능한 LED 간접광 무드램프는 크래쉬패드, 전후 도어 트림에 적용되어 차별화된 감성을 전달합니다.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // layer cont : cell-1 -->
		
		<!-- layer cont : cell-2 -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-2">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature07.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">어댑티브 <br />Full LED 헤드램프</h2>
							<p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature08.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">어댑티브 <br />Full LED 헤드램프</h2>
							<p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
							<a href="#" class="desc2">
								<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
								<em>GENESIS EQ900 Interior #01</em>
							</a>
						</div>
					</li>
				</ul>
				
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // layer cont : cell-2 -->
		
		<!-- layer cont : cell-3 -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-3">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature03.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">어댑티브 <br />Full LED 헤드램프</h2>
							<p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature04.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">어댑티브 <br />Full LED 헤드램프</h2>
							<p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
							<a href="#" class="desc2">
								<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
								<em>GENESIS EQ900 Interior #01</em>
							</a>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature05.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">어댑티브 <br />Full LED 헤드램프</h2>
							<p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
						</div>
					</li>
				</ul>
				
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // layer cont : cell-3 -->
	</div>
</div>
<?else:?>
    <div id="container">

        <div class="inner-container">
            <!-- <a class="btn-back" href="#" title="">BACK <i></i></a> -->

            <!-- kv type -->
            <section class="section m-module-skin1 m-title-type1 m-color-type2">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature_kv.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">DESIGN</h2>
                        <p class="desc">인간을 위한 미학,  긴장감과 아름다움의  조화</p>
                    </div>
                </article>
            </section>
            <!-- // kv type -->

            <!-- feature type1 : text image inner -->
            <section class="section m-module-skin2 m-text-type1 m-color-type2">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature02.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">크레스트 그릴</h2>
                        <p class="desc">유서 깊은 가문에서 전해지는 고유의 문장(Crest)처럼  정중하고 강한 자신감을 드러내는 제네시스의 시그니처  라디에이터 그릴입니다.</p>
                        <a href="#" class="desc2">
                            <span><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_circle.jpg" alt="" /></span>
                            <strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
                        </a>
                    </div>
                </article>
            </section>
            <!-- // feature type1 : text image inner -->

            <!-- feature type2 : text image bottom -->
            <section class="section m-module-skin2 m-text-type2 m-color-type1">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature06.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">크레스트 그릴</h2>
                        <p class="desc">유서 깊은 가문에서 전해지는 고유의 문장(Crest)처럼  정중하고 강한 자신감을 드러내는 제네시스의 시그니처  라디에이터 그릴입니다.</p>
                    </div>
                </article>
            </section>
            <!-- // feature type2 : text image bottom -->

            <!-- feature type3 : sub content -->
            <section class="section m-module-skin4 m-color-type1">
                <article class="feature">
                    <ul>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/img_feature03.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">어댑티브 Full LED 헤드램프</h2>
                                <p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
                                <a href="#" class="desc2">
                                    <span><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_circle.jpg" alt="" /></span>
                                    <strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
                                </a>
                            </div>
                        </li>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/img_feature03.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">어댑티브 Full LED 헤드램프</h2>
                                <p class="desc">입체적인 형상의 포지셔닝 램프로 고급감을 선사하며, 상황별로 빔 패턴을 변환하여 야간 주행 및 고속 주행 시의 안전성을 높였습니다.</p>
                                <p class="refer-text">*내비게이션과 연동하여 안전운행을 유도해주는 고속도로 안전운행 자동 감속기능 포함</p>
                            </div>
                        </li>
                    </ul>
                </article>
            </section>
            <!-- // feature type3 : sub content -->

            <!-- motion type : m-zoom-type -->
            <section class="section m-module-skin2 m-text-type2 m-color-type1 m-zoom-type1">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-real-metal_bg.jpg" alt="">
                        </figure>
                        <figure class="feature on">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-real-metal_detail.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">리얼 메탈 내장재</h2>
                        <p class="desc">렉시콘 프리미엄 사운드(17스피커)에는 메탈 재질그릴이 적용되어 차별화된 외관과 보다 선명한음질을 제공합니다.</p>
                    </div>
                    <button class="btn-zoom" type="button">
                        <span>상세보기</span>
                    </button>
                    <button class="btn-zoom-close" type="button"><img src="<?=Yii::$app->homeUrl?>/images/mobile/btn_close.png" alt=""></button>
                </article>
            </section>
            <!-- // motion type : m-zoom-type -->

            <!-- motion type : fade-in -->
            <section class="section m-module-skin4 m-color-type1 m-opacity-on">
                <article class="feature">
                    <ul>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_feature09.jpg" alt=""></figure>
                            <figure class="on"><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_feature09_on.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">엠비언트 무드램프</h2>
                                <p class="desc">사용자 취향에 따라 7종의 컬러 선택이 가능한 LED 간접광 무드램프는 크래쉬패드, 전후 도어 트림에 적용되어 차별화된 감성을 전달합니다.</p>
                            </div>
                        </li>
                    </ul>
                </article>
            </section>
            <!-- // motion type : fade-in -->

            <!-- motion type : m-motion-type6 -->
            <section class="section m-module-skin2 m-text-type2 m-color-type1 m-motion-type6">
                <article class="feature">
                    <div class="car-box">
                        <div class="car">
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_car.png" alt=""></figure>
                            <span class="wheel-light"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_wheel_light.png" alt=""></span>
                            <span class="wheel-left"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_wheel.png" alt=""></span>
                            <span class="wheel-right"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_wheel.png" alt=""></span>
                        </div>
                        <div class="number-wrap">
							<span class="number">
								<span>8</span>
								<span>9</span>
								<span>10</span>
								<span>11</span>
								<span>12</span>
								<span>13</span>
								<span>14</span>
								<span>15</span>
								<span>16</span>
								<span>17</span>
								<span>18</span>
								<span>19</span>
								<span>20</span>
							</span>
                            <span>0M</span>
                        </div>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">어드밴스드 스마트  크루즈 컨트롤</h2>
                        <p class="desc">선행차량과의 거리를 감지하여 운전자가 설정한 차량 속도 및 앞차와의 거리를 유지해주며, 차량이 완전히 정지한 후에도 선행차량이 출발하면(3초 이내) 자동으로 속도 및 거리 제어를 지원합니다.</p>
                    </div>
                </article>
            </section>
            <!-- // motion type : interval-type -->

            <!-- motion type : m-motion-type -->
            <section class="section m-module-skin2 m-text-type2 m-color-type1 m-motion-type">
                <article class="feature">
                    <div class="drag-box">
                        <figure class="on feature"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_drag_on.jpg" alt="" /></figure>
                        <figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_drag_off.jpg" alt="" /></figure>
                        <div class="control-btn">
                            <button type="button"></button>
                            <span class="control-line"></span>
                        </div>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">9 에어백 시스템</h2>
                        <p class="desc">충돌의 심각성과 탑승객을 감지하여 전개를 제어하는 어드밴스드 에어백이 포함된 9에어백이 적용되었습니다.</p>
                    </div>
                </article>
            </section>
            <!-- // motion type : compare-type -->

            <!-- feature type4 : engine type -->
            <section class="section m-module-skin5 m-color-type1">
                <article class="feature">
                    <figure class="engine-con">
                        <span><img src="<?=Yii::$app->homeUrl?>/images/mobile/img_engine01.jpg" alt=""></span>
                        <figcaption class="spec-desc">
                            <strong>타우 5.0 V8 GDi 엔진</strong>
                            <span>
								<i>최고출력</i>
								425PS/6,000rpm
							</span>
                            <span>
								<i>최대토크</i>
								53.0kg·m/5,000rpm
							</span>
                        </figcaption>
                    </figure>

                    <div class="brand-header">
                        <h2 class="title">람다 3.3 V6 터보 GDi 엔진</h2>
                        <p class="desc">언제라도 달려나갈 듯 노면을 움켜쥔 채 낮게 움츠린 자세, 타깃을 노려보는 듯한 강렬한 인상, 파워풀한 주행성능과 과감하고 공격적인 디자인이 질주본능을 일깨웁니다. GENESIS G80 SPORT의 트윈 터보 시스템은 주행 전 영역대에서 강력한 가속감을 제공합니다.</p>
                    </div>
                </article>
            </section>
            <!-- // feature type4 : engine type -->

            <!-- motion type : swipe-type -->
            <section class="section m-module-skin5 m-color-type1 m-motion-type7">
                <article class="feature">
                    <div class="swipe-wrap">
                        <div class="slide-wrap">
                            <ul>
                                <li>
                                    <figure class="engine-con">
                                        <span><img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-power-tranin_content01.jpg" alt=""></span>
                                        <figcaption class="spec-desc">
                                            <strong>타우 5.0 V8 GDi 엔진</strong>
                                            <span>
												<i>최고출력</i>
												425PS/6,000rpm
											</span>
                                            <span>
												<i>최대토크</i>
												53.0kg·m/5,000rpm
											</span>
                                        </figcaption>
                                    </figure>
                                </li>
                                <li>
                                    <figure class="engine-con">
                                        <span><img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-power-tranin_content02.jpg" alt=""></span>
                                        <figcaption class="spec-desc">
                                            <strong>람다 3.3 V6 T-GDi 엔진</strong>
                                            <span>
												<i>최고출력</i>
												370PS/6,000rpm
											</span>
                                            <span>
												<i>최대토크</i>
												52.0kg·m/1,300-4,500rpm
											</span>
                                        </figcaption>
                                    </figure>
                                </li>
                                <li>
                                    <figure class="engine-con">
                                        <span><img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-power-tranin_content03.jpg" alt=""></span>
                                        <figcaption class="spec-desc">
                                            <strong>람다 3.8 V6 GDi 엔진</strong>
                                            <span>
												<i>최고출력</i>
												315PS/6,000rpm
											</span>
                                            <span>
												<i>최대토크</i>
												40.5kg·m/5,000rpm
											</span>
                                        </figcaption>
                                    </figure>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="brand-header">
                        <h2 class="title">파워트레인</h2>
                        <p class="desc">효율적인 레이아웃을 통해 여유 있는 가속성능을 발휘하는 람다 3.3 V6 트윈 터보 엔진을 포함한 3개의 후륜 구동 전용 엔진 라인업은 어떠한 조건에서도 최적화된 반응을 통해 운전의 즐거움을 드립니다.</p>
                    </div>
                </article>
            </section>
            <!-- // motion type : swipe-type -->

            <!-- feature type2 : text image bottom -->
            <section class="section m-module-skin2 m-text-type2 m-color-type1 m-interval-type">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img class="step1" src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-htrac_content01.jpg" alt="">
                            <img class="step2" src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-htrac_content02.jpg" alt="">
                            <img class="step3" src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-htrac_content03.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">HTRAC, 전자식 AWD </h2>
                        <p class="desc">
                            HTRAC(에이치트랙)은 다른 차에서 이미 선보여온
                            기계식 4륜 구동 방식과는 달리 차량 속도 및 노면 상태를 감지하여 좌우 바퀴의 제동력과 전륜, 후륜의 동력을 가변 제어하여 미끄러운 도로와 코너링 운전에서도 안정적으로 주행이 가능하도록 진화한 4륜 구동 시스템입니다.
                            또한 다양한 구동력 배분 제어가 제한적인 일반 전자식 승용 AWD와 다르게 운전자 선택에 따라 후륜 구동에서 4륜 구동의 변화 특성을 차별화한 3가지 모드로 구동력 배분 제어가 가능합니다.
                        </p>
                    </div>
                </article>
            </section>
            <!-- // feature type2 : text image bottom -->

            <!-- banner -->
            <section class="eq-banner">
                <a href="#">
					<span class="description">
						<span>NEXT</span>
						<strong>SAFETY</strong>
					</span>
                    <img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/img_banner.jpg" alt="">
                </a>
            </section>
            <!-- // banner -->

        </div>

    </div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<?if($mobilePrefix):?>
<script>
	;(function(window, $) {
		$(function() {

			$('section.m-motion-type6').on({
				inView : function() {
					$('.m-motion-type6 .car').stop()._animate({'left' : '50%'}, {
						duration : 2300,
						complete : function() {
							$('.car span').css('animation-play-state', 'paused');
						}
					});
					$('.m-motion-type6 .number span').stop()._animate({'top' : '0'}, 1500);
				},
				outView : function() {
					$('.m-motion-type6 .number span').stop()._animate({'top' : '-1500%'}, 1500);
					$('.car').stop()._animate({'left' : '100%'}, {
						duration : 20,
						complete : function(){
							$('.car span').css('animation-play-state', 'running');
						}
					});
				}
			});

		});
	}(window, jQuery));
</script>
<?else:?>
<script>
	;(function(window, $, undefined) {
		$(function() {

			App.brand.init();
			App.brand.section.init('.Eq900 .section');
			App.brand.navigator.init({
				section: '.section > article'
			});

			// $('select').on('change', function() {
			// 	var str = 'section ';
			// 	$('select').each(function() {
			// 		str += $(this).find(':selected').val()+' ';
			// 	});

			// 	$('#box1')[0].className = str;
			// })

		});
	}(window, jQuery));
</script>
<?endif?>