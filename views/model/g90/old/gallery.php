<?
$this->title = "Gallery - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<div class="inner-container">
		<section class="section gallery-kv">
			<article class="feature">
				<div class="gallery-intro">
					<a href="#" title="" data-kind="exterior">
						<span class="thumb"><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-intro_bg01.jpg" alt=""></span>
						<span class="title">EXTERIOR</span>
					</a>
					<a href="#" title="" data-kind="interior">
						<span class="thumb"><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-intro_bg02.jpg" alt=""></span>
						<span class="title">INTERIOR</span>
					</a>
				</div>
			</article>
		</section>
		<section class="section gallery-detail">
			<article class="feature">
				<div class="slide-wrap" data-kind="exterior">
					<ul>
						<li class="on">
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior01.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior02.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior03.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior04.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior05.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior06.jpg" alt=""></span>
						</li>
					</ul>
					<span class="btn-wrap">
					<a href="javascript:void(0);" class="prev">prev</a>
					<a href="javascript:void(0);" class="next">next</a>
				</span>
					<div class="slide-indicator">
						<span class="arrow-btn">
						<a href="javascript:void(0);" class="prev">prev</a>
						<a href="javascript:void(0);" class="next">next</a>
					</span>
						<div class="thumb-list"></div>
					</div>
				</div>
				<div class="slide-wrap" data-kind="interior">
					<ul>
						<li class="on">
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior01.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior02.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior03.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior04.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior05.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior06.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior07.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior08.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior09.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior10.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior11.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior12.jpg" alt=""></span>
						</li>
					</ul>
					<span class="btn-wrap">
					<a href="javascript:void(0);" class="prev">prev</a>
					<a href="javascript:void(0);" class="next">next</a>
				</span>
					<div class="slide-indicator">
						<span class="arrow-btn">
						<a href="javascript:void(0);" class="prev">prev</a>
						<a href="javascript:void(0);" class="next">next</a>
					</span>
						<div class="thumb-list"></div>
					</div>
				</div>
				<div class="bottom-bar">
					<span class="kind-wrap">
					<button type="button" data-kind="exterior" class="on">EXTERIOR</button>
					<button type="button" data-kind="interior">INTERIOR</button>
				</span>
					<span class="down-wrap">
					<a href="javascript:void(0);" onclick="galleryImgDn();" title="download" class="on">DOWNLOAD</a>
					<a href="javascript:void(0);" onclick="galleryImgDn();" title="download">DOWNLOAD</a>
				</span>
				</div>
			</article>
			
			<!-- <article class="feature">
				<div class="slide-wrap" data-kind="exterior">
					<a class="btn-download" href="#" title="download">DOWNLOAD</a>
					<ul>
						<li class="on">
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior01.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior02.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior03.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior04.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior05.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_exterior06.jpg" alt=""></span>
						</li>
					</ul>
					<span class="btn-wrap">
							<a href="#" class="prev">prev</a>
							<a href="#" class="next">next</a>
						</span>
					<div class="slide-indicator">
						<span class="arrow-btn">
								<a href="#" class="prev">prev</a>
								<a href="#" class="next">next</a>
							</span>
						<div class="thumb-list"></div>
					</div>
				</div>
				<div class="slide-wrap" data-kind="interior">
					<a class="btn-download" href="#" title="download">DOWNLOAD</a>
					<ul>
						<li class="on">
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior01.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior02.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior03.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior04.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior05.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior06.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior07.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior08.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior09.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior10.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior11.jpg" alt=""></span>
						</li>
						<li>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/gallery/eq900-gallery-slide_interior12.jpg" alt=""></span>
						</li>
					</ul>
					<span class="btn-wrap">
							<a href="#" class="prev">prev</a>
							<a href="#" class="next">next</a>
						</span>
					<div class="slide-indicator">
						<span class="arrow-btn">
								<a href="#" class="prev">prev</a>
								<a href="#" class="next">next</a>
							</span>
						<div class="thumb-list"></div>
					</div>
				</div>
				<div class="bottom-bar">
					<span class="kind-wrap">
							<button type="button" data-kind="exterior">EXTERIOR</button>
							<button type="button" data-kind="interior">INTERIOR</button>
						</span>
					<span class="etc-wrap">
							<span class="count">
								<i class="num">1</i> / <i class="total">6</i>
							</span>
					<a href="#" class="btn-toggle">close</a>
					</span>
				</div>
			</article> -->
		</section>
	</div>
</div>
<?else:?>
    <div id="container">
        <div class="inner-container">
            <section class="section gallery-kv">
                <article class="feature">
                    <div class="gallery-intro">
                        <a href="#" title="" data-kind="exterior">
                            <span class="thumb"><img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-intro_bg01.jpg" alt=""></span>
                            <span class="title">EXTERIOR</span>
                        </a>
                        <a href="#" title="" data-kind="interior">
                            <span class="thumb"><img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-intro_bg02.jpg" alt=""></span>
                            <span class="title">INTERIOR</span>
                        </a>
                    </div>
                </article>
            </section>

            <section class="section gallery-detail">
                <article class="feature">

                    <div class="slide-wrap" data-kind="exterior">
                        <ul>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_exterior01.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_exterior02.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_exterior03.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_exterior04.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_exterior05.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_exterior06.jpg" alt="">
                            </li>
                        </ul>

                        <span class="btn-wrap">
							<a href="#" class="prev">prev</a>
							<a href="#" class="next">next</a>
						</span>

                        <div class="slide-indicator">
							<span class="arrow-btn">
								<a href="#" class="prev">prev</a>
								<a href="#" class="next">next</a>
							</span>
                            <div class="thumb-list"></div>
                        </div>
                    </div>

                    <div class="slide-wrap" data-kind="interior">
                        <ul>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior01.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior02.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior03.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior04.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior05.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior06.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior07.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior08.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior09.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior10.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior11.jpg" alt="">
                            </li>
                            <li>
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/gallery/eq900-gallery-slide_interior12.jpg" alt="">
                            </li>
                        </ul>

                        <span class="btn-wrap">
							<a href="#" class="prev">prev</a>
							<a href="#" class="next">next</a>
						</span>

                        <div class="slide-indicator">
							<span class="arrow-btn">
								<a href="#" class="prev">prev</a>
								<a href="#" class="next">next</a>
							</span>
                            <div class="thumb-list"></div>
                        </div>
                    </div>

                    <div class="bottom-bar">
						<span class="kind-wrap">
							<button type="button" data-kind="exterior">EXTERIOR</button>
							<button type="button" data-kind="interior">INTERIOR</button>
						</span>

                        <span class="etc-wrap">
							<a href="#" class="count">
								<i class="num">1</i> / <i class="total">6</i>
							</a>
						</span>

                        <span class="down-wrap">
							<a href="#">download</a>
							<a href="#">download</a>
						</span>
                    </div>
                </article>
            </section>

        </div>
    </div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<?if($mobilePrefix):?>
<script type="text/javascript" src="scripts/slide-mobile.js"></script>
<?else:?>
<script src="<?=Yii::$app->homeUrl?>/scripts/slide-desktop.js"></script>
<script>
	;
	(function(window, $, undefined) {
		$(function() {
			App.brand.init();
			App.brand.section.init('.Eq900 .section');
		});
	}(window, jQuery));

</script>
<?endif?>