<?
$this->title = "Specs - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<div class="inner-container">
		<!-- Specs -->
		<section class="section specs sub-visible">
			<!-- specs select -->
			<div class="brand-content">
				<div class="select-detail">
					<h2 class="title">SPECS</h2>
					<div class="tab-menu">
						<ul>
							<li class="on"><a href="#">KEY SPECS</a></li>
							<li><a href="#">POWERTRAIN</a></li>
							<li><a href="#">EXTERIOR</a></li>
							<li><a href="#">INTERIOR</a></li>
							<li><a href="#">SAFETY</a></li>
							<li><a href="#">CONVENIENCE</a></li>
							<li><a href="#">SEATS</a></li>
							<li><a href="#">MULTIMEDIA</a></li>
							<li><a href="#">options</a></li>
						</ul>
						<span class="line"></span>
					</div>
					<div class="specs-inner">
						<div class="table-cont">
							<table>
								<caption>스펙 제원정보로 구성된 테이블</caption>
								<colgroup>
									<col width="25%" />
									<col width="*" />
									<col width="*" />
									<col width="*" />
								</colgroup>
								<tbody>
								<tr>
									<th>MODEL</th>
									<td>
										<div class="car-model">
											<!-- <figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/specs/img_table_car01.png" alt="" /></figure> -->
											<span class="select-box">
														<label for=""><a href="#a">3.8Luxury</a></label>
														<span class="select-lst">
															<span class="lst-item"><a href="#a">3.8Luxury</a></span>
												<span class="lst-item"><a href="#a">3.8Premium Luxury</a></span>
												<span class="lst-item"><a href="#a">3.8Prestige</a></span>
												<span class="lst-item"><a href="#a">3.3TLuxury</a></span>
												<span class="lst-item"><a href="#a">3.3TPremium Luxury</a></span>
												<span class="lst-item"><a href="#a">3.3TPrestige</a></span>
												<span class="lst-item"><a href="#a">5.0Prestige</a></span>
												<span class="lst-item"><a href="#a">5.0Limousine Prestige</a></span>
												</span>
												<select class="select" style="display:none">
													<option>3.8Luxury</option>
													<option>3.8Premium Luxury</option>
													<option>3.8Prestige</option>
													<option>3.3TLuxury</option>
													<option>3.3TPremium Luxury</option>
													<option>3.3TPrestige</option>
													<option>5.0Prestige</option>
													<option>5.0Limousine Prestige</option>
												</select>
												</span>
											<span class="btn-spec" data-spec="#specs-pop01">
														<a href="#" onclick="toApp_showPopup('#specs-pop01'); return false;">FULL SPECS</a>
													</span>
										</div>
									</td>
									<td class="">
										<div class="car-model">
											<!-- <figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/specs/img_table_car01.png" alt="" /></figure> -->
											<span class="select-box">
														<label for=""><a href="#a">SELECT A VEHICLE</a></label>
														<span class="select-lst">
															<span class="lst-item"><a href="#a">3.8Luxury</a></span>
												<span class="lst-item"><a href="#a">3.8Premium Luxury</a></span>
												<span class="lst-item"><a href="#a">3.8Prestige</a></span>
												<span class="lst-item"><a href="#a">3.3TLuxury</a></span>
												<span class="lst-item"><a href="#a">3.3TPremium Luxury</a></span>
												<span class="lst-item"><a href="#a">3.3TPrestige</a></span>
												<span class="lst-item"><a href="#a">5.0Prestige</a></span>
												<span class="lst-item"><a href="#a">5.0Limousine Prestige</a></span>
												</span>
												<select class="select" style="display:none">
													<option>3.8Luxury</option>
													<option>3.8Premium Luxury</option>
													<option>3.8Prestige</option>
													<option>3.3TLuxury</option>
													<option>3.3TPremium Luxury</option>
													<option>3.3TPrestige</option>
													<option>5.0Prestige</option>
													<option>5.0Limousine Prestige</option>
												</select>
												</span>
											<span class="btn-spec disabled" data-spec="#specs-pop02">
														<a href="#">FULL SPECS</a>
													</span>
										</div>
									</td>
									<td class="">
										<div class="car-model">
											<!-- <figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/specs/img_table_car01.png" alt="" /></figure> -->
											<span class="select-box">
														<label for=""><a href="#a">SELECT A VEHICLE</a></label>
														<span class="select-lst">
															<span class="lst-item"><a href="#a">3.8Luxury</a></span>
												<span class="lst-item"><a href="#a">3.8Premium Luxury</a></span>
												<span class="lst-item"><a href="#a">3.8Prestige</a></span>
												<span class="lst-item"><a href="#a">3.3TLuxury</a></span>
												<span class="lst-item"><a href="#a">3.3TPremium Luxury</a></span>
												<span class="lst-item"><a href="#a">3.3TPrestige</a></span>
												<span class="lst-item"><a href="#a">5.0Prestige</a></span>
												<span class="lst-item"><a href="#a">5.0Limousine Prestige</a></span>
												</span>
												<select class="select" style="display:none">
													<option>3.8Luxury</option>
													<option>3.8Premium Luxury</option>
													<option>3.8Prestige</option>
													<option>3.3TLuxury</option>
													<option>3.3TPremium Luxury</option>
													<option>3.3TPrestige</option>
													<option>5.0Prestige</option>
													<option>5.0Limousine Prestige</option>
												</select>
												</span>
											<span class="btn-spec disabled" data-spec="#specs-pop03">
														<a href="#" onclick="toApp_showPopup('#specs-pop01'); return false;">FULL SPECS</a>
													</span>
										</div>
									</td>
								</tr>
								<tr>
									<th>Overall Length (mm)</th>
									<td>5,205</td>
									<td></td>
									<td>5,205</td>
								</tr>
								<tr>
									<th>Overall Width (mm)</th>
									<td>1,915</td>
									<td></td>
									<td>1,915</td>
								</tr>
								<tr>
									<th>Overall Height (mm)</th>
									<td>1,495</td>
									<td></td>
									<td>1,495</td>
								</tr>
								<tr>
									<th>Wheelbase (mm)</th>
									<td>3,160</td>
									<td></td>
									<td>3,160</td>
								</tr>
								<tr>
									<th>Front Wheeltread (mm)</th>
									<td>1,630(18")</td>
									<td></td>
									<td>1,630(18")</td>
								</tr>
								<tr>
									<th>Rear Wheeltread (mm)</th>
									<td>1,659(18")</td>
									<td></td>
									<td>1,659(18")</td>
								</tr>
								<tr>
									<th>Engine Label</th>
									<td>V6 람다 3.8 GDi</td>
									<td></td>
									<td>V8 타우 5.0 GDi</td>
								</tr>
								<tr>
									<th>Driving System</th>
									<td>2WD/AWD</td>
									<td></td>
									<td>AWD</td>
								</tr>
								<tr>
									<th>Displacement (cc)</th>
									<td>3,778</td>
									<td></td>
									<td>3,778</td>
								</tr>
								<tr>
									<th>Max. Power(PS/rpm)</th>
									<td>315/6,000</td>
									<td></td>
									<td>315/6,000</td>
								</tr>
								<tr>
									<th>Max. Torque(kg.m/rpm)</th>
									<td>40.5/5,000</td>
									<td></td>
									<td>40.5/5,000</td>
								</tr>
								<tr>
									<th>Fuel Tank (L)</th>
									<td>77</td>
									<td></td>
									<td>77</td>
								</tr>
								</tbody>
							</table>
						</div>
						<div class="btn-right">
							<div class="btn-box">
								<a href="/kr/en/luxury-sedan-genesis-eq900-specifications-print.html" class="btn-print"><span class="icon-print">PRINT</span></a>
								<a href="javascript:popManualDown('EQ900');"><span class="icon-down">MANUAL</span></a>
								<span><a href="javascript:void(0);"><span class="icon-down">PRICE-LIST</span></a></span>
								<span><a href="javascript:void(0);"><span class="icon-down">CATALOG</span></a></span>
							</div>
							<div class="txt-box">
								<span data-spec="#pop-level02"><a href="javascript:void(0);">Government-certified fuel efficiency and grade</a></span>
								<span data-spec="#pop-level"><a href="javascript:void(0);">Tire labeling grade</a></span>
							</div>
							<a href="/kr/en/luxury-sedan-genesis-eq900-specifications.html" class="btn-prev">BACK</a>
						</div>
					</div>
				</div>
			</div>
			<!-- // specs select -->
		</section>
		<!-- // Specs -->
	</div>
	<!-- FULL SPECS1 -->
	<section id="specs-pop01" class="layer-specs hide">
		<h2 class="layer-title">3.8Luxury</h2>
		<ul>
			<li>
				<h3 class="sub-title">KEY SPECS</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>Overall Length (mm)</th>
						<td>5,205</td>
					</tr>
					<tr>
						<th>Overall Width (mm)</th>
						<td>1,915</td>
					</tr>
					<tr>
						<th>Overall Height (mm)</th>
						<td>1,495</td>
					</tr>
					<tr>
						<th>Wheelbase (mm)</th>
						<td>3,160</td>
					</tr>
					<tr>
						<th>Front Wheeltread (mm)</th>
						<td>1,630(18")</td>
					</tr>
					<tr>
						<th>Rear Wheeltread (mm)</th>
						<td>1,659(18")</td>
					</tr>
					<tr>
						<th>Engine Label</th>
						<td>V6 Lambda 3.8 GDi</td>
					</tr>
					<tr>
						<th>Driving System</th>
						<td>2WD/AWD</td>
					</tr>
					<tr>
						<th>Displacement (cc)</th>
						<td>3,778</td>
					</tr>
					<tr>
						<th>Max. Power(PS/rpm)</th>
						<td>315/6,000</td>
					</tr>
					<tr>
						<th>Max. Torque(kg.m/rpm)</th>
						<td>40.5/5,000</td>
					</tr>
					<tr>
						<th>Fuel Tank (L)</th>
						<td>77</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">POWERTRAIN</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>Lambda 3.8 V6 GDi Engine</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Lambda 3.3 V6 Turbo GDi Engine</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Tau 5.0 V8 GDi Engine</th>
						<td>-</td>
					</tr>
					<tr>
						<th>8-speed Automatic Transmission</th>
						<td>O</td>
					</tr>
					<tr>
						<th>HTRAC(Electronic AWD)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Direct Steering System (Rack drive-type power steering, variable gear ratio steering)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Drive Mode Control System (Smart/Eco/Sport/Individual)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Genesis Adaptive Control Suspension</th>
						<td>-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">EXTERIOR</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>HID Headlamps (dynamic bending function)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Adaptive Full LED Headlamps(drive condition-variable beam pattern function)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>LED Turn Signals Light</th>
						<td>O</td>
					</tr>
					<tr>
						<th>LED Daytime Running Light</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Full LED Rear Combination Lamps</th>
						<td>O</td>
					</tr>
					<tr>
						<th>UV Protection Glass (front & rear & all doors)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Dual Pane Sound Proof Glass (front & rear & all doors)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rear Seat Privacy Glass</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Dual Muffler Integrated with Bumper</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Outside Mirrors (including side repeater, puddle lamp, heated wire, auto unfolding)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Outside Mirrors chrome molding (lower)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Logo Pattern Puddle Lamp</th>
						<td>-</td>
					</tr>
					<tr>
						<th>245/50R18 Michelin tire & Alloy wheel</th>
						<td>O</td>
					</tr>
					<tr>
						<th>245/45R19 (Front) & 275/40R19 (Rear) Continental tires & wheels</th>
						<td>-</td>
					</tr>
					<tr>
						<th>245/45R19 (Front) & 275/40R19 (Rear) Continental tires & Limousine exclusive wheels</th>
						<td>-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">INTERIOR</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>7" TFT LCD Cluster</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Analog Clock</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Shift-by-wire (SBW)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Paddle Shifter</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Room Mirror (ECM, Hi-pass)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>LED Crystal Room Lamp</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Metal Door Scuff</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rear Seat Vanity Mirror</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Wood Grain Material</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Real Wood Material</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Prime Nappa Leather Material (door trim upper/lower end, center console, crash pad upper end, steering wheel horn cover, etc.)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Real Metal Interior Material (door speaker grilles, trunk plate)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Luxury Suede Material (headlining, pillar trim, etc.)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Ambient Mood Lamps</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Foot Lamp (front, rear seats)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>B Pillar Mood Lamps</th>
						<td>-</td>
					</tr>
					<tr>
						<th>B Pillar Magazine Pocket</th>
						<td>-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">SAFETY</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>9-airbag System (advanced front airbags, driver seat's knee airbag, front/rear sides, curtain airbags for rollover protection)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Electronic Stability Control System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Tire Air Pressure Alert System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Emergency Alert System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rear Center 3-point Seatbelt</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Babyseat Fastening Device</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Safety Unlock</th>
						<td>O</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">CONVENIENCE</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>Front Smartphone Wireless Charging Pad</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Button start &amp; Smart Key system</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Card-type Smart Key</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Hill Start Assist Control Function</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rearview Camera</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Touch-type Outside Door Handle</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Full-auto Air Conditioner with 3-zone Control(including front seat dual wind direction control)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Advanced 3-zone Air Conditioner(including B pillar air outlet)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Safety Power Window (front &amp; rear seats)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Front and Rear Parking Assist System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Electronic Parking Brake (Including auto hold function)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Power Trunk System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Electric Backlite Curtain</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Electric Rear side Curtain</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Temporary Spare Tire</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Around-view Monitoring System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Ghost Door Closing</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Heads-up Display</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Smart HVAC system (including indoor CO2 control, harmful gas cut-off device)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Cruise Control</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Vibration Warning Steering Wheel</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Steering Wheel (heated wire, Motor Driven)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rain Sensor</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Auto Defog</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Cluster Ionizer</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Highway Driving Assistance</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Driver Attention Alert System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Smart Blind Spot Detection System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Lane Keeping Assist System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Advanced Smart Cruise Control</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Automatic Emergency Braking System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Smart High Beam</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Front Seat Preactive Seatbelt</th>
						<td>-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">SEATS</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>Power Driver Seat (including lumbar support, cushion extension)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Driver Modern Ergo Seat (22-way adjustable)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Genuine Leather Seat</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Prime Nappa Leather Seat (piping applied)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Semi-aniline Leather Seat</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Front Seat Power Headrests (4-way)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Front Ventilated/Heated Seats</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Driver Seat Memory System (including smart seat system)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Passenger Seat Memory System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Power Front Passenger Seat (including walk-in switch, height adjustment)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Passenger Lumbar Support</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Heated Seat</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rear Ventilated/Heated Seats</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Armrest Adjustment Control</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rear Seat Ski-through</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Rear Modern Ergo Seat (Right seat 14-way adjustable, 4:2:4 type)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Wing-out Headrests</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Memory System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Neck Pillow (separate)</th>
						<td>-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">MULTIMEDIA</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>DIS(blueLink2.0, 12.3" panoramic display, bluetooth handsfree)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Lexicon Premium Sounds System (14 speakers, quantum logic surround)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Lexicon Premium Sounds System (17 speakers, quantum logic surround)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Dual Monitor</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Front/Rear Seat USB Port (charging, data reading)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>DVD Player (glove box position)</th>
						<td>O</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">OPTIONS</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>HTRAC(Electronic AWD) (2,500,000 KRW)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Safety Sunroof (800,000 KRW)</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Genesis Smart Sense Package (3,000,000 KRW) Highway Driving Assistance, Driver Attention Alert System, Smart Blind Spot Detection System, Advanced Smart Cruise Control, Lane Keeping Assist System, Automatic Emergency Braking System, Vibration Warning Steering Wheel, Smart High Beam, Front Seat Preactive Seatbelt</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Convenience Package (2,000,000 KRW) Heads-up Display, Around View Monitoring System</th>
						<td>O</td>
					</tr>
					<tr>
						<th>Signature Design Collection (3,200,000 KRW) Prime Nappa Leather Seat (piping applied), Prime Nappa Leather Material (door trim lower end, center console, etc.), Real Wood Material</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Dual Monitor (2,500,000 KRW)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>Rear Seat Comfort Package (2,500,000 KRW) Power Rear Seat (4:2:4 division, sliding only), Rear Ventilated/Heated Seats, Rear Seat Electric Side Curtain</th>
						<td>-</td>
					</tr>
					<tr>
						<th>First Class VIP Seat (GENESIS EQ900) (3,000,000 KRW) Power Rear Seat (5:5 type), Tower Console, Rear Seat Smartphone Wireless Charging System</th>
						<td>-</td>
					</tr>
					<tr>
						<th>First Class VIP Seat (GENESIS EQ900L) (3,200,000 KRW) Power Rear Seat (5:5 type), Tower Console, Rear Seat Smartphone Wireless Charging System, Power Leg Support (4-way)</th>
						<td>-</td>
					</tr>
					<tr>
						<th>● Features Included O Options Available - Options Unavailable</th>
						<td> </td>
					</tr>
					</tbody>
				</table>
			</li>
		</ul>
		<div class="btn-area">
			<div class="btn-box">
				<a href="#"><span class="icon-print">PRINT</span></a>
			</div>
			<a href="#" class="btn-close" onclick="toApp_hidePopup('#specs-pop01'); return false;">Close</a>
		</div>
	</section>
	<!-- //FULL SPECS1 -->
	<!-- FULL SPECS2 not use -->
	<section id="specs-pop03" class="layer-specs hide">
		<h2 class="layer-title">3.5 럭셔리 스펙 </h2>
		<ul>
			<li>
				<h3 class="sub-title">제원</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>전장<em>(mm)</em></th>
						<td>5,205</td>
					</tr>
					<tr>
						<th>전폭<em>(mm)</em></th>
						<td>1,915</td>
					</tr>
					<tr>
						<th>전고<em>(mm)</em></th>
						<td>1,495</td>
					</tr>
					<tr>
						<th>축간거리<em>(mm)</em></th>
						<td>3,160</td>
					</tr>
					<tr>
						<th>윤거 전<em>(mm)</em></th>
						<td>1,630(18")</td>
					</tr>
					<tr>
						<th>윤거 후<em>(mm)</em></th>
						<td>1,659(18")</td>
					</tr>
					<tr>
						<th>엔진형식</th>
						<td>V6 람다 3.8 GDi</td>
					</tr>
					<tr>
						<th>구동방식</th>
						<td>2WD/AWD</td>
					</tr>
					<tr>
						<th>배기량<em>(cc)</em></th>
						<td>3,778</td>
					</tr>
					<tr>
						<th>최고출력<em>(PS/rpm)</em></th>
						<td>315/6,000</td>
					</tr>
					<tr>
						<th>최대토크<em>(kg.m/rpm)</em></th>
						<td>40.5/5,000</td>
					</tr>
					<tr>
						<th>연료탱크용량<em>(L)</em></th>
						<td>77</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">파워트레인</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>람다 3.8 V6 GDi 엔진</th>
						<td>O</td>
					</tr>
					<tr>
						<th>8단 자동변속기</th>
						<td>O</td>
					</tr>
					<tr>
						<th>HTRAC<span>(전자식 AWD)</span></th>
						<td>-</td>
					</tr>
					<tr>
						<th>다이렉트 스티어링 시스템<span>(랙 구동형 전동식 파워 스티어링, 가변 기어비 스티어링)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>통합 주행 모드<span>(스마트/에코/스포츠/인디비쥬얼)</span></th>
						<td>O</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">외관</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>HID 헤드램프<span>(다이나믹 벤딩 기능)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>어댑티브 Full LED 헤드램프<span>(주행상황별 빔패턴 변동 기능)</span></th>
						<td>-</td>
					</tr>
					<tr>
						<th>LED 방향 지시등</th>
						<td>O</td>
					</tr>
					<tr>
						<th>LED 주간전조등</th>
						<td>O</td>
					</tr>
					<tr>
						<th>FULL LED 리어 콤비 램프</th>
						<td>O</td>
					</tr>
					<tr>
						<th>자외선 차단 글래스<span>(앞면 &amp; 뒷면 &amp; 전체 도어)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>이중접합 차음 글래스<span>(앞면 &amp; 뒷면 &amp; 전체 도어)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>뒷자석 프라이버시 글래스</th>
						<td>-</td>
					</tr>
					<tr>
						<th>범퍼 일체형 듀얼 머플러</th>
						<td>O</td>
					</tr>
					<tr>
						<th>아웃사이드 미러<span>(사이드 리피터, 퍼들램프, 열선, 오토 언폴딩 포함)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>로고패턴 퍼들램프</th>
						<td>-</td>
					</tr>
					<tr>
						<th>245/50R18 미쉐린 타이어 &amp; 휠</th>
						<td>O</td>
					</tr>
					<tr>
						<th>245/45R19(앞) &amp; 275/40R19(뒤)
							<br /> 콘티넨탈 타이어 &amp; 휠</th>
						<td>-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">내장</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>7인치 TFT LCD 클러스터</th>
						<td>O</td>
					</tr>
					<tr>
						<th>아날로그 시계</th>
						<td>O</td>
					</tr>
					<tr>
						<th>전자식 변속레버<span>(SBW)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>룸미러<span>(ECM, 하이패스)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>LED 크리스탈 룸램프</th>
						<td>O</td>
					</tr>
					<tr>
						<th>메탈 도어스커프</th>
						<td>O</td>
					</tr>
					<tr>
						<th>뒷좌석 화장거울</th>
						<td>O</td>
					</tr>
					<tr>
						<th>우드그레인 내장재</th>
						<td>-</td>
					</tr>
					<tr>
						<th>리얼우드 내장재</th>
						<td>-</td>
					</tr>
					<tr>
						<th>프라임 나파 가죽 내장재<span>(도어트림 상/하단, 센터콘솔, 크래쉬패드 상단, 스티어링 휠 혼 커버 등)</span></th>
						<td>-</td>
					</tr>
					<tr>
						<th>리얼 메탈 내장재<span>(도어스피커 그릴, 트렁크 플레이트)</span></th>
						<td>-</td>
					</tr>
					<tr>
						<th>럭셔리 스웨이드 내장재<span>(헤드라이닝, 필라트림 등)</span></th>
						<td>-</td>
					</tr>
					<tr>
						<th>앰비언트 무드램프</th>
						<td>-</td>
					</tr>
					<tr>
						<th>풋램프<em>(앞좌석 &amp; 뒷좌석)</em></th>
						<td>O-</td>
					</tr>
					</tbody>
				</table>
			</li>
			<li>
				<h3 class="sub-title">안전</h3>
				<table>
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<tbody>
					<tr>
						<th>9에어백<span>(운전석&amp;동승석 어드밴스드 에어백, 운전석 무릎 에어백,<br/> 앞&amp;뒤 사이드 에어백, 전복 대응 커튼 에어백)</span></th>
						<td>O</td>
					</tr>
					<tr>
						<th>차체자세 제어장치</th>
						<td>O</td>
					</tr>
					<tr>
						<th>타이어 공기압 경보장치</th>
						<td>O</td>
					</tr>
					<tr>
						<th>급제동 경보 시스템</th>
						<td>O</td>
					</tr>
					<tr>
						<th>뒷좌석 센터 3점식 시트벨트</th>
						<td>O</td>
					</tr>
					<tr>
						<th>유아용 시트 고정장치</th>
						<td>O</td>
					</tr>
					<tr>
						<th>세이프티 언락</th>
						<td>O</td>
					</tr>
					</tbody>
				</table>
			</li>
		</ul>
		<div class="btn-area">
			<div class="btn-box">
				<a href="#"><span class="icon-print">프린트 하기</span></a>
			</div>
			<a href="#" class="btn-close" onclick="toApp_hidePopup('#specs-pop03'); return false;">닫기</a>
		</div>
	</section>
	<!-- //FULL SPECS2 -->
</div>
<?else:?>
    <div id="container">

        <div class="inner-container">
            <section class="section specs">
                <div class="specs-select">
                    <div class="specs-title">
                        <h2 class="title">SPECS</h2>
                    </div>

                    <!-- tab -->
                    <div class="m-tab-menu">
                        <div>
                            <ul>
                                <li class="on"><a href="#">KEY SPECS</a></li>
                                <li><a href="#">POWERTRAIN</a></li>
                                <li><a href="#">EXTERIOR</a></li>
                                <li><a href="#">INTERIOR</a></li>
                                <li><a href="#">SAFETY</a></li>
                                <li><a href="#">CONVENIENCE</a></li>
                                <li><a href="#">SEATS</a></li>
                                <li><a href="#">MULTIMEDIA</a></li>
                                <li><a href="#">options</a></li>
                            </ul>
                        </div>
                        <span class="line"></span>
                    </div>

                    <!-- specs info -->
                    <div class="m-table-wrap">
                        <div class="m-table-cont th-fixed">
                            <table>
                                <caption>스펙 제원정보로 구성된 테이블</caption>
                                <colgroup>
                                    <col width="160px" />
                                    <col width="*" />
                                    <col width="*" />
                                    <col width="*" />
                                </colgroup>
                                <tbody>
                                <tr>
                                    <th>MODEL</th>
                                    <td>
                                        <div class="car-model">
                                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_table_car01.png" alt="" /></figure>
                                            <span class="select-box">
													<label for=""><a href="#a">3.8Luxury</a></label>
													<select>
														<option>3.8Luxury</option>
														<option>3.8Premium Luxury</option>
														<option>3.8Prestige</option>
														<option>3.3TLuxury</option>
														<option>3.3TPremium Luxury</option>
														<option>3.3TPrestige</option>
														<option>5.0Prestige</option>
														<option>5.0Limousine Prestige</option>
													</select>
												</span>
                                            <span class="btn-spec" data-spec="#specs-pop01">
													<a href="#" onclick="toApp_showPopup('#specs-pop01'); return false;">FULL SPECS</a>
												</span>
                                        </div>
                                    </td>

                                    <td class="dim">
                                        <div class="car-model">
                                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_table_car01.png" alt="" /></figure>
                                            <span class="select-box">
													<label for=""><a href="#a">3.8Luxury</a></label>
													<select>
														<option>3.8Luxury</option>
														<option>3.8Premium Luxury</option>
														<option>3.8Prestige</option>
														<option>3.3TLuxury</option>
														<option>3.3TPremium Luxury</option>
														<option>3.3TPrestige</option>
														<option>5.0Prestige</option>
														<option>5.0Limousine Prestige</option>
													</select>
												</span>
                                            <span class="btn-spec" data-spec="#specs-pop02">
													<a href="#">FULL SPECS</a>
												</span>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="car-model">
                                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_table_car01.png" alt="" /></figure>
                                            <span class="select-box">
													<label for=""><a href="#a">3.8LuxuryKB</a></label>
													<select>
														<option>3.8Luxury</option>
														<option>3.8Premium Luxury</option>
														<option>3.8Prestige</option>
														<option>3.3TLuxury</option>
														<option>3.3TPremium Luxury</option>
														<option>3.3TPrestige</option>
														<option>5.0Prestige</option>
														<option>5.0Limousine Prestige</option>
													</select>
												</span>
                                            <span class="btn-spec" data-spec="#specs-pop03">
													<a href="#">FULL SPECS</a>
												</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Overall Length (mm)</th>
                                    <td>5,205</td>
                                    <td></td>
                                    <td>5,205</td>
                                </tr>
                                <tr>
                                    <th>Overall Width (mm)</th>
                                    <td>1,915</td>
                                    <td></td>
                                    <td>1,915</td>
                                </tr>
                                <tr>
                                    <th>Overall Height (mm)</th>
                                    <td>1,495</td>
                                    <td></td>
                                    <td>1,495</td>
                                </tr>
                                <tr>
                                    <th>Wheelbase (mm)</th>
                                    <td>3,160</td>
                                    <td></td>
                                    <td>3,160</td>
                                </tr>
                                <tr>
                                    <th>Front Wheeltread (mm)</th>
                                    <td>1,630(18")</td>
                                    <td></td>
                                    <td>1,630(18")</td>
                                </tr>
                                <tr>
                                    <th>Rear Wheeltread (mm)</th>
                                    <td>1,659(18")</td>
                                    <td></td>
                                    <td>1,659(18")</td>
                                </tr>
                                <tr>
                                    <th>Engine Label</th>
                                    <td>V6 람다 3.8 GDi</td>
                                    <td></td>
                                    <td>V8 타우 5.0 GDi</td>
                                </tr>
                                <tr>
                                    <th>Driving System</th>
                                    <td>2WD/AWD</td>
                                    <td></td>
                                    <td>AWD</td>
                                </tr>
                                <tr>
                                    <th>Displacement (cc)</th>
                                    <td>3,778</td>
                                    <td></td>
                                    <td>3,778</td>
                                </tr>
                                <tr>
                                    <th>Max. Power(PS/rpm)</th>
                                    <td>315/6,000</td>
                                    <td></td>
                                    <td>315/6,000</td>
                                </tr>
                                <tr>
                                    <th>Max. Torque(kg.m/rpm)</th>
                                    <td>40.5/5,000</td>
                                    <td></td>
                                    <td>40.5/5,000</td>
                                </tr>
                                <tr>
                                    <th>Fuel Tank (L)</th>
                                    <td>77</td>
                                    <td></td>
                                    <td>77</td>
                                </tr>
                                <!-- <tr>
										<th>연료탱크용량(L)</th>
										<td>77</td>
										<td></td>
										<td>77</td>
									</tr>
									<tr>
										<th>연료탱크용량(L)</th>
										<td>77</td>
										<td></td>
										<td>77</td>
									</tr> -->
                                </tbody>
                            </table>
                        </div>
                        <div class="m-table-cont inner-scroll">
                            <table>
                                <caption>스펙 제원정보로 구성된 테이블</caption>
                                <colgroup>
                                    <col width="160px" />
                                    <col width="*" />
                                    <col width="*" />
                                    <col width="*" />
                                </colgroup>
                                <tbody>
                                <tr>
                                    <th>MODEL</th>
                                    <td>
                                        <div class="car-model">
                                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_table_car01.png" alt="" /></figure>
                                            <span class="select-box">
													<label for=""><a href="#a">3.8Luxury</a></label>
													<select>
													<option>3.8Luxury</option>
													<option>3.8Premium Luxury</option>
													<option>3.8Prestige</option>
													<option>3.3TLuxury</option>
													<option>3.3TPremium Luxury</option>
													<option>3.3TPrestige</option>
													<option>5.0Prestige</option>
													<option>5.0Limousine Prestige</option>
												</select>
												</span>
                                            <span class="btn-spec" data-spec="#specs-pop01">
													<a href="#" onclick="toApp_showPopup('#specs-pop01'); return false;">FULL SPECS</a>
												</span>
                                        </div>
                                    </td>

                                    <td class="dim">
                                        <div class="car-model">
                                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_table_car01.png" alt="" /></figure>
                                            <span class="select-box">
													<label for=""><a href="#a">3.8Luxury</a></label>
													<select>
													<option>3.8Luxury</option>
													<option>3.8Premium Luxury</option>
													<option>3.8Prestige</option>
													<option>3.3TLuxury</option>
													<option>3.3TPremium Luxury</option>
													<option>3.3TPrestige</option>
													<option>5.0Prestige</option>
													<option>5.0Limousine Prestige</option>
												</select>
												</span>
                                            <span class="btn-spec" data-spec="#specs-pop02">
													<a href="#">FULL SPECS</a>
												</span>
                                        </div>
                                    </td>

                                    <td class="dim">
                                        <div class="car-model">
                                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_table_car01.png" alt="" /></figure>
                                            <span class="select-box">
													<label for=""><a href="#a">3.8Luxury</a></label>
													<select>
													<option>3.8Luxury</option>
													<option>3.8Premium Luxury</option>
													<option>3.8Prestige</option>
													<option>3.3TLuxury</option>
													<option>3.3TPremium Luxury</option>
													<option>3.3TPrestige</option>
													<option>5.0Prestige</option>
													<option>5.0Limousine Prestige</option>
												</select>
												</span>
                                            <span class="btn-spec" data-spec="#specs-pop03">
													<a href="#">FULL SPECS</a>
												</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Overall Length (mm)</th>
                                    <td>5,205</td>
                                    <td></td>
                                    <td>5,205</td>
                                </tr>
                                <tr>
                                    <th>Overall Width (mm)</th>
                                    <td>1,915</td>
                                    <td></td>
                                    <td>1,915</td>
                                </tr>
                                <tr>
                                    <th>Overall Height (mm)</th>
                                    <td>1,495</td>
                                    <td></td>
                                    <td>1,495</td>
                                </tr>
                                <tr>
                                    <th>Wheelbase (mm)</th>
                                    <td>3,160</td>
                                    <td></td>
                                    <td>3,160</td>
                                </tr>
                                <tr>
                                    <th>Front Wheeltread (mm)</th>
                                    <td>1,630(18")</td>
                                    <td></td>
                                    <td>1,630(18")</td>
                                </tr>
                                <tr>
                                    <th>Rear Wheeltread (mm)</th>
                                    <td>1,659(18")</td>
                                    <td></td>
                                    <td>1,659(18")</td>
                                </tr>
                                <tr>
                                    <th>Engine Label</th>
                                    <td>V6 람다 3.8 GDi</td>
                                    <td></td>
                                    <td>V8 타우 5.0 GDi</td>
                                </tr>
                                <tr>
                                    <th>Driving System</th>
                                    <td>2WD/AWD</td>
                                    <td></td>
                                    <td>AWD</td>
                                </tr>
                                <tr>
                                    <th>Displacement (cc)</th>
                                    <td>3,778</td>
                                    <td></td>
                                    <td>3,778</td>
                                </tr>
                                <tr>
                                    <th>Max. Power(PS/rpm)</th>
                                    <td>315/6,000</td>
                                    <td></td>
                                    <td>315/6,000</td>
                                </tr>
                                <tr>
                                    <th>Max. Torque(kg.m/rpm)</th>
                                    <td>40.5/5,000</td>
                                    <td></td>
                                    <td>40.5/5,000</td>
                                </tr>
                                <tr>
                                    <th>Fuel Tank (L)</th>
                                    <td>77</td>
                                    <td></td>
                                    <td>77</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="btn-area">
                        <a href="g90-specs-mobile.html" class="btn-prev">BACK</a>
                        <a href="#" class="btn-type1 icon-down"><span>MANUAL</span></a>
                        <a href="#" class="btn-type1 icon-down"><span>PRICE-LIST</span></a>
                        <a href="#" class="btn-type1 icon-down"><span>CATALOG</span></a>
                        <span class="btn-type2"><a href="#">Government-certified fuel efficiency and grade</a></span>
                        <span class="btn-type2"><a href="#">Tire labeling grade</a></span>

                    </div>
                    <!-- // specs info -->
                </div>
            </section>

        </div>

    </div>

    <!-- FULL SPECS1 -->
    <section id="specs-pop01" class="layer-wrap hide">
        <div class="layer-specs">
            <a href="#" class="btn-close" onclick="toApp_hidePopup('#specs-pop01'); return false;">닫기</a>
            <div class="layer-inner">

                <h2 class="layer-title">3.8Luxury</h2>
                <ul>
                    <li>
                        <h3 class="sub-title">KEY SPECS</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*"/>
                                <col width="*"/>
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>Overall Length (mm)</th>
                                <td>5,205</td>
                            </tr>
                            <tr>
                                <th>Overall Width (mm)</th>
                                <td>1,915</td>
                            </tr>
                            <tr>
                                <th>Overall Height (mm)</th>
                                <td>1,495</td>
                            </tr>
                            <tr>
                                <th>Wheelbase (mm)</th>
                                <td>3,160</td>
                            </tr>
                            <tr>
                                <th>Front Wheeltread (mm)</th>
                                <td>1,630(18")</td>
                            </tr>
                            <tr>
                                <th>Rear Wheeltread (mm)</th>
                                <td>1,659(18")</td>
                            </tr>
                            <tr>
                                <th>Engine Label</th>
                                <td>V6 Lambda 3.8 GDi</td>
                            </tr>
                            <tr>
                                <th>Driving System</th>
                                <td>2WD/AWD</td>
                            </tr>
                            <tr>
                                <th>Displacement (cc)</th>
                                <td>3,778</td>
                            </tr>
                            <tr>
                                <th>Max. Power(PS/rpm)</th>
                                <td>315/6,000</td>
                            </tr>
                            <tr>
                                <th>Max. Torque(kg.m/rpm)</th>
                                <td>40.5/5,000</td>
                            </tr>
                            <tr>
                                <th>Fuel Tank (L)</th>
                                <td>77</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">POWERTRAIN</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*"/>
                                <col width="*"/>
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>Lambda 3.8 V6 GDi Engine</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Lambda 3.3 V6 Turbo GDi Engine</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Tau 5.0 V8 GDi Engine</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>8-speed Automatic Transmission</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>HTRAC(Electronic AWD)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Direct Steering System (Rack drive-type power steering, variable gear ratio steering)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Drive Mode Control System (Smart/Eco/Sport/Individual)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Genesis Adaptive Control Suspension</th>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">EXTERIOR</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*"/>
                                <col width="*"/>
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>HID Headlamps (dynamic bending function)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Adaptive Full LED Headlamps(drive condition-variable beam pattern function)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>LED Turn Signals Light</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>LED Daytime Running Light</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Full LED Rear Combination Lamps</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>UV Protection Glass (front & rear & all doors)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Dual Pane Sound Proof Glass (front & rear & all doors)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Privacy Glass</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Dual Muffler Integrated with Bumper</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Outside Mirrors (including side repeater, puddle lamp, heated wire, auto unfolding)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Outside Mirrors chrome molding (lower)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Logo Pattern Puddle Lamp</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>245/50R18 Michelin tire & Alloy wheel</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>245/45R19 (Front) & 275/40R19 (Rear) Continental tires & wheels</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>245/45R19 (Front) & 275/40R19 (Rear) Continental tires & Limousine exclusive wheels</th>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">INTERIOR</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>7" TFT LCD Cluster</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Analog Clock</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Shift-by-wire (SBW)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Paddle Shifter</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Room Mirror (ECM, Hi-pass)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>LED Crystal Room Lamp</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Metal Door Scuff</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Vanity Mirror</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Wood Grain Material</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Real Wood Material</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Prime Nappa Leather Material (door trim upper/lower end, center console, crash pad upper end, steering wheel horn cover, etc.)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Real Metal Interior Material (door speaker grilles, trunk plate)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Luxury Suede Material (headlining, pillar trim, etc.)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Ambient Mood Lamps</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Foot Lamp (front, rear seats)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>B Pillar Mood Lamps</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>B Pillar Magazine Pocket</th>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">SAFETY</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>9-airbag System (advanced front airbags, driver seat's knee airbag, front/rear sides, curtain airbags for rollover protection)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Electronic Stability Control System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Tire Air Pressure Alert System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Emergency Alert System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rear Center 3-point Seatbelt</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Babyseat Fastening Device</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Safety Unlock</th>
                                <td>O</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">CONVENIENCE</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>Front Smartphone Wireless Charging Pad</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Button start &amp; Smart Key system</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Card-type Smart Key</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Hill Start Assist Control Function</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rearview Camera</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Touch-type Outside Door Handle</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Full-auto Air Conditioner with 3-zone Control(including front seat dual wind direction control)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Advanced 3-zone Air Conditioner(including B pillar air outlet)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Safety Power Window (front &amp; rear seats)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Front and Rear Parking Assist System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Electronic Parking Brake (Including auto hold function)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Power Trunk System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Electric Backlite Curtain</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Electric Rear side Curtain</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Temporary Spare Tire</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Around-view Monitoring System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Ghost Door Closing</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Heads-up Display</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Smart HVAC system (including indoor CO2 control, harmful gas cut-off device)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Cruise Control</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Vibration Warning Steering Wheel</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Steering Wheel (heated wire, Motor Driven)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rain Sensor</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Auto Defog</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Cluster Ionizer</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Highway Driving Assistance</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Driver Attention Alert System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Smart Blind Spot Detection System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Lane Keeping Assist System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Advanced Smart Cruise Control</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Automatic Emergency Braking System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Smart High Beam</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Front Seat Preactive Seatbelt</th>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">SEATS</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>Power Driver Seat (including lumbar support, cushion extension)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Driver Modern Ergo Seat (22-way adjustable)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Genuine Leather Seat</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Prime Nappa Leather Seat (piping applied)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Semi-aniline Leather Seat</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Front Seat Power Headrests (4-way)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Front Ventilated/Heated Seats</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Driver Seat Memory System (including smart seat system)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Passenger Seat Memory System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Power Front Passenger Seat (including walk-in switch, height adjustment)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Passenger Lumbar Support</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Heated Seat</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rear Ventilated/Heated Seats</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Armrest Adjustment Control</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Ski-through</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Rear Modern Ergo Seat (Right seat 14-way adjustable, 4:2:4 type)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Wing-out Headrests</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Memory System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Neck Pillow (separate)</th>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">MULTIMEDIA</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>DIS(blueLink2.0, 12.3" panoramic display, bluetooth handsfree)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Lexicon Premium Sounds System (14 speakers, quantum logic surround)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Lexicon Premium Sounds System (17 speakers, quantum logic surround)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Dual Monitor</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Front/Rear Seat USB Port (charging, data reading)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>DVD Player (glove box position)</th>
                                <td>O</td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                    <li>
                        <h3 class="sub-title">OPTIONS</h3>
                        <table>
                            <caption></caption>
                            <colgroup>
                                <col width="*" />
                                <col width="*" />
                            </colgroup>
                            <tbody>
                            <tr>
                                <th>HTRAC(Electronic AWD) (2,500,000 KRW)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Safety Sunroof (800,000 KRW)</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Genesis Smart Sense Package (3,000,000 KRW) Highway Driving Assistance, Driver Attention Alert System, Smart Blind Spot Detection System, Advanced Smart Cruise Control, Lane Keeping Assist System, Automatic Emergency Braking System, Vibration Warning Steering Wheel, Smart High Beam, Front Seat Preactive Seatbelt</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Convenience Package (2,000,000 KRW) Heads-up Display, Around View Monitoring System</th>
                                <td>O</td>
                            </tr>
                            <tr>
                                <th>Signature Design Collection (3,200,000 KRW) Prime Nappa Leather Seat (piping applied), Prime Nappa Leather Material (door trim lower end, center console, etc.), Real Wood Material</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Dual Monitor (2,500,000 KRW)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>Rear Seat Comfort Package (2,500,000 KRW) Power Rear Seat (4:2:4 division, sliding only), Rear Ventilated/Heated Seats, Rear Seat Electric Side Curtain</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>First Class VIP Seat (GENESIS EQ900) (3,000,000 KRW) Power Rear Seat (5:5 type), Tower Console, Rear Seat Smartphone Wireless Charging System</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>First Class VIP Seat (GENESIS EQ900L) (3,200,000 KRW) Power Rear Seat (5:5 type), Tower Console, Rear Seat Smartphone Wireless Charging System, Power Leg Support (4-way)</th>
                                <td>-</td>
                            </tr>
                            <tr>
                                <th>● Features Included O Options Available - Options Unavailable</th>
                                <td> </td>
                            </tr>
                            </tbody>
                        </table>
                    </li>
                </ul>
                <div class="btn-area">
                    <!-- <div class="btn-box"> <a href="#"><span class="icon-print">PRINT</span></a> </div> -->
                    <a href="#" class="btn-close" onclick="toApp_hidePopup('#specs-pop01'); return false;">Close</a>
                </div>
    </section>
    <!-- //FULL SPECS1 -->
    <!-- FULL SPECS2 not use -->
    <section id="specs-pop03" class="layer-specs hide">
        <h2 class="layer-title">3.5 럭셔리 스펙 </h2>
        <ul>
            <li>
                <h3 class="sub-title">제원</h3>
                <table>
                    <caption></caption>
                    <colgroup>
                        <col width="*" />
                        <col width="*" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>전장<em>(mm)</em></th>
                        <td>5,205</td>
                    </tr>
                    <tr>
                        <th>전폭<em>(mm)</em></th>
                        <td>1,915</td>
                    </tr>
                    <tr>
                        <th>전고<em>(mm)</em></th>
                        <td>1,495</td>
                    </tr>
                    <tr>
                        <th>축간거리<em>(mm)</em></th>
                        <td>3,160</td>
                    </tr>
                    <tr>
                        <th>윤거 전<em>(mm)</em></th>
                        <td>1,630(18")</td>
                    </tr>
                    <tr>
                        <th>윤거 후<em>(mm)</em></th>
                        <td>1,659(18")</td>
                    </tr>
                    <tr>
                        <th>엔진형식</th>
                        <td>V6 람다 3.8 GDi</td>
                    </tr>
                    <tr>
                        <th>구동방식</th>
                        <td>2WD/AWD</td>
                    </tr>
                    <tr>
                        <th>배기량<em>(cc)</em></th>
                        <td>3,778</td>
                    </tr>
                    <tr>
                        <th>최고출력<em>(PS/rpm)</em></th>
                        <td>315/6,000</td>
                    </tr>
                    <tr>
                        <th>최대토크<em>(kg.m/rpm)</em></th>
                        <td>40.5/5,000</td>
                    </tr>
                    <tr>
                        <th>연료탱크용량<em>(L)</em></th>
                        <td>77</td>
                    </tr>
                    </tbody>
                </table>
            </li>
            <li>
                <h3 class="sub-title">파워트레인</h3>
                <table>
                    <caption></caption>
                    <colgroup>
                        <col width="*" />
                        <col width="*" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>람다 3.8 V6 GDi 엔진</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>8단 자동변속기</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>HTRAC<span>(전자식 AWD)</span></th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>다이렉트 스티어링 시스템<span>(랙 구동형 전동식 파워 스티어링, 가변 기어비 스티어링)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>통합 주행 모드<span>(스마트/에코/스포츠/인디비쥬얼)</span></th>
                        <td>O</td>
                    </tr>
                    </tbody>
                </table>
            </li>
            <li>
                <h3 class="sub-title">외관</h3>
                <table>
                    <caption></caption>
                    <colgroup>
                        <col width="*" />
                        <col width="*" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>HID 헤드램프<span>(다이나믹 벤딩 기능)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>어댑티브 Full LED 헤드램프<span>(주행상황별 빔패턴 변동 기능)</span></th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>LED 방향 지시등</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>LED 주간전조등</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>FULL LED 리어 콤비 램프</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>자외선 차단 글래스<span>(앞면 &amp; 뒷면 &amp; 전체 도어)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>이중접합 차음 글래스<span>(앞면 &amp; 뒷면 &amp; 전체 도어)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>뒷자석 프라이버시 글래스</th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>범퍼 일체형 듀얼 머플러</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>아웃사이드 미러<span>(사이드 리피터, 퍼들램프, 열선, 오토 언폴딩 포함)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>로고패턴 퍼들램프</th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>245/50R18 미쉐린 타이어 &amp; 휠</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>245/45R19(앞) &amp; 275/40R19(뒤)
                            <br /> 콘티넨탈 타이어 &amp; 휠</th>
                        <td>-</td>
                    </tr>
                    </tbody>
                </table>
            </li>
            <li>
                <h3 class="sub-title">내장</h3>
                <table>
                    <caption></caption>
                    <colgroup>
                        <col width="*" />
                        <col width="*" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>7인치 TFT LCD 클러스터</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>아날로그 시계</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>전자식 변속레버<span>(SBW)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>룸미러<span>(ECM, 하이패스)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>LED 크리스탈 룸램프</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>메탈 도어스커프</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>뒷좌석 화장거울</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>우드그레인 내장재</th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>리얼우드 내장재</th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>프라임 나파 가죽 내장재<span>(도어트림 상/하단, 센터콘솔, 크래쉬패드 상단, 스티어링 휠 혼 커버 등)</span></th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>리얼 메탈 내장재<span>(도어스피커 그릴, 트렁크 플레이트)</span></th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>럭셔리 스웨이드 내장재<span>(헤드라이닝, 필라트림 등)</span></th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>앰비언트 무드램프</th>
                        <td>-</td>
                    </tr>
                    <tr>
                        <th>풋램프<em>(앞좌석 &amp; 뒷좌석)</em></th>
                        <td>O-</td>
                    </tr>
                    </tbody>
                </table>
            </li>
            <li>
                <h3 class="sub-title">안전</h3>
                <table>
                    <caption></caption>
                    <colgroup>
                        <col width="*" />
                        <col width="*" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>9에어백<span>(운전석&amp;동승석 어드밴스드 에어백, 운전석 무릎 에어백,<br/> 앞&amp;뒤 사이드 에어백, 전복 대응 커튼 에어백)</span></th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>차체자세 제어장치</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>타이어 공기압 경보장치</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>급제동 경보 시스템</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>뒷좌석 센터 3점식 시트벨트</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>유아용 시트 고정장치</th>
                        <td>O</td>
                    </tr>
                    <tr>
                        <th>세이프티 언락</th>
                        <td>O</td>
                    </tr>
                    </tbody>
                </table>
            </li>
        </ul>
        </div>
        </div>

    </section>
    <!-- //FULL SPECS2 not use -->
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<script>
	;
	(function(window, $, undefined) {
		$(function() {
			App.brand.init();
			App.brand.section.init('.Eq900 .section', false);

		});
	}(window, jQuery));

</script>