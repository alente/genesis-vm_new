<?
$this->title = "Safety - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<nav class="sec-indicator">
		<div class="nav-inner">
			<div class="gnb-area">
				<ul>
					<li><a href="javascript:void(0);" title="" class="cur"><span>SAFETY</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>Highway driving assist</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>Smart blind spot <br>detection</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>Advanced smart <br>cruise control</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>Pre-safe seatbelt</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>9-AIR BAG System</span></a></li>
				</ul>
				<span class="line"></span>
			</div>
		</div>
	</nav>
	<div class="inner-container">
		<div class="btn-down-wrap visible">
			<a class="btn-back on up" href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>#design">BACK <i></i></a>
			<a class="btn-down" href="#"><i></i> CLICK FOR MORE</a>
		</div>
		<!-- Safety kv -->
		<section class="section module-skin1 title-type2 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">БЕЗОПАСНОСТЬ</h2>
					<p class="desc">ПУТЬ <br>К СОВЕРШЕНСТВУ</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature01.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // Safety kv -->
		<!-- sistem -->
		<section class="section module-skin2 title-type1 color-type2">
			<article class="feature">
				<div class="brand-header" data-parallax="y, 30, -50">
					<h2 class="title">ДЛИННЫЕ ДИСТАНЦИИ<br> С УДОВОЛЬСТВИЕМ</h2>
					<p class="desc">Передвижения на длинные дистанции в Genesis G90 не только комфортные,<br> но и безопасные. Все интеллектуальные системы автомобиля работают вместе,<br> чтобы обеспечить предотвращение неконтролируемых дорожных ситуаций.</p>
				</div>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // sistem -->
		<!-- sistem layer : cell-2 -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-2 max">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature03.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Система мониторинга слепых зон</h2>
							<p class="desc">Встроенные в задний бампер датчики предупреждают водителя о приближении других автомобилей сзади. В случае сближения на определённую дистанцию система подаёт водителю специальный сигнал. Это помогает предотвратить опасные ситуации, в особенности при движении на большой скорости.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature04.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Система слежения за дорожной разметкой</h2>
							<p class="desc">С помощью камеры, установленной в верхней части лобового стекла, система слежения за дорожной разметкой контролирует полосы движения и в режиме реального времени предупреждает водителя при смене полос без использования сигналов поворота и активно предотвращает непреднамеренный выезд на соседнюю полосу движения.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // sistem layer : cell-2 -->
		<!-- control -->
		<section class="section module-skin2 title-type5 color-type1 motion-type6">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ИНТЕЛЛЕК&shy;ТУАЛЬНЫЙ КРУИЗ-КОНТРОЛЬ</h2>
					<p class="desc">Интеллектуальный круиз-контроль автоматически сохраняет заданные скорость и расстояние при движении по трассе, не требуя вмешательства водителя. В передней части автомобиля установлен радар, отслеживающий расстояние до движущихся впереди автомобилей, и самостоятельно корректирующий скорость движения вашего G90.</p>
				</div>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature05.jpg" alt=""></figure>
					<div class="content-inner">
						<!-- <figure class="car-shadow"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_car_shadow2.png" alt=""></figure> -->
						<div class="car">
							<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_car.png" alt=""></figure>
							<span class="wheel-light"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_wheel_light.png" alt=""></span>
							<span class="wheel-left"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_wheel.png" alt=""></span>
							<span class="wheel-right"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_wheel.png" alt=""></span>
						</div>
						<div class="line">
							<div class="number-wrap">
								<span class="number">
									<span>8</span>
									<span>9</span>
									<span>10</span>
									<span>11</span>
									<span>12</span>
									<span>13</span>
									<span>14</span>
									<span>15</span>
									<span>16</span>
									<span>17</span>
									<span>18</span>
									<span>19</span>
									<span>20</span>
								</span>
								<span>0M</span>
							</div>
						</div>
					</div>
				</div>
			</article>
			<!-- upper content -->
			<!-- <article class="upper-content">
					<ul>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature06.jpg" alt="" /></figure>
							<div class="brand-header">
								<h2 class="title">앞좌석 프리액티브<br /> 시트벨트</h2>
								<p class="desc">급제동이나 미끄러짐 등의 위험 상황이 감지되면<br /> 시트벨트를 당겨 충돌 시 탑승객을 보호하고, 급선회<br />시에도 시트벨트를 되감아 운전자의 쏠림을 예방합니다.</p>
							</div>
						</li>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature07.jpg" alt="" /></figure>
							<div class="brand-header">
								<h2 class="title">자동 긴급제동 시스템</h2>
								<p class="desc">전방 레이더와 전방 감지 카메라의 신호를 종합적으로 판단하여 선행차량 및 보행자와의 추돌 위험 상황이 감지될 경우, 운전자에게 경보를 하고 필요시 브레이크 작동을 보조해주는 장비입니다.</p>
							</div>
						</li>
					</ul>
				</article> -->
		</section>
		<!-- // control -->
		<!-- control layer : cell-2 -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-2 min">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature06.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Ремни безопасности с преднатяжителями</h2>
							<p class="desc">Эта система надежно и крепко удерживает водителя и пассажиров во время экстренного торможения, скольжения или резкого поворота автомобиля для минимизации риска повреждений.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature07.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Система автоматического экстренного торможения</h2>
							<p class="desc">Радары и камеры, установленные в передней части G90, следят за движущимися впереди автомобилями. В случае опасного сближения система подаёт водителю сигнал тревоги и автоматически включает системы экстренного торможения в критических ситуациях.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature05_bg.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // control layer : cell-2 -->
		<!-- airbag -->
		<section class="section module-skin2 title-type1 color-type2 motion-type4">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">9 ПОДУШЕК БЕЗОПАСНОСТИ</h2>
					<p class="desc">Genesis G90 оборудован большим количеством подушек безопасности<br> и обеспечивает высокую степень защиты для пассажира и водителя. </p>
				</div>
				<div class="brand-content drag-box">
					<figure class="feature inside"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/genesis-eq900-features-safety-9-air-bag-system-inside.jpg" alt=""></figure>
					<figure class="feature outside"><img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_feature08_2.jpg" alt=""></figure>
					<div class="control-box">
						<a href="#none" class="btn-control"></a>
						<span class="control-line"></span>
					</div>
				</div>
			</article>
		</section>
		<!-- // airbag -->
		<!-- banner -->
		<section class="eq-banner">
			<a href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>/performance">
					<span class="description">
						<span>NEXT</span>
						<strong>Производительность</strong>
						<i>SCROLL</i>
					</span>
				<img src="<?=Yii::$app->homeUrl?>/images/desktop/safety/img_banner.jpg" alt="">
			</a>
		</section>
		<!-- // banner -->
	</div>
</div>
<?else:?>
	<div id="container">
		<div class="inner-container">
			<!-- <a class="btn-back" href="#" title="">BACK <i></i></a> -->
			<!-- kv type -->
			<section class="section m-module-skin1 m-title-type2 m-color-type2">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_feature_kv.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">БЕЗОПАСНОСТЬ</h2>
						<p class="desc">ПУТЬ К СОВЕРШЕНСТВУ</p>
					</div>
				</article>
			</section>
			<!-- // kv type -->
			<!-- crest -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_feature02.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">ДЛИННЫЕ ДИСТАНЦИИ С УДОВОЛЬСТВИЕМ</h2>
						<p class="desc">Передвижения на длинные дистанции в Genesis G90 не только комфортные, но и безопасные. Все интеллектуальные системы автомобиля работают вместе, чтобы обеспечить предотвращение неконтролируемых дорожных ситуаций.</p>
					</div>
				</article>
			</section>
			<!-- crest sub content -->
			<section class="section m-module-skin4 m-color-type1">
				<article class="feature">
					<ul>
						<li class="bg-gray">
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_feature03.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Система мониторинга слепых зон</h2>
								<p class="desc">Встроенные в задний бампер датчики предупреждают водителя о приближении других автомобилей сзади. В случае сближения на определённую дистанцию система подаёт водителю специальный сигнал. Это помогает предотвратить опасные ситуации, в особенности при движении на большой скорости.</p>
							</div>
						</li>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_feature04.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Система слежения за дорожной разметкой</h2>
								<p class="desc">С помощью камеры, установленной в верхней части лобового стекла, система слежения за дорожной разметкой контролирует полосы движения и в режиме реального времени предупреждает водителя при смене полос без использования сигналов поворота и активно предотвращает непреднамеренный выезд на соседнюю полосу движения.</p>
							</div>
						</li>
					</ul>
				</article>
			</section>
			<!-- // crest -->
			<!-- control -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1 m-motion-type6">
				<article class="feature">
					<div class="car-box">
						<div class="car">
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_car.png" alt=""></figure>
							<span class="wheel-light"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_wheel_light.png" alt=""></span>
							<span class="wheel-left"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_wheel.png" alt=""></span>
							<span class="wheel-right"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_wheel.png" alt=""></span>
						</div>
						<div class="number-wrap">
							<!-- <strong>선행차량과의 거리(m)</strong> нужен перевод -->
							<span class="number">
								<span>80</span>
						<span>90</span>
						<span>100</span>
						<span>110</span>
						<span>120</span>
						<span>130</span>
						<span>140</span>
						<span>150</span>
						<span>160</span>
						<span>170</span>
						<span>180</span>
						<span>190</span>
						<span>200</span>
						</span>
							<!-- <span>0M</span> -->
						</div>
					</div>
					<div class="brand-header">
						<h2 class="title">ИНТЕЛЛЕКТУАЛЬ&shy;НЫЙ КРУИЗ-КОНТ&shy;РОЛЬ</h2>
						<p class="desc">Интеллектуальный круиз-контроль автоматически сохраняет заданные скорость и расстояние при движении по трассе, не требуя вмешательства водителя. В передней части автомобиля установлен радар, отслеживающий расстояние до движущихся впереди автомобилей, и самостоятельно корректирующий скорость движения вашего G90.</p>
					</div>
				</article>
			</section>
			<!-- // control -->
			<section class="section m-module-skin4 m-color-type1">
				<article class="feature">
					<ul>
						<li class="bg-gray">
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/genesis-eq900-features-safety-pre-safety-seatbelt.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Ремни безопасности с преднатяжителями</h2>
								<p class="desc">Эта система надежно и крепко удерживает водителя и пассажиров во время экстренного торможения, скольжения или резкого поворота автомобиля для минимизации риска повреждений.</p>
							</div>
						</li>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_feature06.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Система автоматического экстренного торможения</h2>
								<p class="desc">Радары и камеры, установленные в передней части G90, следят за движущимися впереди автомобилями. В случае опасного сближения система подаёт водителю сигнал тревоги и автоматически включает системы экстренного торможения в критических ситуациях.</p>
							</div>
						</li>
					</ul>
				</article>
			</section>
			<!-- airbag -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1 m-motion-type4">
				<article class="feature">
					<div class="drag-box">
						<figure class="on feature"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_drag_on.jpg" alt="" /></figure>
						<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_drag_off.jpg" alt="" /></figure>
						<div class="control-btn">
							<button type="button"></button>
							<span class="control-line"></span>
						</div>
					</div>
					<div class="brand-header">
						<h2 class="title">9 ПОДУШЕК БЕЗОПАСНОСТИ</h2>
						<p class="desc">Genesis G90 оборудован большим количеством подушек безопасности и обеспечивает высокую степень защиты для пассажира и водителя. </p>
					</div>
				</article>
			</section>
			<!-- // airbag -->
			<!-- banner -->
			<section class="eq-banner">
				<a href="./g90-performance-mobile.html">
					<span class="description">
						<span>NEXT</span>
						<strong>Производительность</strong>
					</span>
					<img src="<?=Yii::$app->homeUrl?>/images/mobile/safety/img_banner.jpg" alt="">
				</a>
			</section>
			<!-- // banner -->
		</div>
	</div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<?if($mobilePrefix):?>
	<script>
		;
		(function(window, $) {
			$(function() {

				$('.btn-zoom').on('click', function() {
					$('.m-zoom-type1, .m-zoom-type2').addClass('active');
				});
				$('.btn-zoom-close').on('click', function() {
					$('.m-zoom-type1.active, .m-zoom-type2.active').removeClass('active');
				});

			});
		}(window, jQuery));

	</script>
<?else:?>
<script>
	;
	(function(window, $, undefined) {
		$(function() {

			App.brand.init();
			App.brand.section.init('.Eq900 .section');
			App.brand.navigator.init({
				section: '.section > article'
			});
		});
	}(window, jQuery));

</script>
<?endif?>