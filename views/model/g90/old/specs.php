<?
$this->title = "Specs - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<div class="inner-container">
		<!-- Specs select -->
		<section class="section specs specs-select">
			<div class="brand-content">
				<div class="specs-inner">
					<div class="select-category">
						<h2 class="title">SPECS</h2>
						<ul>
							<li>
								<!-- <figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/specs/img_specs_car01.png" alt="" /></figure> -->
								<strong>3.8 GDi</strong>
								<ul>
									<li>
										<input type="checkbox" id="check01" checked="" />
										<label for="check01">Luxury</label>
									</li>
									<li>
										<input type="checkbox" id="check02" />
										<label for="check02">Premium Luxury</label>
									</li>
									<li>
										<input type="checkbox" id="check03" />
										<label for="check03">Prestige</label>
									</li>
								</ul>
							</li>
							<li>
								<!-- <figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/specs/img_specs_car01.png" alt="" /></figure> -->
								<strong>3.3T GDi</strong>
								<ul>
									<li>
										<input type="checkbox" id="check04" />
										<label for="check04">Luxury</label>
									</li>
									<li>
										<input type="checkbox" id="check05" />
										<label for="check05">Premium Luxury</label>
									</li>
									<li>
										<input type="checkbox" id="check06" />
										<label for="check06">Prestige</label>
									</li>
								</ul>
							</li>
							<li>
								<!-- <figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/specs/img_specs_car01.png" alt="" /></figure> -->
								<strong>5.0 GDi</strong>
								<ul>
									<li>
										<input type="checkbox" id="check07" />
										<label for="check07">Prestige</label>
									</li>
									<li>
										<input type="checkbox" id="check08" />
										<label for="check08">Limousine Prestige</label>
									</li>
								</ul>
							</li>
						</ul>
						<p>Please select the trims (up to 3) you wish to view and then click the button below.</p>
						<div class="btn-box">
							<a href="g90-specs-detail.html">SEE DETAILS</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- // Specs select -->
	</div>
</div>
<?else:?>
    <div id="container">

        <div class="inner-container">
            <section class="section specs">
                <div class="specs-select">
                    <div class="specs-title">
                        <h2 class="title">SPECS</h2>
                        <p class="desc">Please select the trims (up to 3) you wish to view and then click the button below.</p>
                    </div>
                    <ul class="specs-list">
                        <li>
                            <strong>3.8 GDi</strong>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_specs_car01.png" alt="" /></figure>
                            <ul>
                                <li>
                                    <input type="checkbox" id="check01"/>
                                    <label for="check01">Luxury</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="check02" />
                                    <label for="check02">Premium Luxury</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="check03" />
                                    <label for="check03">Prestige</label>
                                </li>
                                <!-- <li>
									<input type="checkbox" id="check04" />
									<label for="check04">프레스티지</label>
								</li>
								<li>
									<input type="checkbox" id="check05" />
									<label for="check05">프레스티지</label>
								</li> -->
                            </ul>
                        </li>
                        <li>
                            <strong>3.3T GDi</strong>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_specs_car01.png" alt="" /></figure>
                            <ul>
                                <li>
                                    <input type="checkbox" id="check06"/>
                                    <label for="check06">Luxury</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="check07"/>
                                    <label for="check07">Premium Luxury</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="check08"/>
                                    <label for="check08">Prestige</label>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <strong>5.0 GDi</strong>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/specs/img_specs_car01.png" alt="" /></figure>
                            <ul>
                                <li>
                                    <input type="checkbox" id="check09"/>
                                    <label for="check09">Prestige</label>
                                </li>
                                <li>
                                    <input type="checkbox" id="check10"/>
                                    <label for="check10">Limousine Prestige</label>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <a href="g90-specs_detail-mobile.html" class="btn-more">SEE DETAILS</a>
                </div>
            </section>

        </div>

    </div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<script>
	;
	(function(window, $, undefined) {
		$(function() {

			App.brand.init();
			App.brand.section.init('.Eq900 .section', false);


		});
	}(window, jQuery));

</script>