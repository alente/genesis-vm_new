<?
$this->title = "Яндекс.Драйв - Genesis ".$this->params['modelName'];
$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
$mobilePrefix = $isMobile?'m-':null;


if($isMobile){
	include "../static/models/source/html/g70-ydrive-mobile.html";
}else{
	include "../static/models/source/html/g70-ydrive.html";
}
echo $this->render('/partials/footer');
echo $this->render('/model/scripts');
if($isMobile){
	include "../static/models/source/html/inc/g70-scripts-ydrive-mobile.html";
}else{
	include "../static/models/source/html/inc/g70-scripts-ydrive.html";
}