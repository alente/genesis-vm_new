<?
$this->title = "Gallery - Genesis ".$this->params['modelName'];

$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
$mobilePrefix = $isMobile?'m-':null;


if($isMobile){
	include "../static/models/source/html/g70-gallery-mobile.html";
}else{
	include "../static/models/source/html/g70-gallery.html";
}
echo $this->render('/partials/footer');
echo $this->render('/model/scripts');
if($isMobile){
	include "../static/models/source/html/inc/g70-scripts-gallery-mobile.html";
}else{
	include "../static/models/source/html/inc/g70-scripts-gallery.html";
}
?>