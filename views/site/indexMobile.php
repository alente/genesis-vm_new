<?
use yii\helpers\Url;
?>
<div id="container" class="gatemain">
	<div class="inner-cont">
		<section class="cont-fullsize kv-area js-check-height" data-indicator-index="0">
			<div class="inner-cont-wrap">
				<div class="content">
					<!-- gate-swiper-wrap -->
					<div class="gate-swiper-wrap">
						<div class="swiper-container swiper-container-h">
							<!-- swiper-wrapper -->
							<div class="swiper-wrapper">
								<!-- section1 : key visual -->
								<div class="swiper-slide">
									
									<!-- kv slide -->
									<div class="swiper-container swiper-container-v">
										<div class="swiper-wrapper">
											<!-- kv01 -->
											<div class="swiper-slide">
												<div class="inner-cont-wrap">
													<div class="txt-holder">
														<div class="inner-txt">
															<h1 class="type-tit">
																<span>GENESIS</span>
															</h1>
															<div class="paragraph">
																<p>
																	Полноприводные седаны премиум-класса.
																</p>
															</div>
															
															<div class="gatemain-btn">
																<div class="gatemain-btn-wrap">
																	<a href="/configurator"><span>Конфигуратор</span></a>
																	<a href="/contacts.html"><span>Контакты</span></a>
																	<a href="/test-drive"><span>Тест-драйв</span></a>
																</div>
															</div>
														
														</div>
													</div>
													<div class="bg-holder">
														<img src="/images/gatemain/gatemain_kv.jpg" alt="" />
													</div>
													<span class="line"></span>
												</div>
											</div>
											<!-- //kv01 -->
										</div>
										<div class="swiper-pagination swiper-pagination-v"></div>
									</div>
									<!-- // kv slide -->
								
								</div>
								<!-- //section1 : key visual -->
								
								<!-- section2 : Models -->
								<div class="swiper-slide">
									<div class="intro-wrap">
										<ul class="model-list">
											<li>
												<a href="/g70.html">
													<strong>GENESIS G70</strong>
													<img src="/images/gatemain/genesis-g70-main.png" alt="G70">
												</a>
											</li>
											
											<li>
												<a href="/g80.html">
													<strong>GENESIS g80</strong>
													<img src="/images/gatemain/genesis-g80-main.png" alt="G80">
												</a>
											</li>
											<li>
												<a href="/g90.html">
													<strong>GENESIS g90</strong>
													<img src="/images/gatemain/genesis-g90-main.png" alt="G90">
												</a>
											</li>
											<li>
												<a href="/g90.html#long">
													<strong>GENESIS G90 L</strong>
													<img src="/images/gatemain/genesis-g90l-main.png" alt="G90L">
												</a>
											</li>
										</ul>
									</div>
								</div>
								<!-- //section2 : Models -->
								
								<!-- section3 : contents -->
								<div class="swiper-slide">
									<div class="contents-wrap">
										
										<ul class="contents-tab">
											<li><a href="news.html">НОВОСТИ И МЕРОПРИЯТИЯ</a></li>
										</ul>
										
										<div class="contents-list">
											<!-- NEWS & EVENT contents -->
											<div class="tab01">
												<ul class="contents-area">
													<?php  foreach ($news as $newsItem) {
														$detailUrl = Url::toRoute(['news/view', 'code'=>$newsItem->code]);
														$date = Yii::$app->formatter->asDate($newsItem->date);
														?>
														<li>
															<a href="<?=$detailUrl?>">
																<img src="<?=$newsItem->preview_file?>" alt="<?=$newsItem->name?>">
																<div class="figcaption">
																	<strong><?=$newsItem->name?></strong>
																	<span class="date"><?=$date?></span>
																	<p><?=$newsItem->preview_description?></p>
																</div>
															</a>
														</li>
													<?php }  ?>
												</ul>
            
											</div>
											
										</div>
									
									
									</div>
								</div>
								<!-- //section3 : contents -->
							</div>
							<!-- //swiper-wrapper -->
							<!-- gatemain btn-wrap-->
							<div class="gate-swiper-btn">
								<div class="swiper-pagination swiper-pagination-h"></div>
								<div class="swiper-button-next">НОВОСТИ</div>
								<div class="swiper-button-prev">МОДЕЛИ</div>
								<ul class="">
									<li></li>
									<li></li>
								</ul>
							</div>
							<!-- //gatemain btn-wrap-->
						</div>
					</div>
					<!-- //gate-swiper-wrap -->
				</div>
			</div>
		</section>
	</div>
</div>