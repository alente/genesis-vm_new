<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Genesis';
?>
<div id="container" class="gatemain not-hf-100per">
    <div class="inner-contain">
		<section class="kv-area js-check-height cont-fullsize" data-indicator-index="0" style="background-image: url(/images/g70/desktop/teaser-main-banner.jpg)">
			<div class="inner-cont-wrap"  data-test="1">
				<div class ="content">
					<div class="txt-holder teaser-holder">
						<div class="inner-txt" data-opacity="0,1,1,0">
							<h1 data-top="0,1,0,-150" class="teaser-title">НОВЫЙ GENESIS G70</h1>
							<div class="btns teaser-btns">
								<a href="/g70.html" class="gbtn talign-r"><span class="atxt">подробнее</span></a>
							</div>
						</div>
					</div>
				
					<span class="line"></span>
				
					<div class="btn-scroll active">
						<a href="javascript:void(0);">
							<div class="txt-area">
								<span class="btn-txt">ДАЛЬШЕ</span>
							</div>
							<br>
							<div class="line-area">
                                    <span class="vert-line-mask">
                                        <span class="vert-line"></span>
                                    </span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</section>

        <section class="intro-wrap">
            <div class="inner-cont-wrap">
                <div class ="content">
                    <!-- swiper-container -->
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
							<div class="swiper-slide">
								<a href="/g70.html">
                                    <span class="figure">
                                        <img src="/images/gatemain/genesis-g70-main.png" alt="g70">
                                    </span>
									<span class="figcaption">
                                        <strong>GENESIS G70</strong>
                                        <span>Подробнее</span>
                                    </span>
								</a>
							</div>
							
							<div class="swiper-slide">
                                <a href="/g80.html">
									<span class="figure">
                                    	<img src="/images/gatemain/genesis-g80-main.png" alt="g80">
									</span>
                                    <div class="figcaption">
                                        <strong>GENESIS G80</strong>
                                        <span>Подробнее</span>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="/g90.html">
									<span class="figure">
                                    	<img src="/images/gatemain/genesis-g90-main.png" alt="g90">
									</span>
                                    <div class="figcaption">
                                        <strong>GENESIS G90</strong>
                                        <span>Подробнее</span>
                                    </div>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a href="/g90.html#long">
									<span class="figure">
                                    	<img src="/images/gatemain/genesis-g90l-main.png" alt="g90 L">
									</span>
                                    <div class="figcaption">
                                        <strong>GENESIS G90 L</strong>
                                        <span>Подробнее</span>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- pre next btns -->
                        <a href="javascript:void(0);" class="swiper-button-prev">
                            <span class="img2"><img src="/images/common/btn_prev.png" alt="Prev"></span>
                        </a>
                        <a href="javascript:void(0);" class="swiper-button-next">
                            <span class="img2"><img src="/images/common/btn_next.png" alt="Next"></span>
                        </a>
                        <!-- //pre next btns -->
                    </div>
                    <!-- //swiper-container -->
                </div>
            </div>
        </section>

        <section class="content-wrap">
            <div class="inner-cont-wrap">
                <div class ="content">
                    <!-- tab & contents -->
                    <ul class="content-tab">
                        <li>
                            <a href="news.html">НОВОСТИ И МЕРОПРИЯТИЯ</a>
                            <div class="tab-contents">
                                <!-- tab01 swiper-container -->
                                <div class="swiper-container tab01">
									<div class="swiper-wrapper">
										
										<?php  foreach ($news as $newsItem) {
											$detailUrl = Url::toRoute(['news/view', 'code'=>$newsItem->code]);
											$date = Yii::$app->formatter->asDate($newsItem->date);
											?>
											<div class="swiper-slide">
												<a href="<?=$detailUrl?>">
													<img src="<?=$newsItem->preview_file?>" alt="<?=$newsItem->name?>">
													<div class="figcaption">
														<strong><?=$newsItem->name?></strong>
														<span class="date"><?=$date?></span>
														<p><?=$newsItem->preview_description?></p>
													</div>
												</a>
											</div>
										<? } ?>
	
	
									</div>

                                    <!-- pre next paging btns -->
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-button-wrap">
                                        <a href="javascript:void(0);" class="swiper-button-prev">
                                            <span class="img2"><img src="/images/common/btn_prev.png" alt="Назад"></span>
                                        </a>
                                        <a href="javascript:void(0);" class="swiper-button-next">
                                            <span class="img2"><img src="/images/common/btn_next.png" alt="Вперёд"></span>
                                        </a>
                                    </div>
                                    <!-- //pre next paging btns -->
                                </div>
                                <!-- //tab01 swiper-container -->
                            </div>
                        </li>
                    </ul>
                    <!-- //tab & contents -->
                </div>
            </div>
        </section>

    </div>
</div>
