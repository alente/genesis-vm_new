<?
use yii\helpers\Url,
	yii\widgets\LinkPager;
?>
<link rel="stylesheet" href="/css/news.css" />
<div class="layout__content">
	<?if(!empty($news)):?>
		<?
		$firstItem = array_shift($news);
		$detailUrl = Url::toRoute(['news/view', 'code'=>$firstItem->code]);
		$date = Yii::$app->formatter->asDate($firstItem->date);
		?>
		<div class="news_announcement-top">
			<div class="news_announcement__item al-left">
				<a class="news_announcement__item-half news-an-img" href="<?=$detailUrl?>"><img src="<?=$firstItem->preview_file?>"></a>
				<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date"><?=$date?></span>
					<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="<?=$detailUrl?>"><?=$firstItem->name?></a></h2>
					<p class="news_announcement_desc"><?=$firstItem->preview_description?></p>
					<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="<?=$detailUrl?>">читать далее</a></div>
				</div>
			</div>
		</div>
	<?endif?>
	<?if(!empty($news)):?>
		<div class="layout__wrapper news_announcement-content">
			<?$k = 0; foreach($news as $item): $k++;
				$detailUrl = Url::toRoute(['news/view', 'code'=>$item->code]);
				$date = Yii::$app->formatter->asDate($item->date);
				$classItem = ($k%2 == 0)?'al-left':'al-right';
				?>
				<div class="news_announcement__item <?=$classItem?>">
					<a class="news_announcement__item-half news-an-img" href="<?=$detailUrl?>"><img src="<?=$item->preview_file?>"></a>
					<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date"><?=$date?></span>
						<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="<?=$detailUrl?>"><?=$item->name?></a></h2>
						<p class="news_announcement_desc"><?=$item->preview_description?></p>
						<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="<?=$detailUrl?>">читать далее</a></div>
					</div>
				</div>
			<?endforeach?>
		</div>
	<?endif?>
	
	<div class="layout__wrapper">
		<?=LinkPager::widget([
			'pagination' => $pages,
		]);?>
	</div>
	
	
</div>