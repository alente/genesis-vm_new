<?php
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js" lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="<?=Yii::$app->homeUrl?>images/favicon/favicon.ico"/>
    
    <?/*= Html::csrfMetaTags() */?>
    <title><?= Html::encode($this->title) ?></title>
	<?/*php $this->head() */?>
	<?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/mob/style.css">
	<?else:?>
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/desk/style.css">
	<?endif?>
</head>

<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <!-- Skip Nav -->
    <div id="skip-navi">
        <a href="#container"><span>Закрыть</span></a>
        <a href="#gnb-menu-open-btn"><span>Меню</span></a>
    </div>
    
    <header class="header_2017">
        <?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
            <? echo $this->render('/partials/menuMob'); ?>
        <?else:?>
	        <? echo $this->render('/partials/menu'); ?>
        <?endif?>
    </header>
    
    <?= $content ?>
	
	<?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
		<? echo $this->render('/partials/footerMobile'); ?>
    <?else:?>
		<? echo $this->render('/partials/footer'); ?>
	<?endif?>
    <div id="overlay" class="hide"></div>

</div><?/* .wrapper */?>

<div class="top-btn">
    Наверх
</div>

<?/*php $this->endBody() */?>
<?if(\Yii::$app->devicedetect->isMobile()):?>
    <script src="<?=Yii::$app->homeUrl?>scripts/mob.js"></script>
<?else:?>
    <script src="<?=Yii::$app->homeUrl?>scripts/desktop.js"></script>
<?endif?>

<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/jquery.nanoscroller.min.js"></script>
<script type="text/javascript" src="/js/forma-call.js"></script>

</body>
</html>
<?php $this->endPage() ?>