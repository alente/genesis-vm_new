<?php
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html lang="ru">
	<head>
		<link http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta charset="utf-8">
		<link http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport">
		<meta name="viewport" content="width=device-width">
		<link name="viewport" content="width=device-width, initial-scale=3, user-scalable = no">
		<link rel="shortcut icon" type="image/x-icon" href="<?=Yii::$app->homeUrl?>/images/favicon/favicon.ico"/>
		<link name="msapplication-TileColor" content="#ffffff"/>

		<?/*= Html::csrfMetaTags() */?>
		<title>GENESIS</title>
		<?/*php $this->head() */?>

		<link type="text/css" rel="stylesheet" href="<?=Yii::$app->homeUrl?>/css/build.css"/>
		<link type="text/css" rel="stylesheet" href="<?=Yii::$app->homeUrl?>/css/finance.css"/>


		<link type="text/css" rel="stylesheet" href="<?=Yii::$app->homeUrl?>/styles/header/webfonts.css">
		<link type="text/css" rel="stylesheet" href="<?=Yii::$app->homeUrl?>/styles/header/header.css">

		<?/* include('./js/analitycs.php');*/?>
	</head>


<body>
<?php $this->beginBody() ?>

<?/*include('./js/analitycs_afterBodyOpen.php');*/?>

<div class="navigation__overlay js-navigation js-toggle-navigation"></div>
<div class="navigation js-navigation">
	<div class="navigation__close">
		<button class="button button--navigation-close js-toggle-navigation"><span class="line-wrap"><span class="tb-lines"></span></span></button>
	</div>
	<div class="navigation__wrapper">
		<div class="navigation__search">
			<div class="navigation-search">
				<form class="navigation-search__form" action="search.html" method="post">
					<button class="navigation-search__search button button--transparent"><span class="button__icon"><span class="fa fa-search"></span></span></button>
					<input class="navigation-search__field" type="text">
					<button class="navigation-search__cancel button button--transparent"><span class="button__text">Cancel</span></button>
				</form>
			</div>
		</div>
		<div class="navigation__links">
			<div class="navigation-link">
				<ul class="navigation-link__items navigation-link__items--level-1">
					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="genesis.html" title="О БРЕНДЕ">О БРЕНДЕ<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a>
						<ul class="navigation-link__items navigation-link__items--level-2 second-level">
							<li class="navigation-link__item navigation-link__item--level-2 navigation-link__item--level-2-active"><a class="navigation-link__link navigation-link__link--level-2" href="genesis.html#set-intro" title="Бренд">Бренд</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="genesis.html#set-innovation" title="Инновации">Инновации</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="genesis.html#set-performance" title="Исполнение">Исполнение</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="genesis.html#set-design" title="Дизайн">Дизайн</a></li>
						</ul>
					</li>
					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="g90.html" title="g90">g90<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a>
						<ul class="navigation-link__items navigation-link__items--level-2 second-level">
							<li class="navigation-link__item navigation-link__item--level-2 navigation-link__item--level-2-active"><a class="navigation-link__link navigation-link__link--level-2" href="g90.html#set-intro" title="Обзор">Обзор</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g90.html#set-view360" title="360">360</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g90.html#set-complectations" title="Комплектации g90">Комплектации</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="configurator" title="Создать свой g90">Создать свой g90</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g90.html#set-features" title="ОСОБЕННОСТИ">ОСОБЕННОСТИ</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g90.html#set-gallery" title="ГАЛЕРЕЯ">ГАЛЕРЕЯ</a></li>
						</ul>
					</li>

					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="g80.html" title="g80">g80<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a>
						<ul class="navigation-link__items navigation-link__items--level-2 second-level">
							<li class="navigation-link__item navigation-link__item--level-2 navigation-link__item--level-2-active"><a class="navigation-link__link navigation-link__link--level-2" href="g80.html#set-intro" title="Обзор">Обзор</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g80.html#set-view360" title="360">360</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g80.html#set-complectations" title="Комплектации g80">Комплектации</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="configurator" title="Создать свой g80">Создать свой g80</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g80.html#set-features" title="ОСОБЕННОСТИ">ОСОБЕННОСТИ</a></li>
							<li class="navigation-link__item navigation-link__item--level-2"><a class="navigation-link__link navigation-link__link--level-2" href="g80.html#set-gallery" title="ГАЛЕРЕЯ">ГАЛЕРЕЯ</a></li>
						</ul>
					</li>

					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="configurator" title="Конфигуратор">Конфигуратор<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a></li>

					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="warranty.html" title="Гарантия">Гарантия<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a></li>
					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="test-drive.html" title="Записаться на тест-драйв">Записаться на тест-драйв<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a></li>
					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="dealer-near-you.html" title="Найти дилера">Найти дилера<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a>
					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="genesis-finance.html" title="Найти дилера">Genesis Finance<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a>
					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="owners.html" title="Найти дилера">Владельцам<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a>
						<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="news.html" title="Новости">Новости<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a></li>
					</li>

					<li class="navigation-link__item navigation-link__item--level-1 navigation-link__item--level-1-open first-level"><a class="navigation-link__link navigation-link__link--level-1" href="support.html" title="Партнеры">Служба клиентской поддержки<span class="navigation-link__icon"><span class="fa fa-angle-down"></span><span class="fa fa-angle-up"></span></span></a></li>
				</ul>
			</div><a href="https://www.youtube.com/genesisrussia" target="_blank"></a>
		</div>
		<div class="navigation__options">
			<div class="navigation__languages"><a class="navigation__languages-item navigation__languages-item--active" href="https://www.genesis.com/kr/en/genesis.html" title="Eng">Eng</a>
			</div>
		</div>
	</div>
</div>

<div class="layout layout--page">
	<div class="wrapper">

        <header class="header_2017">
		    <? echo $this->render('/partials/menu'); ?>
        </header>
		<header class="header_2017--mob">
			<? echo $this->render('/partials/menuMob'); ?>
		</header>
	</div>

	<div class="layout__content">

		<?= $content ?>

		<footer class="layout__footer">
			<div class="footer">
				<div class="footer__top">
					<div class="footer-link__items"></div>
				</div>
				<div class="footer__bottom">
					<div class="footer__country-drop-down">
						<div class="country-drop-down">
							<div class="country-drop-down__selected"><span class="icon icon--flag-us"></span></div>
							<select class="country-drop-down__select">
								<option value="ru">Русский</option>
								<option value="us">English</option>
							</select>
						</div>
					</div>
					<div class="footer__copyright"><?=date('Y');?> © ООО "Хендэ Мотор СНГ"</div>
					<div class="footer__disclaimer">
						Вся представленная на сайте информация, касающаяся автомобилей и сервисного обслуживания, носит информационный характер и не является публичной офертой, определяемой положениями ст. 437 (2) ГК РФ. Все цены указанные на данном сайте носят информационный характер и являются максимально рекомендуемыми розничными ценами по расчетам дистрибьютора (ООО «Хендэ Мотор СНГ»). Для получения подробной информации просьба обращаться к ближайшему официальному дилеру ООО «Хендэ Мотор СНГ». Опубликованная на данном сайте информация может быть изменена в любое время без предварительного уведомления.
					</div>
				</div>
			</div>
		</footer>
	</div><?/* .layout__content */?>

</div><?/* .layout.layout--page */?>

<div class="mobileDetector" id="mobileDetector"></div>


<script src="<?=Yii::$app->homeUrl?>/js/build.js" charset="utf-8"></script>
<script src="<?=Yii::$app->homeUrl?>/scripts/header/common.js"></script>
<script src="<?=Yii::$app->homeUrl?>/scripts/header/App.js"></script>
<script src="<?=Yii::$app->homeUrl?>/scripts/header/App.navigation.js"></script>
<script src="<?=Yii::$app->homeUrl?>/scripts/header/App.navigation-mob.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC77VFUh-CNwCCXT2fuNJSojg92ajbLNKo"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/dealers.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/map.js"></script>

<!-- Add scripts here if needed custom for a page as-->
<svg hidden="hidden" style="display: none !important;" width="0" height="0">
    <symbol id="rouble-svg" viewBox="0 0 28.35 36.85">
        <title>Rouble</title>
        <path d="M12.14,21.1q.94,0,2,0a15.9,15.9,0,0,0,5.36-.86,9.43,9.43,0,0,0,4-2.57,8.32,8.32,0,0,0,2.24-6.17A8.23,8.23,0,0,0,25,8.12a7.57,7.57,0,0,0-1.82-2.53,9.17,9.17,0,0,0-3.4-2,15.57,15.57,0,0,0-5-.71q-2,0-3.73.17a29,29,0,0,0-3.12.47V25.15H2.67v2.31H7.94v6.46h2.41V27.46h9.56V25.15H10.36v-4.3A8.61,8.61,0,0,0,12.14,21.1ZM10.36,5.2A16.87,16.87,0,0,1,12.09,5q1.12-.11,2.61-.11a13.9,13.9,0,0,1,3.47.41,7.44,7.44,0,0,1,2.7,1.26A5.79,5.79,0,0,1,22.6,8.69a7.37,7.37,0,0,1,.61,3.15,6.71,6.71,0,0,1-2.33,5.51,10,10,0,0,1-6.54,1.91q-1.14,0-2.17-.09a6.68,6.68,0,0,1-1.82-.39Z"/>
    </symbol>
    <symbol id="arrow-svg" viewBox="0 0 40 15">
        <title>Arrow</title>
        <path d="M37.32,7h0L32.37,2.6v.9l3.48,3.1H2.34v1h33.5l-3.47,3.08a2.91,2.91,0,0,0,0,.86S37.3,7.18,37.3,7.18A.13.13,0,0,0,37.32,7Z"/>
    </symbol>
</svg>
<script>
	// finance scripting
	var _controls = document.querySelectorAll('.finance-features-control'),
		_controlbar = document.querySelector('.unstatic-finance-features-controls'),
		_track = document.querySelector('.unstatic-finance-features .finance-features-content'),
		_items = document.querySelectorAll('.unstatic-finance-features .finance-feature'),
		_length = _items.length,
		_perview = 4,
		_value = 0,
		_idx = 0,
		_featureList = document.querySelectorAll('.finance-feature'),
		_mobileActivityClass = 'is-mobile-active',
		_mobilebp = '740px',
		_isMobileVW,
		_links = document.querySelectorAll('.breed-screen-link'),
		_xContentIds = [],
		_xContentClass = '.finance-x-cont';
	_xContentActivityClass = 'is-current';

	for (var i=0;i<_links.length;++i) {
		_links[i].addEventListener('click', toggleXcontent)
		_xContentIds.push(_links[i].getAttribute('href'))
	}
	for (var i=0;i<_controls.length;++i) {
		_controls[i].addEventListener('click', reslide)
	}
	for (var i=0;i<_featureList.length;++i) {
		_featureList[i].addEventListener('click', toggleMobileActive)
	}
	window.addEventListener('resize', checkViewport)
	setTimeout(function(){
		toggleXcontent()
		checkViewport()
	}, 250)

	function toggleXcontent(ev) {
		var _newContentId;

		if (ev && ev.type !== 'DOMContentLoaded') {
			ev.preventDefault();
			_newContentId = this.getAttribute('href');
		} else {
			_newContentId = window.location.hash;
		}

		if (_newContentId === void 0 || _newContentId.length < 2) {
			return false
		} else {
			for (var i=0;i<_xContentIds.length;++i) {
				if (_xContentIds[i] === _newContentId) {
					var _$currentTarget = $(_newContentId), _$allTargets = $(_xContentClass);

					if (_$currentTarget.length) {
						if (!this.classList || (this.classList && !this.classList.contains(_xContentActivityClass))) {
							for (var i=0;i<_links.length;++i) {
								if (_links[i].getAttribute('href') === _newContentId) {
									_links[i].classList.add(_xContentActivityClass)
								} else {
									_links[i].classList.remove(_xContentActivityClass)
								}
							}
						}
						$('html, body').stop().animate({scrollTop: ($('.section--finance-x-cont').offset().top - $('.layout__header').height())}, function(){
							if (!_$currentTarget.hasClass(_xContentActivityClass)) {
								_$allTargets.filter('.'+_xContentActivityClass).fadeOut(250, function(){
									$(this).removeClass(_xContentActivityClass);
									_$currentTarget.fadeIn(250, function(){
										$(this).addClass(_xContentActivityClass);
									})
								})
							}
						})
					}
				}
			}
		}
	}
	function checkViewport() {
		if (_idx !== 0) {
			_value = 0
			_track.removeAttribute('style')
			_controlbar.setAttribute('data-index', 0)
			_idx = 0
		}
		if ((window.matchMedia && window.matchMedia('(max-width: '+_mobilebp+')').matches) || (window.innerWidth < 641)) {
			_isMobileVW = true;
		} else {
			for (var i=0;i<_featureList.length;++i) {
				_featureList[i].classList.remove(_mobileActivityClass)
			}
			_isMobileVW = false;
		}
		return _isMobileVW;
	}
	function reslide(ev){
		ev.preventDefault();

		if (this.getAttribute('data-role') === 'prev' && _controlbar.getAttribute('data-index') > 0) {
			--_idx;
		} else if (this.getAttribute('data-role') === 'next' && _controlbar.getAttribute('data-index') < (_length - _perview)) {
			++_idx;
		}

		_value = _items[0].getBoundingClientRect().width * -_idx;
		_controlbar.setAttribute('data-index', _idx)

		$(_track).css({
			'-ms-transform':		'translate('+_value+'px, 0)',
			'-moz-transform':		'translate('+_value+'px, 0)',
			'-webkit-transform':	'translate('+_value+'px, 0)',
			'transform':			'translate('+_value+'px, 0)',
		});
	}
	function toggleMobileActive(ev) {
		ev.preventDefault();
		if (_isMobileVW) {
			if (this.classList.contains(_mobileActivityClass)) {
				this.classList.toggle(_mobileActivityClass);
			} else {
				for (var i=0;i<_featureList.length;++i) {
					_featureList[i].classList.remove(_mobileActivityClass)
				}
				this.classList.toggle(_mobileActivityClass);
			}
		}
	}
</script>

<?/*
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/vendor.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/360view.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/easings.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/device.min.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/anim.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/anim1.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/aos.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/aosSettings.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/initAnim.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/app.js?ver=1.2"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/slick.min.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/magnific-popup.min.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/js/jquery.steps.min.js"></script>

<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/scripts/header/common.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/scripts/header/App.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/scripts/header/App.navigation.js"></script>
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>/scripts/header/App.navigation-mob.js"></script>

*/?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>