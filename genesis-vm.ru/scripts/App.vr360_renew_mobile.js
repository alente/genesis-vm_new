/*
* "GreenSock | TweenMax"(http://greensock.com/tweenmax) with jQuery.
* by @psyonline (http://www.psyonline.kr/, majorartist@gmail.com)
* https://github.com/psyonline/jquery-with-gsap
* License - http://creativecommons.org/licenses/by-sa/2.0/kr/
*/
;(function(h){function g(d,b){return function(c){b.call(d,c)}}var l=h.isPlainObject,m=CSSPlugin._internals.getTransform,k=["{self}"];h.fn._css=function(d,b){var c;if(l(d))TweenMax.set(this,d);else if(void 0!==b)c={},c[d]=b,TweenMax.set(this,c);else{if(-1!=="scale,scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,rotationZ,perspective,xPercent,yPercent,zOrigin,".indexOf(d+",")){var e=this[0];c=d;e=e._gsTransform||m(e);"rotationZ"==c?c="rotation":"scale"==c&&(c="scaleX");return e[c]}return this.css(d)}return this};
h.fn._animate=function(d,b,c,e){var a={},f;for(f in d)a[f]=d[f];if(l(b))for(f in b)a[f]=b[f];else"function"==typeof e?(a.duration=b,a.easing=c,a.complete=e):"function"==typeof c?("number"==typeof b?a.duration=b:a.easing=b,a.complete=c):"number"==typeof b?a.duration=b:"string"==typeof b?a.easing=b:"function"==typeof b&&(a.complete=b);a.duration=(void 0!==a.duration?a.duration:400)/1E3;void 0!==a.delay&&(a.delay/=1E3);void 0!==a.repeatDelay&&(a.repeatDelay/=1E3);a.start&&(a.onStart=g(this,a.start),
a.onStartParams=k,delete a.start);if(a.step||a.progress)a.onUpdate=g(this,a.step||a.progress),a.onUpdateParams=k,delete a.step,delete a.progress;a.repeatStep&&(a.onRepeat=g(this,a.repeatStep),a.onRepeatParams=k,delete a.repeatStep);a.complete&&(a.onComplete=g(this,a.complete),a.onCompleteParams=k,delete a.complete);a.easing&&(a.ease=a.easing,delete a.easing);delete a.queue;d=a.duration;delete a.duration;this.data("TweenMax",TweenMax.to(this,d,a));return this};h.fn._stop=function(d,b){var c=this.data("TweenMax");
c&&c.kill(d,b);return this};(function(){function d(a){return function(b){return a.getRatio(b)}}var b,c,e,a,f,g;if(h.easing&&window.GreenSockGlobals&&window.GreenSockGlobals.Ease&&window.GreenSockGlobals.Ease.map)for(b="Quad Cubic Quart Quint Sine Expo Circ Elastic Back Bounce".split(" "),c=["In","Out","InOut"],e=window.GreenSockGlobals.Ease.map,f=0;f<b.length;f++)for(g=0;g<c.length;g++)a="ease"+c[g]+b[f],e[a]&&!h.easing[a]&&(h.easing[a]=d(e[a]))})()})(window.jQuery);

// handle multiple browsers for requestAnimationFrame()
window.raf = (function () {return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || function (callback) {return window.setTimeout(callback, 1000 / 60); }; })();

// handle multiple browsers for cancelAnimationFrame()
window.caf = (function () {return window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || function (id) {window.clearTimeout(id); }; })();


/*! App.vr360.js */
;(function(window, $, undefined) {
	var $window   = $(window);
	var $document = $(document);

	var exteriorCanvas = (function() {
		var CANVAS_SUPPORT = Modernizr.canvas;

		var _$canvas      = CANVAS_SUPPORT ? $('<canvas></canvas>') : $('<div></div>');
		var _canvas       = _$canvas[0];
		var _$container   = $('#exteriorCanvas'); // 캔버스 생성될 부모영역.

		var _$dragAnimate = $({idx:0});		//canvas
		var _canvasDragPlay = false;		//canvas touch
		var _startDragPoint = {};			//canvas touch변수

		// var _countCheck   = 0; // 이미지 로드 체크용
		var _currentIndex = 0; // 현재 선택 이미지번호
		var _datas = [];
		var _currentInstance = {};

		var _canvasWidth;
		var _canvasHeight;

		var _svgLoadingEnd;
		var _interval = undefined;
		var _touchFirst = undefined;

		var _init = function(target) {
			_$container = target ? $(target):_$container;
			_$container.append( _$canvas );

			$(App.Events).on( App.Events.RESIZE_BROWSER, function() {
				_resize();
			});
		}

		var _setup = function(data) {
			_drawReset();

			if ( _datas[data.name] && _datas[data.name].isLoaded ) {
				$('.vr-loading-bar').remove();
                _interval && caf(_interval);

				_currentInstance = _datas[data.name];
				_currentInstance.callback();
				_resize();
			} else {
				var oStillImage = new Image();

				// 스틸컷으로 대체.
				if ( data.stillcut ) {
					oStillImage.src = data.stillcut;
					_setSvgCricle();
					imageLoadCheck(oStillImage, function() {
						_resize(oStillImage);
					});
				}
				_datas[data.name] = {
					name     : data.name,
					isLoaded : false,
					imgLens  : data.images.length,
					images   : data.images,
					oImages  : [],
					stillImage : oStillImage,
					callback : data.callback || function() {}
				}

				_currentInstance = _datas[data.name];
				imageLoadStart( data.name );
			}
		}

		var _setSvgCricle = function() {
			var SVG_SIZE   = 54;
			var SVG_BORDER = 2;
			var SVG_CLASSNAME = 'vr-loading-bar';
			var $svg;
			var $circle;

			var _duration = 1200;

			var init = function() {
				$('.'+SVG_CLASSNAME).remove();
				_svgLoadingEnd = false;
				_interval && caf(_interval);
				_createSvg();
			}

			var _createSvg = function() {
				var s = '';
					s += '<div class="'+SVG_CLASSNAME+'">\n';
					s += '\t<svg viewBox="0 0 [SVG_SIZE] [SVG_SIZE]" height="[SVG_SIZE]" width="[SVG_SIZE]">\n';
					s += '\t\t<circle class="circle1" cx="[SVG_HALF_SIZE]" cy="[SVG_HALF_SIZE]" r="[SVG_RATIO_SIZE]"></circle>\n';
					s += '\t\t<circle class="circle2" cx="[SVG_HALF_SIZE]" cy="[SVG_HALF_SIZE]" r="[SVG_RATIO_SIZE]"></circle>\n';
					s += '\t</svg>\n';
					s += '</div>';

				s = s.replace(/\[SVG_SIZE\]/g, SVG_SIZE);
				s = s.replace(/\[SVG_HALF_SIZE\]/g, (SVG_SIZE/2));
				s = s.replace(/\[SVG_RATIO_SIZE\]/g, (SVG_SIZE/2 - SVG_BORDER));

				$('.m-module-vr360 > article.feature').append( s );

				$svg = $('.m-module-vr360>article.feature>.'+SVG_CLASSNAME);
				$('.m-module-vr360 .exterior-area .vr-ctrl').show();
				$svg.css({'top': $('.m-module-vr360 .exterior-area .vr-ctrl').position().top});
				$circle = $svg.find('.circle1');

				_svgPlay();
			}
			var _svgPlay = function() {
				_interval && caf(_interval);
				$circle[0].style.strokeDashoffset = 0;
				$svg.show();

				var currentTime = +new Date;
				var finalTime = +new Date + _duration;
				var percent;

				var _setAnimation = function() {
					currentTime = +new Date;
					if ( !_svgLoadingEnd && currentTime <= finalTime) {
						percent = (finalTime - currentTime) / _duration;
						$circle[0].style.strokeDashoffset = SVG_SIZE * Math.PI * percent;
						_interval = raf(_setAnimation);
						return;
					} else {
						$circle[0].style.strokeDashoffset = 0;
						_circleEnd();
					}
				}

				_interval = raf(_setAnimation);
				$circle.addClass('on');
			}
			var _circleEnd = function() {
				_interval && caf(_interval);
				$circle[0].style.strokeDashoffset = 0;
				$('.'+SVG_CLASSNAME).remove();
				_svgLoadingEnd = true;
				imageLoadEnd(_currentInstance.name);
			}

			init();
		}
		var imageLoadStart = function( dataName ) {
			var _instance = _datas[dataName];
			_instance.countCheck = 0;
			for (var i=0; i< _instance.imgLens; i++) {
				_instance.oImages[i] = new Image();
				_instance.oImages[i].src = _instance.images[i];
				imageLoad(_instance.oImages[i], dataName);
			}
		}
		var imageLoad = function(image, dataName) {
			var _instance = _datas[dataName];
			var checkLoad = function() {
				_instance.countCheck =_instance.countCheck+1;
				if ( _instance.countCheck == _instance.imgLens ) {
					_instance.isLoaded = true;
					imageLoadEnd(dataName);
				}
			}
			imageLoadCheck(image, checkLoad);
		}
		var imageLoadCheck = function(image, callback) {
			var $image = $(image);
			if (image.complete) {
				$image.off('load', callback);
				callback.call(image);
			} else {
				if (image.src==image.src) {
					if (image.complete) {
						$image.off('load', callback);
						callback.call(image);
					} else {
						$image.off('load', callback).on('load', callback);
					}
				} else {
					$image.off('load', callback);
					image.src = image.src;
				}
			}
		}

		var imageLoadEnd = function(dataName) {
			var _instance = _datas[dataName];
			if ( _instance.name == _currentInstance.name
					&& _instance.isLoaded
					&& _svgLoadingEnd ) {
				_resize();
				_canvasAddEvent();
				_currentInstance.callback();
				// _drawImages(); 리자이즈에 포함.
			}
		}
		var _resize = function(stillcut) {
			// 2017-09-15 update: _$container.is(':visible') 추가.
			if ( _$container.is(':visible') && (_currentInstance.isLoaded || stillcut) ) {
				var tmpImage = ( stillcut ) ? stillcut : _currentInstance.oImages[0];
				var size = {
					w: Math.min(_$container.width() + 380, 1536),
					h: tmpImage.height
				}
				size.h = size.h * (size.w/tmpImage.width);

				var canvasHeight = size.h/2;
				var contHeight = canvasHeight;
				if ($('.brand-footer:visible').length) {
					canvasHeight -= 30;
					contHeight += 278; //$('.brand-footer:visible').innerHeight();
				}

				$('.m-module-vr360').height( contHeight );
				$('.m-module-vr360 .brand-info').length && $('.m-module-vr360 .brand-info').css('top', canvasHeight - 20);

				_canvasWidth  = size.w;
				_canvasHeight = size.h;

				if ( stillcut != null ) {
					_canvasDrawImage(stillcut);
				} else {
					_drawImages();
				}
			}
		}
		var _drawImages = function(no) {
			_currentIndex = no || _currentIndex;
			_canvasDrawImage( _currentInstance.oImages[_currentIndex] );
		}
		var _canvasDrawImage = function( objImage ) {
			if ( CANVAS_SUPPORT ) {
				var ctx = _canvas.getContext('2d');
				_canvas.width  = _canvasWidth;
				_canvas.height = _canvasHeight;
				_canvas.style.width  = _canvasWidth;
				_canvas.style.height = _canvasHeight;
				ctx.drawImage(objImage,0,0,_canvasWidth,_canvasHeight);
			} else {
				_canvas.style.width  = _canvasWidth;
				_canvas.style.height = _canvasHeight;
				_canvas.style.background = 'url('+objImage.src+')';
			}
		}

		var _getInstance = function() {
			return _currentInstance;
		}

		var _drawReset = function() {
			_$dragAnimate._stop();
			_currentIndex = 0;
			_canvasRemoveEvent(); // canvas Drag Event Remove
		}

		/*! -- canvas drag Event -- */
		var _canvasAddEvent = function() {
			_$canvas.on('mousedown.cvDragEvent touchstart.cvDragEvent', _dragStart);
			_$canvas.on('mousemove.cvDragEvent touchmove.cvDragEvent', _dragMove);
			$(document).on('mouseup.cvDragEvent touchend.cvDragEvent', _dragEnd);
		}
		var _canvasRemoveEvent = function() {
			_$canvas.on('.cvDragEvent');
			$(document).on('.cvDragEvent');
		}
		var _touchPoint = function(e) {
			e = e.originalEvent;
			if (e.touches || e.changedTouches) {
				return {
					x:e.touches[0] ? e.touches[0].pageX : e.changedTouches[0].pageX,
					y:e.touches[0] ? e.touches[0].pageY : e.changedTouches[0].pageY,
					t:+new Date
				};
			}
			return {x:e.pageX, y:e.pageY, t:+new Date};
		}
		var _circleIndex = function(idx) {
			var _imgLens = _currentInstance.imgLens;
			return (_imgLens + (idx % _imgLens)) % _imgLens;
		}

		var _dragStart = function(e) {
			if ( _currentInstance.isLoaded && _svgLoadingEnd) {
				// e.preventDefault();

				_$dragAnimate._stop();

				_canvasDragPlay = true;
				_touchFirst = undefined;
				_startDragPoint = _touchPoint(e);
				_deltaPoint     = _$dragAnimate[0].idx;
			}
		}
		var _dragMove = function(e) {
			var dragsOpts = {queue:false, duration:850, rounding:false, easing:'easeOutQuint', step: _dragMoving, complete: _dragAniComplete};
			var offset,delta,toIdx;

			if ( _canvasDragPlay && _currentInstance.isLoaded ) {
				var offset = _touchPoint(e);
				var delta = {
					x: offset.x - _startDragPoint.x,
					y: offset.y - _startDragPoint.y
				}

				if ( _touchFirst === undefined ) {
					_touchFirst = !!(Math.abs(delta.x) > Math.abs(delta.y));
				}

				if ( _touchFirst ) {
					_$dragAnimate._stop();
					toIdx = Math.round(delta.x/14) + _deltaPoint;
					_$dragAnimate._animate({idx: toIdx}, dragsOpts);

					e.preventDefault();
				}
			}
		}

		var _dragMoving = function() {
			var deltaX = _$dragAnimate[0].idx;
			_currentIndex = _circleIndex(Math.round(deltaX));
			_drawImages(_currentIndex);
		}
		var _dragAniComplete = function() {
			_currentIndex = _circleIndex(Math.round(_$dragAnimate[0].idx));
		}
		var _dragEnd = function(e) {
			if ( _canvasDragPlay ) {
				_canvasDragPlay = false;
				// e.preventDefault();
			}
		}


		/*! -- canvas drag EventEnd -- */
		return {
			init    : _init,
			setup   : _setup,
			resize  : _resize,
			getData : _getInstance
		}
	})();

	var colorSelect = function(el) {
		var _this = this;

		if ( el.colorChip ) return false;
		el.colorChip = true;

		var $element   = $(el);
		var $btnPrev   = $element.find("button.btn-prev");
		var $btnNext   = $element.find("button.btn-next");
		var $container = $element.find(".slide-wrap");
		var $slides    = $container.find('>ul');
		var $slideList = $slides.find('>li');
		var MARGIN_SIZE = 0;
		var IMAGE_WIDTH = 60;

		var _slide_width      = $slideList.width();
		var _slide_lens       = $slideList.length;
		var _slide_total_size = _slide_width * _slide_lens;
		var _slide_scroll_max = 0;

		var isAnimation = false;

		this.init = function() {
			_this.addEvent();
			_this.load();
		}
		this.addEvent = function() {
			$btnPrev.on('click', _this.prev);
			$btnNext.on('click', _this.next);
			$(App.Events).on( App.Events.RESIZE_BROWSER, _this.resize );

		}
		this.load = function() {
			$slideList        = $slides.find('>li');
			_slide_width      = IMAGE_WIDTH+MARGIN_SIZE;
			_slide_lens       = $slideList.length;
			_slide_total_size = _slide_width * _slide_lens;
			_slide_scroll_max = _slide_total_size - $container.width();

			$slides.width( _slide_total_size );
			_this.animateEnd();
			_this.resize();

		}
		this.show = function() {
			$element.removeClass('slide-none');
			_this.screenMode = true;
			$container.scrollLeft(0);
		}
		this.hide = function() {
			$element.addClass('slide-none');
			_this.screenMode = false;
			$container.scrollLeft(0);
		}
		this.resize = function() {
			if ( $element.width() < _slide_total_size ) {
				_this.show();
			} else {
				_this.hide();
			}
		}
		this.prev = function() {
			if ( !isAnimation ) {
				isAnimation = true;
				var offsetLeft = Math.max($container.scrollLeft() - _slide_width, 0);
				$container._animate({'scrollLeft': offsetLeft}, {duration:400, complete:_this.animateEnd});
			}
		}
		this.next = function() {
			if ( !isAnimation ) {
				isAnimation = true;
				var offsetLeft = Math.min($container.scrollLeft() + _slide_width, _slide_scroll_max);
				$container._animate({'scrollLeft': offsetLeft}, {duration:400, complete:_this.animateEnd});
			}
		}
		this.animateEnd = function() {
			var offsetLeft = $container.scrollLeft();
			if ( offsetLeft == 0 ) {
				$btnPrev.addClass('disabled');
				$btnNext.removeClass('disabled');
			} else if ( offsetLeft == _slide_scroll_max ) {
				$btnNext.addClass('disabled');
				$btnPrev.removeClass('disabled');
			} else {
				$btnNext.removeClass('disabled');
				$btnPrev.removeClass('disabled');
			}
			isAnimation = false;
		}
		this.init();
	};

	$(function() {
		App.brand = App.brand || {};
		App.brand.vr360 = exteriorCanvas;
		App.brand.vr360.init('#exteriorCanvas');
		App.brand.vr360.colorChip = [];

		var colorChipLoad = function() {
			$('.m-module-vr360 .colors-cont>div').each(function(i) {
				var o = new colorSelect(this);
				App.brand.vr360.colorChip[i] = o;
				this.slideColorChip = o;
				$(this).data('slideColorChip', App.brand.vr360.colorChip[i]);
			});
		}

		colorChipLoad();

	});
}(window, jQuery));