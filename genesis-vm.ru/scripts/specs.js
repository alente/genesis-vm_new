//------------------------------------------------------------------
//  last update: 2017. 08. 25
//------------------------------------------------------------------

    var specs = {
        model :'',
        detail_yn : 'N',
        spec_one:'',
        spec_two:'',
        spec_three:'',
        tabarray:['keyspecs'
                ,'powertrain'
                ,'exterior'
                ,'interior'
                ,'safety'
                ,'convenience'
                ,'seats'
                ,'multimedia'
                ,'option'],
        tabarrayKr:['제원'
                ,'파워트레인'
                ,'외관'
                ,'내장'
                ,'안전'
                ,'편의'
                ,'시트'
                ,'멀티미디어'
                ,'옵션']
    };

    function initData( locale){     

        var langlocale = "KR-"+locale;
        
        var dataDir = "data";
        
        //변수화할 것.
        var year = '2016';

        if(locale=='en'){
            specs.tabarrayKr = ['KEY SPECS'
                ,'POWERTRAIN'
                ,'EXTERIOR'
                ,'INTERIOR'
                ,'SAFETY'
                ,'CONVENIENCE'
                ,'SEATS'
                ,'MULTIMEDIA'
                ,'OPTIONS'];
        }

        if( "G80" == specs.model.toUpperCase() || "G70" == specs.model.toUpperCase() || "G80SPORT" == specs.model.toUpperCase() ){
            specs.tabarrayKr = ['제원'
                ,'파워트레인'
                ,'외관'
                ,'내장'
                ,'안전'
                ,'편의'
                ,'시트'
                ,'멀티미디어'
                ,'개별 옵션'
                ,'패키지 옵션'];
            if(locale=='en'){
                specs.tabarrayKr = ['KEY SPECS'
                    ,'POWERTRAIN'
                    ,'EXTERIOR'
                    ,'INTERIOR'
                    ,'SAFETY'
                    ,'CONVENIENCE'
                    ,'SEATS'
                    ,'MULTIMEDIA'
                    ,'OPTIONS'
                    ,'PACKAGE OPTIONS'];
            }
            specs.tabarray = ['keyspecs'
                ,'powertrain'
                ,'exterior'
                ,'interior'
                ,'safety'
                ,'convenience'
                ,'seats'
                ,'multimedia'
                ,'option'
                ,'packageoption'];
            $.ajax({                
                url:"/content/genesis/kr/ko/func/get_datainfo/_jcr_content.specs.post.html?vehicleCd="+specs.model.toUpperCase()+"&year="+year+"&localeCd="+langlocale,
                 type:"POST",
                success:callTrimMap
            });  
        }else if( "EQ900" == specs.model ){
            $.ajax({                
                url:"/content/genesis/kr/ko/func/get_datainfo/_jcr_content.specs.post.html?vehicleCd=EQ900&year="+year+"&localeCd="+langlocale,
                 type:"POST",
                success:callTrimMap
            });
        }else{
            ComAlert.alert( specs.model +  " 해당 모델의 데이터를 찾을수 없습니다1.");
        }
    }

    function callTrimMap( data ){
        console.log('call callTrimMap');  
        console.log(data);       
        specs.json = data;   
        var locale = _locale;
        if( locale == undefined ){ locale = "ko"; }
        var dataDir = "data";
        

        if( "G80" == specs.model.toUpperCase()  || "G80SPORT" == specs.model.toUpperCase() ){
            $.ajax({
                //url:"/etc/designs/genesis/kr/" + dataDir + "/g80_" + locale + ".json",
                url:"/content/genesis/kr/ko/func/get_datainfo/_jcr_content.build_price.post.html?vehicleCd=G80&localeCd="+locale,
                 type:"POST",
                success:fnAjaxCallback
            });  
        }else if( "G70" == specs.model ){
            $.ajax({                
                url:"/content/genesis/kr/ko/func/get_datainfo/_jcr_content.build_price.post.html?vehicleCd=G70&localeCd="+locale,
                 type:"POST",
                success:fnAjaxCallback
            });
        }else if( "EQ900" == specs.model ){
            $.ajax({
                //url:"/etc/designs/genesis/kr/" + dataDir + "/eq900_" + locale + ".json",
                url:"/content/genesis/kr/ko/func/get_datainfo/_jcr_content.build_price.post.html?vehicleCd=EQ900&localeCd="+locale,
                 type:"POST",
                success:fnAjaxCallback
            });
        }else{
            ComAlert.alert( specs.model +  " 해당 모델의 데이터를 찾을수 없습니다2.");
        }
    }

    function chkSelectedItem(){
        //$('input:checkbox').eq(0).prop("checked");
        var count = 0;
        var this_trims= [];
        for(var i=0 ; i< $('input:checkbox').length ; i++){
            if($('input:checkbox').eq(i).prop("checked")){
                count++;

                if(count>3){
                    break;
                }else{
                    this_trims.push($('input:checkbox').eq(i).data('code'));
                }
            }
        }

        if(count==0){
            alert('최소 1개이상의 트림을 선택하시기 바랍니다.');
        }else if(count>3){
            alert('최대 3개이하의 트림만 선택 가능합니다.');
        }else{
            console.log(this_trims);
            var linkstr = '?';
            if(this_trims.length>0){
                linkstr += 'so='+this_trims[0];
            }

            if(this_trims.length>1){
                linkstr += '&st='+this_trims[1];
            }

            if(this_trims.length>2){
                linkstr += '&sth='+this_trims[2];
            }
            var model = specs.model.toLowerCase();
            if(model.indexOf('sport')!=-1){
                model = 'g80-sport'
                linkstr = linkstr.replace("S1",'S');
            }

            if(specs.svflag=='true'){
                location.href='/kr/'+_locale+'/luxury-sedan-genesis-'+model+'-specifications-detail.html'+linkstr;
            }else{
                location.href='/content/genesis/kr/'+_locale+'/luxury-sedan-genesis-'+model+'-specifications-detail.html'+linkstr;    
            }

            
        }

    }

    //데이터 수급 및 기본 화면 세팅
    function fnAjaxCallback(data, args) {
        specs.trimjson = data;        
        //console.log('specs.trimjson');
        //console.log(specs.json);
        
        //스펙 디테일 화면인경우
        if(specs.detail_yn=='Y' && specs.print_pop_yn != 'Y'){
            initTable(specs.json.spec_data_standard);
            initTrimSelect(specs.trimjson.mapping.trimmap);
            initTrimData();

            var model = specs.model.toLowerCase();            
            if(model.indexOf('sport')!=-1){            
                //g80 sports 의 경우 다른 탭을 삭제 
                $('.specs-inner .tbl-header table tr td.dim').remove();
                $('.specs-inner .tbl-header table colgroup').html('<col width="25%"><col width="*">');
                $('.tbl-content colgroup').html('<col width="25%"><col width="*">');
                
            }

            if(specs.print_yn=='Y'){
                $('.tab-menu ul li').eq(specs.cur_tabindex).trigger('click');

                setTimeout(function() {
                    document.execCommand('print', false, null) || window.print();
                    window.close();
                }, 450);

            }
        }else if(specs.print_pop_yn=='Y'){
            makePopTable();

        }else{
            //스펙 트림 선택 화면인 경우
            var htmlstr = '';
            
            $('.select-category ul').eq(0).html('');
            var checknumber = 0;
            for(var i=0 ; i<specs.json.mapping.trimgroup.length ; i++ ){
                htmlstr += '<li>';
                htmlstr += '<strong>'+specs.json.mapping.trimgroup[i].group_name+'</strong>';
                htmlstr += '<ul>';
                for(var j=0 ; j<specs.json.mapping.trimgroup[i].member.length ; j++ ){
                    checknumber++;
                    var members = specs.json.mapping.trimgroup[i].member[j];
                    htmlstr += '<li>';
                    htmlstr += '<input type="checkbox" id="check0'+checknumber+'" data-code="'+members.trim_code+'" />';
                    htmlstr += '<label for="check0'+checknumber+'">'+members.trim_name+'</label>';
                    htmlstr += '</li>';                    
                }
                htmlstr += '</ul>';
                htmlstr += '</li>';                                    
            }
            $('.select-category ul').eq(0).html(htmlstr);
        }
    }


    function makePopTable(){
        console.log('eeeeeeeeeeeeeeeeeeeeeeeee');
        var htmlstr = '<li>';

        var this_spec = specs.json.trim[specs.spec_one].spec_data;
        var this_spec = specs.json.trim[specs.spec_one].spec_data;

        for(var i=0 ; i<specs.tabarray.length ; i++ ){
            var this_spec_data = this_spec[specs.tabarray[i]];
            

            htmlstr += '<h3 class="sub-title">'+specs.tabarrayKr[i]+'</h3>';
            htmlstr += '<table>';
            htmlstr += '<caption></caption>';
            htmlstr += '<colgroup>';
            htmlstr += '<col width="*"/>';
            htmlstr += '<col width="*"/>';
            htmlstr += '</colgroup>';
            htmlstr += '<tbody>';

            for(var j=0 ; j<this_spec_data.length ; j++ ){
                htmlstr += '<tr>';
                htmlstr += '<th>'+this_spec_data[j].title+'</th>';
                htmlstr += '<td>'+this_spec_data[j].value+'</td>';
                htmlstr += '</tr>';
            }
            
            htmlstr += '</tbody>';
            htmlstr += '</table>';
            htmlstr += '</li>';

        }
        $('#specs-pop01 ul').html(htmlstr);

        for(var i=0 ; i<specs.trimjson.mapping.trimmap.length ; i++){
            var trim = specs.trimjson.mapping.trimmap[i];
            if(trim.trim_code==specs.spec_one){
                $('#specs-pop01 h2').html(trim.name);
                break;
            }
        }
        
        setTimeout(function() {
            document.execCommand('print', false, null) || window.print();
            window.close();
        }, 450);
    }

    function setTrimData(index,trim_code){
        //index : 3가지 컬럼중 몇번째인지
        //count : select 박스 아이템중 몇번째인지
        //console.log(index);

        if(trim_code!='no'){
             console.log("index : "+index);
            if(index==0){
                specs.spec_one = trim_code;
            }else if(index==1){
                specs.spec_two = trim_code;
            }else if(index==2){
                specs.spec_three = trim_code;                
            }

            var data = specs.json.trim[trim_code].spec_data;
            console.log(data);
            var htmlstr = '';
            $('#specs-pop0'+(index+1)+' ul').html('');
            for(var i=0 ; i<specs.tabarray.length ;  i++){
                //console.log("=="+i+"==");
				//console.log(specs.tabarray[i]);
                //console.log(data[specs.tabarray[i]]);
                var this_data = data[specs.tabarray[i]];

                var this_spec_st = specs.json.spec_data_standard[specs.tabarray[i]];
                console.log(this_spec_st);
                var tabKr = specs.tabarrayKr[i];
                htmlstr +='<li>';
                htmlstr +='<h3 class="sub-title">'+tabKr+'</h3>';
                htmlstr +='<table><caption></caption><colgroup><col width="*"/><col width="*"/></colgroup><tbody>';
                //console.log($('.tbl-content').eq(i).find('tbody tr').length);
                for(var j=0 ; j<$('.tbl-content').eq(i).find('tbody tr').length ; j++){
                    $('.tbl-content').eq(i).find('tbody tr').eq(j).find('td').eq(index).html(this_data[j].value);
                    htmlstr += '<tr>';
                    htmlstr += '<th>'+this_spec_st[j].title+'</th>';
                    htmlstr += '<td>'+this_data[j].value+'</td>';
                    htmlstr += '</tr>';
                }

                htmlstr +='</tbody>';
                htmlstr +='</table>';
                htmlstr +='</li>';
            }

            $('#specs-pop0'+(index+1)+' ul').html(htmlstr);
            //console.log(htmlstr);
            $('.sppop'+(index+1)).removeClass('disabled');

            for(var i=0 ; i<specs.trimjson.mapping.trimmap.length ; i++){
                var trim = specs.trimjson.mapping.trimmap[i];
                if(trim.trim_code==trim_code){
                    $('#specs-pop0'+(index+1)+' h2').html(trim.name);
                    console.log('abdfwsdf');
                    $('#specs-pop0'+(index+1)+' .btn-box a').on('click', function(e) {

                        var printPageUrl = $(this).attr('href')+"?so="+trim_code;
                        var printWindow = window.open(printPageUrl, 'printWindow', '');
                        e.preventDefault();
                    });
                    break;
                }
            }

        }else{
            
            //console.log(data);
            var htmlstr = '';
            $('#specs-pop0'+(index+1)+' ul').html('');
            for(var i=0 ; i<specs.tabarray.length ;  i++){            
                var tabKr = specs.tabarrayKr[i];
                
                htmlstr +='<li>';
                htmlstr +='<h3 class="sub-title">'+tabKr+'</h3>';
                htmlstr +='<table><caption></caption><colgroup><col width="*"/><col width="*"/></colgroup><tbody>';
                for(var j=0 ; j<$('.tbl-content').eq(i).find('tbody tr').length ; j++){
                    $('.tbl-content').eq(i).find('tbody tr').eq(j).find('td').eq(index).html('');
                    htmlstr += '<tr>';
                    htmlstr += '<th></th>';
                    htmlstr += '<td></td>';
                    htmlstr += '</tr>';
                }

                htmlstr +='</tbody>';
                htmlstr +='</table>';
                htmlstr +='</li>';
            }

            $('#specs-pop0'+(index+1)+' ul').html(htmlstr);
            //console.log(htmlstr);
            $('.sppop'+(index+1)).addClass('disabled');
        }
    }

    function allSpecPopup(val){
        if($('.sppop'+val).hasClass('disabled')){
        }else{
            toApp_showPopup('#specs-pop0'+val);
        }
        

    }

    function initTrimData(){
        
        if(specs.spec_one!=''){
            //$('.car-model').eq(0).find('.select-box .select-lst span a').eq(7).trigger('click');
            var length = $('.car-model').eq(0).find('.select-box .select-lst span a').length;
            for(var i=0 ; i<length ; i++){
                //console.log($('.car-model').eq(0).find('.select-box .select-lst span a').eq(i).data('code')+" : "+specs.spec_one);
                if($('.car-model').eq(0).find('.select-box .select-lst span a').eq(i).data('code') == specs.spec_one){
                    $('.car-model').eq(0).find('.select-box .select-lst span a').eq(i).trigger('click');
                    break;
                }                
            }
        }

        if(specs.spec_two!=''){
            //$('.car-model').eq(1).find('.select-box .select-lst span a').eq(7).trigger('click');
            var length = $('.car-model').eq(1).find('.select-box .select-lst span a').length;
            for(var i=0 ; i<length ; i++){
                if($('.car-model').eq(1).find('.select-box .select-lst span a').eq(i).data('code') == specs.spec_two){
                    $('.car-model').eq(1).find('.select-box .select-lst span a').eq(i).trigger('click');
                    break;
                }                
            }
        }

        if(specs.spec_three!=''){
            //$('.car-model').eq(2).find('.select-box .select-lst span a').eq(7).trigger('click');
            var length = $('.car-model').eq(2).find('.select-box .select-lst span a').length;
            for(var i=0 ; i<length ; i++){
                if($('.car-model').eq(2).find('.select-box .select-lst span a').eq(i).data('code') == specs.spec_three){
                    $('.car-model').eq(2).find('.select-box .select-lst span a').eq(i).trigger('click');
                    break;
                }                
            }
        }
    }
  
    //기본 트림 셀렉트 세팅
    function initTrimSelect(trimmap){

        
        var model = specs.model.toLowerCase();
        var car_model_length = $('.car-model').length;
        if(model.indexOf('sport')!=-1){            
            car_model_length = 1;
        }
        console.log('this : trimmap');
        console.log(trimmap);


        for(var i=0 ; i<car_model_length ; i++){

            

            $this_target = $('.car-model').eq(i);
            //console.log($this_target);
            var span_str = '';   //<span class="lst-item"><a href="#a">3.8 럭셔리</a></span>
            var select_str = '';    //<option>3.8 럭셔리</option>
            span_str = '<span class="lst-item"><a href="#a" onclick="setTrimData('+i+',\'no\')">'+specs.pip_spec_vehicle_select+'</a></span>';
            select_str = '<option>'+specs.pip_spec_vehicle_select+'</option>';
            for(var j=0 ; j<trimmap.length ; j++){
                if(model.indexOf('g80')!=-1){
            		if(model.indexOf('sport')!=-1 && trimmap[j].name.indexOf('SPORT')==-1){
	                    //g80 스포츠 화면에서 g80 트림 거르기
	                    continue;
	                }else if(model.indexOf('sport')==-1 && trimmap[j].name.indexOf('SPORT')!=-1){
	                    //g80 화면에서 g80 스포츠 트림 거르기
	                    continue;
	                } 
            	}

                if(trimmap[j].name.indexOf('시그니')!=-1 ||trimmap[j].name.indexOf('Signa')!=-1 || trimmap[j].name.indexOf('스포츠 디자인')!=-1 ||trimmap[j].name.indexOf('Sport Design')!=-1){
                	//console.log(trimmap[j].name.indexOf('시그니')); Signa
                	continue;
                }
                span_str += '<span class="lst-item"><a href="#a" onclick="setTrimData('+i+',\''+trimmap[j].trim_code+'\')" data-code="'+trimmap[j].trim_code+'">'+trimmap[j].name+'</a></span>';
                select_str += '<option>'+trimmap[j].name+'</option>';
            }

            $this_target.find('.select-lst').html('');
            $this_target.find('.select-lst').html(span_str);

            $this_target.find('.select').html('');
            $this_target.find('.select').html(select_str);
        }
    }

    //기본 테이블 생성
    function initTable(sds){
        console.log(sds);
        for(var i=0 ; i<specs.tabarray.length ; i++){
            var htmlstr = '';
            $('.specs-inner .table-cont div.tbl-content').eq(i).find('tbody').html('');
            var obj = sds[specs.tabarray[i]];
            //console.log(obj);
            for(var j=0 ; j<obj.length ; j++){

                var unitstr = '';
                if(obj[j].unit!=''){
                    unitstr = '('+obj[j].unit+')';
                }
                htmlstr +='<tr>';
                htmlstr +='<th>'+obj[j].title+unitstr+'</th>';
                htmlstr +='<td></td>';
                var model = specs.model.toLowerCase();            
                if(model.indexOf('sport')==-1){            
                    //g80 sports 의 경우 다른 탭을 삭제 
                    htmlstr +='<td></td>';
                    htmlstr +='<td></td>';    
                }
                
                htmlstr +='</tr>';
            }

            $('.specs-inner .table-cont div.tbl-content').eq(i).find('tbody').html(htmlstr);
        }
    }


