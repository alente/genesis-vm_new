/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 27.07.2017
 * Time: 18:43
 */

window['Slide_3'] = (function(){
	var params = {
		selector: {
			slide: '[data-slide=3]',
			btnPdf: 'btn-pdf',
		},
		data: {

		},
		container: null,
	};
	var wasInit = false;
	var initialized = false;
	
	return class Slide_3 {
		static render () {
			let scope = this;
			scope.__findSlide();

			let slideHtml = views.slide3({ base: app.slides.base, selected: app.slides.selected });
			// console.log(slideHtml);

			params.container.html(slideHtml);
		}

		static init () {
			initialized = true;
		}

		static show () {
			let scope = this;
			scope.__findSlide();
			params.container
				.find('.animate-item')
				.addClass('show');

      if (_isMobile) {
        $('.mobile-panel').show();
      }

      if ($('html.tablet').length) {
        $(window).trigger('resize');
      }

			params.container
				.find('[data-mask="phone"]').inputmask("+7 (999) 999-99-99", {placeholder: "+7 (___) ___-__-__", autoclear: false});

      if (!_isMobile) {
        params.container.find('.info-block').mCustomScrollbar();
      }
			params.container.find('.dropdown-title').on('click', function () {
				let $item = $(this);

				$item.next('.dropdown').slideToggle(function () {
					if ($item.parent().hasClass('open')) {
						$item.parent().removeClass('open');
					} else {
						$item.parent().addClass('open');
					}
				});

				// $this.
			});

			app.assideHandler.bindButtons(params.container);

			let $btnPdf = params.container.find('[data-'+ params.selector.btnPdf +']');
			$btnPdf.on('click', function(){
				let $button = $(this),
					action = $button.data(params.selector.btnPdf),
					calc = new Calculator(),
					calculation = calc.getDefaultCalculation(),
					save = false;
				
				if(action == 'open'){
					save = true;
				}
				
				let programs = {};
				for(let k in app.slides.finance.creditProgram){
					let program = app.slides.finance.creditProgram[k];
					programs[program.id] = program;
				}
				
				let url = app.slides.getPdfUrl({
					credit: true,
					program: programs[calculation.program].front_name,                                       // Программа кредитования
					amount_credit: app.slides.formatPrice(calculation.sumCredit) + ' руб.',            // Сумма кредита
					first_pay: app.slides.formatPrice(calculation.initialFee) + ' руб.',               // Стартовый платёж
					percent: calculation.rate + '%',                                                   // Процентрная ставка
					monthly_payment: app.slides.formatPrice(calculation.month_payment) + ' руб.'       // Ежемесячный платеж
				}, save);
				window.open(url);
			});

			let model = app.slides.base.model[app.slides.selected.model];
			if(typeof model.brochure != 'undefined' && model.brochure){
				params.container.find('[data-brochure-link-popup]').attr('href', model.brochure);
			}
		}

		static hide (callback=()=>{}) {
			let scope = this;
			scope.__findSlide();
			params.container
				.find('.animate-item')
				.removeClass('show');

			callback(scope);
		}

		static isInitizlized(){
			return initialized;
		}

		static __findSlide () {
			params.container = $(params.selector.slide);
		}

	}
})();