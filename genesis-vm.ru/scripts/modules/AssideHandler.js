/**
 * Кастомный модуль на EcmaScript 6
 */

class AssideHandler extends Boreas.module {
    /**
     * Конструктор класса
     * @param arg1
     * @param arg2
     */
    constructor (arg1, arg2) {
        super(arg1, arg2);

        this.data = null;
        this.dealers = {};

        this.params = {
            /**
             * Перечислим все всплывашки
             * значение свойства, это ID asside и класс для вызова, назначаемый кнопкам
             */
            assides: {
                calculator: {name: 'calculator', view: 'assideCalculator', modelField: null},
                testDrive: {name: 'testDrive', view: 'assideTestDrive', modelField: null},
                inquiryDealer: {name: 'inquiryDealer', view: 'assideInquiryDealer', modelField: null},
                assideCharacters: {name: 'assideCharacters', view: 'assideCharacters'},
                assideMessage: {name: 'assideMessage', view: 'assideMessage'},
            },
            activeAsside: null,
            activeForm:null,
            __emptyTt: null,

            calculator: function (params = {}) {
                let scope = this,
                    $asside = $('#'+scope.params.assides.calculator.name);
                const $model = $asside.find("#getModel");
                const $complect = $asside.find("#getComplect");
                const $interior = $asside.find("#interior");

                $model.select2();
                $complect.select2();
                $interior.select2({
                    templateResult: formatState,
                    templateSelection: formatState
                });

                let creditCalc = new Calculator('#calculator');
                creditCalc.initialize();
	
	            scope.params.assides.calculator.modelField = $model;
                
	            //  Google Tag Manager
	            /*if(typeof dataLayer != 'undefined' && typeof dataLayer.push != 'undefined'){
		            let modelName = app.slides.selected.model;
		
		            dataLayer.push({
			            "event": "custom_event",
			            "category" : "Расчет кредита",
		                "action": "Открытие калькулятора",
		                "label" : modelName.toUpperCase()
		            });
	            }*/
                
                function formatState(state) {
                    if (!state.id) {
                        return state.text;
                    }
                    const image = '<span class="select2-selection__image"><img src="/images/demo/select_img.png"></span>';
                    const capt = '<span class="select2-selection__caption">' + state.text + '</span>'
                    return $(image + capt)

                }
            },

            testDrive: function (params = {}) {
                let scope = this,
                    $asside = $('#'+scope.params.assides.testDrive.name),
                    getDriveModel = $asside.find('#getDriveModel'),
                    getDriveCity = $asside.find('[data-city-container] select'),
                    getDriveDil = $asside.find('[data-dealer-container] select'),
                    getDriveTel = $asside.find('input[name=phone]'),
                    $container = $asside.find('#form-test-drive'),
                    promise = [];

                scope.__renderCities(scope.params.assides.testDrive.name);
	
	            scope.params.assides.testDrive.modelField = getDriveModel;
                
                /**
                 * Смена машины
                 */
                let carImage = $asside.find('#carImage');
                getDriveModel
                    .select2()
                    .on("select2:select", function (e) {
                        //const value = $(this).val();
                        let modelCode = getDriveModel.find('option:selected').data('model').toLowerCase();
                        const imageClass = `animated-car ${modelCode}-preview`;

                        carImage
                            .removeAttr('class')
                            .addClass(imageClass);
                    });

                getDriveModel.trigger('select2:select');
                getDriveCity.select2({
                    placeholder: "Выберите город",
                });
                getDriveDil.select2({
                    placeholder: "Выберите дилера",
                });

                getDriveTel.inputmask({
                    "mask": "+7 (999) 999-99-99"
                });
                
                //  Google Tag Manager
	            /*if(typeof dataLayer != 'undefined' && typeof dataLayer.push != 'undefined'){
		            let modelName = app.slides.selected.model;
	                
	                dataLayer.push({
			            "event": "custom_event",
			            "category" : "Тест-драйв",
                        "action": "Открытие заявки",
                        "label" : modelName.toUpperCase()
                    });
	            }*/
            },

            /**
             * Поля для отправки на сервер
             *
             * city - Город
             * dealer - код дилера
             *
             * model - Имя модели авто
             * name - Имя
             * surQname - Фамилия
             * email - Email
             *
             * credit true/false
             * program - Программа кредитования
             * amount_credit - Сумма кредита
             * first_pay - Стартовый платёж
             * percent - Процентрная ставка
             * monthly_payment - Ежемесяцный платеж
             */
            inquiryDealer: function (params = {}) {
                let scope = this,
                    $asside = $('#'+scope.params.assides.inquiryDealer.name),
                    inquiryTel = $asside.find('input[name=phone]');

                scope.__renderCities(scope.params.assides.inquiryDealer.name);
	
	            scope.params.assides.inquiryDealer.modelField = $asside.find('input[name=model]');
                
                inquiryTel.inputmask({
                    "mask": "+7 (999) 999-99-99"
                });
            },

            assideCharacters: function (params = {}) {
                // const getDriveModel = $('#getDriveModel');
            },

            assideMessage: function () {

            }
        }
    }

    __renderCities(asside) {
        let scope = this,
            $asside = $('#'+asside),
            $container = $asside.find('[data-city-container]'),
            getDriveCity = $container.find('select'),
            resultHtml = '',
            firstCities = '',
            dealerView = $('body').data('dealer');

        if(dealerView == true){
        	let field = `<input type="hidden" name="${getDriveCity.attr('name')}" value="${$('body').data('dealer-city-id')}" />`;
        	$container.html(field);
	        scope.__renderDealersList($('body').data('dealer-city-id'), asside);
        	return;
        }
        
        let i = 0;

        for (let k in app.slides.cities) {
            i++;
            let city = app.slides.cities[k];
            let selected = '';
            /*if (i == 1) {
                selected = ' selected';
            }*/

            if(city.name == "Москва" || city.name == "Москва и Подмосковье" || city.name == "Санкт-Петербург"){
                firstCities += `<option value="${city.id}">${city.name}</option>`;
            }else{
                resultHtml += `<option value="${city.id}">${city.name}</option>`;
            }

        }
        resultHtml = '<option></option>'
            + firstCities
            + '<option value="" disabled="disabled"></option>'
            + resultHtml;

        let instance = getDriveCity.data('select2');
        if(instance){
            getDriveCity.select2("destroy");
        }
        getDriveCity.html(resultHtml);
        getDriveCity.select2({
            placeholder: "Выберите город",
        });
        getDriveCity.on('change', function(){
            scope.__renderDealersList($(this).val(), asside);
        });
        getDriveCity.trigger('change');

    }

    __renderDealersList (cityId, asside) {
        let $asside = $('#'+asside),
            $container = $asside.find('[data-dealer-container]'),
            getDriveDil = $container.find('select'),
            resultHtml = '<option></option>',
	        dealerView = $('body').data('dealer');
	
	    if(dealerView == true){
		    let field = `<input type="hidden" name="${getDriveDil.attr('name')}" value="${$('body').data('dealer-code')}" />`;
		    $container.html(field);
		    return;
	    }

        //let city = cityId.split(",");

        let i = 0;
        for (let k in app.slides.dealers) {
            let dealer = app.slides.dealers[k];
            if(cityId != dealer.city_id/*city.indexOf(dealer.city_id) == -1*/) continue;
            i++;
            let selected = '';
            //let selected = (i==1)?' selected':'';
            resultHtml += '<option value="' + dealer.code + '"'+ selected +'>' + dealer.name + '</option>';
        }

        let instance = getDriveDil.data('select2');
        if(instance){
            getDriveDil.select2("destroy");
        }
        getDriveDil.html(resultHtml);
        getDriveDil.select2({
            placeholder: "Выберите дилера",
        });
        getDriveDil.trigger('change');
    }

    bindButtons (selector='body') {
        let scope = this,
            $container = $(selector);

        for (var k in scope.params.assides){
            let asside = scope.params.assides[k];

            $('[data-asside="'+asside.name+'"]').each(function(){
                let $button = $(this);

                $button.off('click');
                $button.on('click', function(){
                    let additional = {};
                    if(asside.name == 'inquiryDealer' || asside.name == 'testDrive'){
                        let calc = new Calculator();
                        let calculation = calc.getDefaultCalculation();
                        let programs = {};

                        for(let k in app.slides.finance.creditProgram){
                            let program = app.slides.finance.creditProgram[k];
                            programs[program.id] = program;
                        }

                        let filePDF = app.slides.getPdfUrl({
                            credit: true,
                            program: programs[calculation.program].name,                                       // Программа кредитования
                            amount_credit: app.slides.formatPrice(calculation.sumCredit) + ' руб.',            // Сумма кредита
                            first_pay: app.slides.formatPrice(calculation.initialFee) + ' руб.',               // Стартовый платёж
                            percent: calculation.rate + '%',                                                   // Процентрная ставка
                            monthly_payment: app.slides.formatPrice(calculation.month_payment) + ' руб.'       // Ежемесячный платеж
                        }, true, true);

                        var model = app.slides.base.model[app.slides.selected.model];
                        var crmModel;
                        switch(model.code){
                            case 'g70': crmModel = 'G70'; break;
                            case 'g80': crmModel = 'G80'; break;
                            case 'g90': case 'g90l': crmModel = 'G90'; break;
                        }

                        additional = {
                            file: filePDF,
                        };
                        if(asside.name == 'inquiryDealer'){
                            additional.lead_type = 'CREDIT_INQUIRY';
                            additional.model = crmModel;
                            //additional.city = "Москва";
                            //additional.dealer = "C40AF10120";
                        }else if(asside.name == 'testDrive'){
                            additional.lead_type = 'TEST_DRIVE';
                        }
                    }
                    scope.showAsside(asside.name, additional);
                });
            });

        }
    }

    showAsside (name, additional = {}) {
        let scope = this,
            params = scope.params.assides[name];
        if(typeof params == 'undefined'){
            console.log('Asside "'+ name +'" is not exists');
        }

        //  Рендерим попап
        let $asside = $('#' + params.name),
            $activeAsside = (scope.params.activeAsside)?$('#' + scope.params.activeAsside):null,
            assideHtml = views[params.view]({ base: app.slides.base, selected: app.slides.selected, additional: additional });
        if(!$asside.length){
            console.log('Asside "'+ name +'" is not exists in DOM')
        }

        $asside.html(assideHtml);
        let $overlay = $('.asside--overlay');

        if(app.slides.selected.slide == 2){
            $asside.find('.toolbar').addClass('show');
        }

        //  Инифиализируем содержимое
        scope.__initAsside(name);

        //  Показываем
        if($activeAsside) {
            $activeAsside.addClass('asside__hidden');
            $overlay.addClass('asside__hidden');

            /*clearTimeout(scope.params.__emptyTt);
            scope.params.__emptyTt = setTimeout(()=>{
                $activeAsside.empty();
            }, 500);       */
        }

        if($asside.hasClass('asside__hidden')) {
            $asside.removeClass('asside__hidden');
            $overlay.removeClass('asside__hidden');
        }
	
	    if(
	        scope.params.activeAsside != 'calculator'
            && scope.params.activeAsside != 'assideCharacters'
            && scope.params.activeAsside != 'assideMessage'
        ){
		    scope.params.activeForm = params.name;
        }
        scope.params.activeAsside = params.name;
	
	
	    if(scope.params.activeAsside != 'assideCharacters' && scope.params.activeAsside != 'assideMessage') {
		    //  Google Tag Manager
		    if(typeof dataLayer != 'undefined' && typeof dataLayer.push != 'undefined'){
                let options = {
                    "event": "custom_event",
                    "category": "Ошибка, не определено",
                    "action": "Открытие заявки",
                    "label": "Ошибка, не определено"
                };
            
                switch (scope.params.activeForm) {
                    case "calculator":
                        options.category = "Расчет кредита";
                        if(scope.params.activeAsside == 'calculator'){
                            options.action = "Открытие калькулятора";
                        }else if(scope.params.activeAsside == 'inquiryDealer'){
                            options.action = "Открытие заявки";
                        }
                        options.label = scope.params.assides.calculator.modelField.val().toUpperCase();
                        break;
                    case "testDrive":
                        options.category = "Тест-драйв (конфигуратор)";
                        options.label = scope.params.assides.testDrive.modelField.val().toUpperCase();
                        break;
                    case "inquiryDealer":
                        options.category = "Отправка конфигурации дилеру";
                        options.label = scope.params.assides.inquiryDealer.modelField.val().toUpperCase();
                        break;
                }
                //console.log(options);
                dataLayer.push(options);
		    }
	    }
    }

    __initAsside (name) {
        let scope = this,
            $asside = $('#' + name),
            $overlay = $('.asside--overlay'),
            $btnClose = $asside.find('.asside--btn-close');

        scope.params[name].call(scope, name);

        scope.__assideInitElements(name);
        scope.bindButtons($asside);
        
        if(name == 'inquiryDealer' || name == 'testDrive'){
            let $form = $asside.find('form[data-ajax-form]');
            scope.__initAjaxForm($form);
        }

        function closeHandler(elem) {
            elem.on('click', (e) =>  {
                e.preventDefault();

                if(!$asside.hasClass('asside__hidden')) {
                    $asside.addClass('asside__hidden');
                    $overlay.addClass('asside__hidden');
                }

                /*clearTimeout(scope.params);
                scope.params.__emptyTt = setTimeout(()=>{
                    $asside.empty();
                }, 500);    */

                this.params.activeAsside = null;
            });
        }

        closeHandler.call(scope, $btnClose);
        closeHandler.call(scope, $overlay);

        if(app.slides.selected.model){
            let model = app.slides.base.model[app.slides.selected.model];
            if(typeof model.brochure != 'undefined' && model.brochure){
                $asside.find('[data-brochure-link-popup]').attr('href', model.brochure);
            }
        }
    }

    __initAjaxForm ($form) {
        let scope = this,
            action = $('body').data('form-action'),
            $loadingBtn = $form.find('[data-loading-text]');

        $loadingBtn.data('orig-text', $loadingBtn.text());
        
        $.validator.setDefaults({
            ignore: [] // DON'T IGNORE PLUGIN HIDDEN SELECTS, CHECKBOXES AND RADIOS!!!
        });

        var options = {
            url: action,
            type: 'get',
            dataType: 'json',
            beforeSubmit: function(){
                $loadingBtn.text($loadingBtn.data('loading-text'));
            },
            success: function(response){
                /*
                {ResultCode: "E", ErrorMessage: "RequestModel is required. please input the right value in the field", LogId: "1-127TMK9"}
                */

                scope.showAsside('assideMessage', {
                    response: response.ResultCode,
                });
                if(response.ResultCode != 'Y'){
                    console.log("FORM ERROR: " + response.ErrorMessage);
                }
                $loadingBtn.text($loadingBtn.data('orig-text'));
            }
        };

        $form.validate({
            rules: {
                name: { required: true },
                surname: { required: true },
                email: { required: true, email: true },
                phone: { required: true },
                agree: { required: true },
	            city: { required: true },
	            dealer : { required: true },
            },
            submitHandler: function(form) {
                //  Google Tag Manager
                if(typeof dataLayer != 'undefined' && typeof dataLayer.push != 'undefined'){
                    //let modelName = app.slides.selected.model;
    
                    let options = {
                        "event": "custom_event",
                        "category" : "Ошибка, не определено",
                        "action": "Отправка заявки",
                        "label" : "Ошибка, не определено"
                    };
                    
                    switch(scope.params.activeForm){
                        case "calculator":
                            options.category = "Расчет кредита";
	                        options.label = scope.params.assides.calculator.modelField.val().toUpperCase();
                            break;
                        case "testDrive":
                            options.category = "Тест-драйв (конфигуратор)";
	                        options.label = scope.params.assides.testDrive.modelField.val().toUpperCase();
                            break;
                        case "inquiryDealer":
                            options.category = "Отправка конфигурации дилеру";
	                        options.label = scope.params.assides.inquiryDealer.modelField.val().toUpperCase();
                            break;
                    }
                    
                    dataLayer.push(options);
                }
                $form.ajaxSubmit(options);
            },
            errorElement: 'span'
        });
        
    }

    __assideInitElements (name) {
        let scope = this,
            $asside = $('#' + name),
            $customScroll = $asside.find('.customScroll'),
            $boxes = $asside.find('.form-element--checkbox-header');

        // Повесим скроллбар

        if($customScroll.length && !_isMobile) {
            $customScroll.mCustomScrollbar();
        }
        /*if($boxes.length) {
            $.each($boxes, function () {
                var $this = $(this);
                var strings = [];
                if ($this.text().indexOf('+') > 0) {

                    strings = $this.text().split('+');
                    var spans = '';
                    //console.log(strings)
                    for (var i = 0; i < strings.length; i++){
                        if (i != strings.length - 1) {
                            spans += '<span>' + strings[i] + ' + ' + '</span>'
                        } else {
                            spans += '<span>' + strings[i] + '</span>'
                        }
                    }
                    var html = '<p class="form-element--checkbox-header-wrap">'+ spans + '</p>';
                    $this.parent().prepend(html);
                    $this.remove();
                }
            })
        }*/
        // '.form-element--checkbox-header-wrap'

        // Обработаем чекбокс - аккордион
        //scope.__initCheckbox($asside);

        // Обработаем список "ПЛЮСЫ"
        let wrap = $asside.find('.dropdown-wrap--gray');
        wrap.find('.dropdown-title')
            .on('click', function () {
                let $this = $(this);

                $this.next('.dropdown').slideToggle(function () {
                    if ($this.parent().hasClass('open')) {
                        $this.parent().removeClass('open');
                    } else {
                        $this.parent().addClass('open');
                    }
                })
            });
    }

    __initCheckbox (target='body') {
        let $target = (typeof target == 'string')?$(target):target;

        //$target.find('.asside--group').each(function () {
        let $childs = $target.find('.asside--group-item');
        $childs.each(function () {
            let $child = $(this);
            let $caption = $child.find('.asside--group-label');

            if($caption.length) {
                let $labelName = $child.find('.form-element--checkbox-header');

                $labelName.on('click', function (e) {
                    //e.preventDefault();

                    $caption.slideToggle(function () {
                        if ($child.hasClass('open')) {
                            $child.removeClass('open');
                        } else {
                            $child.addClass('open');
                        }
                    })
                })
            }
        });
    }

    initialize (params = {}) {

        //this.assideInit();
        // this.params.initTestDrive();
        // this.params.initTestDrive();


        this.trigger('ready');
    }
}
