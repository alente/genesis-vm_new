/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 27.07.2017
 * Time: 18:39
 */

window['Slide_0'] = (function () {
	var params = {
		selector: {
			slide: '[data-slide=0]',
		},
		data: {
			modelId: 'model-id',
		},
		container: null,
	};
	var initialized = false;

	return class Slide_0 {
		static render() {

		}

		static init() {
			let scope = this;
			scope.__findSlide();

			params.container.find('[data-' + params.data.modelId + ']').off('click');
			params.container.find('[data-' + params.data.modelId + ']').on('click', function () {

				app.slides.selected.model = $(this).data(params.data.modelId);
				app.slides.toSlide(1);
			});
			initialized = true;

		}

		static show() {
			let scope = this;
			scope.__findSlide();
			params.container
			.find('.animate-item')
			.addClass('show');

			// выравнивание тестов в одну строку
			//const headers = [...document.querySelectorAll('.model-items [data-model-id] h3')];
			//const props = [...document.querySelectorAll('.props')];
			
			const headers = [].slice.call(document.querySelectorAll('.model-items [data-model-id] h3'));
			const props = [].slice.call(document.querySelectorAll('.props'));
			
			function setHeight(){
				let top;
				headers.forEach( (item, index) => {
					if (!index) {
						top = item.getBoundingClientRect().top;
					}

					const coords = item.getBoundingClientRect();
					const marginTop = parseInt( getComputedStyle(item).marginTop );
					item.style.marginTop = marginTop + top - coords.top + 'px';

				});

				top = 0;
				let maxIndex;
				props.forEach( (item, index) => {

					let elTop = item.getBoundingClientRect().top;
					if (top < elTop) {
						maxIndex = index;
					}

				});
				top = props[maxIndex].getBoundingClientRect().top;

				props.forEach( (item, index) => {

					const coords = item.getBoundingClientRect();
					const marginTop = parseInt( getComputedStyle(item).marginTop );
					item.style.marginTop = marginTop + top - coords.top + 'px';

				});

			}
			setHeight();
			window.addEventListener( 'resize', setHeight);

			// выравние закончено

			$('.mobile-panel').hide();

			if ($('html.tablet').length) {
				let sliderWrap = params.container.find('.model-items');
				//
				sliderWrap.owlCarousel({
					center: true,
					items: 1,
					loop: true,
					margin: 0,
					autoWidth: true
				});
			}

			if ($('html.mobile').length) {
				let sliderWrap = params.container.find('.model-items');

				sliderWrap.owlCarousel({
					center: true,
					items: 1,
					loop: true,
					margin: 0,
					nav: false,
					autoWidth: true
				});
			}

			app.assideHandler.bindButtons(params.container);
		}

		static hide(callback = () => {
		}) {
			let scope = this;
			scope.__findSlide();
			params.container
			.find('.animate-item')
			.removeClass('show');

			callback(scope);
		}

		static isInitizlized() {
			return initialized;
		}

		static __findSlide() {
			params.container = $(params.selector.slide);
		}
	}
})();
