/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 26.07.2017
 * Time: 19:20
 */

class Slides extends Boreas.module {
	constructor () {
		super();

		this.params = {
			selector:{
				indicator: '[data-indicator]',
			}
		};

		this.base = null;
		this.finance = null;
		this.start = true;
		this.startParams = {};
		this.acceptedStartSelectedParams = ['view', 'slide', 'model', 'complectation', 'colorExterior', 'colorInterior', 'package', 'popup', 'view', 'code', 'city_id'];
		this.selected = {
			slide: 0,
			model: null,
			complectation: null,
			colorExterior: null,
			colorInterior: null,
			package: null,
			view: null,
			code: null,
			city_id: null
		};
		this.__$indicator = null;

		this.__registerEvents(['changeParams']);
	}

	initialize (params = {}) {
		let scope = this,
			promise = [];
		$.extend( true, scope.params, params );

		let dataUrl = $('body').data('json-base'),
			financeUrl = $('body').data('json-finance'),
			citiesUrl = $('body').data('json-cities'),
			dealersUrl = $('body').data('json-dealers'),
			dealerView = $('body').data('dealer');
		
		/*if(dealerView==true){
			scope.selected.view = 'dealer';
			scope.selected.code = $('body').data('dealer-code');
			scope.selected.city_id = $('body').data('dealer-city-id');
		}*/

		promise.push(scope.__loadJSONData(dataUrl, 'base'));
		promise.push(scope.__loadJSONData(financeUrl, 'finance'));
		promise.push(scope.__loadJSONData(citiesUrl, 'cities'));
		promise.push(scope.__loadJSONData(dealersUrl, 'dealers'));

		$.when.apply(undefined, promise).promise().done(() => {
			scope.initStartParams();
			scope.initHistory();
			scope.initIndicator();
			scope.toSlide(scope.selected.slide);
			this.trigger('ready');
			this.start = false;
		});
	}

	initStartParams () {
		let scope = this,
			hash = document.location.hash.replace("#", "");

		scope.startParams = scope.__unserializeParams(hash);

		//console.log(scope.startParams);

		for(let k in scope.acceptedStartSelectedParams){
			let paramName = scope.acceptedStartSelectedParams[k];
			if(typeof scope.startParams[paramName] != 'undefined'){
				scope.selected[paramName] = scope.startParams[paramName];
			}
		}

		if(typeof scope.startParams.popup != 'undefined'){
			switch(scope.startParams.popup){
				case 'calculator': app.assideHandler.showAsside(scope.startParams.popup); break;
				case 'testDrive': app.assideHandler.showAsside(scope.startParams.popup); break;
				case 'inquiryDealer': app.assideHandler.showAsside(scope.startParams.popup); break;
			}
		}

	}

	initHistory () {
		let scope = this,
			uri = document.location.href.replace(document.location.origin, "");

		scope.on('changeParams', function (params){
			if(typeof params.addToHistory != 'undefined' && params.addToHistory == true){
				window.history.pushState(scope.selected, document.title, uri + '#' + scope.__serializeParams(scope.selected));
			}else{
				window.history.replaceState(scope.selected, document.title, uri + '#' + scope.__serializeParams(scope.selected));
			}
		});

		window.onpopstate = function(event) {
			scope.start = true;
			scope.initStartParams();
			scope.toSlide(scope.selected.slide, { skipHistory: true });
			scope.start = false;
		};
	}

	initIndicator () {
		let scope = this;
		scope.__$indicator = $(scope.params.selector.indicator);
		scope.__$indicator.find('li').addClass('disable');
		scope.__$indicator.find('li:eq(0)').removeClass('disable');
		scope.__$indicator.find('li').on('click', function(){
			if($(this).hasClass('disable')) return;
			let index = $(this).index();
			scope.toSlide(index);
		});
	}

	toSlide (index = 0, params = {}) {
		let scope = this;

		//if(!window['Slide_'+scope.selected.slide].isInitizlized())

		if(window['Slide_'+scope.selected.slide].isInitizlized() && typeof window['Slide_'+scope.selected.slide] == 'function'){
			window['Slide_'+scope.selected.slide].hide(function(){
				scope.__initSlide(index, params);
			});
		}else{
			scope.__initSlide(index, params);
		}
		return false;
	}

	formatPrice (value){
		return value.toString().split(/(?=(?:\d{3})+$)/).join(" ");
	}
	/**
	 * Получение файла PDF
	 * ссылка /download/pdf
	*/
	getPdfUrl (paramsAttr = {}, save=false, getFilename=false) {
		let calc = new Calculator();
		let calculation = calc.getDefaultCalculation();
		let car = app.slides.base.model[calculation.model],
			compl = car.complectation[calculation.complectation],
			colorExterior = app.slides.base.colorExterior[calculation.colorExterior],
			colorInterior = app.slides.base.colorInterior[calculation.colorInterior],
			url = false,
			pdfAction = $('body').data('pdf-action'),
			pdfPath = $('body').data('pdf-path'),
			params = {
				car: calculation.model,                             //  название машины в нижнем регистре
				modification: calculation.complectation,            //  id комплектации
				packet: calculation.package,                        //  id пакета
				trim_caption: compl.name + ' ' + compl.modificationName,    //  сокращенное название комплектации
				interior: calculation.colorInterior,
				exterior: calculation.colorExterior,
			};

		params = $.extend({}, params, paramsAttr);

		//console.log(params);

		if(save === true || getFilename === true){
			params.save = 1;
			$.ajax({
				url: pdfAction + '?' + $.param(params),
				async: false,
				dataType: 'json',
				beforeSend: (xhr)=>{}
			}).done(function(response) {
				url = response.file;
				if(getFilename !== true){
					url = pdfPath + '/' + url;
				}
			}).fail(function(){
				console.log('Get json error: ' + pdfAction);
			});
		}else{
			url = pdfAction + '?' + $.param(params);
		}
		//console.log(url);
		return url;
	}

	__initSlide (index = 0, params = {}) {
		let scope = this;

		scope.selected.slide = index;
		scope.updateIndicator(index);
		if(typeof params.skipHistory != 'undefined' && params.skipHistory === true){
			app.slides.trigger('changeParams');
		}else{
			app.slides.trigger('changeParams', { addToHistory: true });
		}


		$('[data-slide]').removeClass('top');
		$('[data-slide='+index+']').addClass('top');

		window['Slide_'+scope.selected.slide].render();
		window['Slide_'+scope.selected.slide].show();
		window['Slide_'+scope.selected.slide].init();
	}

	__loadJSONData (url, key) {
		let scope = this,
			defer = new $.Deferred();

		//console.log(url);

		$.ajax({
			url: url,
			dataType: 'json',
			beforeSend: (xhr)=>{}
		}).done(function(data) {
			scope[key] = data;
			defer.resolve();
		}).fail(function(){
			console.log('Get json error: ' + url);
			defer.resolve();
		});

		return defer;
	}

	updateIndicator (index = 0) {
		let scope = this;
		scope.__$indicator.find('li').removeClass('active');
		scope.__$indicator.find('li:eq('+ index +')')
			.removeClass('disable')
			.addClass('active');
	}

	__serializeParams (object = {}, onlyAccepted = true) {
		let scope = this,
			strParams = '',
			arStrParams = [];

		for(let k in object){
			if(object[k] === null) continue;
			if(onlyAccepted && scope.acceptedStartSelectedParams.indexOf(k) === -1) continue;

			arStrParams.push(k + '=' + object[k]);
		}

		if(arStrParams.length){
			strParams = arStrParams.join('&');
		}

		return strParams;
	}

	__unserializeParams (strParams = '') {
		let params = {},
			arStrParams = null;

		if(strParams.indexOf("&") !== -1){
			arStrParams = strParams.split("&");
		}else{
			arStrParams = [strParams];
		}

		for(let k in arStrParams){
			let arParam = null,
				strParam = arStrParams[k];
			if(strParam.indexOf("=") !== -1){
				arParam = strParam.split("=");

				if(!arParam[1]) continue;

				let intValue = parseInt(arParam[1]);
				if(!isNaN(intValue) && intValue == arParam[1]){
					params[arParam[0]] = intValue;
				}else{
					params[arParam[0]] = arParam[1];
				}
			}
		}

		return params;
	}

	__getCurParams () {
		let scope = this;

		for(let k in scope.acceptedStartSelectedParams) {

		}
	}
}