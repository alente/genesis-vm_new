/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 27.07.2017
 * Time: 18:42
 */

window['Slide_2'] = (function(){
	let params = {
		selector: {
			slide: '[data-slide=2]',
		},
		data: {
			colorExterior: 'color-exterior',
			colorInterior: 'color-interior',
			packageId: 'package-id',
			packagePrice: 'package-price',
			monthPayment: 'month-payment',
			finalPrice: 'final-price',
			item360: 'item360',
			btnFinish: 'btn-finish',
		},
		container: null,
	};
	
	let fallBackIe = {};
	let process = {};
	let car = null;
	let ready360 = false;
	//let car360 = {};
	window['car360'] = {};
	
	let car360Slide = 19;
	let initialized = false;
	let wasInit = false;
	let colorButtons = null;
	
	return class Slide_2 {
		static render () {
			let scope = this;
			scope.__findSlide();
			
			let slideHtml = views.slide2({ base: app.slides.base, selected: app.slides.selected });
			params.container.html(slideHtml);
		}
		
		static init () {
			let scope = this,
				calc = new Calculator();
			scope.__findSlide();
			
			$(document).on('change', '[data-'+ params.data.colorExterior +']', function() {
				app.slides.selected.colorExterior = $(this).data(params.data.colorExterior);
				app.slides.trigger('changeParams');
			});
			params.container.on('change', '[data-'+ params.data.colorInterior +']', function() {
				app.slides.selected.colorInterior = $(this).data(params.data.colorInterior);
				app.slides.trigger('changeParams');
			});
			params.container.on('change', '[data-'+ params.data.packageId +']', function() {
				app.slides.selected.package = $(this).data(params.data.packageId);
				
				let complPrice = app.slides.base.model[app.slides.selected.model].complectation[app.slides.selected.complectation].price;
				let packagePrice = app.slides.selected.package?app.slides.base.package[app.slides.selected.package].price:0;
				let calculation = calc.getDefaultCalculation();
				
				params.container.find('[data-'+ params.data.packagePrice +']').text(app.slides.formatPrice(packagePrice));
				params.container.find('[data-'+ params.data.finalPrice +']').text(app.slides.formatPrice(parseInt(complPrice) + parseInt(packagePrice)));
				params.container.find('[data-'+ params.data.monthPayment +']').text(app.slides.formatPrice(calculation.month_payment));
				
				app.slides.trigger('changeParams');
			});
			
			if(app.slides.selected.model){
				let model = app.slides.base.model[app.slides.selected.model];
				if(typeof model.brochure != 'undefined' && model.brochure){
					$('[data-brochure-link-popup]').attr('href', model.brochure);
				}
			}
			
			params.container.find('[data-'+ params.data.btnFinish +']').on('click', function(e){
				e.preventDefault();
				app.slides.toSlide(3);
			});
			
			//	Включение параметров URL при загрузке страницы
			if(app.slides.start && app.slides.startParams.colorExterior){
				params.container.find('[data-'+ params.data.colorExterior +'='+app.slides.startParams.colorExterior+']').trigger('click');
			}
			if(app.slides.start && app.slides.startParams.colorInterior){
				params.container.find('[data-'+ params.data.colorInterior +'='+app.slides.startParams.colorInterior+']').trigger('click');
			}
			if(app.slides.start && app.slides.startParams.package){
				params.container.find('[data-'+ params.data.packageId +'='+app.slides.startParams.package+']').trigger('click');
			}
			
			
			initialized = true;
		}
		
		static show () {
			if(typeof process.show != 'undefined' && process.show === true){
				//console.log('show уже выполняется');
				return;
			}
			
			if (_isMobile) {
				$('.mobile-panel').show();
			}
			if ($('html.tablet').length) {
				$(window).trigger('resize');
			}
			
			process['show'] = true;
			
			let scope = this;
			scope.__findSlide();
			
			params.container
				.removeClass('interior-open')
				.find('.animate-item')
				.removeClass('stop')
				.addClass('show');
			
			let $car = params.container.find('.animated-car.car-main');
			//console.log($car);
			car = new carWheels($car);
			setTimeout(function(){
				car.move(0);
			}, 200);
			
			let $exterior = params.container.find('.exterior');
			
			fallBackIe = {
				svg: $('.ie.image svg'),
				polyType: 0,
				hoverPos: 330,
				hover: '',
				resMask: function () {
					let poly = this.polyType;
					$.each(this.svg, function () {
						let $this = $(this),
							position = 330,
							$thisPoligon = $this.find('polygon'),
							wH = window.innerHeight,
							wW = window.innerWidth;
						
						if (!poly) {
							//уголок
							$thisPoligon.attr('points', '0,0 ' +
								wW + ',0 ' +
								wW + ',' + position +
								' ' + (wW - position) + ',0'
								+ 0, 0);
						} else {
							// многоугольник
							$thisPoligon.attr('points', '0,0 ' +
								(wW - position) + ',0 ' +
								wW + ',' + position + ' ' +
								wW + ',' + wH +
								' 0,' + wH);
						}
					})
				},
				resImg: function () {
					let poly = this.polyType;
					$.each(this.svg, function () {
						let $this = $(this),
							$thisRect = $this.find('rect'),
							$thisImage = $this.find('image'),
							delta = 1.789,
							wH = window.innerHeight,
							wW = window.innerWidth;
						
						if (wW / wH > delta) {
							$thisImage.attr({
								'width': wW + 'px',
								'height': wW * delta + 'px',
								'y': -(wW * delta / 2) + (wH / 2) + 'px',
								'x': 0 + 'px'
								
							});
							$thisRect.attr({'width': wW + 'px', 'height': wH + 'px'})
						} else {
							$thisImage.attr({
								'height': wH + 'px',
								'width': wH * delta + 'px',
								'x': -(wH * delta / 2) + (wW / 2) + 'px',
								'y': 0 + 'px'
							});
							$thisRect.attr({'width': wW + 'px', 'height': wH + 'px'})
						}
					})
				},
				aminOut: function () {
					var fps = 60;
					var position = 0;
					function step() {
						setTimeout(function() {
							position+=200;
							$.each(fallBackIe.svg, function () {
								let $this = $(this),
									$thisPoligon = $this.find('polygon'),
									wW = window.innerWidth;
								
								$thisPoligon.attr('points', '0,0 ' +
									wW + ',0 ' +
									wW + ',' + (330+position) +
									' ' + (wW - (330+position)) + ',0'
									+ 0, 0);
							});
							if (position < 3000){
								requestAnimationFrame(step);
							} else {
								$.each(fallBackIe.svg, function () {
									let $this = $(this),
										$thisPoligon = $this.find('polygon'),
										wH = window.innerHeight,
										wW = window.innerWidth;
									
									$thisPoligon.attr('points', '0,0 ' +
										(wW - 330) + ',0 ' +
										wW + ',' + 330 + ' ' +
										wW + ',' + wH +
										' 0,' + wH);
								});
							}
						}, 1000 / fps);
					}
					step();
					
					
				},
				aminIn: function () {
					var fps = 60;
					var position = 0;
					let poly = this.polyType;
					function step() {
						setTimeout(function() {
							position+=200;
							$.each(fallBackIe.svg, function () {
								let $this = $(this),
									$thisPoligon = $this.find('polygon'),
									wH = window.innerHeight,
									wW = window.innerWidth;
								
								$thisPoligon.attr('points', '0,0 ' +
									(wW - (330+position)) + ',0 ' +
									wW + ',' + (330+position) + ' ' +
									wW + ',' + wH +
									' 0,' + wH);
							});
							if (position < 3000){
								requestAnimationFrame(step);
							} else {
								var fps2 = 60;
								var position2 = 0;
								// console.log('step2');
								function step2() {
									setTimeout(function () {
										position2 += 30;
										$.each(fallBackIe.svg, function () {
											let $this = $(this),
												$thisPoligon = $this.find('polygon'),
												wW = window.innerWidth;
											
											$thisPoligon.attr('points', '0,0 ' +
												wW + ',0 ' +
												wW + ',' + position2 +
												' ' + (wW - position2) + ',0'
												+ 0, 0);
										});
										
										if (position2 <= 330) {
											requestAnimationFrame(step2);
										}
									}, 1000 / fps2);
								}
								step2();
							}
						}, 1000 / fps);
					}
					step();
				},
				aminStart: function () {
					var fps2 = 60;
					var position2 = 0;
					//console.log('start');
					function step2() {
						setTimeout(function () {
							position2 += 30;
							$.each(fallBackIe.svg, function () {
								let $this = $(this),
									$thisPoligon = $this.find('polygon'),
									wW = window.innerWidth;
								
								$thisPoligon.attr('points', '0,0 ' +
									wW + ',0 ' +
									wW + ',' + position2 +
									' ' + (wW - position2) + ',0'
									+ 0, 0);
							});
							
							if (position2 <= 300) {
								requestAnimationFrame(step2);
							}
						}, 1000 / fps2);
					}
					
					step2();
				},
				hoverIn: function () {
					var fps = 60;
					//console.log('hoverIn');
					function step() {
						setTimeout(function () {
							console.log(fallBackIe.hoverPos)
							fallBackIe.hoverPos += 5;
							$.each(fallBackIe.svg, function () {
								let $this = $(this),
									$thisPoligon = $this.find('polygon'),
									wW = window.innerWidth;
								
								$thisPoligon.attr('points', '0,0 ' +
									wW + ',0 ' +
									wW + ',' + fallBackIe.hoverPos +
									' ' + (wW - fallBackIe.hoverPos) + ',0'
									+ 0, 0);
							});
							
							if (fallBackIe.hoverPos <= 360  && fallBackIe.hover == "in") {
								requestAnimationFrame(step);
							}
						}, 1000 / fps);
					}
					
					step();
				},
				hoverOut: function () {
					var fps = 60;
					//console.log('hoverOut');
					function step() {
						setTimeout(function () {
							fallBackIe.hoverPos -= 5;
							$.each(fallBackIe.svg, function () {
								let $this = $(this),
									$thisPoligon = $this.find('polygon'),
									wW = window.innerWidth;
								
								$thisPoligon.attr('points', '0,0 ' +
									wW + ',0 ' +
									wW + ',' + fallBackIe.hoverPos +
									' ' + (wW - fallBackIe.hoverPos) + ',0'
									+ 0, 0);
							});
							
							if (fallBackIe.hoverPos >= 330 && fallBackIe.hover == "out") {
								requestAnimationFrame(step);
							}
						}, 1000 / fps);
					}
					
					step();
				}
			};
			
			fallBackIe.resImg();
			
			setTimeout(function(){
				$exterior.addClass('corner-transition');
				
				
				
				// fallBackIe.resElms();
				$(window).on('resize', function () {
					fallBackIe.resMask();
					fallBackIe.resImg();
				});
				
				setTimeout(function(){
					$exterior.addClass('corner-show');
					process['show'] = false;
					
					// console.log('start')
					fallBackIe.aminStart();
				}, 250);
			}, 1100);
			
			if ($('html.IE').length || $('html.MSIE').length) {
				let interior = params.container
					.find('.interior'),
					exterior = params.container
						.find('.exterior'),
					render = exterior.find('.render').clone(),
					form = exterior.find('form');
				interior.append(form);
			}
			
			//let delay360 = ready360?1200:1500;
			let delay360 = 1500;
			
			setTimeout(function(){
				scope.__init360();
			}, delay360);
			
			var stopCallback = false;
			var stopInput = true;
			
			if (_isMobile) {
				var colors = $('.color-select-interier input');
				var colorsLength = colors.length;
				
				params.container.find('.interior').prepend('<div class="color-slider owl-carousel"></div>');
				
				var colorSlider = params.container.find('.color-slider');
				
				for (var i = 0; i < colorsLength; i++) {
					colorSlider.append('<div class="color-slide" data-id-color="' + $(colors[i]).data('color-interior') + '"></div>');
				}
				
				colorSlider.owlCarousel({
					// center: true,
					items: 1,
					loop: true,
					margin: 0,
					autoWidth: true,
					center: true,
					onChanged: function (event) {
						// Provided by the core
						
						// console.log('change');
						setTimeout(function () {
							var id = params.container.find('.owl-item.active .color-slide').data('id-color');
							if (!stopCallback) {
								stopInput = true;
								params.container.find('.color-select-interier [data-color-interior=' + id + ']').prop('checked', true);
								stopInput = false;
							}
							
							let image = params.container.find('.interior .image');
							
							image.css('opacity', '');
							
							$('[data-interior-color='+ id +']').addClass('under-top')
								.animate({'opacity': 1}, 600, function () {
									$('.image.top').removeClass('top');
									$('[data-interior-color='+ id +']').addClass('top').removeClass('under-top');
								});
						}, 10);
					}
				});
				
				// $('.color-slider .owl-dot:first-child').removeClass('active');
				// $('.color-slider .owl-dot:nth-child(2)').addClass('active');
				
				// console.log('testeste');
				$('.color-slider .owl-stage-outer').swipe( {
					//Generic swipe handler for all directions
					pageScroll: 'auto',
					preventDefaultEvents: false,
					swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
						// console.log(direction);
						let bullets = $('.color-slider .owl-dot');
						let bulletsLength = bullets.length;
						let checkedBulletsIndex = $('.color-slider .owl-dot.active').index();
						
						if (direction == 'left'){
							if (checkedBulletsIndex >= 1) {
								$(bullets[checkedBulletsIndex - 1]).trigger('click');
							} else {
								$(bullets[bulletsLength - 1]).trigger('click');
							}
						}
						if (direction == 'right'){
							if (checkedBulletsIndex < bulletsLength - 1) {
								$(bullets[checkedBulletsIndex + 1]).trigger('click');
							} else {
								$(bullets[0]).trigger('click');
							}
						}
						
					}
				});
			}
			
			if (!_isMobile) {
				params.container.find('.scroll-block').mCustomScrollbar();
			}
			if (!wasInit) {
				
				// console.log('init')
				//переносим форму в интерьер для ie
				
				if ($('html.mobile').length) {
					params.container.on('click', '[data-color-interior] + label', function (e) {
						e.preventDefault();
						
						$($('.color-slider .owl-dot')[$(this).parent().index()]).trigger('click');
						stopCallback = false;
						
					});
				}
				
				if ($('html:not(.mobile)').length) {
					params.container.on('change','input[data-color-interior]', function () {
						let id = params.container.find('input[data-color-interior]:checked').data('color-interior'),
							top = params.container.find('.image.top'),
							image = params.container.find('.interior .image'),
							underTop = params.container.find('[data-interior-color='+ id +']');
						
						if (_isMobile && !stopInput) {
							stopCallback = true;
							$('.color-slider').trigger('to.owl.carousel', $(this).parent().index()+1);
							stopCallback = false;
						}
						
						image.css('opacity', '');
						
						$('[data-interior-color='+ id +']').addClass('under-top')
							.animate({'opacity': 1}, 600, function () {
								$('.image.top').removeClass('top');
								$('[data-interior-color='+ id +']').addClass('top').removeClass('under-top');
							});
						
					});
				}
				
				
				if ($('html.IE').length || $('html.MSIE').length) {
					params.container.on('mouseenter', '.interior > p', function () {
						if (!$('.interior-open').length && !$('.start-animation').length) {
							fallBackIe.hover = 'in';
							fallBackIe.hoverIn();
						}
					});
					params.container.on('mouseleave', '.interior > p', function () {
						if (!$('.interior-open').length &&!$('.start-animation').length) {
							fallBackIe.hover = 'out';
							fallBackIe.hoverOut();
						}
					});
				}
				
				
				params.container.on('click','.exterior .mobile-angle', function () {
					$('.interior > p').trigger('click');
				});
				params.container.on('click','.interior > p', function () {
					let $angle = $(this),
						parent = $angle.parents('.slide'),
						interior = params.container
							.find('.interior'),
						exterior = params.container
							.find('.exterior'),
						form = parent.find('form').clone();
					
					if (!parent.hasClass('start-animation')) {
						parent.toggleClass('interior-open');
						fallBackIe.polyType = fallBackIe.polyType == 1 ? 0 : 1;
						// fallBackIe.resSvg();
						// fallBackIe.resMask();
						
						let $car = $angle.find('.animated-car');
						
						if (parent.hasClass('interior-open')){
							fallBackIe.aminOut();
							setTimeout(function(){
								let angleCar = new carWheels($car);
								angleCar.move(0);
							}, 500);
							
						} else {
							fallBackIe.aminIn();
							setTimeout(function() {
								let angleCar = new carWheels($car);
							}, 500);
						}
						fallBackIe.hover = 325;
						
						//добавляем анимацию треугольнику
						parent.addClass('animate-triangle').addClass('start-animation');
						setTimeout(function () {
							parent.removeClass('animate-triangle');
						}, 1);
						
						if (!($('html.IE').length || $('html.MSIE').length)) {
							if (!exterior.find('form').length) {
								exterior.append(form);
							} else {
								interior.append(form);
							}
						}
						
						if($('html.IE').length || $('html.MSIE').length){
							colorButtons = params.container.find('.color-select');
						}else{
							colorButtons = params.container.find('.exterior .color-select');
						}
						
						exterior.find('form').on('submit', function(e){
							e.preventDefault();
							return false;
						});
						interior.find('form').on('submit', function(e){
							e.preventDefault();
							return false;
						});
						
						if (!($('html.mobile').length)) {
							var tableG = $('.table-group');
							
							if (parent.hasClass('interior-open')) {
								tableG.animate({'margin-bottom': -tableG.height()}, 300, function () {
									tableG.hide();
								});
							} else {
								tableG.show().css({'margin-bottom': -tableG.height()});
								tableG.animate({'margin-bottom': 0}, 300);
							}
						}
						
						setTimeout(function () {
							parent.removeClass('start-animation');
							if (!($('html.IE').length || $('html.MSIE').length)) {
								if (parent.hasClass('interior-open')) {
									exterior.find('form').remove();
								} else {
									interior.find('form').remove();
								}
							}
							app.assideHandler.bindButtons(params.container);
						}, 1000);
					}
				});
				new carWheels(params.container.find('.interior > p .animated-car'));
				
				// ToDo третий слайд Альберт наговнокодил
				params.container.on('click','.mobile-angle', function () {
					let parent = $(this).parents('.slide'),
						interior = params.container
							.find('.interior'),
						exterior = params.container
							.find('.exterior'),
						form = parent.find('form').clone();
					
					if (!parent.hasClass('start-animation')) {
						parent.toggleClass('interior-open');
						fallBackIe.polyType = fallBackIe.polyType == 1 ? 0 : 1;
						// fallBackIe.resSvg();
						// fallBackIe.resMask();
						
						if (parent.hasClass('interior-open')){
							fallBackIe.aminOut();
						} else {
							fallBackIe.aminIn();
						}
						fallBackIe.hover = 325;
						
						//добавляем анимацию треугольнику
						parent.addClass('animate-triangle').addClass('start-animation');
						setTimeout(function () {
							parent.removeClass('animate-triangle');
						}, 1);
						
						if (!($('html.IE').length || $('html.MSIE').length)) {
							if (!exterior.find('form').length) {
								exterior.append(form);
							} else {
								interior.append(form);
							}
						}
						
						exterior.find('form').on('submit', function(e){
							e.preventDefault();
							return false;
						});
						interior.find('form').on('submit', function(e){
							e.preventDefault();
							return false;
						});
						
						setTimeout(function () {
							parent.removeClass('start-animation');
							if (!($('html.IE').length || $('html.MSIE').length)) {
								if (parent.hasClass('interior-open')) {
									exterior.find('form').remove();
								} else {
									interior.find('form').remove();
								}
							}
							app.assideHandler.bindButtons(params.container);
						}, 1500);
					}
				});
				
				
				if (!_isMobile) {
					params.container.on('click','.select-options', function (e) {
						e.stopPropagation();
					});
					params.container.on('click', '.checkbox-dropdown-btn', function (e) {
						e.stopPropagation();
						let $this = $(this),
							dropdown = $this.prev(),
							visible = dropdown.parents('.select-options').find('.checkbox-dropdown:visible');
						
						if (dropdown.is(':visible')) {
							// console.log(visible.length);
							var scrollBlock = $('.bottom-navigation .scroll-block');
							var height = $('.bottom-navigation .price-info').outerHeight();
							var top = scrollBlock.outerHeight() - height;
							
							scrollBlock.css({'top': -top + 'px', 'bottom': 'auto'});
							scrollBlock.animate({'height': height + 'px', 'top': 0}, 500, () => {
								$('.bottom-navigation').removeClass('opened');
								scrollBlock.css({'height': '', 'top': '', 'bottom': ''});
							});
							dropdown.slideUp(500, function () {
								dropdown.addClass('fade');
							});
						} else {
							// console.log(visible.length);
							
							var scrollBlock = $('.bottom-navigation .scroll-block');
							
							scrollBlock.animate({
								'height': scrollBlock.find('.first-ul').outerHeight() + 'px',
								'bottom': 0
							}, 300, () => {
								visible.slideUp(600, function () {
									visible.addClass('fade');
								});
								
								$('.bottom-navigation').addClass('opened');
								scrollBlock.css({'height': '', 'bottom': ''});
								dropdown.slideDown(600, function () {
									dropdown.removeClass('fade');
								});
							});
							
						}
					});
					
					params.container.on('click', function () {
						let dropdown = params.container.find('.checkbox-dropdown');
						if (dropdown.is(':visible')) {
							var scrollBlock = $('.bottom-navigation .scroll-block');
							var height = $('.bottom-navigation .price-info').outerHeight();
							var top = scrollBlock.outerHeight() - height;
							
							scrollBlock.css({'top': -top + 'px', 'bottom': 'auto'});
							scrollBlock.animate({'height': height + 'px', 'top': 0}, 500, () => {
								$('.bottom-navigation').removeClass('opened');
								scrollBlock.css({'height': '', 'top': '', 'bottom': ''});
							});
							dropdown.slideUp(500, function () {
								dropdown.addClass('fade');
							});
						}
					});
				}
				if (_isMobile) {
					params.container.on('click','.checkbox-dropdown-btn', function (e) {
						e.stopPropagation();
						let $this = $(this),
							dropdown = $this.prev(),
							visible = dropdown.parents('.select-options').find('.checkbox-dropdown:visible');
						
						if (dropdown.is(':visible')) {
							//console.log(visible.length);
							$('.bottom-navigation').removeClass('opened');
							dropdown.slideUp(700, function () {
								dropdown.addClass('fade');
							});
						} else {
							//console.log(visible.length);
							visible.slideUp(600, function () {
								visible.addClass('fade');
							});
							$('.bottom-navigation').addClass('opened');
							dropdown.slideDown(600, function () {
								dropdown.removeClass('fade');
							});
						}
					});
					
					params.container.on('click', function () {
						let dropdown = params.container.find('.checkbox-dropdown');
						if (dropdown.is(':visible')) {
							$('.bottom-navigation').removeClass('opened');
							dropdown.slideUp(600, function () {
								dropdown.addClass('fade');
								
							});
						}
					});
				}
				
				wasInit = true;
			}
			app.assideHandler.bindButtons(params.container);
		}
		
		static hide (callback=()=>{}) {
			if(typeof process.show != 'undefined' && process.show === true){
				//console.log('show уже выполняется');
				return;
			}
			
			let scope = this;
			scope.__findSlide();
			params.container
				.find('.animate-item')
				.removeClass('show stop');
			
			//params.container.find('.animated-car.car-main').show();
			let $exterior = params.container.find('.exterior');
			$exterior.removeClass('corner-show');
			setTimeout(function(){
				$exterior.removeClass('corner-transition');
				
				$('body').removeClass('slide-2-init');
				scope.__car360destroy();
				car.move(-1500, 0);
				callback(scope);
			}, 500);
		}
		
		static __init360 () {
			let scope = this;
			scope.__findSlide();
			
			$('.ModalView360').remove();
			let container = $('<div class="ModalView360"><div class="js-closeModalView360"></div></div>');
			container.css({
				position:'fixed',
				width: '100%',
				alignItems: 'center',
				justifyContent: 'center',
				top: 0,
				left: 0,
				height: '100%',
				zIndex: 99,
				background: '#fff',
				display: 'none'
			});
			$('body').append(container);
			
			if($('html.IE').length || $('html.MSIE').length){
				colorButtons = params.container.find('.color-select');
			}else{
				colorButtons = params.container.find('.exterior .color-select');
			}
			
			//console.log(params.container.find('.exterior .color-select'));
			//console.log(colorButtons);
			
			container.find('.js-closeModalView360').on('click', function(){
				container.css({
					opacity: 0
				});
				params.container.find('.bottom-navigation').prepend(colorButtons);
				container.removeClass('opened');
				
				container.on('transitionend',function(){
					container.css({
						display: 'none',
						transition:''
					});
					container.off('transitionend');
				});
			});
			
			let elements360InModal = $('.inNewWindow.car-360');
			if (elements360InModal.length) {
				$('.js-openModalView360').css({
					display: 'inline-block'
				});
				//car360Slide = '09';
			}
			container.append(elements360InModal);
			elements360InModal.removeClass('inNewWindow');
			
			let modal360Navigation  = $('<div class="bottom-navigation animate-item slide-top" style="opacity:1;"></div>');
			container.append(modal360Navigation);
			
			/*container.on('change', '[data-'+ params.data.colorInterior +']', function() {
				app.slides.selected.colorInterior = $(this).data(params.data.colorInterior);
				if($('.ModalView360').is(':visible')){
					$('.js-openModalView360').trigger('click');
				}
				
				app.slides.trigger('changeParams');
			});*/
			
			$('.js-openModalView360').off('click');
			$('.js-openModalView360').on('click',function(){
				var _this = $(this);
				if ( !_this.hasClass('js-openModalView360--show') ) return;
				
				if(!container.hasClass('opened')){
					container.addClass('opened');
					let trx = container.find('.threesixty');
					container.css({
						display: 'flex',
						opacity: 0,
						transition: 'all .7s ease',
						background: `#fff url(${trx.data('path') + trx.data('prefix') + '00' + trx.data('ext')}) center center/cover no-repeat`,
						cursor: "url('images/car-360/cursor_360view.png') 40 0, auto"
					});
					
					//if(!modal360Navigation.find('.color-select').length){
					modal360Navigation.addClass('show');
					//setTimeout(()=>{
						modal360Navigation.append(colorButtons);
						
					//}, 500);
					//}
					
					requestAnimationFrame( () => {
						requestAnimationFrame( () => {
							container.css({
								opacity: 1
							});
						});
					});
				}
				/*
				let dataset = colorButtons.find('input:checked').data(params.data.colorExterior);
				
				
				elements360InModal.each( function( index, item) {
					item = $(item);
					
					item.css({
						width: item.width(),
						display: 'none'
					});
					if ( item.data(params.data.item360) === dataset ) {
						console.log('Нужная 360-ка');
						console.log(item);
						
						
						item.css({
							display: 'block',
							width: '100%',
							height: '100%'
						});
						item.find('.threesixty_images').css({
							width: '100%',
							height: '100%',
							overflow: 'hidden',
							position: 'relative'
						});
						
						item.find('img').css({
							width: 'auto',
							height: 'auto',
							left: '-9999px',
							right: '-9999px',
							top: '-9999px',
							bottom: '-9999px',
							margin: 'auto',
							minWidth: '100%',
							minHeight: '100%'
						});
						
						//car360[dataset].switchToFrame(car360Slide);
					}
				});*/
			});
			
			
			if(!ready360){
				ready360 = true;
				let tt = null;
				$(document).on('change', '[data-'+ params.data.colorExterior +']', function(){
					/**
					//  Переключение 360 в попапе
					if($('.ModalView360').hasClass('opened')){
						params.container.find('.exterior .car-360.active .js-openModalView360').trigger('click');
					}
					*/
					
					let $newItem;
					let $curItem;
					let colorId = $(this).data(params.data.colorExterior);
					let is360InModal = $(`.ModalView360 .threesixty[data-${params.data.item360}=${colorId}]`).length;
					
					if (is360InModal) {
						$('.js-openModalView360').addClass('js-openModalView360--show');
						$('.ie-360-button').addClass('visible');
					} else {
						$('.js-openModalView360').removeClass('js-openModalView360--show');
						$('.ie-360-button').removeClass('visible');
					}
					
					
					if (params.container.hasClass('interior-open')) {
						$newItem = params.container.find(`.interior-wrapper [data-${params.data.item360}=${colorId}]`);
						$curItem = params.container.find(`.interior-wrapper [data-${params.data.item360}].active`);
					} else {
						// есть ли 360 в попапе, если нет то ищет на слайде 2
						if (is360InModal){
							$newItem = $(`.ModalView360 .threesixty[data-${params.data.item360}=${colorId}]`);
							$curItem = $(`.ModalView360 .threesixty[data-${params.data.item360}].active`);
						}else{
							$newItem = params.container.find(`.exterior-wrapper .threesixty[data-${params.data.item360}=${colorId}]`);
							$curItem = params.container.find(`.exterior-wrapper .threesixty[data-${params.data.item360}].active`);
						}
						//let is360 = $newItem.length;
						
						/*var $newItemInModal = $(`.ModalView360 .threesixty[data-${params.data.item360}=${colorId}]`);
						var $curItemInModal = $(`.ModalView360 .threesixty[data-${params.data.item360}].active`);
						$newItem = params.container.find(`.exterior-wrapper .threesixty[data-${params.data.item360}=${colorId}]`);
						$curItem = params.container.find(`.exterior-wrapper .threesixty[data-${params.data.item360}].active`);
						*/
						var $newItemCar = params.container.find(`.exterior-wrapper [data-${params.data.item360}=${colorId}]:not(.threesixty)`);
						var $curItemCar = params.container.find(`.exterior-wrapper [data-${params.data.item360}].active:not(.threesixty)`);
					}
					
					let itemId = $curItem.data(params.data.item360) || $curItemCar.data(params.data.item360);
					//  Определяем текущий слайд
					if(typeof car360[itemId] != 'undefined'){
						car360Slide = car360[itemId].getNormalizedCurrentFrame();
					}
					
					$curItemCar.removeClass('active');
					$curItem.removeClass('active');
					//$curItemInModal.removeClass('active');
					
					$newItemCar.addClass('active');
					$newItem.addClass('active');
					//$newItemInModal.addClass('active');
					
					// далее вспомогательные функции
					
					
					function logic360($newItem) {
						if(!$newItem.hasClass('initialized')){
							
							$curItem
								.removeClass('top')
								.addClass('bottom');
							$newItem
								.removeClass('bottom')
								.addClass('top');
							
							scope.__car360init($newItem, function(){
								//crossfadeItems($curItem, $newItem);
								//  Убираем авто с колёсами
								setTimeout(function(){
									$curItem.removeClass('bottom');
									//params.container.find('.animated-car.car-main').hide();
									let $container360 = params.container.find('.exterior-360');
									if(!$container360.hasClass('visible')){
										$container360.addClass('visible');
									}
								}, 1500);
							});
						}else{
							crossfadeItems($curItem, $newItem);
							car360[colorId].switchToFrame(car360Slide);
						}
					}
					
					function logicImage(){
						if ( !$newItemCar.length) return;
						
						crossfadeItems($curItemCar, $newItemCar);
						
						//  Убираем авто с колёсами
						setTimeout(function(){
							//params.container.find('.animated-car.car-main').hide();
							let $container360 = params.container.find('.exterior-360');
							if(!$container360.hasClass('visible')){
								$container360.addClass('visible');
							}
						}, 1500);
					}
					
					logic360( $newItem);
					logicImage();
					
					function crossfadeItems ($cur, $next) {
						$next.addClass('bottom');
						$cur.fadeOut(500, function(){
							$cur
								.removeClass('top')
								.show();
							$next
								.removeClass('bottom')
								.addClass('top');
						});
					}
					
					// function showItem(item) {
					// 	item.addClass('bottom');
					//
					// 	requestAnimationFrame( () => {
					// 		requestAnimationFrame( () => {
					// 			item
					// 				.removeClass('bottom')
					// 				.addClass('top');
					// 		});
					// 	});
					// }
					//
					// function hideItem(item) {
					// 	let a = $.Deferred();
					// 	item.fadeOut(10, function(){
					// 		a.resolve();
					// 		item
					// 			.removeClass('top')
					// 			.show();
					//
					// 	});
					// 	return a;
					// }
					
					
					
					/*
					params.container.find('[data-'+ params.data.item360 +'].top').each(function(){
						let itemId = $(this).data(params.data.item360);
						if(typeof car360[itemId] != 'undefined'){
							var config = car360[itemId].getConfig();
							car360Slide = config.endFrame;
						}

						$(this)
							.removeClass('top')
							.addClass('under-top')
							.addClass('visible');
						console.log('Перемещаем текущий элемент на уровень 2');
					});

					$item360.addClass('top');
					console.log('Перемещаем выбранный элемент на уровень 1');

					if(!$item360.hasClass('initialized')){
						scope.__car360init($item360, function(){
							//$item360.addClass('visible');
							clearTimeout(tt);
							tt = setTimeout(()=> {
								console.log("Прячем совсем предыдущий элемент");
								params.container.find('[data-'+ params.data.item360 +']')
									.removeClass('visible')
									.removeClass('under-top');
							}, 1500);

							setTimeout(function(){
								params.container.find('.animated-car.car-main').hide();
							}, 1500);
						});
					}else{
						clearTimeout(tt);
						tt = setTimeout(()=> {
							params.container.find('[data-' + params.data.item360 + ']')
								.removeClass('visible')
								.removeClass('under-top');
							console.log('Прячем оставшиеся элементы');
						}, 1500);



						//var config = car360[colorId].getConfig();
						//config.framerate = 0;
						//setTimeout(()=>{

							//car360[colorId].gotoAndPlay(car360Slide);
							car360[colorId].switchToFrame(car360Slide);
							//config.framerate = 60;
							setTimeout(()=>{
								$item360.addClass('visible');
							}, 200);
						//}, 100);
					}*/
				});
			}
			
			params.container.find('[data-'+ params.data.colorExterior +']:checked').trigger('change');
		}
		
		static __car360init ($item360, callback=()=>{}) {
			let itemId = $item360.data(params.data.item360);
			let frameNum = 59;
			let colors = app.slides.base.model[app.slides.selected.model].complectation[app.slides.selected.complectation].colorExterior;
			
			for(let k in colors) {
				let color = colors[k];
				
				//console.log(color.car360.frameNum);
				
				if(color.colorExteriorId == app.slides.selected.colorExterior && color.car360.frameNum){
					frameNum = color.car360.frameNum;
				}
			}
			
			let options = {
				totalFrames: frameNum,
				endFrame: frameNum,
				currentFrame: car360Slide,
				framerate: 10000,
				speedMultiplier:.1,
				imgList: '.threesixty_images',
				progress: '.spinner',
				ext: '.jpg',
				width: 1280,
				height: 720,
				navigation: false,
				//disableSpin: true,
				zeroPadding: true,
				zeroBased: true,
				responsive: true,
				imagePath: $item360.data('path'),
				filePrefix: $item360.data('prefix'),
				onReady: function(){
					/*var config = car360[itemId].getConfig();
					config.framerate = 60;
					car360[itemId].gotoAndPlay(car360Slide);
					setTimeout(function(){*/
					car360[itemId].switchToFrame(car360Slide);
					//$images.removeClass('hidden');
					callback();
					//}, 200);
				}
			};
			
			let ext = $item360.data('ext');
			if(ext){
				options['ext'] = ext;
			}
			let $images = $item360.find('.threesixty_images');
			if(!$images.length){
				callback();
				return;
			}
			//$images.addClass('hidden');
			
			car360[itemId] = $item360.ThreeSixty(options);
			$item360.addClass('initialized');
		}
		
		static isInitizlized(){
			return initialized;
		}
		
		static __car360destroy () {
			params.container.find('[data-'+ params.data.item360 +']')
				.removeClass('visible')
				.removeClass('under-top')
				.removeClass('top');
			
			//var $car = params.container.find('.car-360');
			//var $images = $car.find('.threesixty_images');
			//$images.empty();
		}
		
		static __findSlide () {
			params.container = $(params.selector.slide);
		}
		
	}
})();
