/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 27.07.2017
 * Time: 18:42
 */

window['Slide_1'] = (function () {
	var params = {
		selector: {
			slide: '[data-slide=1]',
		},
		fadeDuration: 1000,
		data: {
			complId: 'compl-id',
			slideId: 'slide-id',
			slideInfo: 'slide-info',
			hideItem: 'hide-item',
		},
		container: null,
	};
	var isMob = $('html.mobile').length;

	var initialized = false;
	var car = null,
		carSliderWrap = $('[data-slide=1] .car-slider'),
		carSliderHtml,
		carSlider = new MasterSlider(),
		counter,
		speed = !$('html.MSIE:not(.Edge)').length ? 25 : 35,
		space = !$('html.MSIE:not(.Edge)').length ? 0 : 50,
		slider,
		activeCar,
		carSelected = false,
		firstInit = true;

	if ($('html.tablet').length) {
		$(window).on('orientationchange', function () {
			setTimeout(function () {
				params.container.find('.car-slider__wrap').addClass('opacity');
				Slide_1.gotoSlide(carSlider.api.index() + 1, false);
			}, 10);
		});
	}

	return class Slide_1 {
		static render() {
			let scope = this;
			scope.__findSlide();

			let modelData = app.slides.base.model[app.slides.selected.model];
/*
			let arrCompl = [];
			for(let k in modelData.complectation){
				arrCompl.push(modelData.complectation[k]);
			}
			let modelTpl = modelData;
			modelTpl.complectation = sortByKey(arrCompl, 'price');
			*/

			let complectations = $.extend({}, modelData.complectation);
            complectations = Object.keys(complectations).map(i => complectations[i]);
            complectations = sortByKey(complectations, 'price');

			let slideHtml = views.slide1Slider({car: modelData, complectations: complectations});

			//console.log(modelData);

			carSliderWrap.html(slideHtml);

			if (activeCar != undefined && activeCar == modelData.name) {
				carSelected = false;
			} else {
				carSelected = true;
				activeCar = modelData.name;
			}
			if (typeof modelData.brochure != 'undefined' && modelData.brochure) {
				params.container.find('[data-brochure-link]').html(`<a target="_blank" href="${modelData.brochure}" class="btn-default">Скачать брошюру</a>`)
			}

			let tt = null;

			if ($('html.tablet').length || $('html.mobile').length) {
				$(window).on('orientationchange', function () {
					let api = carSlider.api;
					let index = (api)?api.index()+1:1;

					clearTimeout(tt);
					tt = setTimeout(() => {
						params.container.find('.car-slider__wrap').addClass('opacity');
						scope.gotoSlide(index, false);
					}, 100)
				});
			}
			if ($('html.desktop').length) {
				$(window).on('resize', function () {
					let api = carSlider.api;
					let index = (api)?api.index()+1:1;

					clearTimeout(tt);
					tt = setTimeout(() => {
						params.container.find('.car-slider__wrap').addClass('opacity');
						scope.gotoSlide(index, false);
					}, 100)
				});
			}
		}

		static init() {
			let scope = this;
			scope.__findSlide();

			if (firstInit) {
				$('body').on('click', '[data-dop-text]', function (e) {
					e.preventDefault();
					e.stopPropagation();
					let $this = $(this),
						status = $this.attr('data-dop-text');
					$this.attr('data-dop-text', $this.text());
					$this.text(status);

					let specs = $this.parents('.will-hide').find('.specs');
					if(!isMob){
						$.each(specs, function () {
							let $this = $(this);
							if ($this.hasClass('spec-hidden')) {
								$this.animate({'opacity': 1}, 300, function () {
									$this.removeClass('spec-hidden');
								});

							} else {
								$this.animate({'opacity': 0}, 300, function () {
									$this.addClass('spec-hidden');
								});

							}
						});
					}else{
						$.each(specs, function () {
							let $this = $(this);
							if ($this.is(':hidden')) {
								//$this.fadeIn(300, function () {
									$this.removeClass('spec-hidden');
								//});

							} else {
								//$this.fadeOut(300, function () {
									$this.addClass('spec-hidden');
								//});

							}
						})
					}

				});

				firstInit = false;
			}
			params.container.find('[data-' + params.data.complId + ']').off('click');
			params.container.find('[data-' + params.data.complId + ']').on('click', function (e) {
				e.stopPropagation();
				var button = this;
				app.slides.selected.complectation = $(button).data(params.data.complId);

				if (_isMobile) {
					var position = $(this).parents('.ms-slide').position().top -
						$(this).parents('.ms-slide').find('.image').outerHeight() / 2.5;

					$('[data-slide=1]').animate({
						scrollTop: position
					}, params.fadeDuration);

				}

				params.container.find('[data-' + params.data.slideId + ']').each(function () {
					//console.log($(this).has(button).length);
					if ($(this).has(button).length) {
						$(this).find('[data-' + params.data.slideInfo + ']').animate({'opacity': 0}, params.fadeDuration);
						$(this).removeClass('ms-sl-selected');
						car = new carWheels($(this).find('.animated-car'));
					} else {
						$(this).find('div').animate({'opacity': 0}, params.fadeDuration);
					}
				});
				params.container.find('[data-' + params.data.hideItem + ']').animate({'opacity': 0}, params.fadeDuration);

				setTimeout(function () {
					car.move($(document).width() / 2, 2);
					setTimeout(function () {
						app.slides.toSlide(2);
						setTimeout(function () {
							car.move(0, 0);
						}, 1000);
					}, 1000);
				}, params.fadeDuration);
			});
			initialized = true;
		}

		static show() {
			let scope = this;
			scope.__findSlide();
			params.container
				.find('.animate-item')
				.addClass('show');

			params.container.find('[data-' + params.data.slideInfo + ']').show().css('opacity', '');
			params.container.find('[data-' + params.data.hideItem + ']').show().css('opacity', '');



			// ToDo поправить...
			var $items = params.container.find('.car-slide');
			if (!$('html').hasClass('mobile')) {
				params.container.find('[data-' + params.data.slideId + '] div').show().css('opacity', '');
			} else {
				$items.find('.car-container, .icon-down').on('click', function (e) {
					e.stopPropagation();

					let $item = $(this),
						$will = $item.parents('.car-slide').find('.will-hide'),
						$icon = $item.parents('.car-slide').find('.icon-down');

					if (!$will.hasClass('will-more')) {
						$will.addClass('will-more');
						$icon.addClass('reverse');
					} else {
						$will.removeClass('will-more');
						$icon.removeClass('reverse');
					}

				})
			}


			$('.mobile-panel').hide();

			carSliderHtml = carSliderWrap.html(),
				counter = $('.ms-nav-count'),
				slider = {
					nextSlide: function nextSlide() {
						carSliderWrap.removeClass('remove');
						carSlider.api.next();
						this.counterUpdate();
					},
					prevSlide: function prevSlide() {
						carSliderWrap.removeClass('remove');
						carSlider.api.previous();
						this.counterUpdate();
					},
					counterUpdate: function counterUpdate() {
						counter.html('<sapn>' + (parseInt(carSlider.api.index()) + 1) + '/' + carSlider.api.count() + '</sapn>');
					},
					addInfo: function addInfo() {
						// console.log('add')
						carSliderWrap.find('.will-hide').removeClass('animate');
						carSliderWrap.find('.ms-sl-selected .will-hide').addClass('animate');
					},
					removeInfo: function removeInfo() {
						carSliderWrap.find('.will-hide').removeClass('animate');
					}
				};
			//
			// if (isMob) {
			// 	// params.container.find('.specs').mCustomScrollbar();
			// }
			if (!isMob) {


				if (!carSliderWrap.hasClass('master-slider')) {


					//	переход к слайду, по URL
					if(app.slides.start && app.slides.startParams.complectation){
						let $activeSlide = params.container.find('[data-' + params.data.slideId + '='+app.slides.startParams.complectation+']');
						if($activeSlide.length){
							let slideOffset = 2,
								index = $activeSlide.index() + 1,
								slidesNum = params.container.find('[data-' + params.data.slideId + ']').length;

							/*let slidePos = index + slideOffset;*/
							let slidePos = index;

							if(slidePos > slidesNum){
								slidePos = slidePos - slidesNum;
							}
							Slide_1.initSlide(slidePos);
						}
					}else{
						Slide_1.initSlide(1);
					}
				} else {
					if (carSelected) {
						params.container
							.find('.car-slider__wrap').addClass('opacity');
						Slide_1.gotoSlide(1);

					} else {
						params.container
							.find('.car-slider__wrap').addClass('opacity');
						Slide_1.gotoSlide(carSlider.api.index() + 1);
					}
				}
			}
			app.assideHandler.bindButtons(params.container);
		}

		static initSlide(slide = 1) {

			carSliderHtml = carSliderWrap.html(),
				counter = $('.ms-nav-count'),
				slider = {
					nextSlide: function () {
						carSliderWrap.removeClass('remove');
						carSlider.api.next();
						this.counterUpdate();
					},
					prevSlide: function () {
						carSliderWrap.removeClass('remove');
						carSlider.api.previous();
						this.counterUpdate();
					},
					counterUpdate: function () {
						counter.html('<span>' + (parseInt(carSlider.api.index()) + 1) + '/' + carSlider.api.count() + '</span>');
					},
					addInfo: function () {
						// console.log('add')
						carSliderWrap.find('.will-hide').removeClass('animate');
						carSliderWrap.find('.ms-sl-selected .will-hide').addClass('animate');
					},
					removeInfo: function () {
						carSliderWrap.find('.will-hide').removeClass('animate');
					}
				};

			//console.log('init');
			var prevbtn = carSliderWrap.prev('.car-slider--arrow').find('.ms-nav-prev'),
				nextbtn = carSliderWrap.prev('.car-slider--arrow').find('.ms-nav-next'),
				start = slide == 1 ? carSliderWrap.find('.ms-slide').length : slide - 1;

			carSlider.setup(carSliderWrap, {
				width: 500,
				height: 900,
				space: space,
				loop: true,
				swipe: false,
				instantStartLayers: true,
				speed: speed,
				view: 'fadeWave',
				layout: 'autofill',
				start: start
			});

			//The slider starting slide number.
			$('body').on('click', '.car-slide--wrap', function () {
				var $thisParent = $(this).parent();
				if (!$thisParent.hasClass('ms-sl-selected')) {
					if (!carSliderWrap.hasClass('wait')) {
						carSliderWrap.addClass('wait');
						slider.prevSlide();
						setTimeout(function () {
							carSliderWrap.removeClass('wait');
						}, 2000);
					}
				}
			});

			$('body').on('click', '.ms-sl-selected .image', function (e) {
				e.preventDefault();
				e.stopPropagation();
				$(this).parents('.ms-sl-selected').find('[data-compl-id]').trigger('click');
			});

			prevbtn.on('click', function () {
				if (!carSliderWrap.hasClass('wait')) {
					carSliderWrap.addClass('wait');
					slider.removeInfo();
					setTimeout(function () {
						slider.prevSlide();
					}, 800);
					setTimeout(function () {
						carSliderWrap.removeClass('wait');
					}, 2000);
				}
			});
			nextbtn.on('click', function () {
				if (!carSliderWrap.hasClass('wait')) {
					carSliderWrap.addClass('wait');
					slider.removeInfo();
					setTimeout(function () {
						slider.nextSlide();
					}, 800);
					setTimeout(function () {
						carSliderWrap.removeClass('wait');
					}, 2500);
				}
			});



			carSlider.api.addEventListener(MSSliderEvent.CHANGE_END, function () {

				slider.addInfo();
				slider.counterUpdate();
				carSliderWrap.removeClass('drag');

				app.slides.selected.complectation = carSlider.api.currentSlide.$element.data('slide-id');
				app.slides.trigger('changeParams');

			});

			if ($('.ms-slide.car-slide').length > 2) {

				let divToPrevPrevSlide;
				carSlider.api.addEventListener(MSSliderEvent.CHANGE_END, function () {
					divToPrevPrevSlide && divToPrevPrevSlide.remove();
					divToPrevPrevSlide = $('<div style="padding:100px;position:absolute;right:0;top:20%;transform:translateZ(10px) translateX(200px);"></div>');

					divToPrevPrevSlide.on('click',function(){
						slider.prevSlide();
						slider.prevSlide();
					});

					let index = $('.ms-slide.car-slide').index($('.ms-sl-selected')[0]) - 2;

					index = index < 0 ? $('.ms-slide.car-slide').length + index: index;

					$('.ms-slide.car-slide')[index].appendChild(divToPrevPrevSlide[0]);

				});
			}

			carSlider.api.view.addEventListener(MSViewEvents.SWIPE_START, function () {
				carSliderWrap.find('.will-hide').removeClass('animate');
				carSliderWrap.addClass('drag');
			});

			if (!isMob) {
				params.container.find('.specs').mCustomScrollbar();
			}

			if (!isMob) {
				setTimeout(function () {
					carSlider.api.gotoSlide(slide);
					slider.addInfo();
					slider.counterUpdate();

					//добавляем класс wait для предотвращения случайных кликов
					carSliderWrap.addClass('wait');
					slider.prevSlide();
					setTimeout(() => {
						carSliderWrap.removeClass('wait');
					}, 2000);
				}, 10)
			}
		}

		static gotoSlide(slide, anim = true) {
			let start = slide == 1 ? carSlider.api.count() : slide - 1;
			start = anim ? start : slide;
			carSlider.api.destroy();
			$('.car-slider').remove();
			$('.car-slider--arrow').after('<div class="car-slider">' + carSliderHtml + '</div>');

			carSliderWrap.removeClass('wait');

			setTimeout(() => {
				carSliderWrap = $('[data-slide=1] .car-slider');
				carSlider = new MasterSlider();
				carSlider.setup(carSliderWrap, {
					width: 500,
					height: 900,
					space: space,
					loop: true,
					swipe: false,
					instantStartLayers: true,
					speed: speed,
					view: 'fadeWave',
					layout: 'autofill',
					start: start, //The slider starting slide number.
				});

				if ($('.ms-slide.car-slide').length > 2) {

					let divToPrevPrevSlide;
					carSlider.api.addEventListener(MSSliderEvent.CHANGE_END, function () {
						divToPrevPrevSlide && divToPrevPrevSlide.remove();
						divToPrevPrevSlide = $('<div style="padding:100px;position:absolute;right:0;top:20%;transform:translateZ(10px) translateX(200px);"></div>');

						divToPrevPrevSlide.on('click',function(){
							slider.prevSlide();
							slider.prevSlide();
						});

						let index = $('.ms-slide.car-slide').index($('.ms-sl-selected')[0]) - 2;

						index = index < 0 ? $('.ms-slide.car-slide').length + index: index;

						$('.ms-slide.car-slide')[index].appendChild(divToPrevPrevSlide[0]);

					});
				}

				params.container.find('.specs').mCustomScrollbar();
				carSlider.api.addEventListener(MSSliderEvent.CHANGE_END, function () {
					slider.addInfo();
					slider.counterUpdate();
					carSliderWrap.removeClass('drag');

					app.slides.selected.complectation = carSlider.api.currentSlide.$element.data('slide-id');
					app.slides.trigger('changeParams');
				});

				setTimeout(() => {
					// slider.addInfo();
					// slider.counterUpdate();

					if (anim) {
						carSlider.api.next();

						//добавляем класс wait для предотвращения случайных кликов
						carSliderWrap.addClass('wait');
						slider.prevSlide();
						setTimeout(() => {
							carSliderWrap.removeClass('wait');
						}, 2000);
					}
					setTimeout(() => {
						carSlider.api.next();
						slider.addInfo();
						params.container
							.find('.car-slider__wrap').removeClass('opacity');

						params.container.find('[data-' + params.data.complId + ']').off('click');
						params.container.find('[data-' + params.data.complId + ']').on('click', function (e) {
							e.stopPropagation();
							var button = this;
							app.slides.selected.complectation = $(button).data(params.data.complId);

							params.container.find('[data-' + params.data.slideId + ']').each(function () {
								//console.log($(this).has(button).length);
								if ($(this).has(button).length) {
									$(this).find('[data-' + params.data.slideInfo + ']').fadeOut(params.fadeDuration);
									$(this).removeClass('ms-sl-selected');
									car = new carWheels($(this).find('.animated-car'));
								} else {
									$(this).find('div').fadeOut(params.fadeDuration);
								}
							});
							params.container.find('[data-' + params.data.hideItem + ']').fadeOut(params.fadeDuration);

							setTimeout(function () {
								car.move($(document).width() / 2, 2);
								setTimeout(function () {
									app.slides.toSlide(2);
									setTimeout(function () {
										car.move(0, 0);
									}, 1000);
								}, 1000);
							}, params.fadeDuration);
						});
					}, 10);
				}, 100);
			}, 1);
		}


		static hide(callback = () => {
		            }) {
			let scope = this;
			scope.__findSlide();
			params.container
				.find('.animate-item')
				.removeClass('show');

			callback(scope);
		}

		static isInitizlized() {
			return initialized;
		}

		static __findSlide() {
			params.container = $(params.selector.slide);
		}

	}
})();
