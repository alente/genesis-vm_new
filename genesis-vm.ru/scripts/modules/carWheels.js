/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 29.07.2017
 * Time: 17:07
 */


window['carWheels'] = (function(){
	return class carWheels {
		constructor ($element, params = {}) {
			let scope = this;

            scope.params = {
                selector: {
                    wheel: '[class^=wheel]',
                },
                move: {
                    duration: 1000,
                    wheelDiameter: 96,
                },
                //regNum: /^(?:\+=|\-=)?(-\d+|\d+)(?:[^\d]+)?$/i,
            };
            $.extend( true, scope.params, params );

            scope.__$container = $element;
            scope.__$wheel = scope.__$container.find(scope.params.selector.wheel);
            scope.__perimeter = scope.params.move.wheelDiameter * Math.PI;

            let startPosition = scope.__$container.data('position');
            if(!startPosition) startPosition = 0;

            //if(startPosition){
            scope.move(startPosition, 0);
            //}
        }

        /**
         * Ехать
         * @param x - Относительная позиция, px
         */
        move (x=0, transition=1) {

            let scope = this,
                params = {},
	            transitionClass,
                angle = x * (360 / scope.__perimeter);


            if(transition > 0){
                switch (transition){
                    case 2: transitionClass = 'car-transition-preview'; break;
                    case 1: default: transitionClass = 'car-transition'; break;
                }
                if(transitionClass){
	                scope.__$container.addClass(transitionClass);
	                scope.__$wheel.addClass(transitionClass);
                }
            }

            params.left =  x + 'px';

            scope.__$container.css(params);
            scope.__$wheel.css({transform: 'rotate('+ angle +'deg)'});

            if(transition) {
                setTimeout(() => {
	                if(transitionClass){
		                scope.__$container.removeClass(transitionClass);
		                scope.__$wheel.removeClass(transitionClass);
                    }
            }, scope.params.move.duration);
            }
        }
    }
})();