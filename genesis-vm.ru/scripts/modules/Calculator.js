/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 09.08.2017
 * Time: 2:53
 */


class Calculator extends Boreas.module {
	constructor (selector='body') {
		super(selector);

		let scope = this;

		scope.params = {
			initialFeePercent: 55,
			initialPeriod: 60,
		};
		scope.selected = {};
		scope.last = {};
		scope.$container = $(selector);
		scope.fields = {
			model: null,
			compl: null,
			interior: null,
		};
		scope.programs = {};
		scope.initialFeeLast = null;
		scope.periodLast = null;

		for(let k in app.slides.finance.creditProgram){
			let program = app.slides.finance.creditProgram[k];
			scope.programs[program.id] = program;
		}
	}

	initialize (params = {}) {
		let scope = this;
		scope.$form = scope.$container.find('form');
		scope.fields.model = scope.$container.find("#getModel");
		scope.fields.compl = scope.$container.find("#getComplect");
		scope.fields.interior = scope.$container.find("#interior");
		scope.fields.package = null;
		scope.fields.initialFee = scope.$container.find('#initialFee');
		scope.fields.sumCredit = scope.$container.find('[data-credit-sum]');
		scope.fields.creditProgram = scope.$container.find('[data-credit-program]');
		scope.fields.period = scope.$container.find('[data-period]');
		scope.packagesList = scope.$container.find("[data-package-items]");
		scope.initialFeeContainer = scope.$container.find("[data-initial-fee-container]");
		scope.carPrice = scope.$container.find("[data-car-price]");
		scope.creditPrograms = scope.$container.find("[data-credit-programs]");
		scope.monthPayment = scope.$container.find("[data-month-payment]");
		scope.rate = scope.$container.find("[data-rate]");

		scope.__initDefaults ();
		scope.__initFields ();

		scope.fields.model.trigger('change');
		scope.fields.compl.trigger('change');
		scope.fields.interior.trigger('change');

		scope.$form.on('submit', function(e){
			e.preventDefault();
			/**
			 * carPrice:2550000
			 * colorInterior:1
			 * complectation:1
			 * initialFee:2040000
			 * model:"g80"
			 * month_payment:24873
			 * package:undefined
			 * period:24
			 * program:194
			 * rate:15.6
			 * sumCredit:510000
			 */

			let filePDF = app.slides.getPdfUrl({
				credit: true,
				program: scope.programs[scope.selected.program].front_name,                                 // Программа кредитования
				amount_credit: app.slides.formatPrice(scope.selected.sumCredit) + ' руб.',            // Сумма кредита
				first_pay: app.slides.formatPrice(scope.selected.initialFee) + ' руб.',               // Стартовый платёж
				percent: scope.selected.rate + '%',                                                   // Процентрная ставка
				monthly_payment: app.slides.formatPrice(scope.selected.month_payment) + ' руб.'       // Ежемесячный платеж
			}, true, true);

			var model = app.slides.base.model[scope.selected.model];
			var crmModel;
			switch(model.code){
				case 'g70': crmModel = 'G70'; break;
				case 'g80': crmModel = 'G80'; break;
				case 'g90': case 'g90l': crmModel = 'G90'; break;
			}

			app.assideHandler.showAsside('inquiryDealer', {
				lead_type: 'CREDIT_INQUIRY',
				model: crmModel,   // Модель
				//city: "Москва",
				//dealer: "C40AF10120",
				file: filePDF,
			});
		});
	}

	getDefaultCalculation () {
		let scope = this;
		scope.__initDefaults();
		let programs = scope.__getPrograms();

		if(programs.length){
			let minRate = null;
			let minProgram = null;
			for(let k in programs){
				let program = programs[k];
				let rate = parseFloat(program.rate);
				if(!minRate || rate < minRate){
					minRate = rate;
					minProgram = program;
				}
			}
			if(minProgram){
				scope.selected.program = parseInt(minProgram.id);
				scope.selected.rate = parseFloat(minProgram.rate);
				scope.selected.month_payment = scope.__getMonthPayment(minProgram);
			}
		}

		return scope.selected;
	}

	__initDefaults () {
		let scope = this;

		scope.selected = {};
		scope.selected.model = app.slides.selected.model;
		scope.selected.complectation = app.slides.selected.complectation;
		scope.selected.colorExterior = app.slides.selected.colorExterior;
		scope.selected.colorInterior = app.slides.selected.colorInterior;
		scope.selected.package = app.slides.selected.package;
		scope.selected.carPrice = 0;
		scope.selected.initialFee = 0;
		scope.selected.sumCredit = 0;
		scope.selected.period = scope.params.initialPeriod;
		scope.selected.program = null;
		scope.selected.rate = 0;
		scope.selected.month_payment = 0;

		//scope.selected = Object.assign({}, app.slides.selected);
		if(typeof scope.selected.model == 'undefined' || !scope.selected.model){
			let model = scope.__getFirstProperty(app.slides.base.model);
			scope.selected.model = model.code;
		}
		if(typeof scope.selected.complectation == 'undefined' || !scope.selected.complectation){
			let compl = scope.__getFirstProperty(app.slides.base.model[scope.selected.model].complectation);
			scope.selected.complectation = compl.id;
		}
		if(typeof scope.selected.colorInterior == 'undefined' || !scope.selected.colorInterior){
			let colorId = scope.__getFirstProperty(app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation].colorInterior);
			scope.selected.colorInterior = colorId;
		}
		if(typeof scope.selected.colorExterior == 'undefined' || !scope.selected.colorExterior){
			let colorId = null;
			let color = scope.__getFirstProperty(app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation].colorExterior);
			if(typeof color != 'undefined' && color){
				let colorId = scope.__getFirstProperty(app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation].colorExterior).colorExteriorId;
			}
			scope.selected.colorExterior = colorId;
		}
		if(typeof scope.selected.package == 'undefined' || !scope.selected.package){
			//let packageId = scope.__getFirstProperty(app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation].package);
			//scope.selected.package = packageId;
			scope.selected.package == null;
		}

		let complectation = app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation];
		let price = parseInt(complectation.price);
		if(scope.selected.package){
			let packageObj = app.slides.base.package[scope.selected.package];
			price += parseInt(packageObj.price);
		}

		scope.selected.carPrice = price;
		scope.selected.initialFee = scope.selected.carPrice / 100 * scope.params.initialFeePercent;
		scope.selected.sumCredit = scope.selected.carPrice - scope.selected.initialFee;
		scope.selected.period = scope.params.initialPeriod;

		//scope.selected.month_payment = 0;
		//scope.selected.rate = 0;
	}

	__initFields () {
		let scope = this;
		scope.fields.model.select2();
		scope.fields.compl.select2();
		scope.fields.interior.select2({
			templateResult: scope.__formatInterior,
			templateSelection: scope.__formatInterior
		});

		scope.fields.model.on('change', function(){
			let value = scope.fields.model.val();
			scope.selected.model = value;
			scope.__renderCompl(value);
			scope.fields.compl.trigger('change');
		});
		scope.fields.interior.on('change', ()=>{
			scope.selected.interior = scope.fields.interior.val();
		});
		scope.fields.initialFee.on('change', ()=>{
			//
		});

		scope.__renderCompl(scope.selected.model);
		scope.__renderInterior(scope.selected.complectation);
		scope.__renderPackages(scope.selected.complectation);
		scope.__initInitialFee(app.slides.selected);
		//scope.__renderPrograms();
	}

	__formatCompl(state) {
		if (!state.id) {
			return state.text;
		}
		let result = '',
			$elt = $(state.element);

		result += state.text;
		if($elt.data('price')){
			result += ' <span class="select2-price-value">' + $elt.data('price') + ' <span class="pt-rub">ю</span></span>';
		}
		result = '<span>' + result + '</span>';
		return $(result);

	}

	__formatInterior(state) {
		if (!state.id) {
			return state.text;
		}
		let result = '',
			$elt = $(state.element);

		result += '<span class="select2-selection__image"><img src="'+ $elt.attr('data-icon-1') +'" class="half" /><img src="'+ $elt.attr('data-icon-2') +'" class="half" /></span>';
		result += '<span class="select2-selection__caption">' + state.text + '</span>';
		return $(result);

	}

	__renderCompl (modelId) {
		let scope = this,
			resultHtml = '',
			model = app.slides.base.model[modelId];
		
        let complectations = $.extend({}, model.complectation);
		complectations = Object.keys(complectations).map(i => complectations[i]);
		complectations = sortByKey(complectations, 'price');

		let i = 0;
		for(let k in complectations){
			i++;
			let complectation = complectations[k];
			let selected = '';
			if(scope.selected.complectation == complectation.id){
				selected = ' selected';
			}
			resultHtml += `<option data-desc="${complectation.modificationName}" data-price="${app.slides.formatPrice(complectation.price)}" ${selected} value="${complectation.id}">${complectation.name}</option>`;
			//resultHtml += '<option data-desc="'+ complectation.modificationName +'"'+ selected +' value="'+ complectation.id +'">'+ complectation.name +' ('')</option>';
		}
		let instance = scope.fields.compl.data('select2');
		if(instance){
			scope.fields.compl.select2("destroy");
		}
		scope.fields.compl.html(resultHtml);
		scope.fields.compl.select2({
			templateResult: scope.__formatCompl,
			templateSelection: scope.__formatCompl
		});
		scope.fields.compl.on('change', function(){
			let value = scope.fields.compl.val();
			scope.selected.complectation = value;

			scope.__initInitialFee(scope.selected);
			scope.__renderInterior(value);
			scope.__renderPackages(value);

		});
	}

	__renderInterior (complId) {
		let scope = this,
			resultHtml = '',
			model = app.slides.base.model[scope.selected.model],
			complectation = model.complectation[complId];

		let i = 0;
		for(let k in complectation.colorInterior){
			i++;
			let colorId = complectation.colorInterior[k],
				colorInterior = app.slides.base.colorInterior[colorId];
			let selected = '';
			if(i==1){
				selected = ' selected';
			}
			resultHtml += '<option data-icon-1="'+ colorInterior.icon1 +'" data-icon-2="'+ colorInterior.icon2 +'"'+ selected +' value="'+ colorId +'">'+ colorInterior.description +'</option>';
		}

		let instance = scope.fields.interior.data('select2');
		if(instance){
			scope.fields.interior.select2("destroy");
		}
		scope.fields.interior.html(resultHtml);
		scope.fields.interior.select2({
			templateResult: scope.__formatInterior,
			templateSelection: scope.__formatInterior
		});
	}

	__renderPackages (complId) {
		let scope = this,
			resultHtml = '',
			valuesList = '',
			model = app.slides.base.model[scope.selected.model],
			complectation = model.complectation[complId];

		let i = 0;

		let packageList = [];
		packageList.push({
			"id": 0,
			"name": "Без пакета",
			"price": 0,
			"values": []
		});

		for(let k in complectation.package){
			let packageId = complectation.package[k],
				packageObj = app.slides.base.package[packageId];
			packageList.push(packageObj);
		}

		for(let k in packageList){
			i++;
			let packageObj = packageList[k];
			let checked = '';

			if(scope.selected.package && packageObj.id == scope.selected.package || !scope.selected.package && i == 1){
				checked = ' checked';
			}

			if(packageObj.values.length){
				for (let kVal in packageObj.values){
					valuesList += packageObj.values[kVal].name + '<br>';
				}
				valuesList = `<div class="asside--group-label">${valuesList}</div>`;
			}

			resultHtml += `<li class="asside--group-item">
								<div class="form-element">
									<input name="package" id="form-package-${packageObj.id}" type="radio" value="${packageObj.id}" class="form-element--checkbox-input" ${checked}>
									<span class="form-element--checkbox-header">
										<span class="form-element--checkbox-header-text">${packageObj.name}</span>
									</span>
									<label for="form-package-${packageObj.id}" class="form-element--checkbox form-element--checkbox-options">
										<span class="form-element--checkbox-sub">
											${app.slides.formatPrice(packageObj.price)} <span class="pt-rub">ю</span>
										</span>
									</label>
								</div>
								${valuesList}
							</li>`;
		}

		scope.packagesList.html(resultHtml);
		app.assideHandler.__initCheckbox(scope.packagesList);
		
		scope.fields.package = scope.$container.find("[name=package]");
		scope.fields.package.on('change', ()=>{
			scope.fields.package.each(function(){
				if(!$(this).is(':checked')) return;
				scope.selected.package = $(this).val();
			});
			
			let complectation = app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation];
			let packagePrice = 0;

			//console.log
			if(scope.selected.package && typeof app.slides.base.package[scope.selected.package] != 'undefined'){
				let packageObj = app.slides.base.package[scope.selected.package];
				packagePrice = packageObj.price;
			}
			
			let price = parseInt(complectation.price) + parseInt(packagePrice);
			
			//console.log(complectation);
			//console.log(packageObj);
			//console.log(price);
			
			scope.selected.carPrice = price;
			scope.__initInitialFee(scope.selected);
			//scope.fields.initialFee.trigger('input');
		});
	}

	__renderPrograms () {
		let scope = this,
			resultHtml = '',
			complectation = app.slides.base.model[scope.selected.model].complectation[scope.selected.complectation];

		let programs = scope.__getPrograms();
		
		//console.log(scope.selected.program);
		
		let i = 0;
		let selectedFound = false;
		for(let k in programs){
			i++;
			let program = programs[k];
			
			let checked = '';
			if(program.id == scope.selected.program){
				selectedFound = true;
				checked = ' checked';
			}else if(i==1 && !selectedFound){
				checked = ' checked';
			}
			
			//let checked = (program.id == scope.selected.program || i==1)?' checked':'';
			if(i==1 && checked.length){
				scope.selected.program = parseInt(program.id);
			}
			
			let payment = scope.__getMonthPayment(program);

			resultHtml += `<li class="asside--group-item">
							<div class="form-element">
								<input id="calc-program-${program.id}" type="radio" name="program" value="${program.id}" class="form-element--checkbox-input" data-credit-program ${checked}>
								<span class="form-element--checkbox-header">
									<span class="form-element--checkbox-header-text">${program.front_name}</span>
								</span>
								<label for="calc-program-${program.id}" class="form-element--checkbox form-element--checkbox-options">
									<span class="form-element--checkbox-sub">
										${app.slides.formatPrice(payment)} <span class="pt-rub">ю</span> /мес.
									</span>
								</label>
							</div>

							<div class="asside--group-label">${program.description}</div>
						</li>`;
		}

		scope.creditPrograms.html(resultHtml);
		scope.fields.creditProgram = scope.$container.find('[data-credit-program]');

		app.assideHandler.__initCheckbox(scope.creditPrograms);
		scope.fields.creditProgram.on('change', function(){
			scope.fields.creditProgram.each(function(){
				if(!$(this).is(':checked')) return;
				scope.selected.program = parseInt($(this).val());
			});
			let program = scope.programs[scope.selected.program];
			scope.selected.rate = parseFloat(program.rate);
			scope.rate.text(scope.selected.rate);

			let payment = scope.__getMonthPayment(program);
			scope.selected.month_payment = payment;
			scope.monthPayment.text(app.slides.formatPrice(payment));
		});
		scope.fields.creditProgram.trigger('change');
		scope.__initTerms();
	}

	__getMonthPayment (program) {
		let scope = this;
		let monthRate = parseFloat(program.rate) / 12 / 100;
		let annuCoef = monthRate + (monthRate / (Math.pow((1 + monthRate), scope.selected.period) - 1));
		
		let sumCredit = scope.selected.sumCredit;
		if(parseInt(program.restcost) > 0){ //  Остаточная стоимость
			let restCost = sumCredit / 100 * parseInt(program.restcost);
			sumCredit = sumCredit - restCost;
		}
		let monthPayment = annuCoef * sumCredit;

		return parseInt(monthPayment);
	}

	__getPrograms () {
		let scope = this,
			result = [],
			prepayPercent = 100 / scope.selected.carPrice * scope.selected.initialFee/*,
			prepayInterval = scope.__getPrepayInterval()*/;

		for(let k in scope.programs){
			let program = scope.programs[k];
			
			if(program.models.indexOf(scope.selected.model) == -1) continue;
			
			if((program.min_credit > 0 && scope.selected.sumCredit < program.min_credit) || (program.max_credit > 0 && scope.selected.sumCredit > program.max_credit)){
				continue;
			}
			if((program.min_prepay > 0 && prepayPercent < program.min_prepay) || (program.max_prepay > 0  &&prepayPercent > program.max_prepay)) {
				continue;
			}
			result.push(program);
		}
		return result;
	}

	__getPrepayInterval () {
		let scope = this,
			prepayInterval = { min:null, max:null };


		for(let k in scope.programs){
			let program = scope.programs[k];
			if(program.max_prepay == 0){
				program.max_prepay = 100;
			}
			if(scope.selected.sumCredit <= program.min_credit || scope.selected.sumCredit >= program.max_credit) continue;
			if(!prepayInterval.min || program.min_prepay <= prepayInterval.min){
				prepayInterval.min = program.min_prepay;
			}
			if(!prepayInterval.max || program.max_prepay >= prepayInterval.max){
				prepayInterval.max = program.max_prepay;
			}
		}
		prepayInterval.min = parseFloat(prepayInterval.min);
		prepayInterval.max = parseFloat(prepayInterval.max);
		return prepayInterval;
	}

	__initInitialFee (selected = app.slides.selected) {
		let scope = this,
			startValue = '',
			model = app.slides.base.model[selected.model];

		if(!model){
			console.log('Calc: model not found');
			return false;
		}
		let complectation = model.complectation[selected.complectation];
		if(!complectation){
			console.log('Calc: complectation not found');
			return false;
		}
		let priceCompl = parseInt(complectation.price);
		if(isNaN(priceCompl)){
			console.log('Calc: price not found');
			return false;
		}

		let sumPrice = priceCompl;
		if(selected.package && selected.package > 0){
			let packageObj = app.slides.base.package[selected.package];
			sumPrice = sumPrice + parseInt(packageObj.price);
		}

		//console.log(selected.package);

		scope.carPrice.text(app.slides.formatPrice(sumPrice));
		startValue = sumPrice - (sumPrice / 100 * (100 - scope.params.initialFeePercent));
		scope.selected.initialFee = startValue;
		scope.selected.carPrice = sumPrice;
		scope.selected.sumCredit = scope.selected.carPrice - scope.selected.initialFee;

		let prepayInterval = scope.__getPrepayInterval()
		let resultHtml = `<div class="form-element form-element--range-wrap">
						<label for="initialFee" class="form-element--label">Первоначальный взнос</label>
						<input name="initial_fee" id="initialFee" type="text" class="form-element--input" value="${app.slides.formatPrice(sumPrice)}" placeholder="Первоначальный взнос">
						<div class="form-element--range" data-range-steps="true"></div>
					</div>`;
		scope.initialFeeContainer.html(resultHtml);
		scope.fields.initialFee = scope.$container.find('#initialFee');

		/**
		 * Пост обработка поля
		 */
		const $initialFee = scope.initialFeeContainer.find("[data-range-steps]");
		const $initialFeeVal = scope.initialFeeContainer.find("#initialFee");

		// Временная переменная со значением прайса
		let price = $initialFeeVal.val().replace(/\s+/g, '');
		let feeVal = null; // Текущее значение

		noUiSlider.create($initialFee[0], {
			start: scope.params.initialFeePercent,
			tooltips: [wNumb({
				decimals: 0,
				postfix: '%',
			})],
			range: {
				'min': Math.round(prepayInterval.min),
				'max': Math.round(prepayInterval.max),
			}
		});

		let tt = null;
		$initialFee[0].noUiSlider.on('update', function( values, handle ){
			let value = Math.floor(((price / 100) * values[handle]), 0);

			/*clearTimeout(tt);
			tt = setTimeout(function(){*/
				scope.__recalcPrice();
				scope.__renderPrograms();
			//}, 500);

			value = value.toString().split(/(?=(?:\d{3})+(?!\d))/);

			$initialFeeVal.val(scope.__shatter(value));

		});
		let maxPrice = price/100*prepayInterval.max;
		$initialFeeVal
			.on('input', function(e) {
				let getValue = $(this).val().replace(/\s+/g, '').replace(/\D/, '').replace(/^0/, '');
				if(+getValue > maxPrice) getValue = maxPrice;

				scope.__recalcPrice();
				scope.__renderPrograms();

				let setValue = getValue.split(/(?=(?:\d{3})+(?!\d))/);
				feeVal = scope.__shatter(setValue);
				if(feeVal.length == 0) feeVal = 0;

				$initialFee[0].noUiSlider.set(Math.floor(((getValue / price) * 100), 0));

				$(this).val(feeVal);
			})
			.on('focusout', function(e) {
				$(this).val(feeVal);
			});

		$initialFeeVal.trigger('input');
	}

	__initTerms () {
		let scope = this,
			$fieldContainer = scope.$container.find('[data-term]'),
			termsInterval = scope.__getTermsInterval();

		let maxTerm = Math.max(...termsInterval);
		if(scope.selected.period > maxTerm){
			scope.selected.period = maxTerm;
		}

		let $fieldHtml = `<div class="form-element">
						<label for="creditTerm" class="form-element--label">Кредит на срок</label>
						<input name="credit_term" value="${scope.selected.period}" id="creditTerm" type="text" class="form-element--input" placeholder="Кредит на срок" data-period>
						<div class="form-element--range" data-range-month="[${termsInterval.join(', ')}]"></div>
					</div>`;
		$fieldContainer.html($fieldHtml);


		const $initialMonth    = scope.$container.find("[data-range-month]");
		const getMonth         = $initialMonth.data().rangeMonth;
		const $initialMonthVal = scope.$container.find("#creditTerm");

		const lgt              = getMonth.length - 1;
		const keySet           = 100 / lgt;

		let setMonth           = {};
		let key                = 'min';

		let monthVal = null; // Текущее значение

		/**
		 * Создадим объект настроек для ползунка
		 */
		getMonth.forEach(function (_v, _k) {
			switch (_k) {
				case 0:
					setMonth[key] = _v,
						key = null; break;

				case lgt:
					setMonth['max'] = _v; break;

				default:
					key += keySet;
					setMonth[key] = _v; break;
			}
		});


		if(typeof setMonth.max == 'undefined'){
			$initialMonthVal.val(setMonth.min);
			$initialMonthVal.prop('readonly', true);
		}else{
			noUiSlider.create($initialMonth[0], {
				start: scope.selected.period,
				range: setMonth,
				pips: {
					mode: 'values',
					values: getMonth,
					density: 100
				}
			});

			$initialMonthVal
				.on('input', function(e) {
					let getValue = $(this).val().replace(/\s+/g, '').replace(/\D/, '');
					let minValue = getMonth[0];
					let maxValue = getMonth[getMonth.length - 1];

					if(getValue.length >= 2) {

						if(getValue > maxValue) {
							$initialMonth[0].noUiSlider.set(maxValue);
							$(this).val(maxValue);
							//scope.selected.period = maxValue;
							//scope.__recalcPrice();
							return;
						}

						if(getValue < minValue) {
							$initialMonth[0].noUiSlider.set(minValue);
							$(this).val(minValue);
							//scope.selected.period = minValue;
							//scope.__recalcPrice();
							return;
						}
						$initialMonth[0].noUiSlider.set(getValue);
					}
				})
				.on('focusout', function(e) {
					$(this).val(monthVal);
					//scope.selected.period = monthVal;
					//scope.__recalcPrice();
				});

			let tt = null;
			$initialMonth[0].noUiSlider.on('update', function(values, handle){
				monthVal = Math.floor(values[handle], 0);

				scope.selected.period = monthVal;
				$initialMonthVal.val(monthVal);
				scope.__recalcPrice();
			});
		}
	}

	__getTermsInterval () {
		let scope = this,
			termInterval = [];

		for(let k in scope.programs){
			let program = scope.programs[k],
				terms = program.terms;

			for(let kk in terms){
				let term = terms[kk];

				if(termInterval.indexOf(term.term) == -1){
					termInterval.push(term.term);
				}
			}
		}

		if(termInterval.length){
			termInterval.sort(function(a, b) {
				return a - b;
			});
		}
		return termInterval;
	}

	__recalcPrice() {
		let scope = this,
			price = scope.fields.initialFee.val().replace(/\s+/g, '').replace(/\D/, '').replace(/^0/, ''),
			sumCredit,
			program = null;
		price = parseInt(price);
		if(isNaN(price)){
			price = 0;
		}

		scope.fields.package.each(function(){
			if(!$(this).is(':checked')) return;
			scope.selected.package = $(this).val();
		});

		scope.selected.initialFee = price;
		scope.selected.sumCredit = scope.selected.carPrice - scope.selected.initialFee;

		//scope.__renderPrograms();
		if(scope.selected.program && scope.selected.program > 0){
			program = scope.programs[scope.selected.program];
			/*if(parseInt(program.restcost) > 0){
				let restCost = scope.selected.carPrice / 100 * parseInt(program.restcost);
				scope.selected.sumCredit = scope.selected.sumCredit - restCost;
			}*/
			scope.selected.month_payment = scope.__getMonthPayment(program);
			scope.monthPayment.text(app.slides.formatPrice(scope.selected.month_payment));
		}
		scope.carPrice.text(app.slides.formatPrice(scope.selected.carPrice));
		
		sumCredit = scope.selected.sumCredit;
		/*
		//console.log(program);
		if(program){
			console.log(parseInt(program.restcost));
		}
		*/
		
		if(program && parseInt(program.restcost) > 0){
			sumCredit = sumCredit - scope.selected.carPrice / 100 * program.restcost;
			
			console.log(sumCredit);
		}
		
		scope.fields.sumCredit.text(app.slides.formatPrice(sumCredit));
	}

	__shatter (arr) {
		let value = '';
		arr.forEach(function (i) {
			let pref = ' ';
			if(!value.length) pref = '';
			value += (pref + i);
		});
		return value;
	}

	__getFirstProperty (prop) {
		for (var i in prop) {
			return prop[i];
			break;
		}
	}


}