/**
 * @author Anton Desin <anton.desin@gmail.com>
 * @link http://pirogov.ru/ Бюро Пирогова
 * Date: 27.07.2017
 * Time: 14:05
 */

class topmenu extends Boreas.module {
	constructor () {
		super();
		this.params = {};
	}

	initialize (params = {}) {
		$('.button-topmenu').click(function(event) {
			$('.button-topmenu').addClass('active');
			$('.navigation__overlay, .js-navigation').addClass('show');
			$('body').addClass('overflow_hidden');
		});
		$('.navigation__overlay, .button--navigation-close, .navigation-link__items a').click(function() {
			$('.button-topmenu').removeClass('active');
			$('.navigation__overlay, .js-navigation').removeClass('show');
			$('body').removeClass('overflow_hidden');
		});

		var timeout;
		if (screen.width == 1024) {
			$(".first-level").addClass("on").find('.second-level').slideDown('200');
		}
		$(".first-level").hover(
			function() {
				var $first = $(this);
				timeout = setTimeout(function() {
					if ($first.hasClass('on')) {
						return true;
					} else {

						$first.parent().find('.on .second-level').slideUp('200').closest('.on').removeClass('on');
						$('.navigation-link__items .first-level').removeClass('on');
						$first.addClass("on").find('.second-level').slideDown('200');
					}
				}, 250);
			},
			function() {
				clearTimeout(timeout);
			}
		);

		this.trigger('ready');
	}
}