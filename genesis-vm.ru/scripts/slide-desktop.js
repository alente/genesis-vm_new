var eqSlideIdx = 0;
$(function () {
	var resizeInterval;

	var
	$eqSlideWrap = $('.slide-wrap'),
	$eqCurrentSlideWrap;

	var
	$eqSlideListUl = $eqSlideWrap.find('> ul'),
	$eqSlideLi = $eqSlideListUl.find('> li');

	var
	$eqSlideBtn = $eqSlideWrap.find('.btn-wrap'),
	$eqSlideCount = $('.gallery-detail .etc-wrap .count'),
	eqSlideLens = $eqSlideLi.length,
	slideMotionStart = true,
	indicatorClick = false,
	$oldList;

	var slideIdxArray = [0,0];

	var
	$galleryKvWrap = $('.section.gallery-kv'),
	$galleryIntroWrap = $('.gallery-intro'),
	$galleryDetailWrap = $('.section.gallery-detail'),
	galleryKind;

	var
	$galleryIndicator = $galleryDetailWrap.find('.slide-indicator'),
	$bottomBar = $galleryDetailWrap.find('.bottom-bar'),
	indicatorShow = true;

	// gallery intro
	$galleryIntroWrap.find('a').on('click', function () {
		changeSlide($(this));

		TweenLite.to($galleryDetailWrap.find('> .feature'), 0, { y : 500});
		TweenLite.to($galleryDetailWrap.find('> .feature'), 1, { y : 0});
		TweenLite.to($galleryKvWrap, 0.8, { y: -$(window).height(), ease: 'easeInQuart', onComplete: function () {
			$galleryKvWrap.addClass('kv-hide');
			// TweenLite.to($galleryDetailWrap.find('> .feature'), 0.5, { 'opacity' : 1 });
		}});

		return false;
	});

	// gallery detail
	$bottomBar.find('.btn-toggle').on('click', function () {
		if (indicatorShow) {
			$(this).addClass('open').text('open');
			indicatorShow = false;
			TweenLite.to($galleryIndicator, 0.8, { 'bottom' : -($galleryIndicator.height() + $bottomBar.height()), 'opacity' : 0 });
		}else {
			$(this).removeClass('open').text('close');
			indicatorShow = true;
			TweenLite.to($('.slide-indicator'), 0.8, { 'bottom' : 55, 'opacity' : 1 });
		}

		return false;
	});

	$('.kind-wrap').find('button').on('click', function () {
		changeSlide($(this));
	});

	function changeSlide($this) {
		slideMotionStart = true;

		galleryKind = $this.attr('data-kind');
		$eqCurrentSlideWrap = $('.slide-wrap[data-kind="'+galleryKind+'"]');

		// 선택자 기준 바꾸기
		$eqSlideListUl = $eqCurrentSlideWrap.find('> ul');
		$eqSlideLi = $eqSlideListUl.find('> li');
		$eqSlideBtn = $eqCurrentSlideWrap.find('.btn-wrap');
		eqSlideLens = $eqSlideLi.length;

		// idx 교체
		eqSlideIdx = slideIdxArray[$eqCurrentSlideWrap.index()];

		$eqSlideCount.find('.num').text(eqSlideIdx+1);
		$eqSlideCount.find('.total').text(eqSlideLens);

		$eqSlideWrap.removeClass('on');
		$eqCurrentSlideWrap.addClass('on');

		$('.kind-wrap').find('button').removeClass('on');
		$('.kind-wrap').find('button').eq($this.index()).addClass('on');
	}

	// slide
	function slideLoad() {
		$eqSlideLi.css('width', $('.section').width());
		$eqSlideListUl.parent().find('.slide-indicator .thumb-list').append('<ul>');

		// 썸네일 리스트 뿌리기
		$eqSlideListUl.each(function () {
			for (var i = 0; i < $(this).find('li').length; i++) {
				// thumbnail indicator
				var thumbImg = $(this).find('li').eq(i).find('span').html();

				if (i==0) {
					$(this).parent().find('.slide-indicator .thumb-list ul').append('<li class="on"><a href="#" title="">' + thumbImg + '</a></li>');
				}else {
					$(this).parent().find('.slide-indicator .thumb-list ul').append('<li><a href="#" title="">' + thumbImg + '</a></li>');
				}
			}
			$(this).parent().find('.slide-indicator .thumb-list ul').css('width', $(this).find('li').length*190);
		});
	}

	function slideMotion(direction) {
		slideMotionStart = false;

		var
		listReady,
		listMotion;

		if (direction == 'prev') {
			listReady = -$('.section').width();
			listMotion = $('.section').width();
			if (!indicatorClick) {
				(eqSlideIdx != 0) ? eqSlideIdx -= 1 : eqSlideIdx = eqSlideLens-1;
			}
		}else {
			listReady = $('.section').width();
			listMotion = -$('.section').width();
			if (!indicatorClick) {
				(eqSlideIdx != eqSlideLens-1) ? eqSlideIdx += 1 : eqSlideIdx = 0;
			}
		}

		$oldList = $eqSlideListUl.find('> li.on');

		$eqSlideLi.eq(eqSlideIdx).addClass('on');
		TweenLite.to($eqSlideLi.eq(eqSlideIdx), 0, { 'x' : listReady });

		TweenLite.to($oldList, .5, { 'x' : listMotion });
		TweenLite.to($eqSlideLi.eq(eqSlideIdx), .5, { 'x' : 0, onComplete: function () {
			slideCallBack();
		}});


		$eqSlideCount.find('.num').text(eqSlideIdx+1);

		$eqCurrentSlideWrap.find('.thumb-list li').removeClass('on');
		$eqCurrentSlideWrap.find('.thumb-list li').eq(eqSlideIdx).addClass('on');

		// thumb list scroll
		thumbListScroll();
	}

	function slideCallBack() {
		slideMotionStart = true;
		indicatorClick = false;

		TweenLite.to($oldList, 0, { 'x' : 0 });
		$eqSlideLi.removeClass('on');
		$eqSlideLi.eq(eqSlideIdx).addClass('on');

		slideIdxArray[$eqCurrentSlideWrap.index()] = eqSlideIdx;
	}

	function thumbListScroll() {
		var _left = (eqSlideIdx * $thumbList.find('li').width()) - ($('.section').width()/2) + $thumbList.find('li').width();
		$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : _left }, 500);
	}

	slideLoad();

	// arrow btn
	$('.slide-wrap .btn-wrap a').on('click', function () {
		if(slideMotionStart) {
			if ($(this).hasClass('prev')) {
				slideMotion('prev');
			}else {
				slideMotion('next');
			}
		}

		return false;
	});

	// indicator thumbnail btn
	var $thumbList = $('.thumb-list');

	$thumbList.find('li a').on('click', function () {
		if (slideMotionStart && !$(this).parent().hasClass('on')) {
			indicatorClick = true;
			var idx = $(this).parent().index();
			var oldIdx = $eqCurrentSlideWrap.find('.thumb-list li.on').index();

			$eqCurrentSlideWrap.find('.thumb-list li').removeClass('on');
			$eqCurrentSlideWrap.find('.thumb-list li').eq(idx).addClass('on');

			var curIdx = $eqCurrentSlideWrap.find('.thumb-list li.on').index();

			eqSlideIdx = idx;
			if (oldIdx > curIdx) {
				slideMotion('prev');
			}else if (oldIdx < curIdx) {
				slideMotion('next');
			}
		}

		return false;
	});

	// indicator arrow btn
	var $indicatorArrow = $('.slide-indicator .arrow-btn');

	function thumbListScrollArrow(direction) {
		var scrollLeft = $eqCurrentSlideWrap.find('.thumb-list').scrollLeft();
		if (direction == 'prev') {
			$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : scrollLeft - $('.section').width()/2 }, 500);
		}else {
			$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : scrollLeft + $('.section').width()/2 }, 500);
		}
	}
	$indicatorArrow.find('a').on('click', function () {
		if ($(this).hasClass('prev')) {
			thumbListScrollArrow('prev');
		}else {
			thumbListScrollArrow('next');
		}

		return false;
	});

	var galleryResize = function () {
		var sizeWidth = Math.max( Math.min( App.GlobalVars.WIN_MAX_WIDTH, App.GlobalVars.windowWidth ), App.GlobalVars.WIN_MIN_WIDTH );
		var sizeHeight = Math.max( App.GlobalVars.windowHeight, App.GlobalVars.WIN_MIN_HEIGHT );
		if(sizeWidth < sizeHeight) sizeHeight = App.GlobalVars.WIN_MIN_HEIGHT;

		$('.slide-wrap > ul > li').css('width', sizeWidth);
		var _left = (eqSlideIdx * $('.thumb-list').find('li').width()) - ($('.section').width()/2) + $('.thumb-list').find('li').width();
		$('.thumb-list').stop().animate({'scrollLeft' : _left }, 0);
		if (sizeWidth <= 1024) {
			$('.slide-wrap > ul > li img').css('min-height',720);
		}else {
			$('.slide-wrap > ul > li img').css('min-height','');
		}
	}
	
	$(App.Events).on( App.Events.RESIZE_COMPLETE, function() {
		galleryResize();
	});
	
});





























