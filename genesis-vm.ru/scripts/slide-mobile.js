var eqSlideIdx = 0;
$(function () {
	var
	$eqSlideWrap = $('.slide-wrap'),
	$eqCurrentSlideWrap;

	var
	$eqSlideListUl = $eqSlideWrap.find('> ul'),
	$eqSlideLi = $eqSlideListUl.find('> li');

	var
	$eqSlideCount = $('.gallery-detail .etc-wrap .count'),
	eqSlideLens = $eqSlideLi.length,
	$oldList;

	var slideIdxArray = [0,0];

	var
	$galleryKvWrap = $('.section.gallery-kv'),
	$galleryIntroWrap = $('.gallery-intro'),
	$galleryDetailWrap = $('.section.gallery-detail'),
	galleryKind;

	var
	$galleryIndicator = $galleryDetailWrap.find('.slide-indicator'),
	$bottomBar = $galleryDetailWrap.find('.bottom-bar'),
	indicatorShow = true,
	introClick = true;

	var _rotate = function() {
		if ( $(window).width() > $(window).height() ) {
			var h = $(window).height() + $('header').height() + $('.sub-navi').height();
		} else {
			var h = $(window).height();
		}
		$('.section.gallery-detail').height(h);
	}
	$(App.Events).on( App.Events.RESIZE_BROWSER, _rotate);

	// gallery intro
	$galleryIntroWrap.find('a').on('click', function () {
		if (introClick) {
			introClick = false;
			changeSlide($(this));
			_rotate();
			TweenLite.to($galleryKvWrap, 0.8, { y: -$galleryKvWrap.height(), ease: 'easeInQuart', onComplete: function () {
				$galleryKvWrap.addClass('kv-hide');
				$('.section.gallery-detail').css('min-height',0);
				// TweenLite.to($galleryDetailWrap, 0.5, { 'opacity' : 1 });
			}});
		}

		return false;
	});

	// gallery detail
	$bottomBar.find('.etc-wrap .count').on('click', function () {
		if (indicatorShow) {
			$(this).addClass('open');
			indicatorShow = false;
			TweenLite.to($galleryIndicator, 0.8, { 'bottom' : -($galleryIndicator.height() + $bottomBar.height()), 'opacity' : 0 });
		}else {
			$(this).removeClass('open');
			indicatorShow = true;
			TweenLite.to($('.slide-indicator'), 0.8, { 'bottom' : 45, 'opacity' : 1 });
		}

		return false;
	});

	$('.kind-wrap').find('button').on('click', function () {
		changeSlide($(this));
	});

	function changeSlide($this) {

		galleryKind = $this.attr('data-kind');
		$eqCurrentSlideWrap = $('.slide-wrap[data-kind="'+galleryKind+'"]');

		// 선택자 기준 바꾸기
		$eqSlideListUl = $eqCurrentSlideWrap.find('> ul');
		$eqSlideLi = $eqSlideListUl.find('> li');
		eqSlideLens = $eqSlideLi.length;

		// idx 교체
		eqSlideIdx = slideIdxArray[$eqCurrentSlideWrap.index()];

		$eqSlideCount.find('.num').text(eqSlideIdx+1);
		$eqSlideCount.find('.total').text(eqSlideLens);

		$eqSlideWrap.removeClass('on');
		$eqCurrentSlideWrap.addClass('on');

		$('.kind-wrap').find('button').removeClass('on');
		$('.kind-wrap').find('button').eq($this.index()).addClass('on');
		$('.down-wrap').find('a').removeClass('on');
		$('.down-wrap').find('a').eq($this.index()).addClass('on');

		var _left = (eqSlideIdx * $thumbList.find('li').width()) - ($thumbList.width()/2) + ($thumbList.find('li').width()/2);
		$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : _left }, 500);
	}

	// slide
	function slideLoad() {
		$eqSlideListUl.parent().find('.slide-indicator .thumb-list').append('<ul>');

		// 썸네일 리스트 뿌리기
		$eqSlideListUl.each(function () {
			for (var i = 0; i < $(this).find('li').length; i++) {
				// thumbnail indicator
				var thumbImg = $(this).find('li').eq(i).html();

				if (i==0) {
					$(this).parent().find('.slide-indicator .thumb-list ul').append('<li class="on"><a href="#" title="">' + thumbImg + '</a></li>');
				}else {
					$(this).parent().find('.slide-indicator .thumb-list ul').append('<li><a href="#" title="">' + thumbImg + '</a></li>');
				}
			}
			$(this).parent().find('.slide-indicator .thumb-list ul').css('width', $(this).find('li').length*90);
		});
	}
	slideLoad();

	var $thumbList;

	function thumbListScroll() {
		var _left = (eqSlideIdx * $thumbList.find('li').width()) - ($thumbList.width()/2) + ($thumbList.find('li').width()/2);
		$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : _left }, 500);
	}

	// indicator arrow btn
	var $indicatorArrow = $('.slide-indicator .arrow-btn');

	function thumbListScrollArrow(direction) {
		var scrollLeft = $eqCurrentSlideWrap.find('.thumb-list').scrollLeft();
		if (direction == 'prev') {
			$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : scrollLeft - $(window).width()/2 }, 500);
		}else {
			$eqCurrentSlideWrap.find('.thumb-list').stop().animate({'scrollLeft' : scrollLeft + $(window).width()/2 }, 500);
		}
	}
	$indicatorArrow.find('a').on('click', function () {
		if ($(this).hasClass('prev')) {
			thumbListScrollArrow('prev');
		}else {
			thumbListScrollArrow('next');
		}

		return false;
	});


	// swipe
	$('.slide-wrap').each(function () {
		var $this = $(this);
		var obj = $this[0];

		var oSwipe = new Swipe(obj, {
			callback: function(index, elem) {
				slideIdxArray[$eqCurrentSlideWrap.index()] = index;
				$eqSlideCount.find('.num').text(index+1);

				$eqCurrentSlideWrap.find('.thumb-list li').removeClass('on');
				$eqCurrentSlideWrap.find('.thumb-list li').eq(index).addClass('on');

				eqSlideIdx = index;

				thumbListScroll();
			}
		});

		// arrow btn
		var $arrowBtn = $this.find('.btn-wrap');

		$arrowBtn.find('a').on('click', function () {
			if ($(this).hasClass('prev')) {
				oSwipe.prev();
			}else {
				oSwipe.next();
			}
			return false;
		});

		// indicator thumbnail btn
		$thumbList = $this.find('.thumb-list');

		$thumbList.find('a').on('click', function () {
			if (!$(this).parent().hasClass('on')) {
				var idx = $(this).parent().index();

				oSwipe.slide(idx);
			}
			return false;
		});
	});

	$(window).on('load',function () {
		$('.gallery-detail').css({'width' : $(window).width(), 'height' : $(window).height()});
	});
	$(window).on('load resize',function () {
		$('.gallery-detail').css('width', $(window).width());
		
		var _left = (eqSlideIdx * $('.thumb-list').find('li').width()) - ($('.thumb-list').width()/2) + ($('.thumb-list').find('li').width()/2);
		$('.thumb-list').stop().animate({'scrollLeft' : _left }, 0);
	});
});





























