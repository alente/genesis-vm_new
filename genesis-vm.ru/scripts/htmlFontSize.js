'use strict';

var _isMobile = false;
var _isTab = false;
var _isDesktop = false;
var _html = document.querySelector('html');

if (document.querySelector('html.mobile') != null) {
    _isMobile = true;
}
if (document.querySelector('html.desktop') != null) {
    _isDesktop = true;
}
if (document.querySelector('html.tablet') != null) {
    _isTab = true;
}
window.initMap = function () {};

var baseSize = 10.7;

function currentView() {
    var baseWidth = 1440,
        baseHeight = 900,
        winWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
        winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
        fontSize /*,
                 baseSize = 10*/;
    // if (winWidth > 1024) {
    // 	baseWidth = 1600;
    // }
    if (_isMobile) {
        console.log('mobile');
        baseSize = 10;
        if (winHeight > winWidth) {
            baseWidth = 640;
            document.querySelector('html').classList.remove('landscape');
        } else {
            baseWidth = 640;
            document.querySelector('html').classList.add('landscape');
        }
    }
    if (_isTab) {
        baseSize = 10;
        if (winHeight > winWidth) {
            baseWidth = 1024;
            document.querySelector('html').classList.remove('landscape');
            document.querySelector('html').classList.remove('landscape-wide');
        } else if (winHeight * 1.3 < winWidth) {
            baseWidth = 1200;
            document.querySelector('html').classList.add('landscape-wide');
        } else {
            baseWidth = 1024;
            document.querySelector('html').classList.remove('landscape-wide');
            document.querySelector('html').classList.add('landscape');
        }
    }

    if (_isDesktop) {
        if (winWidth / winHeight > 1.6) {
            fontSize = Math.min(winHeight / baseHeight * baseSize);
            //baseWidth = 1600;
        } else {
                fontSize = Math.min(winWidth / baseWidth * baseSize);
                // baseWidth = 1440;
            }
    } else {
            fontSize = Math.min(winWidth / baseWidth * baseSize);
        }

    // _html.classList.add('resizing');

    _html.style.fontSize = fontSize + 'px';
    _html.setAttribute('data-font-size', fontSize);

    // _html.classList.remove('resizing');
}
if (window.navigator.userAgent.indexOf("Edge") > -1) {
    document.querySelector('html').classList.add('MSIE');
}
currentView();

setTimeout(function () {
    if (document.querySelector('html.msie') == null) _html.style.fontSize = 0 + 'px';
    currentView();
}, 100);

window.addEventListener("resize", currentView);
//# sourceMappingURL=htmlFontSize.js.map
//# sourceMappingURL=htmlFontSize.js.map
//# sourceMappingURL=htmlFontSize.js.map
//# sourceMappingURL=htmlFontSize.js.map
//# sourceMappingURL=htmlFontSize.js.map
//# sourceMappingURL=htmlFontSize.js.map
