//------------------------------------------------------------------
//  last update: 2017. 07. 28
//------------------------------------------------------------------

(function(window, $, undefined){

    var navigation = (function(){

        var _conHeight = $('.side-menu .hoverable.default').find('.depth-2').height();  // 170720 / 이혜진 / side-menu 추가

        var _isShowBg = false;
        var _isShowMenu = false;
        var _isModelPage = false;

        var _hasKeyvisual = false;

        var _currentIndex = -1;

        var _isLinkMenu = false;

        var _init = function(){
                _isModelPage = $(".wrapper").find(".model").length != 0 ? true : false;

                _setContentsHeight();
                // _addEvent();
                _onResize();
                _navigationOut();

                _isLinkMenu = $(".js-focus-menu").hasClass("js-link-menu");

                _onCloseMenuComplete();
                //_onCloseHeaderLocComplete();

            },

/*
            _addEvent = function(){
                //  리사이징
                $(AppH.Events).on(AppH.Events.RESIZE_BROWSER, _onResize);

                // scroll event
                $(AppH.Events).on(AppH.Events.SCROLL_MOVE, _onScrollMove);

                $(AppH.Events).on(AppH.Events.CHANGE_SECTION, _onChange_section);

                $(AppH.Events).on(AppH.Events.SHOW_POPUP, _onCloseMenu);

                // models 서브 메뉴 클릭시
                $("header .side-menu .cont-mid .nav-high-priority .depth-2 li").on("click", _onClick_Indicator);

                // concierge 메뉴 클릭시
                $("header .common-menu .js-concierge a").on("click", _onClick_concierge);

                $(".side-menu .hoverable").on("mouseover", _onOverMenu);  // 170720 / 이혜진 / side-menu 추가

                $(".btn-open-menu").on("click", _onOpenMenu);
                $(".btn-close-menu").on("click", _onCloseMenu);

                $("header.header_2017 .side-menu .input-wrap input").on("keydown", _onKeydown_search);
                $("header.header_2017 .side-menu .input-wrap input").on("focus", _onFocus_search);
                $("header.header_2017 .side-menu .input-wrap a.btn-cancle").on("click", _onClick_searchCanelBtn);
                $("header.header_2017 .side-menu .input-wrap>img").on("click", _onClick_searchIcon);
                $("header.header_2017 .side-menu .input-wrap input").on("focusout", _onFocusOut_search);
                //$("header .side-menu .input-wrap a.btn-search").on("click", _onClick_searchSearchBtn);

                $("header.header_2017 .side-menu").on("mouseover", _navigationOver);
                $("header.header_2017 .side-menu").on("mouseout", _navigationOut);

                // Accessibility - for access : VAM 20160816
                $(".side-menu .hoverable>a").on("focusin", _onFocus_hoverMenu);  // 170720 / 이혜진 / side-menu 추가
                $(".side-menu .hoverable ul li a").on("focusin", _onFocus_hoverMenu_sub);  // 170720 / 이혜진 / side-menu 추가
                $(".btn-open-menu").on("focusin", _onFocus_openMenu);
                $("header .common-menu .location a").on("focusin", _onFocus_location);
                $(".shortcut-wrap .shortcuts li a").on("focusin", _onFocus_shortcut);
                $("header .side-menu a:last").on("focusout", _onFocusOut_lastFocus);

                if(AppH.GlobalVars.currentDevice == AppH.GlobalVars.DEVICE_TYPE_TABLET){
                    var highNaviLen = $(".nav-high-priority>li").length;
                    var i = 0;
                    for(i=0; i<highNaviLen; i++){
                        var hasSubMenu = $(".nav-high-priority>li>ul").length;
                        if(hasSubMenu != 0){
                            $(".nav-high-priority>li").eq(i).find(">a").on("click", _onClick_tabletMenu)
                        }
                    }
                }
            },
*/
            _navigationOver = function(e){
                TweenLite.killDelayedCallsTo(_controlHoverable_SideMenu);
                TweenLite.killDelayedCallsTo(_controlSideMenu);
            },

            _navigationOut = function(e){
                if($(".js-focus-menu").find(".depth-2").length != 0){
                    //hoveralble menu focus
                    TweenLite.killDelayedCallsTo(_controlHoverable_SideMenu);
                    TweenLite.killDelayedCallsTo(_controlSideMenu);
                    TweenLite.delayedCall(1, _controlSideMenu, [$(".js-focus-menu"), Quart.easeInOut]);
                } else {
                    TweenLite.killDelayedCallsTo(_controlHoverable_SideMenu);
                    TweenLite.killDelayedCallsTo(_controlSideMenu);
                    TweenLite.delayedCall(1, _controlHoverable_SideMenu, [-1, Quart.easeInOut]);
                }
            },

        // 컨텐츠 높이 넣어주기
            _setContentsHeight = function(){
                var sub = $('.side-menu .hoverable').find('.depth-2');  // 170720 / 이혜진 / side-menu 추가
                var subLen = sub.length;
                for(var i=0 ; i<subLen; i++){
                    $(sub[i]).attr("data-height", $(sub[i]).height());
                }
            },

            _onResize = function(){
                // 사이드 메뉴 높이 넣어주기
                var sMenuHeight = $(window).innerHeight();
                if(sMenuHeight < 530) sMenuHeight = 530;
                $('.side-menu').css({lineHeight: sMenuHeight+'px'});

                //keyvisual check
                _hasKeyvisual =  $(".wrapper").find(".kv-area").length != 0 ? true : false;
            },

            _onScrollMove = function(e, scrollTop){
                _onCloseMenu();

                _hasKeyvisual =  $(".wrapper").find(".kv-area").length != 0 ? true : false;

                //if(!_hasKeyvisual) return;
                if(!_hasKeyvisual){

                    if(!_isShowBg && _isModelPage){
                        _showHeaderBg();
                    }
                    return;
                }

                var winW = $(window).width();
                var winH = $(window).height();

                if(winW < winH) {
                    if(winH>AppH.GlobalVars.WIN_MIN_HEIGHT) winH = AppH.GlobalVars.WIN_MIN_HEIGHT;
                }

                if(winH <= scrollTop+130){
                    if(!_isShowBg){
                        _showHeaderBg();
                    }
                }else{
                    if(_isShowBg){
                        _hideHeaderBg();
                    }
                }
            },

        // top blackbar control
            _showHeaderBg = function(){
                _isShowBg = true;
                TweenLite.killTweensOf($(".head-bg"));
                TweenLite.to($(".head-bg"), 0.4, {"css":{"top":0}, ease:Quart.easeOut});

                var loc = $("header .common-menu .location");
                loc.removeClass("hide");

                TweenLite.killTweensOf(loc);
                TweenLite.to(loc, 0.4, {"css":{"top":30}, ease:Quart.easeOut});

                $(AppH.Events).trigger(AppH.Events.SHOW_HEADER_BG);
            },

            _hideHeaderBg = function(){
                _isShowBg = false;
                TweenLite.killTweensOf($(".head-bg"));
                TweenLite.to($(".head-bg"), 0.6, {"css":{"top":-80}, ease:Quart.easeInOut});

                var loc = $("header .common-menu .location");
                TweenLite.killTweensOf(loc);
                TweenLite.to(loc, 0.6, {"css":{"top":-20}, ease:Quart.easeInOut, onComplete:_onCloseHeaderLocComplete});

                $(AppH.Events).trigger(AppH.Events.HIDE_HEADER_BG)
            },

            _onCloseHeaderLocComplete = function(){

                _isShowBg = false;
                TweenLite.killTweensOf($(".head-bg"));
                TweenLite.set($(".head-bg"), {"css":{"top":-80}});

                var loc = $("header .common-menu .location");
                TweenLite.killTweensOf(loc);
                TweenLite.set(loc, {"css":{"top":-20}});

                //loc.addClass("hide");
            },

            _onFocus_location = function(){
                _showHeaderBg();
            },

            _onFocus_shortcut = function(){
                _showHeaderBg();
            },

            _changeColor = function(){

                var i = 0;
                var len = $(".js-check-height").length;
                var hasClass = $("header .common-menu").hasClass("dark");
                var indicatorColor = $(".js-check-height").eq(_currentContentsIndex).data("indicator-color");
                if(indicatorColor == "black"){
                    if(!hasClass){
                        $("header .common-menu").addClass("dark")
                    }
                }else{
                    if(hasClass){
                        $("header .common-menu").removeClass("dark")
                    }
                }
            },


        // 메뉴 오버
            _onOverMenu = function(e){
                var target = $(e.currentTarget);
                if(!$(target).hasClass('on-over')) {
                    _controlSideMenu($(target))

                }
            },

            _onFocus_hoverMenu = function(e){
                var target = $(e.currentTarget).closest(".hoverable");
                //console.log("_onFocus_hoverMenu" + target);
                if(!$(target).hasClass('on-over')) {
                    _controlSideMenu($(target))

                }
            },

            _onFocus_hoverMenu_sub = function(e){
                var target = $(e.currentTarget).closest(".hoverable");
                //console.log("_onFocus_hoverMenu_sub" + target);

                if(!$(target).hasClass('on-over')) {
                    _controlSideMenu($(target))

                }
            },

            _controlSideMenu = function(openMnu, easing){
                _controlHoverable_SideMenu( openMnu.index(), easing )
            },

            _controlHoverable_SideMenu = function(index, easing){
                if(index == -1) _controlLow_SideMenu($(".js-focus-menu").index());
                else _controlLow_SideMenu(-1);
                var hoverMnu = $('.side-menu .nav-high-priority .hoverable');  // 170720 / 이혜진 / side-menu 추가
                var openMnu, openMnu_depth2, _conHeight;
                var time = 0.5;
                var ease = easing == undefined ? Cubic.easeOut : easing;
                for(var i=0 ; i<hoverMnu.length ; i++){
                    openMnu = hoverMnu.eq(i);
                    openMnu_depth2 = openMnu.find('.depth-2');

                    if(index == i){
                        //hover
                        _conHeight = openMnu_depth2.attr("data-height");

                        openMnu.addClass('on-over');
                        openMnu_depth2.addClass('on');
                        TweenLite.killTweensOf(openMnu);
                        TweenLite.to(openMnu, time, {"css":{paddingBottom:parseInt(_conHeight) + 20}, ease:ease});
                        TweenLite.killTweensOf(openMnu.find('.depth-2'));
                        TweenLite.to(openMnu.find('.depth-2'), time, {"css":{height:_conHeight}, ease:ease});

                    } else {

                        openMnu.removeClass('on-over');
                        openMnu_depth2.removeClass('on');
                        TweenLite.killTweensOf(openMnu);
                        TweenLite.to(openMnu, time, {"css":{paddingBottom:0}, ease:ease});
                        TweenLite.killTweensOf(openMnu.find('.depth-2'));
                        TweenLite.to(openMnu.find('.depth-2'), time, {"css":{height:0}, ease:ease});

                    }
                }
            },

            _controlLow_SideMenu = function(index){
                var lowMnu = $('.nav-low-priority li');
                for(var i=0 ; i<lowMnu.length ; i++) {
                    var mnu = lowMnu.eq(i);
                    if(index == i){
                        mnu.addClass("activated")
                    } else {
                        mnu.removeClass("activated")
                    }

                }
            },


        // 섹션 변경
            _onChange_section = function(e, indicatorIndex, sectionIndex){
                _onChange_indicator(indicatorIndex);
            },

        // 서브메뉴 변경
            _onChange_indicator = function(index){

                if(_currentIndex!=index && !_isLinkMenu ){
                    _offIndicator(_currentIndex);
                    _currentIndex = index;
                    _onIndicator(_currentIndex);
                }
            },

        // 서브메뉴 활성화
            _onIndicator = function(index){
                $("header .side-menu .cont-mid .nav-high-priority .js-focus-menu .depth-2 li").eq(index).addClass("on");
            },

        // 서브메뉴 비활성 화
            _offIndicator = function(index){
                $("header .side-menu .cont-mid .nav-high-priority .depth-2 li").removeClass("on");
            },

        // models 서브메뉴 클릭했을 때
            _onClick_Indicator = function(e){

                // 같은 메뉴에서 서브메뉴 클릭시 href무시하고 인디게이터 클릭이동으로
                var arrNavClass = $(this).parent().parent().attr("class").split(" ");
                var focusClass = "js-focus-menu";

                var navClass, isSame = false;
                for(var i=0; i<arrNavClass.length ; i++){
                    navClass = arrNavClass[i];
                    if(navClass == focusClass){
                        isSame = true;
                    }
                }

                if(_isLinkMenu) isSame = false;

                if(isSame){
                    //trigger
                    _onCloseMenu();
                    var indicatorIndex = $(this).index();
                    $(AppH.Events).trigger(AppH.Events.CLICK_INDICATOR, [indicatorIndex]);

                    e.preventDefault();
                    return false;
                } else {
                    //href 이동
                }
            },

            _onClick_concierge = function(e){
                var isHasClass = $("#container").hasClass("concierge");
                if(isHasClass){
                    $(AppH.Events).trigger(AppH.Events.CLICK_CONCIERGE);

                    e.preventDefault();
                    return false;
                } else {
                    //href 이동
                }
            },

        // 메뉴 열기
            _onOpenMenu = function(){
                if(!_isShowMenu){
                    var _sideMenu = $('.side-menu');
                    _sideMenu.removeClass("hide");

                    _isShowMenu = true;
                    TweenLite.killTweensOf(_sideMenu);
                    TweenLite.to(_sideMenu, 0.7, {"css":{right:0}, ease:Quart.easeOut});

                    $(AppH.Events).trigger(AppH.Events.OPEN_NAVI);

                    // 2016. 08. 16 : VAM
                    $("._sideMenu").focus();
                }

                return false;
            },

            _onFocus_openMenu = function(e){
                _onCloseMenu();
            },

            _onFocusOut_lastFocus = function(e){
                _onCloseMenu();
            },


        // 메뉴 닫기
            _onCloseMenu = function(){
                if(_isShowMenu) {
                    var _sideMenu = $('.side-menu');

                    _isShowMenu = false;
                    TweenLite.killTweensOf(_sideMenu);
                    TweenLite.to(_sideMenu, 0.6, {"css": {right: -470}, ease: Quart.easeInOut, onComplete:_onCloseMenuComplete});
                    $("header .side-menu .input-wrap input").blur();

                    $(AppH.Events).trigger(AppH.Events.CLOSE_NAVI);

                    // 2016. 08. 12 : VAM
                    $(".btn-open-menu").focus();

                    console.log("_onCloseMenu")
                }
                return false;
            },

            _onCloseMenuComplete = function(){

                _isShowMenu = false;

                var _sideMenu = $('.side-menu');
                TweenLite.killTweensOf(_sideMenu);
                TweenLite.set(_sideMenu, {"css": {right: -470}});

                $(_sideMenu).addClass("hide");
            },

        //-------------------------------------------------
        //  검색 취소 버튼
        //-------------------------------------------------

            _onClick_tabletMenu = function(e){
                var index = $(e.currentTarget).parent().index();
                _controlHoverable_SideMenu(index);

                return false;
            },




        //-------------------------------------------------
        //  검색 취소 버튼
        //-------------------------------------------------


        // 서치 인풋박스에서 키 다운
            _onKeydown_search = function(e){
                _checkSearchVal();
            },


            _onFocus_search = function(e){
                if(AppH.GlobalVars.currentDevice == AppH.GlobalVars.DEVICE_TYPE_TABLET && _Device.os == 1) {
                    toApp_onSearchClick();
                }else{
                    _showBtn_search();
                }
            },

            _onFocusOut_search = function(e){
                if($("header .side-menu .input-wrap input").val() == ""){

                    _hideBtn_search();

                }else{
                    _showBtn_search();
                }
            },

            _showBtn_search = function(){
                $("header .side-menu .input-wrap").addClass("has-txt");
            },

            _hideBtn_search = function(){
                $("header .side-menu .input-wrap").removeClass("has-txt");
            },


        // 검색 값 체크
            _checkSearchVal = function(){
                /*
                 var val = $("header .side-menu .input-wrap input").val();
                 if(val.length != 0){
                 _showSearchCancelBtn();
                 }else{
                 _hideSearchCancelBtn();
                 }
                 */
            },

        // 검색 취소 버튼 보여주기
            _showSearchCancelBtn = function(){
                $("header .side-menu .input-wrap").addClass("has-txt");
            },

        // 검색 취소버튼 감추기
            _hideSearchCancelBtn = function(){
                $("header .side-menu .input-wrap").removeClass("has-txt");
            },

            _onClick_searchCanelBtn = function(e){
                $("header .side-menu .input-wrap input").val("");
                _checkSearchVal();
                $("header .side-menu .input-wrap input").focus();
                return false;
            },

            _onClick_searchIcon = function(){
                $("header .side-menu .input-wrap input").focus();
            };

        return {
            init : _init
        };
    })();

    var header = (function(){

        var _init = function(){
                _addEvent();
            },

            _addEvent = function(){
                _gnbEvent();
                _slideMenuEvent($('header.header_2017 .quicklink-btns'));
                _slideMenuEvent($('header.header_2017 .lang-btns'));

                // $(window).scroll(function(){
                //     // _headerBgEvent();
                //
                // });
                // $(window).load(function(){
                //     // _headerBgEvent();
                // });
                
                $("header.header_2017 .right-menus .input-wrap input").on("keydown", _onKeydown_search);
                $("header.header_2017 .right-menus .input-wrap input").on("focus", _onFocus_search);
                $("header.header_2017 .right-menus .input-wrap a.btn-cancle").on("click", _onClick_searchCanelBtn);
                $("header.header_2017 .right-menus .input-wrap>img").on("click", _onClick_searchIcon);
                $("header.header_2017 .right-menus .input-wrap input").on("focusout", _onFocusOut_search);
            };

            _gnbEvent = function() {
                var $gnbWrap = $('header.header_2017 .gnb-menu');
                var $gnbMenu = $gnbWrap.find('.nav-high-priority > li');
                var $gnbMenuLink = $gnbMenu.find(' > a');
                var $subMenu = $gnbMenuLink.next('.sub-menu');
                var $gnbBg = $gnbWrap.find('.gnb-background');

                $gnbMenuLink.on('mouseenter focus', function() {
                    onOpenMenu($(this));
                });

                $gnbWrap.on('mouseleave', onCloseMenu);
                $gnbWrap.prevAll().find('a, input').on('focus', onCloseMenu);
                $gnbWrap.nextAll().find('a, input').on('focus', onCloseMenu);
                brandMenuEvent();

                function onOpenMenuBg() {
                    $("header.header_2017 .cont-mid").stop(true, false).animate({
                        "height":"290px"
                    }, 500, 'easeOutCubic');
                }

                function onCloseMenuBg() {
                    $("header.header_2017 .cont-mid").stop(true, false).animate({
                        "height":"80px"
                    }, 300, 'easeOutCubic');
                }

                function onOpenMenu($this){
                    $gnbMenu.removeClass('on');

                    if(!($gnbWrap.hasClass('opened'))) {
                        if(!$this.parent('.hoverable').hasClass('single')){
                            $gnbWrap.addClass('opened');
                            onOpenMenuBg();
                        }
                        setTimeout(function(){
                            $gnbMenu.removeClass('on');
                            $subMenu.hide();
                            $this.parent('li').addClass('on');
                            $this.parent('li').find('.sub-menu').show();
                        }, 10)
                    } else if($gnbWrap.hasClass('opened')) {
                        if($this.parent('.hoverable').hasClass('single')){
                            $gnbWrap.removeClass('opened');
                            onCloseMenuBg();
                        }

                        $gnbBg.show();
                        $gnbMenu.removeClass('on');
                        $subMenu.hide();
                        $this.parent('li').addClass('on');
                        $this.parent('li').find('.sub-menu').show();
                    }
                }

                function onCloseMenu(){
                    if($gnbWrap.hasClass('opened')){
                        setTimeout(function(){
                            onCloseMenuBg();
                        }, 0);
                    }

                    $gnbWrap.removeClass('opened');
                    $gnbMenu.removeClass('on');
                    $subMenu.hide();
                }

                function brandMenuEvent(){
                    $('header.header_2017 .brand .depth-2 li a').on('mouseenter focus', function(){
                        figFadeIn($(this));
                    });
                    $('header.header_2017 .brand .depth-2 li a').on('mouseleave blur', function(){
                        figFadeOut($(this));
                    });

                    function figFadeIn($this) {
                        $this.parent().find('.figure').stop().fadeIn();
                    };

                    function figFadeOut($this) {
                        $this.parent().find('.figure').stop().fadeOut();
                    };
                }
            },

            _slideMenuEvent = function(target){
                target.find('> a').on('mouseenter focus', function() {
                    onOpenMenu($(this));
                });
                target.on('mouseleave', function() {
                    onCloseMenu($(this));
                });
                target.nextAll().find('a, input').on('focus', function() {
                    onCloseMenu(target);
                });
                target.prevAll().find('a, input').on('focus', function() {
                    onCloseMenu(target);
                });
                target.closest('header').nextAll().find('a, input').on('focus', function() {
                    onCloseMenu(target);
                });

                function onOpenMenu($this){
                    if(!($this.parent().hasClass('opened'))) {
                        $this.parent().addClass('opened');
                        $this.parent().find('ul').stop().slideDown(300, 'easeOutQuart');
                    }
                }

                function onCloseMenu($this){
                    $this.removeClass('opened');
                    $this.find('ul').stop().slideUp(300, 'easeOutQuart');
                }
            },
            //
            // _headerBgEvent = function() {
            //     var $hd = $('header.header_2017');
            //     var $hdBg = $hd.find('.header-background');
            //     var hdH = $hd.height();
            //     var hdTop = $hd.offset().top;
            //     if ($('section').eq(1).length) {
            //       var sec2Top = $('section').eq(1).offset().top - hdH;
            //     }
            //     var $gnbMenu = $hd.find('.gnb-menu');
            //     var $quickMenu = $hd.find('.quicklink-btns');
            //     var $langMenu = $hd.find('.lang-btns');
            //     var condition = $gnbMenu.hasClass('opened') || $quickMenu.hasClass('opened') || $langMenu.hasClass('opened');
            //
            //     var arr = [$gnbMenu, $quickMenu, $langMenu];
            //
            //   if ($('section').eq(1).length) {
            //     for (var i = 0; i < arr.length; i++) {
            //       arr[i].on('mouseover keyup', function () {
            //         if (hdTop >= 0 && hdTop < sec2Top) {
            //           if ($(this).hasClass('opened')) {
            //             showHeaderBg();
            //           }
            //         }
            //       })
            //
            //       arr[i].on('mouseout', function () {
            //         if (hdTop >= 0 && hdTop < sec2Top) {
            //           if (!$(this).hasClass('opened')) {
            //             hideHeaderBg();
            //           }
            //         } else {
            //           showHeaderBg();
            //         }
            //       })
            //     }
            //
            //     $hd.find('.input-wrap').find('a, input').on('click focus keyup keydown', function () {
            //       if (hdTop >= 0 && hdTop < sec2Top) {
            //         showHeaderBg();
            //       }
            //     })
            //
            //     $hd.find('.input-wrap').find('a, input').on('blur', function () {
            //       if (hdTop >= 0 && hdTop < sec2Top) {
            //         hideHeaderBg();
            //       } else {
            //         showHeaderBg();
            //       }
            //     })
            //
            //     if (hdTop >= 0 && hdTop < sec2Top) {
            //       if (condition) {
            //         showHeaderBg();
            //       } else {
            //         hideHeaderBg();
            //       }
            //     } else {
            //       showHeaderBg();
            //     }
            //
            //     function showHeaderBg() {
            //       $hdBg.stop(true, false).animate({
            //         "opacity": "1"
            //       }, 300, 'easeOutCubic');
            //     }
            //
            //     function hideHeaderBg() {
            //       $hdBg.stop(true, false).delay(300).animate({
            //         "opacity": "0.9"
            //       }, 300, 'easeOutCubic');
            //     }
            //   }
            // }

            //-------------------------------------------------
            //  검색 취소 버튼
            //-------------------------------------------------
        
        // 서치 인풋박스에서 키 다운
            _onKeydown_search = function(e){
                _checkSearchVal();
            },

            _onFocus_search = function(e){
                if(AppH.GlobalVars.currentDevice == AppH.GlobalVars.DEVICE_TYPE_TABLET && _Device.os == 1) {
                    toApp_onSearchClick();
                }else{
                    _showBtn_search();
                }
            },

            _onFocusOut_search = function(e){
                if($("header.header_2017 .right-menus .input-wrap input").val() == ""){

                    _hideBtn_search();

                }else{
                    _showBtn_search();
                }
            },

            _showBtn_search = function(){
                $("header.header_2017 .right-menus .input-wrap").addClass("has-txt");
            },

            _hideBtn_search = function(){
                $("header.header_2017 .right-menus .input-wrap").removeClass("has-txt");
            },


        // 검색 값 체크
            _checkSearchVal = function(){
                // var val = $("header .right-menus .input-wrap input").val();
                // if(val.length != 0){
                // _showSearchCancelBtn();
                // }else{
                // _hideSearchCancelBtn();
                // }
                 
            },

        // 검색 취소 버튼 보여주기
            _showSearchCancelBtn = function(){
                $("header.header_2017 .right-menus .input-wrap").addClass("has-txt");
            },

        // 검색 취소버튼 감추기
            _hideSearchCancelBtn = function(){
                $("header.header_2017 .right-menus .input-wrap").removeClass("has-txt");
            },

            _onClick_searchCanelBtn = function(e){
                $("header.header_2017 .right-menus .input-wrap input").val("");
                _checkSearchVal();
                $("header.header_2017 .right-menus .input-wrap input").focus();
                return false;
            },

            _onClick_searchIcon = function(){
                $("header.header_2017 .right-menus .input-wrap input").focus();
            }

        return {
            init : _init
        };
    })();

    $(function(){
        AppH.navigation = navigation;
        AppH.navigation.init();
        AppH.header = header;
        AppH.header.init();

      var topBtn = $('.top-btn');
      if (topBtn.length) {
        $(window).on('load scroll resize', function () {
          var scrollY = window.pageYOffset || document.documentElement.scrollTop;
          var windowHeight = window.outerHeight;

          if (scrollY > windowHeight / 2) {
            topBtn.addClass('show');
          } else {
            topBtn.removeClass('show');
          }
        });
        topBtn.on('click', function () {
          $('body,html').animate({scrollTop: 0}, 1000);
        })
      }
    });

}(window, jQuery));
