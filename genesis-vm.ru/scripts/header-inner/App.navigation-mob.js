//------------------------------------------------------------------
//  last update: 2017.07.31 - ver 1.0
//------------------------------------------------------------------

(function(window, $, undefined){

    var navigation = (function(){

        var _isShowBg = false;
        var _isOpenMenu = false;
        var _contentsScrollTop = 0;

        var _currentIndex = -1;

        var _currentScreenW = 0;
        var _isLinkMenu = false;

        var _init = function(){
                _show();
            },
            _show = function(){
                _addEvent();
                _focusCurrentMenu();

                _isLinkMenu = $(".js-focus-menu").hasClass("js-link-menu");
            },

            _addEvent = function(){

                $(AppH.Events).on(AppH.Events.SCROLL_MOVE, _onScrollMove);
                $(AppH.Events).on(AppH.Events.CHANGE_SECTION, _onChange_section);

                //$(AppH.Events).on(AppH.Events.RESIZE_BROWSER, _onResize);
                $(window).on("orientationchange", _onOrientationChange);

                // 서브 메뉴 클릭시
                $("header .side-menu .nav-high-priority .depth-2 li").on("click", _onClick_Indicator);

                $(".btn-open-menu").on("click", _onClick_btnMenu);
                $(".btn-close-menu").on("click", _onClick_btnClose);

                $("header .side-menu .cont-mid .btn-accordion").on("click", _onClickOpenMnu);

                // concierge 메뉴 클릭시
                $("header .common-menu .js-concierge a").on("click", _onClick_concierge);

                //share btn click
                $("header .side-menu .cont-bot .btn-side-share").on("click", _onShowShareBtn);

                $("header .side-menu .input-wrap input").on("keydown", _onKeydown_search);
                $("header .side-menu .input-wrap a.btn-cancel").on("click", _onClick_searchCanelBtn);
                $("header .side-menu .input-wrap>img").on("click", _onClick_searchIcon)
            },

            _onOrientationChange = function(){
                if(_isOpenMenu){
                    _closeMenu();
                }
            },


        // 섹션 변경
            _onChange_section = function(e, indicatorIndex, sectionIndex){
                if(_isOpenMenu) return;
                _onChange_indicator(indicatorIndex);
            },

        // 서브메뉴 변경
            _onChange_indicator = function(index){
                if(_currentIndex!=index && !_isLinkMenu ){
                    _offIndicator(_currentIndex);
                    _currentIndex = index;
                    _onIndicator(_currentIndex);
                }
            },

        // 서브메뉴 활성화
            _onIndicator = function(index){
                var indexCheck = index;
                $("header .side-menu .nav-high-priority .js-focus-menu .depth-2 li").eq(index).addClass("on");
            },

        // 서브메뉴 비활성 화
            _offIndicator = function(index){
                $("header .side-menu .nav-high-priority .depth-2 li").removeClass("on");
            },

        // models 서브메뉴 클릭했을 때
            _onClick_Indicator = function(e){

                _closeMenu();

                var arrNavClass = $(this).parent().parent().parent().attr("class").split(" ");
                var focusClass = "js-focus-menu";

                var navClass, conClass, isSame = false;
                for(var i=0; i<arrNavClass.length ; i++){
                    navClass = arrNavClass[i];
                    if(navClass == focusClass){
                        isSame = true;
                    }
                }

                if(_isLinkMenu) isSame = false;

                if(isSame){
                    //trigger
                    var indicatorIndex = $(this).index();
                    $(AppH.Events).trigger(AppH.Events.CLICK_INDICATOR, [indicatorIndex]);

                    e.preventDefault();
                    return false;
                } else {
                    //href 이동
                }


            },

            _onClick_concierge = function(e){
                var isHasClass = $("#container").hasClass("concierge");
                if(isHasClass){
                    _closeMenu();
                    $(AppH.Events).trigger(AppH.Events.CLICK_CONCIERGE);

                    e.preventDefault();
                    return false;
                } else {
                    //href 이동
                }
            },



            _onClickOpenMnu = function(e){
                if($(e.currentTarget).parent().hasClass("opened")){
                    $(e.currentTarget).parent().toggleClass("opened");
                    var tgH = $(e.currentTarget).parent().find(".depth-2").innerHeight();

                    $(e.currentTarget).parent().find(".depth-2").parent().css("height", 0);
                } else {

                    var openmenu = $(e.currentTarget).parent().parent().find(".opened")
                    if(openmenu.length > 0){
                        openmenu.removeClass("opened")
                        openmenu.find(".depth-2").parent().css("height", 0);
                    }

                    $(e.currentTarget).parent().toggleClass("opened");
                    var tgH = $(e.currentTarget).parent().find(".depth-2").innerHeight();

                    $(e.currentTarget).parent().find(".depth-2").parent().css("height", tgH);
                }




            },

        // 컨텐츠 높이 넣어주기
            _setContentsHeight = function(){
                var targetHeigth = $('.common-menu .hoverable.default').find('.depth-2').height(); // 170727 / 이혜진 .common-menu 추가
                $('.common-menu .hoverable').find('.depth-2').attr("data-height", targetHeigth); // 170727 / 이혜진 .common-menu 추가
            },

            _onScrollMove = function(e, scrollTop){
                var winH = $(window).height();
                if(winH < scrollTop){
                    if(!_isShowBg){
                        //_showHeaderBg();
                    }
                }else{
                    if(_isShowBg){
                        //_hideHeaderBg();
                    }
                }

            },

            _showHeaderBg = function(){
                _isShowBg = true;
                TweenLite.killTweensOf($(".head-bg"));
                TweenLite.to($(".head-bg"), 0.4, {"css":{"top":0}, ease:Quart.easeOut});
            },

            _hideHeaderBg = function(){
                _isShowBg = false;
                TweenLite.killTweensOf($(".head-bg"));
                TweenLite.to($(".head-bg"), 0.6, {"css":{"top":-80}, ease:Quart.easeInOut});
            },

            _onResize = function(e){
                var winW = $(window).width();
                var winH = $(window).height();
            },


        // 메뉴 버튼을 클릭했을 때
            _onClick_btnMenu = function(e){
                _openMenu();
                return false;
            },

        // 닫기 버튼을 클릭했을 때
            _onClick_btnClose = function(e){
                _closeMenu();
                return false;
            },

        // 메뉴 열기
            _openMenu = function(){
                $(AppH.Events).trigger(AppH.Events.SHOW_POPUP);
                _isOpenMenu = true;

                $("header .side-menu").css({transform: "translate3d("+ -$(window).width()+"px, 0, 0)"});
                $("header .side-menu .btn-close-menu").css({transform: "translate3d("+ -$(window).width()+"px, 0, 0)"});
                $("header .common-menu .inner-wrap .right-menus .btn-open-menu").css({transform: "translate3d("+ -$(window).width()+"px, 0, 0)"});

                _contentsOverflowHidden();
                $('.side-menu').addClass('opened');
                $('header .common-menu .inner-wrap .right-menus .btn-open-menu').addClass('opened');
                $('html').addClass('no-sroll');

                TweenLite.killDelayedCallsTo(_closeMenuComplete);
                if( $('body').find(".overlay-bg").length == 0 ){
                    $("body").append("<div class='overlay-bg'><div class='overlay-bg-alpha'></div><div class='overlay-bg-black'></div></div>");
                }
                $(".overlay-bg .overlay-bg-black").css({transform: "translate3d("+ (-$(window).width()+50)+"px, 0, 0)"});
                TweenLite.to($('.overlay-bg'), 0, {"css":{"opacity":1}});

                return false;
            },


            _focusCurrentMenu = function(){
                $(".js-focus-menu").addClass("opened")

                var tgH = $('.common-menu .hoverable').find(".depth-2").innerHeight();   // 170727 / 이혜진 .common-menu 추가
                $('.common-menu .hoverable').find(".depth-2").parent().css("height", 0);    // 170727 / 이혜진 .common-menu 추가

                var tgH = $(".js-focus-menu").find(".depth-2").innerHeight();
                $(".js-focus-menu").find(".depth-2").parent().css("height", tgH);
            }

        // 메뉴
        _closeMenu = function(){

            _focusCurrentMenu();

            _isOpenMenu = false;

            $("header .side-menu").css({transform: "translate3d("+ 0+"px, 0, 0)"});
            $("header .side-menu .btn-close-menu").css({transform: "translate3d("+ 0+"px, 0, 0)"});
            $("header .common-menu .inner-wrap .right-menus .btn-open-menu").css({transform: "translate3d("+ 0+"px, 0, 0)"});


            $('.side-menu').removeClass('opened');
            $('header .common-menu .inner-wrap .right-menus .btn-open-menu').removeClass('opened');
            $('html').removeClass('no-sroll');

            TweenLite.to($('.overlay-bg'), 0, {"css":{"opacity":0}});
            $(".overlay-bg .overlay-bg-black").css({transform: "translate3d("+ 50+"px, 0, 0)"});
            TweenLite.delayedCall(0.9, _closeMenuComplete);

            _contnetsOverflowVisible();
            return false;
        },

            _closeMenuComplete = function(){
                $(".overlay-bg").remove();
                $(AppH.Events).trigger(AppH.Events.HIDE_POPUP);
            },

            _contentsOverflowHidden = function(){
                _contentsScrollTop = $(window).scrollTop();
                $("#container").css({"overflow":"hidden", "top":-_contentsScrollTop});
            },

            _contnetsOverflowVisible = function(){
                $("#container").css({"overflow":"visible", "top":0});
                $(window).scrollTop(_contentsScrollTop);
            },



            //
            _onShowShareBtn = function(){
                //console.log("_onShowShareBtn")
                $("header .side-menu .cont-bot").toggleClass("on");

            },


            //-------------------------------------------------
            //  검색 취소 버튼
            //-------------------------------------------------

            // 서치 인풋박스에서 키 다운
            _onKeydown_search = function(e){
                _checkSearchVal();
            },

            // 검색 값 체크
            _checkSearchVal = function(){
                /*
                 var val = $("header .side-menu .input-wrap input").val();
                 if(val.length != 0){
                 _showSearchCancelBtn();
                 }else{
                 _hideSearchCancelBtn();
                 }
                 */
            },

            // 검색 취소 버튼 보여주기
            _showSearchCancelBtn = function(){
                $("header .side-menu .input-wrap").addClass("has-txt");
            },

            // 검색 취소버튼 감추기
            _hideSearchCancelBtn = function(){
                $("header .side-menu .input-wrap").removeClass("has-txt");
            },

            _onClick_searchCanelBtn = function(e){
                $("header .side-menu .input-wrap input").val("");
                _checkSearchVal();
                $("header .side-menu .input-wrap input").focus();
                return false;
            },

            _onClick_searchIcon = function(){
                $("header .side-menu .input-wrap input").focus();
            }



        return {
            init : _init
        };
    })();

    var header = (function(){
        var _init = function(){
                _addEvent();
            },

            _addEvent = function(){
                _gnbEvent();
            },

            _gnbEvent = function(){
                var $hd = $('header.header_2017--mob');
                var $gnbWrap = $hd.find('.gnb-menu');
                var $gnbMenu = $gnbWrap.find('.cont-mid > ul > li');
                var $toggleBtn = $hd.find('.btn-open-menu2');

                $toggleBtn.on('click', function(e){
                  e.preventDefault();
                    if(!($gnbWrap.hasClass('opened'))) {
                        $gnbWrap.addClass('opened');
                        onOpenMenu();
                        $gnbMenu.on('click', function(){
                            onOpenSubMenu($(this));
                        })
                    } else if($gnbWrap.hasClass('opened')) {
                        onCloseMenu();
                        onCloseSubMenu();
                    }
                });

                function onOpenMenu() {
                    $('body').css('overflow', 'hidden');
                    $toggleBtn.addClass('on');
                    $gnbWrap.addClass('opened');
                }

                function onCloseMenu() {
                    $('body').css('overflow', '');
                    $toggleBtn.removeClass('on');
                    $gnbWrap.removeClass('opened');
                }

                function onOpenSubMenu($this) {
                    $gnbMenu.removeClass('on');
                    $this.addClass('on');
                    $gnbMenu.find('.sub-menu').hide();
                    $this.find('.sub-menu').show();
                    $this.find('.sub-menu').scrollTop(0);
                }

                function onCloseSubMenu(){
                    $gnbMenu.removeClass('on');
                    $gnbMenu.find('.sub-menu').hide();
                }
            };

        return {
            init : _init
        };
    })();

    $(function(){
        AppH.navigation = navigation;
        AppH.navigation.init();
        AppH.header = header;
        AppH.header.init();
    });
}(window, jQuery));
