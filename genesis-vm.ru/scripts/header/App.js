//------------------------------------------------------------------
//  last update: 2016.08.30
//------------------------------------------------------------------
"use strict";

(function () {
	if (typeof App == 'undefined' || !App) {
		var App = {};

		App.GlobalVars = {
			/* 디바이스 종류 */
			currentDevice: -1,
			DEVICE_TYPE_PC: 0,
			DEVICE_TYPE_TABLET: 1,
			DEVICE_TYPE_MOBILE: 2,

			/* OS 종류 */
			currentOS: -1,
			OS_TYPE_ANDROID: 0,
			OS_TYPE_IOS: 1,
			OS_TYPE_ETC: 2,
			//
			OS_VER: 0,

			isLowIE: false,
			isIE8: false,
			isIE9: false,
			isMSIE: false,

			/* window size */
			windowWidth: 0,
			windowHeight: 0,
			windowInnerWidth: 0,
			windowInnerHeight: 0,

			WIN_MIN_WIDTH: 1024,
			WIN_MAX_WIDTH: 1920,

			WIN_MIN_HEIGHT: 730,

			isWheelLock: false,
			isSnappingContents: false
		}, App.Events = {
			BEFORE_DOCUMENT_READY: "beforedocumentready",
			DOCUMENT_READY: "documentready",
			RESIZE_BROWSER: "resizebrowser",
			RESIZE_COMPLETE: "resizecomplete",
			SCROLL_MOVE: "scrollmove",
			LOAD_COMPLETE: "loadcomplete",
			LOAD_PROGRESS: "loadprogress",
			MOUSE_WHEEL: "wheel",
			VIDEO_COMPLETE: "videocomplete",
			CLICK_INDICATOR: "clickindicator",
			CLICK_CONCIERGE: "clickconcierge",
			CHANGE_SECTION: "changesection",
			CHANGE_MOVE_PER: "changemoveper",
			CHANGE_INDIGATOR_COLOR: "changeIndigatorColor",
			IMAGE_LOAD_COMPLETE: "imageloadcomplete",

			SHOW_HEADER_BG: "showHeaderBg",
			HIDE_HEADER_BG: "hideHeaderBg",

			OPEN_NAVI: "openNavigation",
			CLOSE_NAVI: "closeNavigation",

			OPEN_SELECT_BOX: "openSelectBox",
			CLOSE_SELECT_BOX: "closeSelectBox",

			CHECK_FIRST_VISIT: "checkfirstvisit",
			CHANGE_360VR: "change360vr",
			BNP_CHANGE_STEP: "bnpchangestep",
			APP_READY: "appready",
			SHOW_SHARE_GALLERY: "showsharegallery",
			SHOW_SHARE_HIGHLIGHT: "showsharehighlight",
			SHOW_POPUP: "showpopup",
			HIDE_POPUP: "hidepopup",
			CHANGE_CUSTOM_SELECT: "changecustomselect",

			DIVISION_PAGE_LOAD_START: "divisionpageloadstart",
			DIVISION_PAGE_LOAD_COMPLETE: "divisionpageloadcomplete",
			DIVISION_PAGE_LOAD_IMAGE_COMPLETE: "divisionpageloadimagecomplete",
			DIVISION_PAGE_LOAD_CONTINUE: "divisionpageloadcontinue",

			PIP_DEV_SETTING_COMPLETE: "pipDevSettingComplete",

			// reset 360 spec : 2016 08 30 - VAM
			RESET_MODEL_360_SPEC: "resetModel360Spec"
		}, App.init = function () {
			// detect.js의 ie8 여부 저장하기
			if (_Browser.ie8) {
				App.GlobalVars.isIE8 = true;
			}

			// IE9 이하 버전 일때
			if (_Browser.ie7 || _Browser.ie8 || _Browser.ie9) {
				App.GlobalVars.isLowIE = true;
			}

			// detect.js의 디바이스 타입 가져오기
			App.GlobalVars.currentDevice = _Device.type;
			App.GlobalVars.isIE9 = _Browser.ie9;
			App.GlobalVars.currentOS = _Device.os;
			App.GlobalVars.isMSIE = _Browser.msie;
			App.GlobalVars.OS_VER = _Device.osVer;

			// 마지막 App.js까지 완료되었을 때
			$(App.Events).on(App.Events.APP_READY, function () {
				toAEM_appReady();
			});
			$(App.Events).on(App.Events.IMAGE_LOAD_COMPLETE, function () {
				toAEM_imageLoadComplete();
			});
			$(App.Events).on(App.Events.BNP_CHANGE_STEP, function (e, arrElement, prevStep, currentStep) {
				toAEM_bnp_changeStep(arrElement, prevStep, currentStep);
			});
		};
	}

	//------------------------------------------------------------------
	//  last update: 2017. 07. 28
	//------------------------------------------------------------------

	(function (window, $, undefined) {
		if($('body').data('dealer')==true) return;
		
		
		if ($('html.tablet').length || $('html.desktop').length) {
			var navigation = (function () {

				var _conHeight = $('.side-menu .hoverable.default').find('.depth-2').height(); // 170720 / 이혜진 / side-menu 추가

				var _isShowBg = false;
				var _isShowMenu = false;
				var _isModelPage = false;

				var _hasKeyvisual = false;

				var _currentIndex = -1;

				var _isLinkMenu = false;

				var _init = function _init() {
						if(!$('header.header_2017').length) return;

						_isModelPage = $(".wrapper").find(".model").length != 0 ? true : false;

						_setContentsHeight();
						_addEvent();
						_onResize();
						_navigationOut();

						_isLinkMenu = $(".js-focus-menu").hasClass("js-link-menu");

						_onCloseMenuComplete();
						//_onCloseHeaderLocComplete();
					},
					_addEvent = function _addEvent() {
						//  리사이징
						$(App.Events).on(App.Events.RESIZE_BROWSER, _onResize);

						// scroll event
						$(App.Events).on(App.Events.SCROLL_MOVE, _onScrollMove);

						$(App.Events).on(App.Events.CHANGE_SECTION, _onChange_section);

						$(App.Events).on(App.Events.SHOW_POPUP, _onCloseMenu);

						// models 서브 메뉴 클릭시
						$("header .side-menu .cont-mid .nav-high-priority .depth-2 li").on("click", _onClick_Indicator);

						// concierge 메뉴 클릭시
						$("header .common-menu .js-concierge a").on("click", _onClick_concierge);

						$(".side-menu .hoverable").on("mouseover", _onOverMenu); // 170720 / 이혜진 / side-menu 추가

						$(".btn-open-menu").on("click", _onOpenMenu);
						$(".btn-close-menu").on("click", _onCloseMenu);

						$("header.header_2017 .side-menu .input-wrap input").on("keydown", _onKeydown_search);
						$("header.header_2017 .side-menu .input-wrap input").on("focus", _onFocus_search);
						$("header.header_2017 .side-menu .input-wrap a.btn-cancle").on("click", _onClick_searchCanelBtn);
						$("header.header_2017 .side-menu .input-wrap>img").on("click", _onClick_searchIcon);
						$("header.header_2017 .side-menu .input-wrap input").on("focusout", _onFocusOut_search);
						//$("header .side-menu .input-wrap a.btn-search").on("click", _onClick_searchSearchBtn);

						$("header.header_2017 .side-menu").on("mouseover", _navigationOver);
						$("header.header_2017 .side-menu").on("mouseout", _navigationOut);

						// Accessibility - for access : VAM 20160816
						$(".side-menu .hoverable>a").on("focusin", _onFocus_hoverMenu); // 170720 / 이혜진 / side-menu 추가
						$(".side-menu .hoverable ul li a").on("focusin", _onFocus_hoverMenu_sub); // 170720 / 이혜진 / side-menu 추가
						$(".btn-open-menu").on("focusin", _onFocus_openMenu);
						$("header .common-menu .location a").on("focusin", _onFocus_location);
						$(".shortcut-wrap .shortcuts li a").on("focusin", _onFocus_shortcut);
						$("header .side-menu a:last").on("focusout", _onFocusOut_lastFocus);

						if (App.GlobalVars.currentDevice == App.GlobalVars.DEVICE_TYPE_TABLET) {
							var highNaviLen = $(".nav-high-priority>li").length;
							var i = 0;
							for (i = 0; i < highNaviLen; i++) {
								var hasSubMenu = $(".nav-high-priority>li>ul").length;
								if (hasSubMenu != 0) {
									$(".nav-high-priority>li").eq(i).find(">a").on("click", _onClick_tabletMenu);
								}
							}
						}
					},
					_navigationOver = function _navigationOver(e) {
						TweenLite.killDelayedCallsTo(_controlHoverable_SideMenu);
						TweenLite.killDelayedCallsTo(_controlSideMenu);
					},
					_navigationOut = function _navigationOut(e) {
						if ($(".js-focus-menu").find(".depth-2").length != 0) {
							//hoveralble menu focus
							TweenLite.killDelayedCallsTo(_controlHoverable_SideMenu);
							TweenLite.killDelayedCallsTo(_controlSideMenu);
							TweenLite.delayedCall(1, _controlSideMenu, [$(".js-focus-menu"), Quart.easeInOut]);
						} else {
							TweenLite.killDelayedCallsTo(_controlHoverable_SideMenu);
							TweenLite.killDelayedCallsTo(_controlSideMenu);
							TweenLite.delayedCall(1, _controlHoverable_SideMenu, [-1, Quart.easeInOut]);
						}
					},

					// 컨텐츠 높이 넣어주기
					_setContentsHeight = function _setContentsHeight() {
						var sub = $('.side-menu .hoverable').find('.depth-2'); // 170720 / 이혜진 / side-menu 추가
						var subLen = sub.length;
						for (var i = 0; i < subLen; i++) {
							$(sub[i]).attr("data-height", $(sub[i]).height());
						}
					},
					_onResize = function _onResize() {
						// 사이드 메뉴 높이 넣어주기
						var sMenuHeight = $(window).innerHeight();
						if (sMenuHeight < 530) sMenuHeight = 530;
						$('.side-menu').css({ lineHeight: sMenuHeight + 'px' });

						//keyvisual check
						_hasKeyvisual = $(".wrapper").find(".kv-area").length != 0 ? true : false;
					},
					_onScrollMove = function _onScrollMove(e, scrollTop) {
						_onCloseMenu();

						_hasKeyvisual = $(".wrapper").find(".kv-area").length != 0 ? true : false;

						//if(!_hasKeyvisual) return;
						if (!_hasKeyvisual) {

							if (!_isShowBg && _isModelPage) {
								_showHeaderBg();
							}
							return;
						}

						var winW = $(window).width();
						var winH = $(window).height();

						if (winW < winH) {
							if (winH > App.GlobalVars.WIN_MIN_HEIGHT) winH = App.GlobalVars.WIN_MIN_HEIGHT;
						}

						if (winH <= scrollTop + 130) {
							if (!_isShowBg) {
								_showHeaderBg();
							}
						} else {
							if (_isShowBg) {
								_hideHeaderBg();
							}
						}
					},

					// top blackbar control
					_showHeaderBg = function _showHeaderBg() {
						_isShowBg = true;
						TweenLite.killTweensOf($(".head-bg"));
						TweenLite.to($(".head-bg"), 0.4, { "css": { "top": 0 }, ease: Quart.easeOut });

						var loc = $("header .common-menu .location");
						loc.removeClass("hide");

						TweenLite.killTweensOf(loc);
						TweenLite.to(loc, 0.4, { "css": { "top": 30 }, ease: Quart.easeOut });

						$(App.Events).trigger(App.Events.SHOW_HEADER_BG);
					},
					_hideHeaderBg = function _hideHeaderBg() {
						_isShowBg = false;
						TweenLite.killTweensOf($(".head-bg"));
						TweenLite.to($(".head-bg"), 0.6, { "css": { "top": -80 }, ease: Quart.easeInOut });

						var loc = $("header .common-menu .location");
						TweenLite.killTweensOf(loc);
						TweenLite.to(loc, 0.6, { "css": { "top": -20 }, ease: Quart.easeInOut, onComplete: _onCloseHeaderLocComplete });

						$(App.Events).trigger(App.Events.HIDE_HEADER_BG);
					},
					_onCloseHeaderLocComplete = function _onCloseHeaderLocComplete() {

						_isShowBg = false;
						TweenLite.killTweensOf($(".head-bg"));
						TweenLite.set($(".head-bg"), { "css": { "top": -80 } });

						var loc = $("header .common-menu .location");
						TweenLite.killTweensOf(loc);
						TweenLite.set(loc, { "css": { "top": -20 } });

						//loc.addClass("hide");
					},
					_onFocus_location = function _onFocus_location() {
						_showHeaderBg();
					},
					_onFocus_shortcut = function _onFocus_shortcut() {
						_showHeaderBg();
					},
					_changeColor = function _changeColor() {

						var i = 0;
						var len = $(".js-check-height").length;
						var hasClass = $("header .common-menu").hasClass("dark");
						var indicatorColor = $(".js-check-height").eq(_currentContentsIndex).data("indicator-color");
						if (indicatorColor == "black") {
							if (!hasClass) {
								$("header .common-menu").addClass("dark");
							}
						} else {
							if (hasClass) {
								$("header .common-menu").removeClass("dark");
							}
						}
					},

					// 메뉴 오버
					_onOverMenu = function _onOverMenu(e) {
						var target = $(e.currentTarget);
						if (!$(target).hasClass('on-over')) {
							_controlSideMenu($(target));
						}
					},
					_onFocus_hoverMenu = function _onFocus_hoverMenu(e) {
						var target = $(e.currentTarget).closest(".hoverable");
						//console.log("_onFocus_hoverMenu" + target);
						if (!$(target).hasClass('on-over')) {
							_controlSideMenu($(target));
						}
					},
					_onFocus_hoverMenu_sub = function _onFocus_hoverMenu_sub(e) {
						var target = $(e.currentTarget).closest(".hoverable");
						//console.log("_onFocus_hoverMenu_sub" + target);

						if (!$(target).hasClass('on-over')) {
							_controlSideMenu($(target));
						}
					},
					_controlSideMenu = function _controlSideMenu(openMnu, easing) {
						_controlHoverable_SideMenu(openMnu.index(), easing);
					},
					_controlHoverable_SideMenu = function _controlHoverable_SideMenu(index, easing) {
						if (index == -1) _controlLow_SideMenu($(".js-focus-menu").index());else _controlLow_SideMenu(-1);
						var hoverMnu = $('.side-menu .nav-high-priority .hoverable'); // 170720 / 이혜진 / side-menu 추가
						var openMnu, openMnu_depth2, _conHeight;
						var time = 0.5;
						var ease = easing == undefined ? Cubic.easeOut : easing;
						for (var i = 0; i < hoverMnu.length; i++) {
							openMnu = hoverMnu.eq(i);
							openMnu_depth2 = openMnu.find('.depth-2');

							if (index == i) {
								//hover
								_conHeight = openMnu_depth2.attr("data-height");

								openMnu.addClass('on-over');
								openMnu_depth2.addClass('on');
								TweenLite.killTweensOf(openMnu);
								TweenLite.to(openMnu, time, { "css": { paddingBottom: parseInt(_conHeight) + 20 }, ease: ease });
								TweenLite.killTweensOf(openMnu.find('.depth-2'));
								TweenLite.to(openMnu.find('.depth-2'), time, { "css": { height: _conHeight }, ease: ease });
							} else {

								openMnu.removeClass('on-over');
								openMnu_depth2.removeClass('on');
								TweenLite.killTweensOf(openMnu);
								TweenLite.to(openMnu, time, { "css": { paddingBottom: 0 }, ease: ease });
								TweenLite.killTweensOf(openMnu.find('.depth-2'));
								TweenLite.to(openMnu.find('.depth-2'), time, { "css": { height: 0 }, ease: ease });
							}
						}
					},
					_controlLow_SideMenu = function _controlLow_SideMenu(index) {
						var lowMnu = $('.nav-low-priority li');
						for (var i = 0; i < lowMnu.length; i++) {
							var mnu = lowMnu.eq(i);
							if (index == i) {
								mnu.addClass("activated");
							} else {
								mnu.removeClass("activated");
							}
						}
					},

					// 섹션 변경
					_onChange_section = function _onChange_section(e, indicatorIndex, sectionIndex) {
						_onChange_indicator(indicatorIndex);
					},

					// 서브메뉴 변경
					_onChange_indicator = function _onChange_indicator(index) {

						if (_currentIndex != index && !_isLinkMenu) {
							_offIndicator(_currentIndex);
							_currentIndex = index;
							_onIndicator(_currentIndex);
						}
					},

					// 서브메뉴 활성화
					_onIndicator = function _onIndicator(index) {
						$("header .side-menu .cont-mid .nav-high-priority .js-focus-menu .depth-2 li").eq(index).addClass("on");
					},

					// 서브메뉴 비활성 화
					_offIndicator = function _offIndicator(index) {
						$("header .side-menu .cont-mid .nav-high-priority .depth-2 li").removeClass("on");
					},

					// models 서브메뉴 클릭했을 때
					_onClick_Indicator = function _onClick_Indicator(e) {

						// 같은 메뉴에서 서브메뉴 클릭시 href무시하고 인디게이터 클릭이동으로
						var arrNavClass = $(this).parent().parent().attr("class").split(" ");
						var focusClass = "js-focus-menu";

						var navClass,
							isSame = false;
						for (var i = 0; i < arrNavClass.length; i++) {
							navClass = arrNavClass[i];
							if (navClass == focusClass) {
								isSame = true;
							}
						}

						if (_isLinkMenu) isSame = false;

						if (isSame) {
							//trigger
							_onCloseMenu();
							var indicatorIndex = $(this).index();
							$(App.Events).trigger(App.Events.CLICK_INDICATOR, [indicatorIndex]);

							e.preventDefault();
							return false;
						} else {
							//href 이동
						}
					},
					_onClick_concierge = function _onClick_concierge(e) {
						var isHasClass = $("#container").hasClass("concierge");
						if (isHasClass) {
							$(App.Events).trigger(App.Events.CLICK_CONCIERGE);

							e.preventDefault();
							return false;
						} else {
							//href 이동
						}
					},

					// 메뉴 열기
					_onOpenMenu = function _onOpenMenu() {
						if (!_isShowMenu) {
							var _sideMenu = $('.side-menu');
							_sideMenu.removeClass("hide");

							_isShowMenu = true;
							TweenLite.killTweensOf(_sideMenu);
							TweenLite.to(_sideMenu, 0.7, { "css": { right: 0 }, ease: Quart.easeOut });

							$(App.Events).trigger(App.Events.OPEN_NAVI);

							// 2016. 08. 16 : VAM
							$("._sideMenu").focus();
						}

						return false;
					},
					_onFocus_openMenu = function _onFocus_openMenu(e) {
						_onCloseMenu();
					},
					_onFocusOut_lastFocus = function _onFocusOut_lastFocus(e) {
						_onCloseMenu();
					},

					// 메뉴 닫기
					_onCloseMenu = function _onCloseMenu() {
						if (_isShowMenu) {
							var _sideMenu = $('.side-menu');

							_isShowMenu = false;
							TweenLite.killTweensOf(_sideMenu);
							TweenLite.to(_sideMenu, 0.6, {
								"css": { right: -470 },
								ease: Quart.easeInOut,
								onComplete: _onCloseMenuComplete
							});
							$("header .side-menu .input-wrap input").blur();

							$(App.Events).trigger(App.Events.CLOSE_NAVI);

							// 2016. 08. 12 : VAM
							$(".btn-open-menu").focus();

							console.log("_onCloseMenu");
						}
						return false;
					},
					_onCloseMenuComplete = function _onCloseMenuComplete() {

						_isShowMenu = false;

						var _sideMenu = $('.side-menu');
						TweenLite.killTweensOf(_sideMenu);
						TweenLite.set(_sideMenu, { "css": { right: -470 } });

						$(_sideMenu).addClass("hide");
					},

					//-------------------------------------------------
					//  검색 취소 버튼
					//-------------------------------------------------

					_onClick_tabletMenu = function _onClick_tabletMenu(e) {
						var index = $(e.currentTarget).parent().index();
						_controlHoverable_SideMenu(index);

						return false;
					},

					//-------------------------------------------------
					//  검색 취소 버튼
					//-------------------------------------------------

					// 서치 인풋박스에서 키 다운
					_onKeydown_search = function _onKeydown_search(e) {
						_checkSearchVal();
					},
					_onFocus_search = function _onFocus_search(e) {
						if (App.GlobalVars.currentDevice == App.GlobalVars.DEVICE_TYPE_TABLET && _Device.os == 1) {
							toApp_onSearchClick();
						} else {
							_showBtn_search();
						}
					},
					_onFocusOut_search = function _onFocusOut_search(e) {
						if ($("header .side-menu .input-wrap input").val() == "") {

							_hideBtn_search();
						} else {
							_showBtn_search();
						}
					},
					_showBtn_search = function _showBtn_search() {
						$("header .side-menu .input-wrap").addClass("has-txt");
					},
					_hideBtn_search = function _hideBtn_search() {
						$("header .side-menu .input-wrap").removeClass("has-txt");
					},

					// 검색 값 체크
					_checkSearchVal = function _checkSearchVal() {
						/*
						 var val = $("header .side-menu .input-wrap input").val();
						 if(val.length != 0){
						 _showSearchCancelBtn();
						 }else{
						 _hideSearchCancelBtn();
						 }
						 */
					},

					// 검색 취소 버튼 보여주기
					_showSearchCancelBtn = function _showSearchCancelBtn() {
						$("header .side-menu .input-wrap").addClass("has-txt");
					},

					// 검색 취소버튼 감추기
					_hideSearchCancelBtn = function _hideSearchCancelBtn() {
						$("header .side-menu .input-wrap").removeClass("has-txt");
					},
					_onClick_searchCanelBtn = function _onClick_searchCanelBtn(e) {
						$("header .side-menu .input-wrap input").val("");
						_checkSearchVal();
						$("header .side-menu .input-wrap input").focus();
						return false;
					},
					_onClick_searchIcon = function _onClick_searchIcon() {
						$("header .side-menu .input-wrap input").focus();
					};

				return {
					init: _init
				};
			})();

			var header = (function () {
				var _gnbEvent = function _gnbEvent() {
						var $gnbWrap = $('header.header_2017 .gnb-menu');
						var $gnbMenu = $gnbWrap.find('.nav-high-priority > li');
						var $gnbMenuLink = $gnbMenu.find(' > a');
						var $subMenu = $gnbMenuLink.next('.sub-menu');
						var $gnbBg = $gnbWrap.find('.gnb-background');

						$gnbMenuLink.on('mouseenter focus', function () {
							onOpenMenu($(this));
						});

						$gnbWrap.on('mouseleave', onCloseMenu);
						$gnbWrap.prevAll().find('a, input').on('focus', onCloseMenu);
						$gnbWrap.nextAll().find('a, input').on('focus', onCloseMenu);
						brandMenuEvent();

						function onOpenMenuBg() {
							$("header.header_2017 .cont-mid").stop(true, false).animate({
								"height": "290px"
							}, 500, 'easeOutCubic');
						}

						function onCloseMenuBg() {
							$("header.header_2017 .cont-mid").stop(true, false).animate({
								"height": "60px"
							}, 300, 'easeOutCubic');
						}

						function onOpenMenu($this) {
							$gnbMenu.removeClass('on');

							if (!$gnbWrap.hasClass('opened')) {
                                if(!$this.parent('.hoverable').hasClass('single')){
                                    $gnbWrap.addClass('opened');
                                    onOpenMenuBg();
                                }
								setTimeout(function () {
									$gnbMenu.removeClass('on');
									$subMenu.hide();
									$this.parent('li').addClass('on');
									$this.parent('li').find('.sub-menu').show();
								}, 10);
							} else if ($gnbWrap.hasClass('opened')) {
                                if($this.parent('.hoverable').hasClass('single')){
                                    $gnbWrap.removeClass('opened');
                                    onCloseMenuBg();
                                }

								$gnbBg.show();
								$gnbMenu.removeClass('on');
								$subMenu.hide();
								$this.parent('li').addClass('on');
								$this.parent('li').find('.sub-menu').show();
							}
						}

						function onCloseMenu() {
                            if($gnbWrap.hasClass('opened')){
                                setTimeout(function(){
                                    onCloseMenuBg();
                                }, 0);
                            }

							$gnbWrap.removeClass('opened');
							$gnbMenu.removeClass('on');
							$subMenu.hide();
						}

						function brandMenuEvent() {
							$('header.header_2017 .brand .depth-2 li a').on('mouseenter focus', function () {
								figFadeIn($(this));
							});
							$('header.header_2017 .brand .depth-2 li a').on('mouseleave blur', function () {
								figFadeOut($(this));
							});

							function figFadeIn($this) {
								$this.parent().find('.figure').stop().fadeIn();
							};

							function figFadeOut($this) {
								$this.parent().find('.figure').stop().fadeOut();
							};
						}
					},
					_slideMenuEvent = function _slideMenuEvent(target) {
						target.find('> a').on('mouseenter focus', function () {
							onOpenMenu($(this));
						});
						target.on('mouseleave', function () {
							onCloseMenu($(this));
						});
						target.nextAll().find('a, input').on('focus', function () {
							onCloseMenu(target);
						});
						target.prevAll().find('a, input').on('focus', function () {
							onCloseMenu(target);
						});
						target.closest('header').nextAll().find('a, input').on('focus', function () {
							onCloseMenu(target);
						});

						function onOpenMenu($this) {
							if (!$this.parent().hasClass('opened')) {
								$this.parent().addClass('opened');
								$this.parent().find('ul').stop().slideDown(300, 'easeOutQuart');
							}
						}

						function onCloseMenu($this) {
							$this.removeClass('opened');
							$this.find('ul').stop().slideUp(300, 'easeOutQuart');
						}
					},
					_headerBgEvent = function _headerBgEvent() {
						var $hd = $('header.header_2017');
						var $hdBg = $hd.find('.header-background');
						var hdH = $hd.height();
						var hdTop = $hd.offset().top;
						if ($('section').eq(1).length) {
							var sec2Top = $('section').eq(1).offset().top - hdH;
						}
						var $gnbMenu = $hd.find('.gnb-menu');
						var $quickMenu = $hd.find('.quicklink-btns');
						var $langMenu = $hd.find('.lang-btns');
						var condition = $gnbMenu.hasClass('opened') || $quickMenu.hasClass('opened') || $langMenu.hasClass('opened');

						var arr = [$gnbMenu, $quickMenu, $langMenu];

						if ($('section').eq(1).length) {
							var i;

							(function () {
								var showHeaderBg = function showHeaderBg() {
									$hdBg.stop(true, false).animate({
										"opacity": "1"
									}, 300, 'easeOutCubic');
								};

								var hideHeaderBg = function hideHeaderBg() {
									$hdBg.stop(true, false).delay(300).animate({
										"opacity": "0.3"
									}, 300, 'easeOutCubic');
								};

								for (i = 0; i < arr.length; i++) {
									arr[i].on('mouseover keyup', function () {
										if (hdTop >= 0 && hdTop < sec2Top) {
											if ($(this).hasClass('opened')) {
												showHeaderBg();
											}
										}
									});

									arr[i].on('mouseout', function () {
										if (hdTop >= 0 && hdTop < sec2Top) {
											if (!$(this).hasClass('opened')) {
												hideHeaderBg();
											}
										} else {
											showHeaderBg();
										}
									});
								}

								$hd.find('.input-wrap').find('a, input').on('click focus keyup keydown', function () {
									if (hdTop >= 0 && hdTop < sec2Top) {
										showHeaderBg();
									}
								});

								$hd.find('.input-wrap').find('a, input').on('blur', function () {
									if (hdTop >= 0 && hdTop < sec2Top) {
										hideHeaderBg();
									} else {
										showHeaderBg();
									}
								});

								if (hdTop >= 0 && hdTop < sec2Top) {
									if (condition) {
										showHeaderBg();
									} else {
										hideHeaderBg();
									}
								} else {
									showHeaderBg();
								}
							})();
						}
					};

				var _init = function _init() {
						_addEvent();
					},
					_addEvent = function _addEvent() {
						_gnbEvent();
						_slideMenuEvent($('header.header_2017 .quicklink-btns'));
						_slideMenuEvent($('header.header_2017 .lang-btns'));

						$(window).scroll(function () {
							_headerBgEvent();
						});
						$(window).load(function () {
							_headerBgEvent();
						});

						$("header.header_2017 .right-menus .input-wrap input").on("keydown", _onKeydown_search);
						$("header.header_2017 .right-menus .input-wrap input").on("focus", _onFocus_search);
						$("header.header_2017 .right-menus .input-wrap a.btn-cancle").on("click", _onClick_searchCanelBtn);
						$("header.header_2017 .right-menus .input-wrap>img").on("click", _onClick_searchIcon);
						$("header.header_2017 .right-menus .input-wrap input").on("focusout", _onFocusOut_search);
					};

				//-------------------------------------------------
				//  검색 취소 버튼
				//-------------------------------------------------

				// 서치 인풋박스에서 키 다운
				var _onKeydown_search = function _onKeydown_search(e) {
						_checkSearchVal();
					},
					_onFocus_search = function _onFocus_search(e) {
						if (App.GlobalVars.currentDevice == App.GlobalVars.DEVICE_TYPE_TABLET && _Device.os == 1) {
							toApp_onSearchClick();
						} else {
							_showBtn_search();
						}
					},
					_onFocusOut_search = function _onFocusOut_search(e) {
						if ($("header.header_2017 .right-menus .input-wrap input").val() == "") {

							_hideBtn_search();
						} else {
							_showBtn_search();
						}
					},
					_showBtn_search = function _showBtn_search() {
						$("header.header_2017 .right-menus .input-wrap").addClass("has-txt");
					},
					_hideBtn_search = function _hideBtn_search() {
						$("header.header_2017 .right-menus .input-wrap").removeClass("has-txt");
					},

					// 검색 값 체크
					_checkSearchVal = function _checkSearchVal() {
						// var val = $("header .right-menus .input-wrap input").val();
						// if(val.length != 0){
						// _showSearchCancelBtn();
						// }else{
						// _hideSearchCancelBtn();
						// }

					},

					// 검색 취소 버튼 보여주기
					_showSearchCancelBtn = function _showSearchCancelBtn() {
						$("header.header_2017 .right-menus .input-wrap").addClass("has-txt");
					},

					// 검색 취소버튼 감추기
					_hideSearchCancelBtn = function _hideSearchCancelBtn() {
						$("header.header_2017 .right-menus .input-wrap").removeClass("has-txt");
					},
					_onClick_searchCanelBtn = function _onClick_searchCanelBtn(e) {
						$("header.header_2017 .right-menus .input-wrap input").val("");
						_checkSearchVal();
						$("header.header_2017 .right-menus .input-wrap input").focus();
						return false;
					},
					_onClick_searchIcon = function _onClick_searchIcon() {
						$("header.header_2017 .right-menus .input-wrap input").focus();
					};

				return {
					init: _init
				};
			})();

			$(function () {
				App.navigation = navigation;
				App.navigation.init();
				App.header = header;
				App.header.init();
			});
		}
	})(window, jQuery);

	//------------------------------------------------------------------
	//  last update: 2017.07.31 - ver 1.0
	//------------------------------------------------------------------

	(function (window, $, undefined) {
		if($('body').data('dealer')==true) return;
		
		if ($('html.mobile').length) {
			var navigation = (function () {

				var _isShowBg = false;
				var _isOpenMenu = false;
				var _contentsScrollTop = 0;

				var _currentIndex = -1;

				var _currentScreenW = 0;
				var _isLinkMenu = false;

				var _init = function _init() {
						_show();
					},
					_show = function _show() {
						_addEvent();
						_focusCurrentMenu();

						_isLinkMenu = $(".js-focus-menu").hasClass("js-link-menu");
					},
					_addEvent = function _addEvent() {

						$(App.Events).on(App.Events.SCROLL_MOVE, _onScrollMove);
						$(App.Events).on(App.Events.CHANGE_SECTION, _onChange_section);

						//$(App.Events).on(App.Events.RESIZE_BROWSER, _onResize);
						$(window).on("orientationchange", _onOrientationChange);

						// 서브 메뉴 클릭시
						$("header .side-menu .nav-high-priority .depth-2 li").on("click", _onClick_Indicator);

						$(".btn-open-menu").on("click", _onClick_btnMenu);
						$(".btn-close-menu").on("click", _onClick_btnClose);

						$("header .side-menu .cont-mid .btn-accordion").on("click", _onClickOpenMnu);

						// concierge 메뉴 클릭시
						$("header .common-menu .js-concierge a").on("click", _onClick_concierge);

						//share btn click
						$("header .side-menu .cont-bot .btn-side-share").on("click", _onShowShareBtn);

						$("header .side-menu .input-wrap input").on("keydown", _onKeydown_search);
						$("header .side-menu .input-wrap a.btn-cancel").on("click", _onClick_searchCanelBtn);
						$("header .side-menu .input-wrap>img").on("click", _onClick_searchIcon);
					},
					_onOrientationChange = function _onOrientationChange() {
						if (_isOpenMenu) {
							_closeMenu();
						}
					},

					// 섹션 변경
					_onChange_section = function _onChange_section(e, indicatorIndex, sectionIndex) {
						if (_isOpenMenu) return;
						_onChange_indicator(indicatorIndex);
					},

					// 서브메뉴 변경
					_onChange_indicator = function _onChange_indicator(index) {
						if (_currentIndex != index && !_isLinkMenu) {
							_offIndicator(_currentIndex);
							_currentIndex = index;
							_onIndicator(_currentIndex);
						}
					},

					// 서브메뉴 활성화
					_onIndicator = function _onIndicator(index) {
						var indexCheck = index;
						$("header .side-menu .nav-high-priority .js-focus-menu .depth-2 li").eq(index).addClass("on");
					},

					// 서브메뉴 비활성 화
					_offIndicator = function _offIndicator(index) {
						$("header .side-menu .nav-high-priority .depth-2 li").removeClass("on");
					},

					// models 서브메뉴 클릭했을 때
					_onClick_Indicator = function _onClick_Indicator(e) {

						_closeMenu();

						var arrNavClass = $(this).parent().parent().parent().attr("class").split(" ");
						var focusClass = "js-focus-menu";

						var navClass,
							conClass,
							isSame = false;
						for (var i = 0; i < arrNavClass.length; i++) {
							navClass = arrNavClass[i];
							if (navClass == focusClass) {
								isSame = true;
							}
						}

						if (_isLinkMenu) isSame = false;

						if (isSame) {
							//trigger
							var indicatorIndex = $(this).index();
							$(App.Events).trigger(App.Events.CLICK_INDICATOR, [indicatorIndex]);

							e.preventDefault();
							return false;
						} else {
							//href 이동
						}
					},
					_onClick_concierge = function _onClick_concierge(e) {
						var isHasClass = $("#container").hasClass("concierge");
						if (isHasClass) {
							_closeMenu();
							$(App.Events).trigger(App.Events.CLICK_CONCIERGE);

							e.preventDefault();
							return false;
						} else {
							//href 이동
						}
					},
					_onClickOpenMnu = function _onClickOpenMnu(e) {
						if ($(e.currentTarget).parent().hasClass("opened")) {
							$(e.currentTarget).parent().toggleClass("opened");
							var tgH = $(e.currentTarget).parent().find(".depth-2").innerHeight();

							$(e.currentTarget).parent().find(".depth-2").parent().css("height", 0);
						} else {

							var openmenu = $(e.currentTarget).parent().parent().find(".opened");
							if (openmenu.length > 0) {
								openmenu.removeClass("opened");
								openmenu.find(".depth-2").parent().css("height", 0);
							}

							$(e.currentTarget).parent().toggleClass("opened");
							var tgH = $(e.currentTarget).parent().find(".depth-2").innerHeight();

							$(e.currentTarget).parent().find(".depth-2").parent().css("height", tgH);
						}
					},

					// 컨텐츠 높이 넣어주기
					_setContentsHeight = function _setContentsHeight() {
						var targetHeigth = $('.common-menu .hoverable.default').find('.depth-2').height(); // 170727 / 이혜진 .common-menu 추가
						$('.common-menu .hoverable').find('.depth-2').attr("data-height", targetHeigth); // 170727 / 이혜진 .common-menu 추가
					},
					_onScrollMove = function _onScrollMove(e, scrollTop) {
						var winH = $(window).height();
						if (winH < scrollTop) {
							if (!_isShowBg) {
								//_showHeaderBg();
							}
						} else {
							if (_isShowBg) {
								//_hideHeaderBg();
							}
						}
					},
					_showHeaderBg = function _showHeaderBg() {
						_isShowBg = true;
						TweenLite.killTweensOf($(".head-bg"));
						TweenLite.to($(".head-bg"), 0.4, { "css": { "top": 0 }, ease: Quart.easeOut });
					},
					_hideHeaderBg = function _hideHeaderBg() {
						_isShowBg = false;
						TweenLite.killTweensOf($(".head-bg"));
						TweenLite.to($(".head-bg"), 0.6, { "css": { "top": -80 }, ease: Quart.easeInOut });
					},
					_onResize = function _onResize(e) {
						var winW = $(window).width();
						var winH = $(window).height();
					},

					// 메뉴 버튼을 클릭했을 때
					_onClick_btnMenu = function _onClick_btnMenu(e) {
						_openMenu();
						return false;
					},

					// 닫기 버튼을 클릭했을 때
					_onClick_btnClose = function _onClick_btnClose(e) {
						_closeMenu();
						return false;
					},

					// 메뉴 열기
					_openMenu = function _openMenu() {
						$(App.Events).trigger(App.Events.SHOW_POPUP);
						_isOpenMenu = true;

						$("header .side-menu").css({ transform: "translate3d(" + -$(window).width() + "px, 0, 0)" });
						$("header .side-menu .btn-close-menu").css({ transform: "translate3d(" + -$(window).width() + "px, 0, 0)" });
						$("header .common-menu .inner-wrap .right-menus .btn-open-menu").css({ transform: "translate3d(" + -$(window).width() + "px, 0, 0)" });

						_contentsOverflowHidden();
						$('.side-menu').addClass('opened');
						$('header .common-menu .inner-wrap .right-menus .btn-open-menu').addClass('opened');
						$('html').addClass('no-sroll');

						TweenLite.killDelayedCallsTo(_closeMenuComplete);
						if ($('body').find(".overlay-bg").length == 0) {
							$("body").append("<div class='overlay-bg'><div class='overlay-bg-alpha'></div><div class='overlay-bg-black'></div></div>");
						}
						$(".overlay-bg .overlay-bg-black").css({ transform: "translate3d(" + (-$(window).width() + 50) + "px, 0, 0)" });
						TweenLite.to($('.overlay-bg'), 0, { "css": { "opacity": 1 } });

						return false;
					},
					_focusCurrentMenu = function _focusCurrentMenu() {
						$(".js-focus-menu").addClass("opened");

						var tgH = $('.common-menu .hoverable').find(".depth-2").innerHeight(); // 170727 / 이혜진 .common-menu 추가
						$('.common-menu .hoverable').find(".depth-2").parent().css("height", 0); // 170727 / 이혜진 .common-menu 추가

						var tgH = $(".js-focus-menu").find(".depth-2").innerHeight();
						$(".js-focus-menu").find(".depth-2").parent().css("height", tgH);
					};

				// 메뉴
				var _closeMenu = function _closeMenu() {

						_focusCurrentMenu();

						_isOpenMenu = false;

						$("header .side-menu").css({ transform: "translate3d(" + 0 + "px, 0, 0)" });
						$("header .side-menu .btn-close-menu").css({ transform: "translate3d(" + 0 + "px, 0, 0)" });
						$("header .common-menu .inner-wrap .right-menus .btn-open-menu").css({ transform: "translate3d(" + 0 + "px, 0, 0)" });

						$('.side-menu').removeClass('opened');
						$('header .common-menu .inner-wrap .right-menus .btn-open-menu').removeClass('opened');
						$('html').removeClass('no-sroll');

						TweenLite.to($('.overlay-bg'), 0, { "css": { "opacity": 0 } });
						$(".overlay-bg .overlay-bg-black").css({ transform: "translate3d(" + 50 + "px, 0, 0)" });
						TweenLite.delayedCall(0.9, _closeMenuComplete);

						_contnetsOverflowVisible();
						return false;
					},
					_closeMenuComplete = function _closeMenuComplete() {
						$(".overlay-bg").remove();
						$(App.Events).trigger(App.Events.HIDE_POPUP);
					},
					_contentsOverflowHidden = function _contentsOverflowHidden() {
						_contentsScrollTop = $(window).scrollTop();
						$("#container").css({ "overflow": "hidden", "top": -_contentsScrollTop });
					},
					_contnetsOverflowVisible = function _contnetsOverflowVisible() {
						$("#container").css({ "overflow": "visible", "top": 0 });
						$(window).scrollTop(_contentsScrollTop);
					},

					//
					_onShowShareBtn = function _onShowShareBtn() {
						//console.log("_onShowShareBtn")
						$("header .side-menu .cont-bot").toggleClass("on");
					},

					//-------------------------------------------------
					//  검색 취소 버튼
					//-------------------------------------------------

					// 서치 인풋박스에서 키 다운
					_onKeydown_search = function _onKeydown_search(e) {
						_checkSearchVal();
					},

					// 검색 값 체크
					_checkSearchVal = function _checkSearchVal() {
						/*
						 var val = $("header .side-menu .input-wrap input").val();
						 if(val.length != 0){
						 _showSearchCancelBtn();
						 }else{
						 _hideSearchCancelBtn();
						 }
						 */
					},

					// 검색 취소 버튼 보여주기
					_showSearchCancelBtn = function _showSearchCancelBtn() {
						$("header .side-menu .input-wrap").addClass("has-txt");
					},

					// 검색 취소버튼 감추기
					_hideSearchCancelBtn = function _hideSearchCancelBtn() {
						$("header .side-menu .input-wrap").removeClass("has-txt");
					},
					_onClick_searchCanelBtn = function _onClick_searchCanelBtn(e) {
						$("header .side-menu .input-wrap input").val("");
						_checkSearchVal();
						$("header .side-menu .input-wrap input").focus();
						return false;
					},
					_onClick_searchIcon = function _onClick_searchIcon() {
						$("header .side-menu .input-wrap input").focus();
					};

				return {
					init: _init
				};
			})();

			var header = (function () {
				var _init = function _init() {
						_addEvent();
					},
					_addEvent = function _addEvent() {
						_gnbEvent();
					},
					_gnbEvent = function _gnbEvent() {
						var $hd = $('header.header_2017--mob');
						var $gnbWrap = $hd.find('.gnb-menu');
						var $gnbMenu = $gnbWrap.find('.cont-mid > ul > li');
						var $toggleBtn = $hd.find('.btn-open-menu2');

						$toggleBtn.on('click', function () {
							if (!$gnbWrap.hasClass('opened')) {
								$gnbWrap.addClass('opened');
								onOpenMenu();
								$gnbMenu.on('click', function () {
									onOpenSubMenu($(this));
								});
							} else if ($gnbWrap.hasClass('opened')) {
								onCloseMenu();
								onCloseSubMenu();
							}
						});

						function onOpenMenu() {
							$('body').css('overflow', 'hidden');
							$toggleBtn.addClass('on');
							$gnbWrap.addClass('opened');
							$gnbWrap.css({ transform: "translate3d(" + -$(window).width() + "px, 0, 0)" });
						}

						function onCloseMenu() {
							$('body').css('overflow', '');
							$toggleBtn.removeClass('on');
							$gnbWrap.removeClass('opened');
							$gnbWrap.css({ transform: "translate3d(" + 0 + "px, 0, 0)" });
						}

						function onOpenSubMenu($this) {
							$gnbMenu.removeClass('on');
							$this.addClass('on');
							$gnbMenu.find('.sub-menu').hide();
							$this.find('.sub-menu').show();
							$this.find('.sub-menu').scrollTop(0);
						}

						function onCloseSubMenu() {
							$gnbMenu.removeClass('on');
							$gnbMenu.find('.sub-menu').hide();
						}
					};

				return {
					init: _init
				};
			})();

			$(function () {
				App.navigation = navigation;
				App.navigation.init();
				App.header = header;
				App.header.init();
			});
		}
	})(window, jQuery);
})();
//# sourceMappingURL=App.js.map
//# sourceMappingURL=App.js.map
//# sourceMappingURL=App.js.map
//# sourceMappingURL=App.js.map
//# sourceMappingURL=App.js.map
//# sourceMappingURL=App.js.map
