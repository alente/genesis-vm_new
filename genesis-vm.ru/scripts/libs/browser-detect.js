/*
 * Browser Detect script
 */
'use strict';

var BrowserDetect = (function () {
  // script settings

  navigator.sayswho = (function () {
    var ua = navigator.userAgent,
        tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return 'MSIE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
      if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(' ');
  })();

  console.log(navigator.sayswho);

  // helper functions
  var addClass = function addClass(cls) {
    var html = document.documentElement;
    html.className += (html.className ? ' ' : '') + cls;
  };

  addClass(navigator.sayswho);
})();
//# sourceMappingURL=browser-detect.js.map
//# sourceMappingURL=browser-detect.js.map
//# sourceMappingURL=browser-detect.js.map
//# sourceMappingURL=browser-detect.js.map
//# sourceMappingURL=browser-detect.js.map
//# sourceMappingURL=browser-detect.js.map
