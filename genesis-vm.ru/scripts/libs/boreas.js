"use strict";

var Boreas = (function (e) {
  function t(r) {
    if (n[r]) return n[r].exports;var o = n[r] = { i: r, l: !1, exports: {} };return e[r].call(o.exports, o, o.exports, t), o.l = !0, o.exports;
  }var n = {};return t.m = e, t.c = n, t.i = function (e) {
    return e;
  }, t.d = function (e, n, r) {
    t.o(e, n) || Object.defineProperty(e, n, { configurable: !1, enumerable: !0, get: r });
  }, t.n = function (e) {
    var n = e && e.__esModule ? function () {
      return e["default"];
    } : function () {
      return e;
    };return t.d(n, "a", n), n;
  }, t.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t);
  }, t.p = "", t(t.s = 11);
})([function (e, t, n) {
  "use strict";function r(e) {
    return e && e.__esModule ? e : { "default": e };
  }function o(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function a(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function i(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var u = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
    return typeof e;
  } : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
  },
      s = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      l = n(2),
      c = r(l),
      f = n(10),
      d = r(f),
      p = (function (e) {
    function t() {
      o(this, t);var e = a(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));return e.__events = {}, e.__registerEvents(["ready"]), e;
    }return i(t, e), s(t, [{ key: "initialize", value: function value() {
        arguments.length > 0 && void 0 !== arguments[0] && arguments[0];this.trigger("ready");
      } }, { key: "on", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
            t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : function (e, t) {};return e && t && "function" == typeof t ? void 0 === this.__events[e] ? (this.log(this.constructor.name + '.on: Событие "' + e + '" не определено'), !1) : this.__events[e].addHandler(t) : (this.log(this.constructor.name + ".on: Не переданы обязательные параметры"), !1);
      } }, { key: "off", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
            t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null;return !!e && (void 0 === this.__events[e] ? (this.log(this.constructor.name + '.off: Событие "' + e + '" не определено'), !1) : t && "function" == typeof t ? void this.__events[e].removeHandler(t) : (delete this.__events[e], !0));
      } }, { key: "trigger", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
            t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};return void 0 !== this.__events[e] && this.__events[e].execute(t, this);
      } }, { key: "__registerEvents", value: function value() {
        var e = this,
            t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null;if (!t) return !1;if ("object" == (void 0 === t ? "undefined" : u(t))) t.forEach(function (t, n) {
          e.__events[t] = new d["default"]({ name: t });
        });else if ("string" == typeof t) {
          var n = t;this.__events[n] = new d["default"]({ name: n });
        }return !0;
      } }]), t;
  })(c["default"]);t["default"] = p;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      u = n(0),
      s = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(u),
      l = (function (e) {
    function t() {
      r(this, t);var e = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));return e.__registerEvents(["progress"]), e.params = {}, e;
    }return a(t, e), i(t, [{ key: "initialize", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};$.extend(!0, this.params, e), this.trigger("progress", this.getStatus()), this.trigger("ready");
      } }, { key: "getStatus", value: function value() {
        return { total: 0, loaded: 0, src: null };
      } }]), t;
  })(s["default"]);t["default"] = l;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }Object.defineProperty(t, "__esModule", { value: !0 });var o = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      a = (function () {
    function e() {
      r(this, e);
    }return o(e, [{ key: "log", value: function value(e) {
        console && console.log && console.log(e);
      } }]), e;
  })();t["default"] = a;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      u = n(1),
      s = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(u),
      l = (function (e) {
    function t() {
      r(this, t);var e = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));return e.params = { selector: "html", regex: { file: /(?:href="(.*?)")|(?:src="(.*?)")|(?:url\((.*?)\))/gi, quote: /(&quot;)|(')|(")/g, font: /@font-face\s*{([^}]*)}/gi, url: /url\(([^\)]+)\)/gi }, extensions: { image: ["jpg", "jpeg", "png", "gif", "svg"], css: ["css", "less", "scss"] }, searchInCss: !0 }, e.__found = null, e.__loaded = [], e;
    }return a(t, e), i(t, [{ key: "initialize", value: function value() {
        var e = this,
            t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};$.extend(!0, this.params, t), this.__found = { font: [] };for (var n in this.params.extensions) this.__found[n] = [];var r = $(this.params.selector).html();this.__findSources(r), !0 === this.params.searchInCss && this.__found.css.length ? this.__processCss(r).done(function () {
          e.__loadFiles();
        }) : this.__loadFiles();
      } }, { key: "getStatus", value: function value() {
        return { total: this.__found.image.length, loaded: this.__loaded.length, src: null, desc: null };
      } }, { key: "__findSources", value: function value(e) {
        this.__findFonts(e);for (var t = void 0; t = this.params.regex.file.exec(e);) for (var n = t.length - 1; n >= 0; n--) if (void 0 !== t[n]) {
          var r = t[n];if ((r = r.replace(this.params.regex.quote, ""), !r.length || "#" == r || -1 !== r.indexOf("data:"))) break;if (-1 !== this.__found.font.indexOf(r)) break;var o = this.__identifyFileType(r);o && -1 == this.__found[o].indexOf(r) && this.__found[o].push(r);break;
        }
      } }, { key: "__findFonts", value: function value(e) {
        for (var t = void 0, n = void 0; t = this.params.regex.font.exec(e);) for (var r = t.length - 1; r >= 0; r--) if (void 0 !== t[r]) {
          for (var o = t[r]; n = this.params.regex.url.exec(o);) for (var a = n.length - 1; a >= 0; a--) if (void 0 !== n[a]) {
            var i = n[a];if ((i = i.replace(this.params.regex.quote, ""), !i.length || "#" == i || -1 !== i.indexOf("data:"))) break;this.__found.font.push(i);break;
          }break;
        }
      } }, { key: "__identifyFileType", value: function value(e) {
        var t = !1,
            n = /[.]/.exec(e) ? /[^.]+$/.exec(e).toString() : void 0;if (void 0 === n) return !1;for (var r in this.params.extensions) if (-1 !== this.params.extensions[r].indexOf(n)) {
          t = r;break;
        }return t;
      } }, { key: "__loadFiles", value: function value() {
        var e = this;if (void 0 !== this.__found.image && this.__found.image.length) {
          for (var t in this.__found.image) !(function (t) {
            e.__loadImageAsync(e.__found.image[t]).promise().done(function () {
              e.__updateStatus(e.__found.image[t]);
            });
          })(t);
        }
      } }, { key: "__updateStatus", value: function value(e) {
        this.__loaded || (this.__loaded = []), this.__loaded.push(e);var t = this.getStatus();t.src = e, this.trigger("progress", t), t.total == t.loaded && this.trigger("ready");
      } }, { key: "__loadImageAsync", value: function value(e) {
        var t = new $.Deferred(),
            n = new Image();return n.src = e, n.onload = function () {
          t.resolve();
        }, n.onerror = function () {
          t.resolve();
        }, t;
      } }, { key: "__processCss", value: function value() {
        var e = [];for (var t in this.__found.css) e.push(this.__processUrlAsync(this.__found.css[t]));return $.when.apply(void 0, e).promise();
      } }, { key: "__processUrlAsync", value: function value(e) {
        var t = this,
            n = new $.Deferred();return $.ajax({ url: e }).done(function (e) {
          t.__findSources(e), n.resolve();
        }).fail(function () {
          n.resolve();
        }), n;
      } }]), t;
  })(s["default"]);t["default"] = l;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      u = n(1),
      s = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(u),
      l = (function (e) {
    function t() {
      r(this, t);var e = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));return e.params = { selector: "audio, video", blob: !1 }, e.__total = 0, e.__loaded = 0, e;
    }return a(t, e), i(t, [{ key: "initialize", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = this;$.extend(!0, this.params, e), this[t.params.blob ? "__loadMediaBlob" : "__loadMedia"]().done(function () {
          t.trigger("ready");
        });
      } }, { key: "getStatus", value: function value() {
        return { total: this.__total, loaded: this.__loaded, src: null, desc: null };
      } }, { key: "__loadMedia", value: function value() {
        var e = this,
            t = [];if (($(this.params.selector).each(function () {
          var n = this,
              r = new $.Deferred();if ("none" != n.preload) {
            e.__total++;var o = n.tagName.toLowerCase(),
                a = document.createElement(o);a.src = n.currentSrc, a.addEventListener("canplaythrough", function () {
              e.__updateItem(n.currentSrc), r.resolve(), a.oncanplay = null;
            }, !1), a.addEventListener("onerror", function () {
              e.__updateItem(n.currentSrc), r.resolve(), a.onerror = null;
            }, !1), t.push(r);
          }
        }), !t.length)) {
          var n = new $.Deferred();n.resolve(), t.push(n);
        }return $.when.apply(void 0, t).promise();
      } }, { key: "__loadMediaBlob", value: function value() {
        var e = this,
            t = [];if (($(this.params.selector).each(function () {
          var n = this,
              r = new $.Deferred();e.__total++;var o = new XMLHttpRequest();o.open("GET", n.currentSrc, !0), o.responseType = "blob", o.onload = function () {
            if (200 === this.status) {
              var t = this.response,
                  o = URL.createObjectURL(t);n.src = o;
            }e.__updateItem(n.currentSrc), r.resolve();
          }, o.onerror = function () {
            e.__updateItem(n.currentSrc), r.resolve();
          }, o.send(), t.push(r);
        }), !t.length)) {
          var n = new $.Deferred();n.resolve(), t.push(n);
        }return $.when.apply(void 0, t).promise();
      } }, { key: "__updateItem", value: function value(e) {
        this.__loaded++;var t = this.getStatus();t.src = e, this.trigger("progress", t);
      } }]), t;
  })(s["default"]);t["default"] = l;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      u = n(0),
      s = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(u),
      l = 0,
      c = (function (e) {
    function t() {
      return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments));
    }return a(t, e), i(t, [{ key: "process", value: function value() {
        arguments.length > 0 && void 0 !== arguments[0] && !arguments[0] ? this.hide() : this.show();
      } }, { key: "show", value: function value() {
        1 === ++l && this.__showLoader();
      } }, { key: "hide", value: function value() {
        l > 0 && l--, 0 === l && this.__hideLoader();
      } }, { key: "__showLoader", value: function value() {
        "undefined" != typeof BX ? BX.showWait() : console && console.log && console.log("Для работы данного метода должна быть загружена и инициализирована библиотека CJSCore из 1C-Битрикс");
      } }, { key: "__hideLoader", value: function value() {
        "undefined" != typeof BX ? BX.showWait() : console && console.log && console.log("Для работы данного метода должна быть загружена и инициализирована библиотека CJSCore из 1C-Битрикс");
      } }]), t;
  })(s["default"]);t["default"] = c;
}, function (e, t, n) {
  "use strict";function r(e) {
    return e && e.__esModule ? e : { "default": e };
  }function o(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function a(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function i(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var u = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      s = n(0),
      l = r(s),
      c = n(3),
      f = r(c),
      d = n(4),
      p = r(d),
      _ = (function (e) {
    function t() {
      o(this, t);var e = a(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this)),
          n = e;return n.__registerEvents(["progress"]), n.params = { handlers: [], methods: { show: e.__showPreloader, update: e.__updateBar, hide: e.__hidePreloader }, media: !0, delay: 800, timeout: 3e4, watcher: !1 }, n.__handlers = [], n.__status = { total: 0, loaded: 0, src: null, desc: null }, n.__$preloader = null, n.__watcherTt = null, n.__ready = !1, e;
    }return i(t, e), u(t, [{ key: "initialize", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = this;if (($.extend(!0, t.params, e), t.params.methods.show(), t.addHandler("images", f["default"]), !0 === t.params.media && t.addHandler("media", p["default"]), t.params.handlers.length)) for (var n in t.params.handlers) {
          var r = t.params.handlers[n];void 0 === r.params && (r.params = {}), this.addHandler(r.name, r["class"], r.params);
        }t.__initHandlers(), !1 !== t.params.watcher && t.__animationWatcher(), t.params.timeout > 0 && setTimeout(function () {
          !1 === t.__ready && (t.__forceFinish(), t.__ready = !0);
        }, t.params.timeout), t.on("progress", function (e) {
          null === t.__watcherTt && t.params.methods.update(e), e.loaded == e.total && setTimeout(function () {
            !1 === t.__ready && (t.trigger("ready"), t.__ready = !0);
          }, t.params.delay);
        }), t.on("ready", function () {
          window.scrollTo(0, 0), t.params.methods.hide(), !1 !== t.params.watcher && t.__animationWatcher(!1);
        });
      } }, { key: "addHandler", value: function value(e, t) {
        var n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};this.__handlers.push({ name: e, "class": t, params: n });
      } }, { key: "__animationWatcher", value: function value() {
        var e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];if (!1 !== t.params.watcher) {
          var t = this,
              n = 0;!1 === e ? (clearInterval(this.__watcherTt), t.__watcherTt = null) : !0 === e && null === t.__watcherTt && (t.__watcherTt = setInterval(function () {
            n < t.__status.loaded && (t.params.methods.update(t.__status), n = t.__status.loaded);
          }, t.params.watcher));
        }
      } }, { key: "__showPreloader", value: function value() {
        $("body").addClass("boreas-preloader-opened"), this.__$preloader = $(".boreas-preloader"), this.__$preloader.length || (this.__$preloader = $('<div class="boreas-preloader">\n\t\t\t\t<div class="progress">\n\t\t\t\t\t<div class="progress-bar" role="progressbar" style="width: 0%"></div>\n\t\t\t\t</div>\n\t\t\t</div>'), $("body").append(this.__$preloader));
      } }, { key: "__updateBar", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : null,
            t = parseInt(100 / e.total * e.loaded);this.__$preloader.find(".progress-bar").css({ width: t + "%" }).attr("aria-valuenow", t);
      } }, { key: "__hidePreloader", value: function value() {
        $("body").removeClass("boreas-preloader-opened"), this.__$preloader.fadeOut();
      } }, { key: "__forceFinish", value: function value() {
        var e = this.__status;e.loaded = e.total, this.params.methods.update(e), this.trigger("ready");
      } }, { key: "__initHandlers", value: function value() {
        var e = this;for (var t in this.__handlers) this.__handlers[t].instance = new this.__handlers[t]["class"](), this.__handlers[t].instance.on("progress", function (t) {
          e.__updateStatus(t), e.trigger("progress", e.__status);
        }), this.__handlers[t].instance.initialize(this.__handlers[t].params);
      } }, { key: "__updateStatus", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};this.__status.total = 0, this.__status.loaded = 0, this.__status.src = void 0 !== e.src ? e.src : null, this.__status.desc = void 0 !== e.desc ? e.desc : null;for (var t in this.__handlers) {
          var n = this.__handlers[t].instance.getStatus();this.__status.total += n.total, this.__status.loaded += n.loaded;
        }
      } }]), t;
  })(l["default"]);t["default"] = _;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      u = n(0),
      s = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(u),
      l = { name: null, load: "auto", async: !0, params: void 0 },
      c = (function (e) {
    function t() {
      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};r(this, t);var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));return n.params = { modules: [{ name: "loader", load: !0, async: !0, params: {} }], modulesDataAttribute: "boreas-modules" }, $.extend(!0, n.params, e), n.__includeModules(n.params.modules), n;
    }return a(t, e), i(t, [{ key: "initialize", value: function value() {
        var e = this;this.__loadModules(this.params.modules, function () {
          e.trigger("ready");
        });
      } }, { key: "registerModule", value: function value(e) {
        var t = l;e = $.extend(!0, t, e), this.params.modules.push(e);
      } }, { key: "__includeModules", value: function value(e) {
        for (var t in e) {
          var r = $.extend({}, l);if (("string" == typeof r ? r.name = e[t] : r = $.extend(!0, r, e[t]), void 0 !== r["class"])) this[r.name] = new r["class"]();else {
            var o = n(12)("./" + r.name)["default"];this[r.name] = new o();
          }
        }
      } }, { key: "__loadModules", value: function value(e) {
        var t = this,
            n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : null,
            r = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
            o = this,
            a = !1 === r ? null : [];for (var i in e) {
          (function (t) {
            var n = $.extend({}, l);"string" == typeof n ? n.name = e[t] : n = $.extend(!0, n, e[t]), r !== n.async || o.__isModuleEnabled(n) && (!1 === r ? null === a ? a = o.__loadModule(n).promise() : a.done(function () {
              a = o.__loadModule(n).promise();
            }) : a.push(o.__loadModule(n, !0)));
          })(i);
        }if (!1 !== r) return $.when.apply(void 0, a).promise();null === a ? this.__loadModules(e, null, !0).done(n) : a.done(function () {
          t.__loadModules(e, null, !0).done(n);
        });
      } }, { key: "__isModuleEnabled", value: function value(e) {
        var t = this;if (!0 === e.load) return !0;if ("auto" === e.load) {
          var n = [];if (($("[data-" + this.params.modulesDataAttribute + "]").each(function () {
            var e = $(this).data(t.params.modulesDataAttribute).split(" ");n = n.concat(e);
          }), -1 !== n.indexOf(e.name))) return !0;
        }return !1;
      } }, { key: "__loadModule", value: function value(e) {
        var t = new $.Deferred();return this[e.name].on("ready", function () {
          t.resolve();
        }), this[e.name].initialize(e.params), t;
      } }]), t;
  })(s["default"]);t["default"] = c;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + (void 0 === t ? "undefined" : o(t)));e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var o = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
    return typeof e;
  } : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
  };t["default"] = r;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      u = n(2),
      s = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(u),
      l = (function (e) {
    function t() {
      return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments));
    }return a(t, e), i(t, null, [{ key: "bandwidthTest", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : function () {},
            t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "100kb",
            n = new Date().getTime(),
            r = this,
            o = $.ajax({ dataType: "text", url: t + "?t=" + Math.random(), success: function success(a) {
            var i = new Date().getTime(),
                u = o.getResponseHeader("Content-Length"),
                s = (i - n) / 1e3,
                l = 8 * u,
                c = (l / s).toFixed(2),
                f = (c / 1024).toFixed(2),
                d = (f / 1024).toFixed(2);"function" == typeof e ? e({ url: t, size: u, bps: c, kbps: f, mbps: d }) : r.log("Boreas.network.bandwidthTest: callback не является функцией");
          } });
      } }]), t;
  })(s["default"]);t["default"] = l;
}, function (e, t, n) {
  "use strict";function r(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }function o(e, t) {
    if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !t || "object" != typeof t && "function" != typeof t ? e : t;
  }function a(e, t) {
    if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);e.prototype = Object.create(t && t.prototype, { constructor: { value: e, enumerable: !1, writable: !0, configurable: !0 } }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t);
  }Object.defineProperty(t, "__esModule", { value: !0 });var i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
    return typeof e;
  } : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
  },
      u = (function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var r = t[n];r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
      }
    }return function (t, n, r) {
      return n && e(t.prototype, n), r && e(t, r), t;
    };
  })(),
      s = n(2),
      l = (function (e) {
    return e && e.__esModule ? e : { "default": e };
  })(s),
      c = (function (e) {
    function t() {
      var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};r(this, t);var n = o(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));return n.name = void 0 !== e.name ? e.name : null, n.handlers = [], n;
    }return a(t, e), u(t, [{ key: "addHandler", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = {};return "object" == (void 0 === e ? "undefined" : i(e)) && "function" == typeof e.callback ? t = e : "function" == typeof e ? t.callback = e : this.log("Неправильные параметры хэндлера"), "function" == typeof t.callback && (this.handlers.push(t), !0);
      } }, { key: "removeHandler", value: function value(e) {
        return this.handlers.length && this.handlers.forEach(function (t, n, r) {
          t.callback === e && r.splice(n, 1);
        }), !1;
      } }, { key: "execute", value: function value() {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};return this.handlers.length && this.handlers.forEach(function (t, n) {
          return t.callback(e, t), !0;
        }), !1;
      } }]), t;
  })(l["default"]);t["default"] = c;
}, function (e, t, n) {
  "use strict";function r(e) {
    return e && e.__esModule ? e : { "default": e };
  }Object.defineProperty(t, "__esModule", { value: !0 }), t.network = t.preloaderHandler = t.extend = t.module = t.application = void 0;var o = n(7),
      a = r(o),
      i = n(0),
      u = r(i),
      s = n(1),
      l = r(s),
      c = n(8),
      f = r(c),
      d = n(9),
      p = r(d);t.application = a["default"], t.module = u["default"], t.extend = f["default"], t.preloaderHandler = l["default"], t.network = p["default"];
}, function (e, t, n) {
  function r(e) {
    return n(o(e));
  }function o(e) {
    var t = a[e];if (!(t + 1)) throw new Error("Cannot find module '" + e + "'.");return t;
  }var a = { "./loader": 5, "./loader.js": 5, "./preloader": 6, "./preloader.js": 6, "./preloader/handler": 1, "./preloader/handler.js": 1, "./preloader/imagesHandler": 3, "./preloader/imagesHandler.js": 3, "./preloader/mediaHandler": 4, "./preloader/mediaHandler.js": 4 };r.keys = function () {
    return Object.keys(a);
  }, r.resolve = o, e.exports = r, r.id = 12;
}]);
//# sourceMappingURL=boreas.js.map
//# sourceMappingURL=boreas.js.map
//# sourceMappingURL=boreas.js.map
//# sourceMappingURL=boreas.js.map
//# sourceMappingURL=boreas.js.map
//# sourceMappingURL=boreas.js.map
