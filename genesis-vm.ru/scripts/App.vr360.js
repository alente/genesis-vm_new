/*! App.vr360.js */
;(function(window, $, undefined) {
	var $window   = $(window);
	var $document = $(document);

	var exteriorCanvas = (function() {
		var CANVAS_SUPPORT = Modernizr.canvas;

		var _$canvas      = CANVAS_SUPPORT ? $('<canvas></canvas>') : $('<div></div>');
		var _canvas       = _$canvas[0];
		var _$container   = $('#exteriorCanvas'); // 캔버스 생성될 부모영역.

		var _$dragAnimate = $({idx:0});		//canvas
		var _canvasDragPlay = false;		//canvas touch
		var _startDragPoint = {};			//canvas touch변수

		var _countCheck   = 0; // 이미지 로드 체크용
		var _currentIndex = 0; // 현재 선택 이미지번호
		var _datas = [];
		var _currentInstance = {};

		var MAX_IMAGE_HEIGHT = 1080;
		var _canvasWidth;
		var _canvasHeight_currentInstance
		var _init = function(target) {
			_$container = target ? $(target):_$container;
			_$container.append( _$canvas );
		}

		/*!
		 * data {Object}
		 * data.name {String} - 고유의 데이터명. 추후 같은 네임값으로 호출시 같은 네임값의 데이터를 호출.
		 * data.images {Array} - 이미지SRC 배열 값.
		 * data.stillcut {String} - 이미지SRC 배열 값.
		 * data.callback {Function} - 이미지로드 후 callback함수.
		 */
		var _setup = function(data) {
			_drawReset();

			if ( _datas[data.name] && _datas[data.name].isLoaded ) {
				_currentInstance = _datas[data.name];
				_currentInstance.callback();
				_resize();
			} else {
				var oStillImage = new Image();

				// 스틸컷으로 대체.
				if ( data.stillcut ) {
					oStillImage.src = data.stillcut;

					imageLoadCheck(oStillImage, function() {
						_resize(oStillImage);
					});
				}
				_datas[data.name] = {
					name     : data.name,
					isLoaded : false,
					imgLens  : data.images.length,
					images   : data.images,
					oImages  : [],
					stillImage : oStillImage,
					callback : data.callback || function() {}
				}

				_currentInstance = _datas[data.name];
				imageLoadStart( data.name );
			}

		}
		var imageLoadStart = function( dataName ) {
			var _instance = _datas[dataName];
			_countCheck = 0;
			for (var i=0; i< _instance.imgLens; i++) {
				_instance.oImages[i] = new Image();
				_instance.oImages[i].src = _instance.images[i];
				imageLoad(_instance.oImages[i], dataName);
			}
		}
		var imageLoad = function(image, dataName) {
			var _instance = _datas[dataName];
			var checkLoad = function() {
				_countCheck = _countCheck+1;
				if ( _countCheck == _instance.imgLens ) {
					_instance.isLoaded = true;
					imageLoadEnd(dataName);
				}
			}
			imageLoadCheck(image, checkLoad);
		}
		var imageLoadCheck = function(image, callback) {
			var $image = $(image);
			if (image.complete) {
				$image.off('load', callback);
				callback.call(image);
			} else {
				if (image.src==image.src) {
					if (image.complete) {
						$image.off('load', callback);
						callback.call(image);
					} else {
						$image.off('load', callback).on('load', callback);
					}
				} else {
					$image.off('load', callback);
					image.src = image.src;
				}
			}
		}
		var imageLoadEnd = function(dataName) {
			var _instance = _datas[dataName];
			if ( _instance.name == _currentInstance.name ) {
				_resize();
				_canvasAddEvent();
				_currentInstance.callback();
				// _drawImages(); 리자이즈에 포함.
			}
		}
		var _resize = function(stillcut) {
			if ( _currentInstance.isLoaded || stillcut ) {
				var tmpImage = ( stillcut ) ? stillcut : _currentInstance.oImages[0];
				var size = {
					w: tmpImage.width,
					h: Math.max( $(window).height(), 875 )
				}
				size.w = size.w * (size.h/tmpImage.height);

				_canvasWidth  = size.w;
				_canvasHeight = size.h;

				if ( stillcut != null ) {
					_canvasDrawImage(stillcut);
				} else {
					_drawImages();
				}
			}
		}
		var _drawImages = function(no) {
			_currentIndex = no || _currentIndex;
			_canvasDrawImage( _currentInstance.oImages[_currentIndex] );
		}
		var _canvasDrawImage = function( objImage ) {
			if ( CANVAS_SUPPORT ) {
				var ctx = _canvas.getContext('2d');
				_canvas.width  = _canvasWidth;
				_canvas.height = _canvasHeight;
				_canvas.style.width  = _canvasWidth;
				_canvas.style.height = _canvasHeight;
				ctx.drawImage(objImage,0,0,_canvasWidth,_canvasHeight);
			} else {
				_canvas.style.width  = _canvasWidth;
				_canvas.style.height = _canvasHeight;
				_canvas.style.background = 'url('+objImage.src+')';
			}
		}

		var _getInstance = function() {
			return _currentInstance;
		}

		var _drawReset = function() {
			_$dragAnimate._stop();
			_currentIndex = 0;
			_canvasRemoveEvent(); // canvas Drag Event Remove
		}

		/*! -- canvas drag Event -- */
		var _canvasAddEvent = function() {
			_$canvas.on('mousedown.cvDragEvent touchstart.cvDragEvent', _dragStart);
			_$canvas.on('mousemove.cvDragEvent touchmove.cvDragEvent', _dragMove);
			$(document).on('mouseup.cvDragEvent touchend.cvDragEvent', _dragEnd);
		}
		var _canvasRemoveEvent = function() {
			_$canvas.on('.cvDragEvent');
			$(document).on('.cvDragEvent');
		}
		var _touchPoint = function(e) {
			e = e.originalEvent;
			if (e.touches || e.changedTouches) {
				return {
					x:e.touches[0] ? e.touches[0].pageX : e.changedTouches[0].pageX,
					y:e.touches[0] ? e.touches[0].pageY : e.changedTouches[0].pageY,
					t:+new Date
				};
			}
			return {x:e.pageX, y:e.pageY, t:+new Date};
		}
		var _circleIndex = function(idx) {
			var _imgLens = _currentInstance.imgLens;
			return (_imgLens + (idx % _imgLens)) % _imgLens;
		}
		var _dragStart = function(e) {
			if ( _currentInstance.isLoaded ) {
				e.preventDefault();

				_$dragAnimate._stop();

				_canvasDragPlay = true;
				_startDragPoint = _touchPoint(e);
				_deltaPoint     = _$dragAnimate[0].idx;
			}
		}
		var _dragMove = function(e) {
			var dragsOpts = {queue:false, duration:850, rounding:false, easing:'easeOutQuint', step: _dragMoving, complete: _dragAniComplete};
			var offset,delta,toIdx;

			if ( _canvasDragPlay && _currentInstance.isLoaded ) {
				e.preventDefault();

				var offset = _touchPoint(e);
				var delta = {
					x: offset.x - _startDragPoint.x,
					y: offset.y - _startDragPoint.y
				}

				if ( Math.abs(delta.x) > Math.abs(delta.y) ) {
					_$dragAnimate._stop();
					toIdx = Math.round(delta.x/14) + _deltaPoint;
					_$dragAnimate._animate({idx: toIdx}, dragsOpts);
				}
			}
		}

		var _dragMoving = function() {
			var deltaX = _$dragAnimate[0].idx;
			_currentIndex = _circleIndex(Math.round(deltaX));
			_drawImages(_currentIndex);
		}
		var _dragAniComplete = function() {
			_currentIndex = _circleIndex(Math.round(_$dragAnimate[0].idx));
		}
		var _dragEnd = function(e) {
			if ( _canvasDragPlay ) {
				_canvasDragPlay = false;
				e.preventDefault();
			}
		}

		/*! -- canvas drag EventEnd -- */
		return {
			init  : _init,
			setup : _setup,
			getData : _getInstance
		}
	})();

	$(function() {
		App.brand = App.brand || {};
		App.brand.vr360 = exteriorCanvas;
		App.brand.vr360.init('#exteriorCanvas');
	});
}(window, jQuery));