var View360 = {
	_settings: {
		_sensitivity: 15,
		_speedStep: 10,
		_maxAutoscrollInterval: 200,
		_startingAutoscrollInterval: 20,
		_autoscrollIntervalStep: 8,
		_maxSpeed: 500
	},
	_private: {
		_isIE: false,
		_currentColor: 'oni',
		_timeChangeIndex: undefined,
		_currentIndex: -1,
		_imageArray: {},
		_$view360cache: undefined,
		_$cacheLabel: undefined,
		_currentImageCode: '',
		_isDragging: false,
		_startTimeDragging: 0,
		_autoscrollSpeed: 0,
		_currentAutoscrollInterval: 0,
		_isAutoScrolling: false,
		_autoScrollTimer: undefined,
		_startIndex: 0,
		_mouseStartPos: 0,
		_mouseCurPos: 0,
		_mousePrevPos: undefined,
		_dragDirection: undefined,
		_oldDragDirection: undefined,
		_isCaching: false,
		_cachedImages: [],
		_cachedColors: [],
		_switchToImage: function (newImage) {
			console.log(newImage);
			var img = View360._private._$view360cache.find('[data-img=' + newImage + ']');
			if (View360._private._currentImageCode != newImage || img.length > 0) {
				View360._private._currentImageCode = newImage;
				View360._private._imageArray[newImage] = img;
				if (View360._private._isIE) {
					$('#main360image').css('background-image', "url('" + img.attr('src') + "')");
				} else {
					View360._private._$view360cache.find(':not(.cache)').addClass('cache');
					img.removeClass('cache');
				}
			}
		},
		_reloadImage: function () {
			var newImage = View360._private._currentColor + View360._private._currentIndex;
			var hasImage = typeof View360._private._imageArray[newImage] !== 'undefined' || View360._private._cachedImages.indexOf(newImage) > -1;
			if (!hasImage) {
				View360._private._cacheImage(View360._private._currentColor, View360._private._currentIndex, function () {
					View360._private._switchToImage(View360._private._currentColor + View360._private._currentIndex);
				});
			} else {
				View360._private._switchToImage(newImage);
			}
		},
		_cacheImage: function (color, index, onReady) {
			var newImage = color + index;
			var hasImage = View360._private._cachedImages.indexOf(newImage) > -1;
			if (!hasImage) {
				View360._private._stopAutoscroll();
				View360._private._cachedImages.push(newImage);
				$('<img src="' + View360._private._getImageSrc(color, index) + '" alt="360view" class="cache" data-img="' + newImage + '" />').on("load", function () {
					$(this).appendTo(View360._private._$view360cache);
					if (typeof onReady == 'function') onReady();
				});
			} else {
				if (typeof onReady == 'function') onReady();
			}
		},
		_getImageSrc: function (color, index) {
			console.log('images/360_g80_new/g80_exterior_' + color + '_000' + (index < 10 ? '0' : '') + index + '.jpg');
			return 'images/360_g80_new/g80_exterior_' + color + '_000' + (index < 10 ? '0' : '') + index + '.jpg'
		},
		_startCaching: function (startIndex, startColor) {
			var color = typeof startColor == 'undefined' ? View360._private._currentColor : startColor;
			console.log('color = ' + color);
			console.log(View360._private._isCaching);
			console.log(View360._private._cachedColors.indexOf(color));
			//if (View360._private._isCaching || View360._private._cachedColors.indexOf(color) > -1) return;

			View360._private._isCaching = true;
			function correctIndex(x) {
				while (x >= 60) x = x - 60;
				while (x < 0) x = x + 60;
//				console.log(x);
				return x;
			}

			var delta = 0;

			function getNextDelta() {
				View360._private._cacheImage(color, correctIndex(startIndex + delta), function () {
					if (View360._private._isCaching) {
						View360._private._cacheImage(color, correctIndex(startIndex - delta), function () {
							delta++;
							if (View360._private._isCaching && delta >= 61) {
								View360._private._stopCaching();
								View360._private._cachedColors.push(color);
							} else {
								View360._private._$cacheLabel.show();
								//var percent = ((delta / 30 * 100) >> 0);
							}
							if (View360._private._isCaching) getNextDelta();
						});
					}
				});
			}

			getNextDelta();
		},
		_stopCaching: function () {
			View360._private._isCaching = false;
			View360._private._$cacheLabel.hide();
		},
		_getDistance: function() {
			return ((View360._private._mouseCurPos - View360._private._mouseStartPos) / View360._settings._sensitivity) >> 0;
		},
		_startAutoscroll: function(startInterval, step) {
			if (typeof View360._private._autoScrollTimer == 'undefined') {
				View360._settings._autoscrollIntervalStep = step;
				View360._private._currentAutoscrollInterval = startInterval; //View360._settings._startingAutoscrollInterval;
				View360._private._autoScrollTimer = setInterval(View360._private._autoscrollStep, View360._private._currentAutoscrollInterval);
			}
		},
		_autoscrollStep: function() {
			if (typeof View360._private._autoScrollTimer != 'undefined') {
				if (Math.abs(View360._private._autoscrollSpeed) > View360._settings._speedStep) {
					var direction = View360._private._autoscrollSpeed > 0 ? 1 : -1;
					View360._private._autoscrollSpeed -= direction * View360._settings._speedStep;
					View360.setIndex(View360.getIndex() + direction);
					if (View360._private._currentAutoscrollInterval < View360._settings._maxAutoscrollInterval) {
						View360._private._currentAutoscrollInterval += View360._settings._autoscrollIntervalStep;
						// restart timer:
						clearInterval(View360._private._autoScrollTimer);
						View360._private._autoScrollTimer = setInterval(View360._private._autoscrollStep, View360._private._currentAutoscrollInterval);
					}
				} else {
					View360._private._stopAutoscroll();
				}
			}
		},
		_stopAutoscroll: function() {
			if (typeof View360._private._autoScrollTimer != 'undefined') {
				clearInterval(View360._private._autoScrollTimer);
				View360._private._autoScrollTimer = undefined;
			}
		}
	},
	setColor: function (newColor, is360) {
		this.setIndex(0);
		console.log('index = ' + this._private._currentIndex);
		if (this._private._currentColor == newColor) return;
		View360._private._$cacheLabel.removeClass(View360._private._currentColor);
		this._private._currentColor = newColor;
		this._private._reloadImage();
		if(is360 === true)
		{
			console.log('is360 true');
			if (View360._private._$cacheLabel.is(":visible")) {
				View360._private._$cacheLabel.addClass(View360._private._currentColor);
			}
			this._private._startCaching(this.getIndex(), newColor);
		}
		else{
			//this._private._stopCaching();
			this._private._cachedColors = [];
			this._private._isCaching = false;
		}
	},
	getImageSrc: function () {
		return this._private._getImageSrc(this._private._currentColor, this._private._currentIndex);
	},
	setIndex: function (newIndex) {
		while (newIndex >= 60) newIndex = newIndex - 60;
		while (newIndex < 0) newIndex = newIndex + 60;
		if (this._private._currentIndex == newIndex) return;
		this._private._currentIndex = newIndex;
		this._private._reloadImage();
		if (performance) this._private._timeChangeIndex = performance.now();
	},
	getIndex: function () {
		return this._private._currentIndex;
	},
	setDragging: function (isDragging, mouseStartPos) {
		this._private._isDragging = isDragging;
		if (isDragging) {
			this._private._stopAutoscroll();
			this._private._mouseStartPos = mouseStartPos;
			this._private._startIndex = this._private._currentIndex;
			if (performance) {
				this._private._startTimeDragging = performance.now();
				this._private._isAutoScrolling = true;
			}
		} else {
			this._private._mousePrevPos = undefined;
			if (performance && this._private._isAutoScrolling) {
				var stopTime = performance.now();
				var ms = stopTime - this._private._startTimeDragging;
				var distance = this._private._getDistance();
				var speedInMs = ms != 0 ? distance / ms * 1000 : 0;
				var newspeed = (stopTime - this._private._timeChangeIndex) != 0 ? 1000 / (stopTime - this._private._timeChangeIndex) : 0;
				newspeed = Math.min(Math.max(newspeed, -View360._settings._maxSpeed), View360._settings._maxSpeed);
				this._private._autoscrollSpeed = distance > 0 ? newspeed : -newspeed;
				this._private._isAutoScrolling = false;
				if (Math.abs(this._private._autoscrollSpeed) > this._settings._speedStep) {
					this._private._startAutoscroll((1000 / newspeed) >> 0, (1000 / newspeed) >> 0);
				} else {
					this._private._autoscrollSpeed = 0;
				}
			}
		}
	},
	setMouseCurPos: function (mouseCurPos) {
		this._private._oldDragDirection = this._private._dragDirection;
		this._private._mousePrevPos = this._private._mouseCurPos;
		this._private._mouseCurPos = mouseCurPos;
		this._private._dragDirection = mouseCurPos > this._private._mousePrevPos ? 1 : -1;
		if (this._private._isDragging) {
			var newIndex = this._private._startIndex + this._private._getDistance();
			if (this._private._dragDirection != this._private._oldDragDirection) {
				this._private._mouseStartPos = mouseCurPos;
				this._private._startIndex = newIndex;
				if (performance) this._private._startTimeDragging = performance.now();
			}
			this.setIndex(newIndex);
		}
	},
	getMousePosFromEvent: function(e) {
		var position = 0;
		if (typeof TouchEvent !== 'undefined' && (e instanceof TouchEvent || (typeof e.originalEvent !== 'undefined' && e.originalEvent instanceof TouchEvent))) {
			if (e.touches.length > 0) {
				if (typeof e.touches[0].offsetX !== 'undefined') {
					position = e.touches[0].offsetX;
				} else {
					position = e.touches[0].clientX;
				}
			}
		} else {
			position = e.offsetX;
		}
		return position;
	},
	init: function (view360BlockWithImagesSelector) {
		$(view360BlockWithImagesSelector).unbind("mousedown touchstart").on("mousedown touchstart", function (e) {
			if (!View360._private._isCaching) {
				View360.setDragging(true, View360.getMousePosFromEvent(e));
			}
		}).unbind("mouseup mouseleave touchend").on("mouseup mouseleave touchend", function (e) {
			if (!View360._private._isCaching) {
				View360.setDragging(false, View360.getMousePosFromEvent(e));
			}
		}).unbind("mousemove touchmove").on("mousemove touchmove", function (e) {
			if (!View360._private._isCaching) {
				View360.setMouseCurPos(View360.getMousePosFromEvent(e));
			}
		});

		this._private._isIE = /Edge\/|Trident\/|MSIE /.test(window.navigator.userAgent);
		if (!this._private._isIE) $('#main360image').hide();
		this._private._$view360cache = $(view360BlockWithImagesSelector);
		this._private._$cacheLabel = this._private._$view360cache.find('label');
		if (this._private._isIE) $('#view360cache').css('cursor', 'url("../images/icons/cursor_360view.cur"), ew-resize');

		this.setIndex(0);
		this._private._startCaching(0);
	}
};

$(document).ready(function () {
	$('div.exterior-interior__color').unbind("click").on("click", function () { // COLORS SELECTOR
		$('div.exterior-interior__color.active').removeClass("active").animate({width: "16"}, {duration: 200});
		$(this).addClass("active").animate({width: "32"}, {duration: 200});
		if($(this).hasClass('no360'))
		{
			//View360.setIndex(0);
			View360.setColor($(this).data('color'), false);
			// Попробуем сделать анбинд
			$('#view360cache').unbind("mousedown touchstart").unbind("mouseup mouseleave touchend").unbind("mousemove touchmove");
			$('#view360cache').addClass('cur_def');

		}
		else
		{
			View360.init('#view360cache');
			View360.setColor($(this).data('color'), true);
			$('#view360cache').removeClass('cur_def');
		}
	});

	View360.init('#view360cache');
});