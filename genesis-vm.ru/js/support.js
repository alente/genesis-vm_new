$(document).ready(function(){

	function formHasErrors() {
		var hasErrors = false;
		$('.incorrect').removeClass('incorrect');

		$('#fName,#lName,#cNumber,#email').each(function () {
			if ($(this).val() == '') {
					$(this).parent().addClass('incorrect');
					$(this).attr('placeholder','Пожалуйста, введите корректные данные.');
				hasErrors = true;
				return false;
			}
		});

		if (hasErrors) return true;

		var $email = $('#email');
		var vEmail = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}/;
		var Email = $email.val();
		var firstName = vEmail.exec(Email);
		if (!firstName)
		{
			$email.parent().addClass('incorrect');
			return true;
		}


		if ($('#agree_rules').is(':not(:checked)')) {
			$('.legal-info').addClass('incorrect');
			return true;
		}

		return false;
	}
	$("#fName").on('keyup', function(e) {
			var val = $(this).val();
		 if (val.match(/[^A-Za-zА-Яа-яЁё]/g)) {
				 $(this).val(val.replace(/[^A-Za-zА-Яа-яЁё]/g, ''));
		 }
	});
	$("#lName").on('keyup', function(e) {
			var val = $(this).val();
		 if (val.match(/[^A-Za-zА-Яа-яЁё-]/g)) {
				 $(this).val(val.replace(/[^A-Za-zА-Яа-яЁё-]/g, ''));
		 }
	});
	$("#email").on('keyup change', function(e) {
		var val = $(this).val().toLowerCase();
		if (val.match(/[А-Яа-яЁё-]/g)) {
			$(this).val(val.replace(/[А-Яа-яЁё-]/g, ''));
		} else {
			$(this).val(val);
		}
	});
	function sendForm() {
		if (!formHasErrors()) {
			$.post(
				'sendquestion.html',
				{
					name: $('#fName').val(),
					surname: $('#lName').val(),
					phone: $('#cNumber').val(),
					email: $('#email').val(),
					msg: $('#msg').val(),
					fname: $('#lFatherName').val(),
					city: $('#lCity').val(),
					car: $('#car').val(),
					dealer: $('#dealer').val(),
					theme: $('#theme').val(),
					salutation: $('input[name=mrOrms]:checked').val(),
				},
				function (response) {
					if (response == 'Y') {
						showResult(true);
					} else {
						console.log('Error:', response);
						showResult(false);
					}
				}
			);
		}
	}


	function showResult(res)
	{
		$('.send-question__form').slideUp();
		$('.send-question__submit').slideUp();
		$('.legal-info').slideUp();
		$('.send-question__success').slideDown();
	}


	function showForm(res)
	{
		$('.send-question__success').slideUp();
		$('.send-question__form').slideDown();
		$('.send-question__submit').slideDown();
		$('.legal-info').slideDown();
	}

	
	
	
	

	function refreshDealers(id)
	{
		var $id = id;
		var res = '';

        $('#dealer').empty();
        $('#dealer').append('<option disabled selected>Выберите дилера</option>');


		DealerMapStructure.markers.forEach(function(item, i, arr) {
			//console.log($id);
			if(parseInt($id) == parseInt(item[5]))
			{
				res += '<option value="' + item[6] + '">' + item[0] + '</option>';
			}
		});

		$('#dealer').append(res);
	}
	
	function initForm() {
		$('#showForm').on('click', function(e){
			e.preventDefault();
			showForm(true);
		});


		$(document).on('submit', '#questionform', function (e) {
				e.preventDefault();
				sendForm();

				//console.log('submit');

				return false;
			});

		$('#questionform input').focus(function () {
				$(this).parent().removeClass('incorrect');
			});

		$('#cNumber').mask("+7 (999)999-99-99");

		$('#agree_rules').change(function () {
			$('.legal-info').removeClass('incorrect');
		});
		$(document).on('click', '.show-legal', function (e) {
			e.preventDefault();
			$('.legal-wrap').fadeIn();
		});
		$(document).on('click', '.closeIt', function (e) {
			e.preventDefault();
			$('.overlayed').fadeOut();
		});
		$(document).keydown('click', function (e) {
			if (e.keyCode == 27) {
				$('.overlayed').fadeOut();
			}
		});
		
		$(document).on('change', '#cityId', function(){
			//console.log(parseInt($(this).val()));
			refreshDealers(parseInt($(this).val()));
		});

        $('#cityId').append('<option disabled selected>Выберите город</option>');

        var cityFirst = '',
			cityList = '';

		DealerMapStructure.citymaps.forEach(function(item, i, arr) {
			//console.log(item[0]);
			if(item[0] == 'Москва' || item[0] == 'Санкт-Петербург'){
                cityFirst += '<option value="' + item[3] + '">' + item[0] + '</option>';
			}else{
                cityList += '<option value="' + item[3] + '">' + item[0] + '</option>';
			}


			//$('#cityId').append('<option value="' + item[3] + '">' + item[0] + '</option>');
		});
		if(cityFirst.length){
            $('#cityId').append(cityFirst);
            $('#cityId').append('<option disabled="disabled"></option>');
		}
        $('#cityId').append(cityList);

		$('#cityId').trigger('change');
		
	}
	
	var jsonData = {};
	
	function initJSON (){
		var promise = [],
			urlCity = '/ajax/dealer-cities.json',
			urlDealer = '/ajax/dealer-list.json';
		
		promise.push(loadJSONData(urlCity, 'cities'));
		promise.push(loadJSONData(urlDealer, 'dealers'));
		$.when.apply(undefined, promise).promise().done(function() {
			var resultHtml = '',
				i = 0;

			DealerMapStructure.citymaps = [];
			DealerMapStructure.markers = [];
			
			for (var k in jsonData.cities) {
				var city = jsonData.cities[k];
				DealerMapStructure.citymaps.push([city.name, city.latitude, city.longitude, city.id]);
			}
			for (var k in jsonData.dealers) {
				var dealer = jsonData.dealers[k];
				DealerMapStructure.markers.push([dealer.name, dealer.latitude, dealer.longitude, dealer.address, dealer.phone, dealer.city_id, dealer.code, dealer.site]);
			}
			
			initForm();
		});
	}
	
	function loadJSONData (url, key) {
		var defer = new $.Deferred();

		$.ajax({
			url: url,
			dataType: 'json',
			beforeSend: function(xhr){}
		}).done(function(response) {
			jsonData[key] = response;
			defer.resolve();
		}).fail(function(){
			console.log('Get json error: ' + url);
			defer.resolve();
		});

		return defer;
	}
	
	initJSON ();

	
});