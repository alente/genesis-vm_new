(function($) {

    // to 60fps
/*
    var body = document.body,
        timer;

    window.addEventListener('scroll', function() {
        clearTimeout(timer);
        if (!body.classList.contains('disable-hover')) {
            body.classList.add('disable-hover')
        }

        timer = setTimeout(function() {
            body.classList.remove('disable-hover')
        }, 500);
    }, false);
*/

    $('.button-topmenu').click(function(event) {
        $('.button-topmenu').addClass('active');
        $('.navigation__overlay, .js-navigation').addClass('show');
        $('body').addClass('overflow_hidden');
    });
    $('.navigation__overlay, .button--navigation-close, .navigation-link__items a').click(function() {
        $('.button-topmenu').removeClass('active');
        $('.navigation__overlay, .js-navigation').removeClass('show');
        $('body').removeClass('overflow_hidden');
    });

    // $('.js-toggle-navigation').on(
    //     'click',
    //     toggleNavigation
    // );
    var timeout;
    if (screen.width == 1024) {
        $(".first-level").addClass("on").find('.second-level').slideDown('200');
    }
    $(".first-level").hover(

        function() {
            $first = $(this);
            timeout = setTimeout(function() {
                if ($first.hasClass('on')) {
                    return true;
                } else {

                    $first.parent().find('.on .second-level').slideUp('200').closest('.on').removeClass('on');
                    $('.navigation-link__items .first-level').removeClass('on');
                    $first.addClass("on").find('.second-level').slideDown('200');
                }
            }, 250);
        },
        function() {
            clearTimeout(timeout);
        }
    );
    $('.btn-play').on('click', function() {
        $("body").append('<div class="modal-video"><div class="wrapper-modal"><div class="modal-close"></div><div class="box-video"><iframe class="video-player-popup" width="100%" height="480px" src="https://www.youtube.com/embed/' + $(this).data('play') + '?controls=0&rel=0&autoplay=1" frameborder="0" allowfullscreen" frameborder="0" allowfullscreen autoplay></iframe></div></div></div>');
    });
    $('body').on('click', '.modal-close', function() {
        $('.modal-video').fadeOut().find('.video-player-popup').remove();
    });

    $('body').on('click', '.modal-video', function() {
        $('.modal-video').fadeOut().find('.video-player-popup').remove();
    });

    $(document).ready(function() {
        // $('.tablist li')
        var headerDelta = getHeaderDelta();
        var easing = 'easeOutExpo';
        var timing = 750;
        var scrollSpy = true;
        var scrollSpySetup = getScrollSetup();
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        //todo - get rid of this ugly hack to wait for load
        setInterval(function() {
            scrollSpySetup = getScrollSetup();
        }, 1000);
        var $doc = $(document);
        var curSlide = $('.breadcrumb__link--active').data('link');

        if (iOS) {
            $('.intro__image').css('background-attachment', 'scroll');
        }

        function getSliderSettings() {
            return {
                nextArrow: "<div class='gallery-images__next'><button class='button slick-buttons button--transparent'><span class='button__icon'><span class='fa fa-angle-right'></span></span></button></div>",
                prevArrow: "<div class='gallery-images__prev'><button class='button slick-buttons button--transparent'><span class='button__icon'><span class='fa fa-angle-left'></span></span></button></div>",
                dots: false,
                speed: 300,
                fade: true,
                cssEase: 'linear'
            }
        }

        $('.slick-galery').slick(getSliderSettings());

        $('.col1, .col2').hover(function() {
            $(this).animate({ 'opacity': '1' }, 400);
        }, function() {
            $(this).animate({ 'opacity': '0.8' }, 400);
        });

        $('.gallery-heading__options-item').on('click', function() {
            var elem = $(this);
            if (!elem.hasClass('gallery-heading__options-item--active')) {
                $('.gallery-heading__options .gallery-heading__options-item--active').removeClass('gallery-heading__options-item--active');
                elem.addClass('gallery-heading__options-item--active');
                if (elem.hasClass('exterier')) {
                    $('.gallery-interior').css('display', 'none');
                    $('.gallery-exterier').css({
                        'display': 'block',
                        'opacity': 0
                    });
                    $('.slick-galery').slick('unslick').slick(getSliderSettings());
                    $('.gallery-exterier').animate({ 'opacity': '1' }, 400);
                } else {
                    $('.gallery-exterier').css('display', 'none');
                    $('.gallery-interior').css({
                        'display': 'block',
                        'opacity': 0
                    });
                    $('.slick-galery').slick('unslick').slick(getSliderSettings());
                    $('.gallery-interior').animate({ 'opacity': '1' }, 400);
                }
            }
        });

        $('.gallery1, .gallery3').each(function() {
            $(this).magnificPopup({
                delegate: 'a', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery: {
                    enabled: true,
                    arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button

                    tPrev: '����֧�. (�ݧ֧ӧѧ� ����֧ݧܧ�)',
                    tNext: '���ݧ֧�. (���ѧӧѧ� ����֧ݧܧ�)',
                },
            });
        });

        $('.scrollTo').on('click', function(e) {
            var $el = $(e.target);
            if (!$el.hasClass('scrollTo')) $el = $el.parents('.scrollTo');
            var target = $el.data('link');
            animateScrollToTarget(target);
            e.stopImmediatePropagation();
            return false;
        });

        $(document).on('scroll', function() {
            if (!scrollSpy) return;
            var curSc = $doc.scrollTop();
            var newSlide = '';
            for (var target in scrollSpySetup) {
                if (curSc >= scrollSpySetup[target]) newSlide = target;
            }
            if (newSlide != curSlide) {
                $('.breadcrumb__link--active').removeClass('breadcrumb__link--active');
                $('.breadcrumb__link[data-link="' + newSlide + '"]').addClass('breadcrumb__link--active');
                curSlide = newSlide;
            }

        });

        function getHeaderDelta() {
            var res = $('header').outerHeight();
            var $crumbs = $('.layout__breadcrumb');
            if ($crumbs.is(':visible')) res += $crumbs.outerHeight();
            return res;
        }

        function getScrollSetup() {
            var setup = {};
            headerDelta = getHeaderDelta();

            $('.breadcrumb__link').each(function() {
                var target = $(this).data('link');
                var offset = $('#' + target).offset();
                if (typeof offset !== 'undefined') {
                    var scrollTo = offset.top - headerDelta - 5; //some thrashhold
                    if (scrollTo < 0) scrollTo = 0;
                    setup[target] = scrollTo;
                }
            });

            return setup;
        }

        $(window).on('load', function() {
            AOS.refresh();
        });

        if (window.location.hash) {
            var target = window.location.hash.split('-');
            if (target[0] && target[0] == '#set' && target[1]) {
                target = target[1];
                var scrollTo = $('#' + target).offset().top - headerDelta;
                if (scrollTo < 0) scrollTo = 0;
                $('html, body').stop().animate({ scrollTop: scrollTo }, 1);
            }
/*
            if (target[0] && target[0] == '#long') {
	            console.log('target=' + window.location.hash);


	            var scrollTo = $('#long').offset().top - headerDelta;
                if (scrollTo < 0) scrollTo = 0;

                $('html, body').scrollTop();

				window.onload = function(){

					$('html, body').stop().animate({ scrollTop: scrollTo }, 2000);

				};
	        }
*/
        }

        $('a.navigation-link__link--level-2').on('click', function(e) {
            var target = $(e.target).attr('href');
            var curLoc = window.location.pathname.split('/').reverse()[0]; //basename
            target = target.split('#');
            if (target[0] == curLoc) {
                target = target[1].split('-')[1];
                animateScrollToTarget(target);
                return false;
            } else {
                return true;
            }
        });


        function animateScrollToTarget(target) {
            var $scrollTo = $('#' + target);
            if (!$scrollTo.length) return;

            window.location.hash = 'set-' + target;
            var scrollTo = $scrollTo.offset().top - headerDelta;
            if (scrollTo < 0) scrollTo = 0;

            //scrollSpy = false;
            if ($('.breadcrumb__link[data-link="' + target + '"]').length > 0) {
                $('.breadcrumb__link--active').removeClass('breadcrumb__link--active');
                $('.breadcrumb__link[data-link="' + target + '"]').addClass('breadcrumb__link--active');
            }
            $('html, body').stop().animate({ scrollTop: scrollTo }, timing, easing, function() {
                //  scrollSpy = true;
            });

        }

        $(document).trigger('scroll');


        $('.tabs__list-item').on('click', function(e) {
            var $el = $(e.target);
            if (!$el.hasClass('.tabs__list-item')) $el = $el.parents('.tabs__list-item');
            if ($el.hasClass('tabs__list-item--active')) return;

            var num = $el.data('num');
            var $box = $el.parents('.tabs');
            $box.find('.tabs__list-item--active').removeClass('tabs__list-item--active');
            $el.addClass('tabs__list-item--active');

            var $hide = $box.find('.tabs__content.tabs__content--show');
            var $show = $box.find('.tabs__content.tab' + num);

            var time = 1000;
            $hide.stop().animate({ opacity: 0 }, Math.round(time / 2), function() {
                $hide.removeClass('tabs__content--show');
                $show.css('opacity', 0).addClass('tabs__content--show').stop().animate({ opacity: 1 }, time);
            });

        });


        $('.accordion-prices__item-heading').on('click', function(e) {
            $(this).parent().find('.accordion-prices__item-content').first().slideToggle("slow");
            $(this).find('.accordion-prices__item-heading-price').toggleClass('opened');
        })



    });

}(jQuery));
//parallax//task3
function parallax() {

    function luxury(Id, imgBack, vert, hor) {
        var path = $(Id);
        path.find('.line-item__image > img').css({
            'opacity': 0
        });
        path.find('.line-item__image').css({
            'height': '100%',
            'background-image': 'url("images/' + imgBack + '")',
            'background-attachment': 'fixed',
            'background-size': 'cover',
            'background-position': 'center center'
        });
    }

    luxury('#innovation-parallax', 'innovation-1.jpg');
    luxury('#dynamics', 'performance-1.jpg');
    luxury('#materials-parallax', 'design-6.jpg');
    luxury('.features-parallax', 'g90-3.jpg');
    luxury('#security', 'g90-4.jpg');
    luxury('.security-parallax', 'g90-6.jpg');
    luxury('.first-parallax', 'g90-c1.jpg');
    luxury('.full-parallax', 'g90-c2.jpg');
    luxury('.refined-parallax', 'g90-c3_new.jpg');
    luxury('.resp-parallax', 'g90-2-v2.jpg');
    luxury('.you-parallax', 'g90-c5.jpg');
    luxury('.genesis-parallax', 'g90l-v2.jpg');
    luxury('.g-80-genesis-sport-first', 'genesis-g80-sport-00-hero.jpg');
    luxury('.g-80-genesis-sport-second', 'genesis-g80-sport-feature-00-overview-01-performance.jpg');
    luxury('.g-80-genesis-performance', 'G80-sport_Feature_01_performance_01.jpg');
    luxury('.g-80-genesis-inspiration', 'G80-sport_Feature_02_inspiration_01.jpg');
    luxury('.g-80-genesis-design', 'genesis-g80-feature-00-overview-04-design.jpg');
    luxury('.g-80-genesis-IntelligentSafety', 'G80_Feature_01_Safety_01_IntelligentSafety_01.jpg');
    luxury('.g-80-genesis-bodyStiffness', 'genesis-g80-feature-01-safety-02-body-stiffness-01.jpg');
    luxury('.g-80-genesis-technology', 'genesis-g80-feature-01-safety-03-mindfull-technology-01.jpg');
    luxury('.g-80-genesis-multisensory', 'G80_Feature_02_Innovation_01_multisensory_space_01.jpg');
    luxury('.g-80-genesis-perfectBalance', 'genesis-g80-feature-03-performance-01-perfect-balance-01.jpg');
    luxury('.g-80-genesis-firstcomfort', 'G80_Feature_03_performance_02_Firstcomfort_01.jpg');
    luxury('.g-80-genesis-perfectProportion', 'G80_Feature_04_design_01_perfect-proportion_01.jpg');
    luxury('.g-80-genesis-craftmanship', 'g80-feature-04-design-02-craftmanship-01.jpg');
}


function noParallax() {

    function luxury(Id) {
        var path = $(Id);
        path.find('.line-item__image > img').css({
            'opacity': 1
        });
    }

    luxury('#innovation-parallax', 'innovation-1.jpg');
    luxury('#dynamics', 'performance-1.jpg');
    luxury('#materials-parallax', 'design-6.jpg');
    luxury('.features-parallax', 'g90-3.jpg');
    luxury('#security', 'g90-4.jpg');
    luxury('.security-parallax', 'g90-6.jpg');
    luxury('.first-parallax', 'g90-c1.jpg');
    luxury('.full-parallax', 'g90-c2.jpg');
    luxury('.refined-parallax', 'g90-c3_new.png');
    luxury('.resp-parallax', 'g90-2-v2.jpg');
    luxury('.you-parallax', 'g90-c5.jpg');
    luxury('.genesis-parallax', 'g90l-v2.jpg');
}

$(document).ready(function() {

    $('.specifications__full .link--uppercase-primary').click(function(event) {
        event.preventDefault();
        $(this).parent().find('.specifications__full__item').slideToggle("slow");
    });
    if ($(window).width() < 1250) {
        noParallax();
    } else {
        parallax();
    }
    // $(".layout__header").show();
    // hide menu on scrolling in mobile
    // var senseSpeed = 5;
    // var previousScroll = 0;

    // $(window).scroll(function(event){
    //   if(screen.width <= 768 & $(window).scrollTop() > 50){
    //      var scroller = $(this).scrollTop();
    //      if (scroller-senseSpeed > previousScroll){
    //         $(".layout__header").filter(':not(:animated)').fadeOut("fast");
    //      } else if (scroller+senseSpeed < previousScroll) {
    //         $(".layout__header").filter(':not(:animated)').fadeIn("fast");
    //      }
    //      previousScroll = scroller;
    //    }
    // });
});

$(window).resize(function() {
    // $(".layout__header").filter(':not(:animated)').fadeIn("fast");
    if ($(window).width() < 1350) {
        noParallax();
    } else {
        parallax();
    }
});

var pageG90 = document.title = "Genesis";

$(window).scroll(function() {
    function scrollListener(e) {
        var offset = $('body').scrollTop();
        if (offset > 1000) {
            $(e).css('opacity', 0);
            $(e).parent().css('opacity', 0);
        } else {
            $(e).css('opacity', 1);
            $(e).parent().css('opacity', 1);
        }
    };
    scrollListener('.g90white');
    scrollListener('.g90whiteMobile');
    scrollListener('.genesisWhite');
    scrollListener('.genesisWhiteMobile');
});

$(document).ready(function() {
    $('body', 'html').animate({
        scrollTop: 0
    }, 0);
});

// var al = $(window).width();
// var al2 = $(window).height();
// alert(al);
// alert(al2);

function disabledDarkBlock(page, top){
    var href = location.href;
    if(href.indexOf(page) > 0) {
        $('.page__form').css('display','none');
    }
};

disabledDarkBlock('partners');
disabledDarkBlock('warranty');


// $(window).on("load resize",function(){
//     if(device.mobile() & device.landscape()) {
        // $('.genesisInfo').css({
        //     'position':'absolute',
        //     'margin-top':'150px'
        // });
        // $('.g90whiteMobile').css('top','-100px');
        // $('.g90Info').css('margin-top','-45px');
    // }
// });
