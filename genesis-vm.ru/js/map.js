$(document).ready(function($) {
	//set your google maps parameters
	var map_zoom = 9,
		bounds = new google.maps.LatLngBounds(),
		i;
	
	// marker url
	var marker_url = 'images/icons/map_pin.png';
	
	//define the basic color map, plus a value for saturation and brightness
	var main_color = '#000',
		saturation_value = -100,
		brightness_value = -30;
	
	//we define here the style of the map
	var style = [{
		//set saturation for the labels on the map
		elementType: "labels",
		stylers: [
			{ saturation: saturation_value }
		]
	}, { //poi stands for point of interest - don't show these lables on the map
		featureType: "poi",
		elementType: "labels",
		stylers: [
			{ visibility: "off" }
		]
	}, {
		//don't show highways lables on the map
		featureType: 'road.highway',
		elementType: 'labels',
		stylers: [
			{ visibility: "off" }
		]
	}, {
		//don't show local road lables on the map
		featureType: "road.local",
		elementType: "labels.icon",
		stylers: [
			{ visibility: "off" }
		]
	}, {
		//don't show road lables on the map
		featureType: "road",
		elementType: "geometry.stroke",
		stylers: [
			{ visibility: "off" }
		]
	}, {
		//don't show arterial road lables on the map
		featureType: "road.arterial",
		elementType: "labels.icon",
		stylers: [
			{ visibility: "off" }
		]
	}, {
		featureType: "administrative",
		elementType: "labels.text.stroke",
		stylers: [
			{ visibility: "off" }
		]
	}, {
		featureType: "administrative",
		elementType: "labels.text.fill",
		stylers: [
			{ color: "#ffffff" },
			{ visibility: "on" }
		]
	}, {
		featureType: "transit",
		elementType: "labels.text.stroke",
		stylers: [
			{ visibility: "off" }
		]
	}, {
		featureType: "transit",
		elementType: "labels.text.fill",
		stylers: [
			{ color: "#ffffff" },
			{ visibility: "on" }
		]
	},
		//style different elements on the map
		{
			featureType: "transit",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "poi",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "poi.government",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "poi.sport_complex",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "poi.attraction",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "poi.business",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "transit",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "transit.station",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "landscape",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
			
		}, {
			featureType: "road",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "road",
			elementType: "labels.text.stroke",
			stylers: [
				{ visibility: "off" }
			]
		}, {
			featureType: "road",
			elementType: "labels.text.fill",
			stylers: [
				{ color: "#ffffff" },
				{ visibility: "on" }
			]
		}, {
			featureType: "road.highway",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}, {
			featureType: "water",
			elementType: "geometry",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}
	];
	
	//set google map options
	var map_options = {
		center: new google.maps.LatLng(dealerData.latitude, dealerData.longitude),
		zoom: map_zoom,
		panControl: false,
		zoomControl: false,
		mapTypeControl: false,
		streetViewControl: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		styles: style
	};
	
	// Create a map object and specify the DOM element for display.
	var map = new google.maps.Map(document.getElementById('map'), map_options);
	
	// Loop through our array of center & place each one on the map
	/*for (i = 0; i < DealerMapStructure.citymaps.length; i++) {
		var citymap = DealerMapStructure.citymaps[i];
		$("#selectlocation").append('<option value="' + [citymap[1], citymap[2]].join('|') + '">' + citymap[0] + '</option>');
	}*/
	
	/*$(document).on('change', '#selectlocation', function() {
		var latlngzoom = $(this).val().split('|');
		var newlat = 1 * latlngzoom[0],
			newlng = 1 * latlngzoom[1];
		map.setCenter({ lat: newlat, lng: newlng });
		map.setZoom(map_zoom);
		var scrollToEle = $('.dealer-locations-near__map').offset().top;
		setTimeout(function() {
			$(document).scrollTop(scrollToEle);
		}, 300);
	});*/
	
	// Display multiple markers on a map
	var infoWindow = new google.maps.InfoWindow(),
		marker;
	
	//dealerData
	var position = new google.maps.LatLng(dealerData.latitude, dealerData.longitude);
	marker = new google.maps.Marker({
		position: position,
		map: map,
		visible: true,
		icon: marker_url,
		title: dealerData.name
	});
	
	
	showInfo(dealerData, marker);
	// Allow each marker to have an info window
	google.maps.event.addListener(marker, 'click', (function(marker, i) {
		return function() {
			showInfo(dealerData, marker);
		}
	})(marker, i));
	
	function showInfo (dealerData, marker) {
		var contentString = '<div class="map_content">' +
			'<strong class="map_content-title">' +
			dealerData.name +
			'</strong>' +
			'<span class="map_content-address">' +
			dealerData.address +
			'</span>' +
			'<span class="map_content-tel">' +
			dealerData.phone +
			'</span>';
		/*if(dealerData.site.length > 0)
		{
			contentString += '<span class="map_content-tel"><a href="' + dealerData.site+ '" target="_blank">РџРµСЂРµР№С‚Рё РЅР° СЃР°Р№С‚</a></span>';
		}*/
		
		contentString += '</div>';
		
		infoWindow.setContent(contentString);
		infoWindow.open(map, marker);
	}
	
	//	Loop through our array of markers & place each one on the map
	/*
		for (i = 0; i < DealerMapStructure.markers.length; i++) {
			var position = new google.maps.LatLng(DealerMapStructure.markers[i][1], DealerMapStructure.markers[i][2]);
			bounds.extend(position);
			marker = new google.maps.Marker({
				position: position,
				map: map,
				visible: true,
				icon: marker_url,
				title: DealerMapStructure.markers[i][0]
			});
	
			// Allow each marker to have an info window
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					var contentString = '<div class="map_content">' +
						'<strong class="map_content-title">' +
						DealerMapStructure.markers[i][0] +
						'</strong>' +
						'<span class="map_content-address">' +
						DealerMapStructure.markers[i][3] +
						'</span>' +
						'<span class="map_content-tel">' +
						DealerMapStructure.markers[i][4] +
						'</span>';
						console.log(DealerMapStructure.markers[i][7]);
						if(DealerMapStructure.markers[i][7].length > 0)
						{
							contentString += '<span class="map_content-tel"><a href="' + DealerMapStructure.markers[i][7]+ '" target="_blank">РџРµСЂРµР№С‚Рё РЅР° СЃР°Р№С‚</a></span>';
						}
	
						contentString += '</div>';
	
					infoWindow.setContent(contentString);
					infoWindow.open(map, marker);
				}
			})(marker, i));
	
			// Automatically center the map fitting all markers on the screen
			// map.fitBounds(bounds);
		}
	
		// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
		var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
			this.setZoom(map_zoom);
			google.maps.event.removeListener(boundsListener);
		});
	*/
	
	
	//add custom buttons for the zoom-in/zoom-out on the map
	function CustomZoomControl(controlDiv, map) {
		//grap the zoom elements from the DOM and insert them in the map
		var controlUIzoomIn = document.getElementById('map-zoom-in'),
			controlUIzoomOut = document.getElementById('map-zoom-out');
		controlDiv.appendChild(controlUIzoomIn);
		controlDiv.appendChild(controlUIzoomOut);
		
		// Setup the click event listeners and zoom-in or out according to the clicked element
		google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
			map.setZoom(map.getZoom() + 1)
		});
		google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
			map.setZoom(map.getZoom() - 1)
		});
	}
	
	
	
	var zoomControlDiv = document.createElement('div');
	var zoomControl = new CustomZoomControl(zoomControlDiv, map);
	
	//insert the zoom div on the top left of the map
	map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
	
	$('.modal-button').click(function() {
		$('.map_modal').addClass('show');
	});
	$('.modal_close, .modal_complete').click(function() {
		$(this).parents('.map_modal').removeClass('show');
	});
});