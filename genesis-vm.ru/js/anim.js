function AnimScene(anims) {
	this.anims = anims;
	this.resizeTimer = null;
	this.resizeTimeout = 250;

	this.init = function () {
		this.$doc = $(document);
		this.bindEvents();
		this.calculatePositions();
		this.onScroll();
	};

	this.bindEvents = function () {
		$(window).on('resize', this.trackPositions.bind(this));
		$(document).on('scroll', this.onScroll.bind(this));
	};

	this.trackPositions = function () {
		if (typeof this.resizeTimer == 'number') clearTimeout(this.resizeTimer);
		this.resizeTimer = setTimeout(this.calculatePositions.bind(this), this.resizeTimeout);
		//bg size cover hack-fix
		$('.intro__image').hide();
		setTimeout(function() {
			$('.intro__image').show();
		}, 0);
	};

	this.calculatePositions = function () {
		var winH = $(window).height();
		var delta = getHeaderDelta();

		for (var i = 0; i < this.anims.length; i++) {
			this.anims[i].calculatePositions(winH, delta);
		}
	};

	this.onScroll = function () {
		var curSc = this.$doc.scrollTop();

		for (var i = 0; i < this.anims.length; i++) {
			this.anims[i].onScroll(curSc);
		}
	};

	this.init();
}


function ParallaxAnim(elementId, elements, startMode, endMode) {
	this.selector = elementId;
	this.$el = $(elementId);
	this.$el.css('transform', 'translate3d(0,0,0)');
	if (!this.$el.length) return;

	this.elements = elements;
	this.startMode = startMode;
	this.endMode = endMode;


	this.init = function () {
		//todo: easing
		for (var i = 0; i < this.elements.length; i++) {
			var el = this.elements[i];
			el.$el = this.$el.find(el.selector);
		}
	};


	this.calculatePositions = function (winH, delta) {
		var $el = this.$el;

		this.coords = {};

		this.coords.top = $el.offset().top;
		this.coords.height = $el.outerHeight();
		this.coords.bot = this.coords.top + this.coords.height;
		var parentW = this.$el.outerWidth();

		if (this.startMode == 'inWindow') {
			this.coords.start = this.coords.top - winH;
			this.coords.end = this.coords.bot - winH;
		} else if (!!this.startMode) {
			this.coords.start = (this.coords.top - winH) + this.coords.height * this.startMode / 100;
		}
		if (this.endMode == 'half') {
			this.coords.end = this.coords.start + this.coords.height / 2;
		} else if (!!this.endMode) {
			this.coords.end = this.coords.start + this.coords.height * this.endMode / 100;
		}
		this.coords.length = this.coords.end - this.coords.start;

		for (var i = 0; i < this.elements.length; i++) {
			var el = this.elements[i];
			if (!!el.start) {
				if (el.start == 'inWindow') el.startY = el.$el.offset().top - winH;
				else el.startY = this.coords.start + this.coords.height * el.start / 100;
			}
			if (!!el.end) {
				if (el.end == "half") el.endY = this.coords.end;
				else if (el.end == "full") el.endY = el.startY + el.$el.outerHeight();
				else if (el.end == "outWindow") el.endY = el.$el.offset().top + el.$el.outerHeight();
				else el.endY = this.coords.start + winH + this.coords.height * el.end / 100;
			}

			for (var j = 0; j < el.props.length; j++) {
				var prop = el.props[j];
				if (prop.name == "translateX" || prop.name == "translateY") {
					if (!prop.origVals) prop.origVals = $.extend(true, [], prop.vals);
					prop.vals[0] = (prop.origVals[0] / 100) * parentW;
					prop.vals[1] = (prop.origVals[1] / 100) * parentW;
				}
			}

			el.endY -= delta;

		}

	};

	this.onScroll = function (curSc) {
		for (var i = 0; i < this.elements.length; i++) {
			var el = this.elements[i];
			var percent = this.getPercentForScroll(el, curSc);

			if (percent.anim) {
				this.doAnimateElement(el, percent.num);
			}
		}
	};

	this.getPercentForScroll = function (el, curSc) {
		var percent = {anim: false};

		var animStart = (!!el.startY) ? el.startY : this.coords.start;
		var animEnd = (!!el.endY) ? el.endY : this.coords.end;

		if (curSc >= animStart && curSc <= animEnd) {
			var diff = curSc - animStart;
			var full = animEnd - animStart;
			percent.num = Math.round(diff / full * 100);
			percent.anim = true;
			percent.start = animStart;
			percent.end = animEnd;
		}
		else if (curSc < animStart) percent.num = 0;
		else percent.num = 100;
		return percent;
	};

	this.doAnimateElement = function (animElement, percent) {
		var animProps = {};
		for (var i = 0; i < animElement.props.length; i++) {
			var prop = animElement.props[i];
			if (typeof prop != 'undefined') {
				var propForAnim = this.getAnimateProps(prop.name, prop.vals, percent);
				$.extend(animProps, propForAnim);
			}
		}

		var cssProps = {};
		if (typeof animProps.translateX != "undefined") {
			cssProps.transform = 'translateX(' + animProps.translateX + 'px)';
			delete(animProps.translateX);
		}
		if (typeof animProps.translateY != "undefined") {
			cssProps.transform = 'translateY(' + animProps.translateY + 'px)';
			delete(animProps.translateY);
		}
		if (typeof animProps.scale != "undefined") {
			cssProps.transform = 'scale(' + animProps.scale + ')';
			delete(animProps.scale);
		}

		// todo: multyCss object + trans concat
		if (!!Object.keys(cssProps).length) {
			animElement.$el.css(cssProps);
		}

		animElement.$el.finish().animate(animProps, 25, 'linear');
	};

	this.getAnimateProps = function (propName, prop, percent) {
		var min = prop[0];
		var max = prop[1];
		var diff = max - min;
		var newVal = min + diff * (percent / 100);
		var res = {};
		res[propName] = newVal;
		return res;
	};

	this.init();
}

function getHeaderDelta() {
	var res = $('header').outerHeight();
	var $crumbs = $('.layout__breadcrumb');
	if ($crumbs.is(':visible')) res += $crumbs.outerHeight();
	return res;
}

var SmoothScrollAndSecondBlock = {
	menuHeight: 0,
	isScrollingToSecond: false,
	hasSecondBlock: false,
	isFirefox: false,
	scrollToSecondBlock: function() {
		SmoothScrollAndSecondBlock.isScrollingToSecond = true;
		// var targetY = $(window).height() + 1 - SmoothScrollAndSecondBlock.menuHeight;
		var targetY = $('#intro').height() + 61;
		$('html, body').stop().animate({ scrollTop: targetY}, 1200, "easeInOutExpo", function() {
			SmoothScrollAndSecondBlock.isScrollingToSecond = false;
		});
	},
	getDelta: function(ee) {
		if (SmoothScrollAndSecondBlock.isFirefox) {
			return ee.detail;
		} if (typeof ee.deltaY != 'undefined') {
			return ee.deltaY;
		} else {
			return -ee.wheelDelta;
		}
	},
	onMouseWheel: function(e) {
		if (typeof e == 'object' && typeof e.originalEvent != 'undefined') var ee = e.originalEvent;
		var isConfigurator = $(ee.target).parents('.configurator, .wizard').length > 0;
		if (!isConfigurator) {
			var deltaY = SmoothScrollAndSecondBlock.getDelta(ee);
			if (SmoothScrollAndSecondBlock.hasSecondBlock && deltaY > 0 && $(window).scrollTop() < ($('#intro').height() + 61)) {
				if (!SmoothScrollAndSecondBlock.isScrollingToSecond) SmoothScrollAndSecondBlock.scrollToSecondBlock();
			} else {
				SmoothScrollAndSecondBlock.isScrollingToSecond = false;
				var _tgScroll = $(window).scrollTop() + (deltaY > 0 ? 300 : -300);
				$("html, body").stop().animate({scrollTop: _tgScroll}, 400, "easeOutQuad");
			}
			e.preventDefault();
		}
	},
	init: function () {
		SmoothScrollAndSecondBlock.hasSecondBlock = $('#brand').length > 0 || $('.intro__g90').length > 0;
		SmoothScrollAndSecondBlock.menuHeight = $('.breadcrumb').height();
		SmoothScrollAndSecondBlock.isFirefox = (/Firefox/i.test(navigator.userAgent));
		var mousewheelevt = SmoothScrollAndSecondBlock.isFirefox ? "DOMMouseScroll" : "mousewheel"; //FF doesn't recognize mousewheel as of FF3.x
		$(document).unbind(mousewheelevt).on(mousewheelevt, SmoothScrollAndSecondBlock.onMouseWheel);
	}
};

$(document).ready(function() {
// 	SmoothScrollAndSecondBlock.init();
});
