
$( document ).ready(function() {

   var timerId;

    $('.js-address').keyup(function () {

        var address = $(this).val();

        clearTimeout(timerId);

        timerId = setTimeout(function() {
            $.ajax({
                method: 'POST',
                url: '/admin/dealer/ajax-find-point',
                data: {address: address}
            }).done(function (msg) {

                if (msg.length > 0) {
                    $('#dealer-coordinates').val(msg);
                    mapInputWidgetManager.reInitialize();
                }

            });
        }, 500);
    });

});