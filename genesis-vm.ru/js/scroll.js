var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

function scroller(e) {
	// current top offset on page
	var topPosition = Math.floor($(document).scrollTop());
	var brandY = 0;

	var deltaYMenu = $('.layout__header').height() + $('.layout__breadcrumb').height();

	if ($('#brand').length > 0) {
		brandY = Math.floor($("#brand").offset().top - deltaYMenu);
	} else if ($('#intro').length > 0) {
		if (topPosition < Math.floor($('#long').offset().top - deltaYMenu)) {
			brandY = Math.floor($("#introphoto").offset().top - deltaYMenu);
		} else {
			brandY = Math.floor($("#long-feature").offset().top - deltaYMenu);
		}
	}

	var dir,
		scrollSize = 300;

	e.preventDefault();
	if (e.type === 'mousewheel') {
		dir = e.originalEvent.wheelDelta > 0 ? '-=' : '+=';
	}
	else {
		dir = e.originalEvent.detail < 0 ? '-=' : '+=';
	}

	if (topPosition < brandY && dir == '+=') {
		if (brandY - topPosition > 100) {
			$('html, body').stop(true).animate({
				scrollTop: brandY
			}, 1000, 'easeInOutExpo');
		} else {
			$('html, body').stop(true).animate({
				scrollTop: brandY
			}, 100, 'swing');
		}
	} else {
		$('html, body').stop(true).animate({
			scrollTop: dir + scrollSize
		}, 400, 'swing');
	}
}

if (isChrome) {
	$(window).on('mousewheel', function (e) {
		scroller(e)
	});
} else {
	$(window).on('DOMMouseScroll', function (e) {
		scroller(e)
	});
}
