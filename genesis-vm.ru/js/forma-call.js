$(document).ready(function ($) {
	//$(".send-req").click(function(){return false;});
    console.log('test');
    var PhoneMask = "+7 (999) 999-9999";
    console.log($('input[name=phone_call], #cNumber').prop('name'));
    //$('#cNumber').mask("+7 (999)999-99-99");
	$('input[name=phone_call]').mask(PhoneMask);
	
	
    var formTrigger = $('.toggleForm'),
        form = $('.callback'),

        title = form.find('h2'),

        name = form.find('[name=name_call]'),
        surname = form.find('[name=surname_call]'),
        phone = form.find('[name=phone_call]'),
        email = form.find('[name=email_call]'),
        vin = form.find('[name=vin]'),
        fields = $('[name=name_call],[name=surname_call],[name=phone_call]'),

        checkboxWrap = $('.legal-info.stock'),
        checkbox = $('[name=agreed]'),

        submit = form.find('button').first();

    init();

    // trigger buttons
    formTrigger.on('click', function (event) {
        event.preventDefault();
        var trigger = $(this);
				
				$("#case3 input[name=vin]").val($(this).data('vin') || '');

        /*
				if(trigger.hasClass('genesis-btn')){
	        $("#case1").show();
	        $("#case2").hide();
        }else{
	        $("#case1").hide();
	        $("#case2").show();
				}
				*/
				
        
        form.addClass('active');
        $('html, body').addClass('noscroll');

        title.text( trigger.data('model') );
        
        
        submit.data('id', trigger.data('id') );
    });


    submit.on('click', function (event) {
        event.preventDefault();

        if(submit.hasClass('sent')) {
            alert('Заявка уже отправлена');
            return;
        }

        var valid = agreed = true;
        fields.each(function() {
            var field = $(this);
            console.log(field.val());
            if(field.val() === '' || field.val()==='+7 (___) ___-____') {
                field.addClass('wrong');
                valid = false;
            }
        });

        if(valid) {
            if(!checkbox.is(':checked')) {
                console.log(checkboxWrap);
                checkboxWrap.addClass('incorrect');
                agreed = false;
            };
        };

        if(valid && agreed) {
            sendForm({
                "name": name.val(),
                "surname": surname.val(),				
                "phone": phone.val(),
                "email": email.val(),
                "vin": vin.val(),
                "id": submit.data('id')
            });
        };
    });

    // result
    function showResult(isSuccess) {
        if(isSuccess) {
            submit.addClass('sent');
            form.removeClass('active');
            $('.success-msg').addClass('active');
        } else {
            alert('Сервис временно недоступен');
        }
    }

    // ajax request
    function sendForm(formValues) {
//        $.post('sendstockrequest.html', formValues, function (response) {
		console.log('send emp');
        //$.post('sendcrm-call.html', formValues, function (response) {
        $.post('/emails', formValues, function (response) {
            if (response === 'Y') {
				//window.yaCounter49477666 && yaCounter49477666.reachGoal('call');
                //window.ga('send', 'event', 'Forms', 'sent', 'CallRequestAll');
                showResult(true);
            } else {
                console.log('Error:', response);
                showResult(false);
            }
        });
    };
});

function init() {
    //$('.phone-default-mask').mask('+7 (000)000-00-00');

    // close buttons
    $('.callback__close').on('click', function() {
        $('.callback').removeClass('active');
        $('html, body').removeClass('noscroll');
    });

    $('.callback').click(function(event) {
      if ($(event.target).closest('.callback__content').length) return;
      $('.callback').removeClass('active');
      event.stopPropagation();
    });
  // rules open
    $('.rules-opener').click(function() {
        $('.legal-wrap').show();
    });

    // rules close
    $('.closeIt').click(function() {
        $('.legal-wrap').hide();
    });

    $('.success-msg__close').click(function() {
        $('.success-msg').removeClass('active');
    });

    $('[name]').on('change keydown', function() {
        $(this).removeClass('wrong');
    });

    $('.legal-info.stock').on('click', function () {
        $('.legal-info.stock').removeClass('incorrect');
    })
}