<?php

namespace app\models;

use ereminmdev\yii2\cropimageupload\CropImageUploadBehavior;
use sadovojav\cutter\behaviors\CutterBehavior;
use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $preview_description
 * @property string $description
 * @property string $preview_file
 * @property string $detail_file
 */
class News extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['active', 'press_id'], 'integer'],
            [['date'], 'safe'],
            [['name', 'code', 'preview_description', 'old_url'], 'string', 'max' => 255],
            [['detail_file', 'preview_file'], 'file', 'extensions' => 'jpeg, gif, png', 'on' => ['insert', 'update']],
        ];
    }

/*
    public function behaviors()
    {

        return [
        'detail_file' => [
            'class' => CropImageUploadBehavior::className(),
            'attribute' => 'detail_file',
            'scenarios' => ['create', 'update'],
            'placeholder' => '@app/modules/user/assets/images/avatar.jpg',
            'path' => '',
            'url' => '',
            'ratio' => [],
            'crop_field' => '',
            'cropped_field' => 'detail_file',
        ],
        'preview_file' => [
            'class' => CropImageUploadBehavior::className(),
            'attribute' => 'preview_file',
            'scenarios' => ['create', 'update'],
            'placeholder' => '@app/modules/user/assets/images/avatar.jpg',
            'path' => '',
            'url' => '@web/images/{id}',
            'ratio' => [],
            'crop_field' => '',
            'cropped_field' => 'preview_file',
        ],
    ];
}*/

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'name'                => 'Name',
            'active'              => 'Active',
            'code'                => 'Code',
            'old_url'             => 'Old url',
            'preview_description' => 'Preview Description',
            'description'         => 'Description',
            'preview_file'        => 'Preview File',
            'detail_file'         => 'Detail File',
        ];
    }
}
