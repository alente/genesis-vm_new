<?

namespace app\models;

use yii\db\ActiveQuery;

class CActiveRecord extends \yii\db\ActiveRecord {
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        //$outres = print_r($scenarios, true);$outres=str_replace(">", "&gt;", $outres);$outres=str_replace("<", "&lt;", $outres);echo "<pre>".$outres."</pre>";
        if (!isset($scenarios['search'])) {
            $scenarios['search'] = $scenarios['default'];
        }
        //$outres = print_r($scenarios, true);$outres=str_replace(">", "&gt;", $outres);$outres=str_replace("<", "&lt;", $outres);echo "<pre>".$outres."</pre>";

        return $scenarios;
    }
    
    public static function getDropDownList($type = false, $public = false, $defaultValue = false, $orderBy = false) {
        $result = array();
        $criteria = self::find();
        
        if ($type !== false) {
            $criteria->andWhere(['type' => $type]);
        }
        if ($public  !== false) {
            $criteria->published();
        }
        if ($orderBy !== false) {
            $criteria->addOrderBy($orderBy);
        }
        $items = $criteria->all();

        if($defaultValue !== false) {
            $result[0] = $defaultValue;
        }
        foreach($items as $item) {
            $result[$item->id] = $item->title;
        }
        return $result;
    }

    public static function find() {
        $f = new ScopeQuery(get_called_class());
        return $f;
    }
}

class ScopeQuery extends ActiveQuery {
    public function published() {
        $this->alias('t');
        $this->andWhere(['t.public' => 1]);
        return $this;
    }
    public function random() {
        $this->addOrderBy('rand()');
        return $this;
    }
}

?>