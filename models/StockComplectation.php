<?php

namespace app\models;

//use app\components\behaviors\DateBehavior;
use Yii;
//use app\components\behaviors\HandleBehavior;
/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $handle
 * @property string $image
 * @property string $description
 * @property string $text
 * @property integer $weight
 * @property integer $public
 */
class StockComplectation extends CActiveRecord
{
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_complectation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'string', 'max' => 50],
            [['complectation'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код цвета',
            'name' => 'Название',
            'complectation' => 'Характеристики',
        ];
    }
}