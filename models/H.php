<?php

namespace app\models;

use Yii;

class H
{

    public static function unique($size = 3, $add_key = '')
    {
        $value = md5(time().rand(0,1000000000).$add_key);

        return substr($value,0, $size);
    }

    public static function costForm($value = 0)
    {
        $value = (float)$value;

        return number_format($value,0,',',' ');
    }

    /*public function arr_sort_cost($arr)
    {
        usort($array, build_sorter('cost'));
    }

    public function  build_sorter($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }*/

    public static function  build_sorter($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }
}