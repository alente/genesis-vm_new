<?php

/**
 * Genesis
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Buroburo
 * @link https://buroburo.ru
 */

namespace app\components;

use yii\base\Component,
	Yii,
	app\models\News;


/**
 * Класс для логирования всего и вся
 * @package app\components\tools
 */
class PressReleaseLoader extends Component
{
	public $apiUrl = "";
	
	public function __construct(array $config = [])
	{
		parent::__construct($config);
	}
	
	public function process () {
		$result = $this->getItemsMethod();
		if($result){
			$itemsId = [];
			foreach($result['items'] as $k => $item){
				$itemsId[] = $item['id'];
			}
			
			$existsNews = [];
			foreach(News::find('id', 'press_id')
				        ->where(['press_id' => $itemsId])
				        ->each() as $newsItem)
			{
				$existsNews[$newsItem->press_id] = $newsItem->id;
			}
			
			//var_export($result);
			
			foreach($result['items'] as $k => $item){
				if(array_key_exists($item['id'], $existsNews)) continue;
				
				$previewText = $text_top = strip_tags(trim(html_entity_decode($item['detail_text'],   ENT_QUOTES, 'UTF-8'), "\xc2\xa0"));
				$previewText = preg_replace("|[\s]+|s", " ", $previewText);
				$previewText = $this->cutByWords(180, $previewText);
				
				$model = new News();
				$model->active = 1;
				$model->press_id = $item['id'];
				$model->date = date("Y-m-d H:i:s", strtotime($item['date']));
				$model->name = $item['name'];
				$model->code = $item['code'];
				$model->old_url = $item['url'];
				$model->preview_description = $previewText;
				$model->description = $item['detail_text'];
				$model->preview_file = $item['preview_picture'];
				$model->detail_file = $item['detail_picture'];
				if($model->save()){
					Yii::$app->logger->write('press', "Пресс-релиз добавлен: {$item['name']}");
				}else{
					foreach($model->errors as $error){
						Yii::$app->logger->write('press', $error);
					}
				}
			}
		}
	}
	
	protected function getItemsMethod () {
		try {
			$result = $this->request('getItems', [
				'page' => 1,
				'onpage' => 100
			]);
			
			return $result['response']['data'];
		} catch (\Exception $e) {
			Yii::$app->logger->write('press', $e->getMessage());
		}
		
		return false;
		//var_export($result);
	}
	
	
	protected function request ($method, $params=[]) {
		$requestUrl = $this->apiUrl . $method;
		if(!empty($params)){
			$arParams = [];
			foreach($params as $name => $value){
				$arParams[] = $name . '=' . $value;
			}
			$requestUrl .= '?' . implode("&", $arParams);
		}
		
		$headers = array(
			'Content-Type: application/json',
			//'Content-Length: ' . mb_strlen($post, 'UTF-8')
		);
		
		$ch = curl_init($requestUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if(!empty($headers)){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		$response = curl_exec($ch);
		$error    = curl_error($ch);
		$errno    = curl_errno($ch);
		$status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if (is_resource($ch)) {
			curl_close($ch);
		}
		
		if (0 !== $errno) {
			throw new \RuntimeException($error, $errno);
		}
		
		$responseArr = json_decode($response, true);
		
		if($status !== 200){
			throw new \Exception("Сервис недоступен");
		}elseif($responseArr['success'] !== true){
			throw new \Exception("Ошибка: " . $responseArr['message']);
		}
		
		return array(
			'status' => $status,
			'response' => $responseArr,
		);
	}
	
	protected function cutByWords($maxlen, $text) {
		$len = (mb_strlen($text) > $maxlen)? mb_strripos(mb_substr($text, 0, $maxlen), ' ') : $maxlen;
		$cutStr = mb_substr($text, 0, $len);
		$temp = (mb_strlen($text) > $maxlen)? $cutStr. '...' : $cutStr;
		return $temp;
	}
}