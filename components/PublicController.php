<?php

namespace app\components;

use app\models\Backgrounds;
use app\models\Gallery;
use app\models\Sections;
use app\models\Settings;
use app\models\Tags;

class PublicController extends CommonPublicController {
    public function init()
    {
        parent::init();
        //$this->layout = 'second';
    }

    public function tempInit($pt, $handle, $last_section = false)
    {
        parent::tempInit($pt, $handle, $last_section);
    }
}