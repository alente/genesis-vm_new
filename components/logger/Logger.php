<?php

/**
 * Genesis
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Buroburo
 * @link https://buroburo.ru
 */

namespace app\components\logger;

use yii\base\Component;


/**
 * Класс для логирования всего и вся
 * @package app\components\tools
 */
class Logger extends Component
{
	public $log;
	public $dateFormat = 'Y.m.d H:i:s';
	public $maxSize = '262144';
	public $path = null;
	public $type = [
		'csv' => [
			'extension' => 'csv',
			'method' => 'csvWriteLog',
		],
		'text' => [
			'extension' => 'log',
			'method' => 'writeLogTxt',
		],
	];
	
	public function __construct(array $config = [])
	{
		$this->path = realpath($_SERVER['DOCUMENT_ROOT']/* . '/../logs'*/);
		
		parent::__construct($config);
	}
	
	public function write ($name, $data, $filenameEnding="") {
		$params = $this->log[$name];
		
		if($params && method_exists($this, $this->type[$params['type']]['method'])){
			$this->{$this->type[$params['type']]['method']}($name, $data, $filenameEnding);
		}
		return false;
	}
	
	/**
	 * Запись в CSV лог
	 * @param string $name
	 * @param array $values
	 * @param string $filenameEnding
	 */
	private function csvWriteLog ($name, array $values = [], $filenameEnding="") {
		$values = array_merge([date($this->dateFormat, time())], $values);
		$fp = $this->processFile($name, $filenameEnding);
		
		if($fp){
			fputcsv($fp, $values, ";");
			fclose($fp);
		}
	}
	
	/**
	 * Запись лога в текстовом формате
	 * @param $name
	 * @param array $values
	 * @param string $filenameEnding
	 */
	private function writeLogTxt ($name, $data, $filenameEnding="") {
		$fp = $this->processFile($name, $filenameEnding);
		$data = date($this->dateFormat, time()) . ": " . $data . "\r\n";
		
		if($fp){
			fwrite($fp, $data);
			fclose($fp);
		}
	}
	
	/**
	 * Обработка файла лога
	 * @param $name
	 */
	private function processFile ($name, $filenameEnding="") {
		$logType = $this->log[$name]['type'];
		$logParams = $this->type[$logType];
		$logFilename = 'log_' . $name;
		if(!empty($filenameEnding)){
			$logFilename = $logFilename . '_' . $filenameEnding;
		}
		$logPath = $this->path . DIRECTORY_SEPARATOR . $logFilename . '.' . $logParams['extension'];
		$writeHeader = false;   //  Запись заголовка таблицы
		
		if(!file_exists($logPath)){
			$writeHeader = true;
		}elseif(filesize($logPath) > $this->maxSize){
			rename($logPath, $this->path . DIRECTORY_SEPARATOR . $logFilename . '_' . date('Y-m-d-H-i-s') . '.' .$logParams['extension']);
			$writeHeader = true;
		}
		
		$fp = fopen($logPath, 'a');
		
		if($fp && $writeHeader){
			if($logType == 'csv'){
				fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
				$headers = array_merge(['Дата'], $this->log[$name]['fields']);
				fputcsv($fp, $headers, ";");
			}
		}
		return $fp;
	}
}