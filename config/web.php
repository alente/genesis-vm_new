<?php

defined('SITE_DIR') or define('SITE_DIR', '/');

$params = array_merge(
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-dealer.php')
);

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
	//'homeUrl' => '/ru/ru',
    'bootstrap' => ['log','devicedetect'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'D*rZ.YV)Qq4vubRX%KI;MHF_T,2yANW7B6',
			//'baseUrl' => '/ru/ru'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => [
                'guest'
                /*'user',
                'moderator',
                //'admin',
                //'superadmin'
                */
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'configurator/error',
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_MailTransport',
                /*
,
                'encryption' => 'ssl',
*/
            ],
        ],
        'urlManager' => [
			'baseUrl' => '',
			'enablePrettyUrl' => true,
            'showScriptName' => false,
	        'rules' => [
		        '<model:(g70|g80|g90)>.html'		    => 'model/index',
		        '<model:(g70|g80|g90)>-<page:[\w-]+>.html'	=> 'model/index',
		        '<model:(g90)><page:l>.html'	=> 'model/index',
	        	'search'                  => 'site/search',
				'genesis.html'            => 'site/genesis',
                'ajax/dealer-cities.json' => 'configurator/json-city',
                'ajax/dealer-list.json'   => 'configurator/json-dealer',
		        'news.html'								=> 'news/index',
		        'news/<code>.html'						=> 'news/view',
				'testdrive'               => 'test-drive/index',
				'stock'                   => 'stock/index',	
				'emails'				=> 'emails/index',				
	        ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
	    'db' => array_merge(
		    require(__DIR__ . '/db.php'),
		    require(__DIR__ . '/db-local.php')
	    ),
	    'formatter' => [
		    'dateFormat' => 'd MMMM Y',
		    'datetimeFormat' => 'd MMMM Y',
		    'decimalSeparator' => '.',
		    'thousandSeparator' => ' ',
		    'currencyCode' => 'EUR',
			'locale' => 'ru-RU',
	    ],
    ],
    'params' => $params,
    'modules' =>[
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ]
    ],
];


if (YII_ENV_DEV) {

    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = 'yii\debug\Module';
//
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = 'yii\gii\Module';
//
//	$config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//        'allowedIPs' => ['*']
//    ];

    /*$config['modules']['admin'] = [
        'class' => 'app\modules\admin\Module',
    ];*/
}

return $config;
